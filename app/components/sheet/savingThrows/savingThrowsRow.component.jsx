import {Field, Flex, Input, Operator} from '../basic';

import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

const Fortitude = ({
    onAbilityModChange,
    onBaseChange,
    onMagicModChange,
    onMiscModChange,
    onOtherModChange,
    onTempModChange,
    t,
    type,
    value
}) => (
    <Flex
        align="center"
        spaced
    >
        <Field
            inverted
            multiply={3}
        >
            {t(type.toUpperCase())}
        </Field>
        <Field>
            {(+value.base || 0) +
                (+value.abilityMod || 0) +
                (+value.magicMod || 0) +
                (+value.miscMod || 0) +
                (+value.tempMod || 0) +
                (+value.otherMod || 0)}
        </Field>
        <Operator>{' = '}</Operator>
        <Input
            border="full"
            field
            onChange={onBaseChange}
            type="number"
            value={value.base}
        />
        <Operator>{'+'}</Operator>
        <Input
            border="full"
            field
            onChange={onAbilityModChange}
            type="number"
            value={value.abilityMod}
        />
        <Operator>{'+'}</Operator>
        <Input
            border="full"
            field
            onChange={onMagicModChange}
            type="number"
            value={value.magicMod}
        />
        <Operator>{'+'}</Operator>
        <Input
            border="full"
            field
            onChange={onMiscModChange}
            type="number"
            value={value.miscMod}
        />
        <Operator>{'+'}</Operator>
        <Input
            border="full"
            field
            onChange={onTempModChange}
            type="number"
            value={value.tempMod}
        />
        <Operator>{'+'}</Operator>
        <Input
            border="full"
            field
            multiply={2}
            onChange={onOtherModChange}
            type="number"
            value={value.otherMod}
        />
    </Flex>
);

Fortitude.propTypes = {
    onAbilityModChange: PropTypes.func.isRequired,
    onBaseChange: PropTypes.func.isRequired,
    onMagicModChange: PropTypes.func.isRequired,
    onMiscModChange: PropTypes.func.isRequired,
    onOtherModChange: PropTypes.func.isRequired,
    onTempModChange: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired,
    type: PropTypes.string.isRequired,
    value: PropTypes.shape({
        base: PropTypes.number,
        abilityMod: PropTypes.number,
        magicMod: PropTypes.number,
        miscMod: PropTypes.number,
        tempMod: PropTypes.number,
        otherMod: PropTypes.number
    }).isRequired
};

export default translate(['savingThrows'])(Fortitude);
