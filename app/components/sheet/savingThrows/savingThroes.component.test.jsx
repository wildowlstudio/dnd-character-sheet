import React from 'react';
import SavingThrows from './savingThrows.component';
import {mount} from 'enzyme';
import renderer from 'react-test-renderer';

/* eslint-disable react/jsx-no-bind */
/* eslint-disable react/jsx-handler-names */
describe('Sheet saving throws tests', () => {
    test('Should match previous SavingThrows snapshot', () => {
        const Rendered = renderer.create(
            <SavingThrows
                fortitude={{}}
                onFortitudeAbilityModChange={() => null}
                onFortitudeBaseChange={() => null}
                onFortitudeMagicModChange={() => null}
                onFortitudeMiscModChange={() => null}
                onFortitudeOtherModChange={() => null}
                onFortitudeTempModChange={() => null}
                onReflexAbilityModChange={() => null}
                onReflexBaseChange={() => null}
                onReflexMagicModChange={() => null}
                onReflexMiscModChange={() => null}
                onReflexOtherModChange={() => null}
                onReflexTempModChange={() => null}
                onSavingThrowsReset={() => null}
                onWillpowerAbilityModChange={() => null}
                onWillpowerBaseChange={() => null}
                onWillpowerMagicModChange={() => null}
                onWillpowerMiscModChange={() => null}
                onWillpowerOtherModChange={() => null}
                onWillpowerTempModChange={() => null}
                reflex={{}}
                willpower={{}}
            />
        );
        expect(Rendered).toMatchSnapshot();
    });

    test('Should check saving throws component integration', () => {
        const mockResetClick = jest.fn();
        const fields = [
            {text: 'FORTITUDE_BASE', fun: jest.fn(), value: Math.random()},
            {text: 'FORTITUDE_ABILITY_MOD', fun: jest.fn(), value: Math.random()},
            {text: 'FORTITUDE_MAGIC_MOD', fun: jest.fn(), value: Math.random()},
            {text: 'FORTITUDE_MISC_MOD', fun: jest.fn(), value: Math.random()},
            {text: 'FORTITUDE_TEMP_MOD', fun: jest.fn(), value: Math.random()},
            {text: 'FORTITUDE_OTHER_MOD', fun: jest.fn(), value: Math.random()},
            {text: 'REFLEX_BASE', fun: jest.fn(), value: Math.random()},
            {text: 'REFLEX_ABILITY_MOD', fun: jest.fn(), value: Math.random()},
            {text: 'REFLEX_MAGIC_MOD', fun: jest.fn(), value: Math.random()},
            {text: 'REFLEX_MISC_MOD', fun: jest.fn(), value: Math.random()},
            {text: 'REFLEX_TEMP_MOD', fun: jest.fn(), value: Math.random()},
            {text: 'REFLEX_OTHER_MOD', fun: jest.fn(), value: Math.random()},
            {text: 'WILLPOWER_BASE', fun: jest.fn(), value: Math.random()},
            {text: 'WILLPOWER_ABILITY_MOD', fun: jest.fn(), value: Math.random()},
            {text: 'WILLPOWER_MAGIC_MOD', fun: jest.fn(), value: Math.random()},
            {text: 'WILLPOWER_MISC_MOD', fun: jest.fn(), value: Math.random()},
            {text: 'WILLPOWER_TEMP_MOD', fun: jest.fn(), value: Math.random()},
            {text: 'WILLPOWER_OTHER_MOD', fun: jest.fn(), value: Math.random()}
        ];
        const component = mount(
            <SavingThrows
                fortitude={{base: 5}}
                onFortitudeAbilityModChange={fields[1].fun}
                onFortitudeBaseChange={fields[0].fun}
                onFortitudeMagicModChange={fields[2].fun}
                onFortitudeMiscModChange={fields[3].fun}
                onFortitudeOtherModChange={fields[5].fun}
                onFortitudeTempModChange={fields[4].fun}
                onReflexAbilityModChange={fields[7].fun}
                onReflexBaseChange={fields[6].fun}
                onReflexMagicModChange={fields[8].fun}
                onReflexMiscModChange={fields[9].fun}
                onReflexOtherModChange={fields[11].fun}
                onReflexTempModChange={fields[10].fun}
                onSavingThrowsReset={mockResetClick}
                onWillpowerAbilityModChange={fields[13].fun}
                onWillpowerBaseChange={fields[12].fun}
                onWillpowerMagicModChange={fields[14].fun}
                onWillpowerMiscModChange={fields[15].fun}
                onWillpowerOtherModChange={fields[17].fun}
                onWillpowerTempModChange={fields[16].fun}
                reflex={{}}
                willpower={{}}
            />
        );
        component
            .find('.fa-eraser')
            .last()
            .simulate('click');
        expect(mockResetClick).toBeCalled();

        component.find('input').forEach((input, i) => {
            input.simulate('change', {
                target: {name: 'change', value: fields[i].value}
            });
            input.simulate('blur', {target: {name: 'change'}});
            expect(fields[i].fun).toBeCalled();
            expect(fields[i].fun.mock.calls[0][0]).toBe(fields[i].value);
        });
    });
});
