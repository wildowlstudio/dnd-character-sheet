import {Element, Flex} from '../basic';

import PropTypes from 'prop-types';
import React from 'react';
import SavingThrowsHeader from './savingThrowsHeader.component';
import SavingThrowsRow from './savingThrowsRow.component';
import styled from 'styled-components';

const StyledElement = styled(Element)`
    /* position: absolute;
    top: 297px;
    left: 0; */
    width: 608px;
`;

const SavingThrows = ({
    fortitude,
    onFortitudeAbilityModChange,
    onFortitudeBaseChange,
    onFortitudeMagicModChange,
    onFortitudeMiscModChange,
    onFortitudeOtherModChange,
    onFortitudeTempModChange,
    onReflexAbilityModChange,
    onReflexBaseChange,
    onReflexMagicModChange,
    onReflexMiscModChange,
    onReflexOtherModChange,
    onReflexTempModChange,
    onSavingThrowsReset,
    onWillpowerAbilityModChange,
    onWillpowerBaseChange,
    onWillpowerMagicModChange,
    onWillpowerMiscModChange,
    onWillpowerOtherModChange,
    onWillpowerTempModChange,
    reflex,
    willpower
}) => (
    <StyledElement onReset={onSavingThrowsReset}>
        <Flex
            direction="column"
            spaced
        >
            <SavingThrowsHeader />
            <SavingThrowsRow
                onAbilityModChange={onFortitudeAbilityModChange}
                onBaseChange={onFortitudeBaseChange}
                onMagicModChange={onFortitudeMagicModChange}
                onMiscModChange={onFortitudeMiscModChange}
                onOtherModChange={onFortitudeOtherModChange}
                onTempModChange={onFortitudeTempModChange}
                type="fortitude"
                value={fortitude}
            />
            <SavingThrowsRow
                onAbilityModChange={onReflexAbilityModChange}
                onBaseChange={onReflexBaseChange}
                onMagicModChange={onReflexMagicModChange}
                onMiscModChange={onReflexMiscModChange}
                onOtherModChange={onReflexOtherModChange}
                onTempModChange={onReflexTempModChange}
                type="reflex"
                value={reflex}
            />
            <SavingThrowsRow
                onAbilityModChange={onWillpowerAbilityModChange}
                onBaseChange={onWillpowerBaseChange}
                onMagicModChange={onWillpowerMagicModChange}
                onMiscModChange={onWillpowerMiscModChange}
                onOtherModChange={onWillpowerOtherModChange}
                onTempModChange={onWillpowerTempModChange}
                type="willpower"
                value={willpower}
            />
        </Flex>
    </StyledElement>
);

SavingThrows.propTypes = {
    fortitude: PropTypes.shape({
        base: PropTypes.number,
        abilityMod: PropTypes.number,
        magicMod: PropTypes.number,
        miscMod: PropTypes.number,
        tempMod: PropTypes.number,
        otherMod: PropTypes.number
    }).isRequired,
    onFortitudeAbilityModChange: PropTypes.func.isRequired,
    onFortitudeBaseChange: PropTypes.func.isRequired,
    onFortitudeMagicModChange: PropTypes.func.isRequired,
    onFortitudeMiscModChange: PropTypes.func.isRequired,
    onFortitudeOtherModChange: PropTypes.func.isRequired,
    onFortitudeTempModChange: PropTypes.func.isRequired,
    onReflexAbilityModChange: PropTypes.func.isRequired,
    onReflexBaseChange: PropTypes.func.isRequired,
    onReflexMagicModChange: PropTypes.func.isRequired,
    onReflexMiscModChange: PropTypes.func.isRequired,
    onReflexOtherModChange: PropTypes.func.isRequired,
    onReflexTempModChange: PropTypes.func.isRequired,
    onSavingThrowsReset: PropTypes.func.isRequired,
    onWillpowerAbilityModChange: PropTypes.func.isRequired,
    onWillpowerBaseChange: PropTypes.func.isRequired,
    onWillpowerMagicModChange: PropTypes.func.isRequired,
    onWillpowerMiscModChange: PropTypes.func.isRequired,
    onWillpowerOtherModChange: PropTypes.func.isRequired,
    onWillpowerTempModChange: PropTypes.func.isRequired,
    reflex: PropTypes.shape({
        base: PropTypes.number,
        abilityMod: PropTypes.number,
        magicMod: PropTypes.number,
        miscMod: PropTypes.number,
        tempMod: PropTypes.number,
        otherMod: PropTypes.number
    }).isRequired,
    willpower: PropTypes.shape({
        base: PropTypes.number,
        abilityMod: PropTypes.number,
        magicMod: PropTypes.number,
        miscMod: PropTypes.number,
        tempMod: PropTypes.number,
        otherMod: PropTypes.number
    }).isRequired
};

export default SavingThrows;
