import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';
import {Flex, Label, Operator} from '../basic';

const SavingThrowsHeader = ({t}) => (
    <Flex
        align="flex-end"
        spaced
    >
        <Label
            field
            multiply={3}
        >
            {t('SAVING_THROWS')}
        </Label>
        <Label field>{t('common:TOTAL')}</Label>
        <Operator />
        <Label field>{t('BASE_SAVE')}</Label>
        <Operator />
        <Label field>{t('ABILITY_MODIFIER')}</Label>
        <Operator />
        <Label field>{t('MAGIC_MODIFIER')}</Label>
        <Operator />
        <Label field>{t('MISC_MODIFIER')}</Label>
        <Operator />
        <Label field>{t('TEMP_MODIFIER')}</Label>
        <Operator />
        <Label
            field
            multiply={2}
        >
            {t('OTHER_MODIFIERS')}
        </Label>
    </Flex>
);

SavingThrowsHeader.propTypes = {
    t: PropTypes.func.isRequired
};

export default translate(['savingThrows', 'common'])(SavingThrowsHeader);
