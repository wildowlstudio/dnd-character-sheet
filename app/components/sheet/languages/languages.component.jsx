import {Element, Flex} from '../basic';

import Header from './header.component';
import Language from './language.component';
import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';

const StyledElement = styled(Element)`
    width: 367px;
`;

const Languages = ({
    languages,
    onLanguageAdd,
    onLanguageClean,
    onLanguageDelete,
    onLanguageMove,
    onLanguageReset,
    onLanguageChange
}) => {
    return (
        <StyledElement
            onAdd={onLanguageAdd}
            onRemove={onLanguageClean}
            onReset={onLanguageReset}
        >
            <Header />
            <Flex
                spaced
                wrap="true"
            >
                {languages
                    .concat({id: 'new'})
                    .map((language) => (
                        <Language
                            key={language.id}
                            language={language}
                            onChange={onLanguageChange}
                            onDelete={onLanguageDelete}
                            onMove={onLanguageMove}
                        />
                    ))}
            </Flex>
        </StyledElement>
    );
};

Languages.propTypes = {
    languages: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            value: PropTypes.string
        })
    ).isRequired,
    onLanguageAdd: PropTypes.func.isRequired,
    onLanguageChange: PropTypes.func.isRequired,
    onLanguageClean: PropTypes.func.isRequired,
    onLanguageDelete: PropTypes.func.isRequired,
    onLanguageMove: PropTypes.func.isRequired,
    onLanguageReset: PropTypes.func.isRequired
};

export default Languages;
