import Languages from './languages.component';
import React from 'react';
import {mount} from 'enzyme';
import renderer from 'react-test-renderer';

/* eslint-disable react/jsx-no-bind */
/* eslint-disable react/jsx-handler-names */
describe('Sheet languages tests', () => {
    test('Should match previous Languages snapshot', () => {
        const Rendered = renderer.create(
            <Languages
                languages={[{id: 0}]}
                onLanguageAdd={() => null}
                onLanguageChange={() => null}
                onLanguageClean={() => null}
                onLanguageDelete={() => null}
                onLanguageMove={() => null}
                onLanguageReset={() => null}
            />
        );
        expect(Rendered).toMatchSnapshot();
    });

    test('Should check languages component integration', () => {
        const mockResetClick = jest.fn();
        const mockAddClick = jest.fn();
        const mockRemoveClick = jest.fn();
        const mockDeleteClick = jest.fn();
        const mockChange = jest.fn();

        const component = mount(
            <Languages
                languages={[{id: 0}]}
                onLanguageAdd={mockAddClick}
                onLanguageChange={mockChange}
                onLanguageClean={mockRemoveClick}
                onLanguageDelete={mockDeleteClick}
                onLanguageMove={() => null}
                onLanguageReset={mockResetClick}
            />
        );

        component.find('.fa').forEach((element) => element.simulate('click'));
        expect(mockResetClick).toBeCalled();
        expect(mockAddClick).toBeCalled();
        expect(mockRemoveClick).toBeCalled();

        const valueField = component.find('input').first();
        valueField.simulate('change', {
            target: {name: 'change', value: 'test'}
        });
        valueField.simulate('blur', {target: {name: 'change'}});
        expect(mockChange).toBeCalled();
        expect(mockChange.mock.calls[0][0]).toBe(0);
        expect(mockChange.mock.calls[0][1]).toBe('test');
    });
});
