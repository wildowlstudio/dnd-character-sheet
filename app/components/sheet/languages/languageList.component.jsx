import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

const LanguageList = ({t}) => {
    return (
        <datalist id="languageList">
            {[
                'COMMON',
                'DWARVISH',
                'ELVISH',
                'GIANT',
                'GNOMISH',
                'GOBLIN',
                'HALFLING',
                'ORC',
                'ABYSSAL',
                'CELESTIAL',
                'DRACONIC',
                'DEEP',
                'INFERNAL',
                'PRIMORDIAL',
                'SYLVAN',
                'UNDERCOMMON'
            ]
                .map((languageType) => t(languageType))
                .map((languageType) => (
                    <option
                        key={languageType}
                        value={languageType}
                    />
                ))}
        </datalist>
    );
};

LanguageList.propTypes = {
    t: PropTypes.func.isRequired
};

export default translate(['languages'])(LanguageList);
