import {Flex, Input, TrashIcon} from '../basic';
import {handleDragOver, isDroppedBelow} from '../../../utils/dnd.util';

import PropTypes from 'prop-types';
import React from 'react';
import {findDOMNode} from 'react-dom';

class Language extends React.PureComponent {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleDragStart = this.handleDragStart.bind(this);
        this.handleDrop = this.handleDrop.bind(this);
    }

    handleChange(value) {
        const {language} = this.props;
        if (language.id === 'new') {
            findDOMNode(this) // eslint-disable-line react/no-find-dom-node
                .getElementsByTagName('input')[0]
                .focus();
        }
        this.props.onChange(language.id, value);
    }

    handleDelete() {
        this.props.onDelete(this.props.language.id);
    }

    handleDragStart(event) {
        event.dataTransfer.setData('id', this.props.language.id);
    }

    handleDrop(event) {
        event.preventDefault();
        const id = +event.dataTransfer.getData('id');
        const {language} = this.props;
        if (id !== language.id) {
            const below = isDroppedBelow(event);
            this.props.onMove(id, below, language.id);
        }
    }

    render() {
        const {language} = this.props;

        return (
            <Flex
                draggable={language.id !== 'new'}
                grow
                maxWidth="33%"
                minWidth="30%"
                onDragOver={handleDragOver}
                onDragStart={this.handleDragStart}
                onDrop={this.handleDrop}
                trash
            >
                <Input
                    align="left"
                    key={language.id}
                    list="languageList"
                    onChange={this.handleChange}
                    value={language.value}
                />
                {language.id !== 'new' ? (
                    <TrashIcon
                        aria-label="delete language"
                        onClick={this.handleDelete}
                    />
                ) : null}
            </Flex>
        );
    }
}

Language.propTypes = {
    language: PropTypes.shape({
        id: PropTypes.number.isRequired,
        value: PropTypes.string
    }).isRequired,
    onChange: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    onMove: PropTypes.func.isRequired
};

export default Language;
