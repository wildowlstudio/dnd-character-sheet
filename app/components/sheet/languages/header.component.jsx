import {Field} from '../basic';
import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

const LanguagesHeader = ({t}) => {
    return (
        <Field
            inverted
            noWidth
        >
            {t('LANGUAGES')}
        </Field>
    );
};

LanguagesHeader.propTypes = {
    t: PropTypes.func.isRequired
};

export default translate(['common'])(LanguagesHeader);
