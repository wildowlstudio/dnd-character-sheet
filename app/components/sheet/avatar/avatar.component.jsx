import {Element} from '../basic';
import PropTypes from 'prop-types';
import React from 'react';
import media from '../../../utils/style.utils';
import styled from 'styled-components';

const StyledElement = styled(Element)`
    margin-left: auto;
    order: 4;
    margin-bottom: -39px;
    ${media.avatarWrap`
        order: 2;
        margin-left: 5px;
        margin-bottom: 0;
    `};
`;

const Container = styled.div`
    height: 205px;
    width: 164px;
    margin-bottom: -39px;
    display: flex;
    border: 3px solid;
    align-items: center;
    justify-content: center;
    ${media.avatarWrap`
        height: 166px;
        width: 135px;
        margin-bottom: 0;
    `};
`;

const Icon = styled.i`
    font-size: 120px;
    ${media.print`
        display: none;
    `};
`;

const Image = styled.img`
    width: 100%;
    height: 100%;
`;

const HiddenInput = styled.input`
    display: none;
`;

class Avatar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            avatar: props.avatar || null
        };

        this.inputRef = this.inputRef.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.avatar !== this.props.avatar) {
            this.setState({avatar: nextProps.avatar});
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextState.avatar !== this.state.avatar;
    }

    inputRef(ref) {
        if (ref) {
            this.input = ref;
        }
    }

    handleClick() {
        this.input.click();
    }

    handleChange(event) {
        const files = event.target.files;
        if (files.length) {
            const reader = new FileReader();
            reader.addEventListener(
                'load',
                () => {
                    this.props.onChange(reader.result);
                },
                false
            );

            if (files[0]) {
                reader.readAsDataURL(files[0]);
            }
        }
    }

    render() {
        const {avatar} = this.state;

        return (
            <StyledElement onReset={this.props.onReset}>
                <Container onClick={this.handleClick}>
                    {avatar ? (
                        <Image
                            alt="Avatar"
                            src={avatar}
                        />
                    ) : (
                        <Icon className="fa fa-user" />
                    )}
                </Container>
                <HiddenInput
                    accept="image/*"
                    innerRef={this.inputRef}
                    onChange={this.handleChange}
                    type="file"
                />
            </StyledElement>
        );
    }
}

Avatar.propTypes = {
    avatar: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    onReset: PropTypes.func.isRequired
};

export default Avatar;
