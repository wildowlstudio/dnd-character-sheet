import Avatar from './avatar.component';
import React from 'react';
import {mount} from 'enzyme';
import renderer from 'react-test-renderer';

/* eslint-disable react/jsx-no-bind */
/* eslint-disable react/jsx-handler-names */
describe('Sheet avatar tests', () => {
    test('Should match previous Avatar snapshot', () => {
        const Rendered = renderer.create(
            <Avatar
                Avatar=""
                onAvatarChange={() => null}
                onAvatarReset={() => null}
            />
        );
        expect(Rendered).toMatchSnapshot();
    });

    test('Should check avatar component partial integration', () => {
        const mockResetClick = jest.fn();
        const component = mount(
            <Avatar
                avatar=""
                onChange={() => null}
                onReset={mockResetClick}
            />
        );
        component
            .find('.fa-eraser')
            .last()
            .simulate('click');
        expect(mockResetClick).toBeCalled();
    });
});
