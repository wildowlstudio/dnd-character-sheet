import Input from './input.component';
import React from 'react';
import enzyme from 'enzyme';

describe('Sheet input', () => {
    test('Blur should trigger onChange callback', () => {
        const mockOnChange = jest.fn();
        const component = enzyme.shallow(<Input
            onChange={mockOnChange}
            value=""
                                         />);
        const event1 = {target: {name: 'change', value: 'test'}};
        const event2 = {target: {name: 'blur'}};
        component.simulate('change', event1);
        component.simulate('blur', event2);
        expect(mockOnChange).toBeCalled();
        expect(mockOnChange.mock.calls[0][0]).toBe('test');
    });

    test('Blur should not trigger onChange callback when state is equal to value', () => {
        const mockOnChange = jest.fn();
        const component = enzyme.shallow(<Input
            onChange={mockOnChange}
            value="test"
                                         />);
        const event1 = {target: {name: 'change', value: 'test'}};
        const event2 = {target: {name: 'blur'}};
        component.simulate('change', event1);
        component.simulate('blur', event2);
        expect(mockOnChange).not.toBeCalled();
    });

    test('Should populate with initial value', () => {
        const component = enzyme.shallow(<Input value="test" />);
        expect(component.state().value).toBe('test');
    });
});
