import styled from 'styled-components';

const Label = styled.span`
    color: ${(props) => props.theme.labelColor};
    font-size: small;
    text-align: center;
    vertical-align: bottom;
    border: ${(props) => props.theme.borderWidth}px solid transparent;
    ${(props) => {
        if (props.field) {
            return `width: ${props.theme.fieldWidth * (props.multiply || 1) +
                props.theme.itemHorizontalSpacing * ((props.multiply || 1) - 1) +
                (props.widthFudgeFactor || 0) +
                props.theme.operatorWidth * (props.operators || 0) +
                props.theme.itemHorizontalSpacing * (props.operators || 0) * 2 +
                props.theme.borderWidth * 2 * (props.operators || 0)}px;`;
        }
    }};
`;

export default Label;
