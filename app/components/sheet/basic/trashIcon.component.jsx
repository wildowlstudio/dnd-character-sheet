import React from 'react';
import media from '../../../utils/style.utils';
import styled from 'styled-components';

const Icon = styled.button`
    width: 20px;
    border: none;
    top: 0;
    cursor: pointer;
    background-color: transparent;
    position: absolute;
    right: -2px;
    display: none;

    ${media.print`
        display: none !important;
    `};
`;

const TrashIcon = ({...props}) => {
    return <Icon
        className="fa fa-trash"
        {...props}
           />;
};

export default TrashIcon;
