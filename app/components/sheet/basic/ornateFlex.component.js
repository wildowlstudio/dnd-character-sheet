import styled from 'styled-components';

const OrnateFlex = styled.div`
    display: flex;
    align-items: flex-end;
    > div:not(:first-child) > * {
        border-left: none;
    }
    > div:not(:last-child) > div {
        border-right-color: ${(props) => props.theme.itemOutlineInvert};
    }
`;

export default OrnateFlex;
