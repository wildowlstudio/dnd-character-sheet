import styled from 'styled-components';

const VerticalText = styled.div`
    transform: rotate(180deg);
    text-transform: none;
    writing-mode: vertical-lr;
    font-size: smaller;
    border: ${(props) => `${props.theme.itemOutline} ${props.theme.borderWidth}`}px solid;
    text-align: center;
    width: ${(props) => props.theme.fieldHeight}px;
    background-color: ${(props) => props.theme.fieldBackgroundInvertColor};
    color: ${(props) => props.theme.fieldInvertColor};
    ${(props) => {
        if (props.onChange) {
            return 'cursor: pointer';
        }
    }};
`;

export default VerticalText;
