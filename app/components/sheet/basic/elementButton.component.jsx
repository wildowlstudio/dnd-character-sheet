import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';

const Button = styled.i`
    color: ${(props) => props.theme.labelColor};
    border: 1px solid ${(props) => props.theme.labelColor};
    border-radius: 10px;
    width: 18px;
    height: 18px;
    text-align: center;
    background-color: white;
    line-height: 18px;
    transition: ${(props) => props.theme.activeTransition};
    &:hover,
    &:focus {
        cursor: pointer;
        color: ${(props) => props.theme.activeColor};
        border-color: ${(props) => props.theme.activeColor};
    }
`;

const ElementButton = ({children, className, onClick}) => {
    return (
        <Button
            className={className}
            onClick={onClick}
            tabIndex="0"
        >
            {children}
        </Button>
    );
};

ElementButton.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    onClick: PropTypes.func.isRequired
};

export default ElementButton;
