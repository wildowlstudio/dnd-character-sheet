import media from '../../../utils/style.utils';
import styled from 'styled-components';

const Flex = styled.div`
    display: flex;
    ${options};
    ${align};
    ${({wrap}) => (wrap ? 'flex-wrap: wrap' : '')};
    ${({unwrapOnPrint}) => (unwrapOnPrint ? media.print`flex-wrap: nowrap` : '')};
    ${({minWidth, maxWidth}) =>
        minWidth && maxWidth ? `min-width: ${minWidth}; max-width: ${maxWidth}` : ''};
    ${({grow}) => (grow ? 'flex-grow: 1' : '')};
    ${({justify}) => (justify ? 'justify-content: ' + justify : '')};
    ${({order}) => (order ? 'order: ' + order : '')};
    ${({trash}) =>
        trash
            ? 'position: relative; &:focus-within, &:hover { button { display: block; }}'
            : ''};
`;

function options({spaced, direction, theme, wrap}) {
    if (spaced) {
        if (direction === 'column') {
            return `flex-direction: ${direction};
        > * {
            margin: ${theme.itemVerticalSpacing}px 0;
        }
        ${
            wrap
                ? ''
                : `> *:first-child, {
            margin: 0 0 ${theme.itemVerticalSpacing}px 0;
        }
        > *:last-child {
            margin: ${theme.itemVerticalSpacing}px 0 0 0;
        `
        }}`;
        } else {
            return `> * {
            margin: 0 ${theme.itemHorizontalSpacing}px;
        }
        ${
            wrap
                ? ''
                : `> *:first-child {
            margin: 0 ${theme.itemHorizontalSpacing}px 0 0;
        }
        > *:last-child{
            margin: 0 0 0 ${theme.itemHorizontalSpacing}px;
        }`
        }`;
        }
    } else if (direction === 'column') {
        return `flex-direction: ${direction};`;
    }
    return '';
}

function align({align}) {
    if (align) {
        return `align-items: ${align}`;
    }
}

export default Flex;
