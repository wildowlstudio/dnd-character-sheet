import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';

const StyledTextArea = styled.div`
    min-height: ${({theme}) => theme.fieldHeight * 3}px;
    width: 100%;
    font-size: small;
`;

class TextArea extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            value: props.value || ''
        };

        this.handleBlur = this.handleBlur.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (
            nextProps.value !== this.state.value &&
            !(!this.state.value && !nextProps.value)
        ) {
            this.setState({value: nextProps.value || ''});
        }
    }

    handleBlur(event) {
        if (
            this.state.value !== this.props.value &&
            !(!this.state.value && !this.props.value)
        ) {
            this.props.onChange(this.state.value, event);
        }
    }

    handleChange(event) {
        this.setState({
            value: event.target.innerHTML
        });
    }

    render() {
        const {
            onChange, // eslint-disable-line no-unused-vars
            value, // eslint-disable-line no-unused-vars
            ...other
        } = this.props;
        return (
            <StyledTextArea
                contentEditable
                dangerouslySetInnerHTML={{__html: this.props.value}}
                onBlur={this.handleBlur}
                onInput={this.handleChange}
                {...other}
            />
        );
    }
}

TextArea.propTypes = {
    onChange: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired
};

export default TextArea;
