import DropdownMenu from 'react-dd-menu';
import ElementButton from './elementButton.component';
import Flex from './flex.component';
import PropTypes from 'prop-types';
import React from 'react';
import media from '../../../utils/style.utils';
import styled from 'styled-components';

const Container = styled.div`
    padding: 2px;
    position: relative;
    border: 1px solid transparent;
    &:hover,
    &:focus-within {
        border-color: ${(props) => props.theme.labelColor};
    }
    &:hover > div:first-child,
    &:focus-within > div:first-child {
        display: block;
    }
    ${media.print`
        page-break-inside: avoid;
        border: 1px solid transparent !important;

        > div:first-child {
            display: none !important;
    `};
`;

const StyledFlex = Flex.extend`
    top: -9px;
    position: absolute;
    display: none;
    z-index: 100;
`;

class Element extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            open: false
        };

        this.handleReset = this.handleReset.bind(this);
        this.handleOpen = this.handleOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    handleReset(event) {
        event.stopPropagation();
        this.props.onReset();
    }

    handleOpen(event) {
        event.stopPropagation();
        this.setState({open: true});
    }

    handleClose() {
        this.setState({open: false});
    }

    render() {
        const {
            children,
            className,
            dropdown,
            onAdd,
            onClick,
            onDelete,
            onMove,
            onRemove,
            onReset,
            readOnly
        } = this.props;

        return (
            <Container
                className={className}
                onClick={onClick}
            >
                <StyledFlex>
                    {onReset &&
                        !readOnly && (
                            <ElementButton
                                className="fa fa-eraser"
                                onClick={this.handleReset}
                            />
                        )}
                    {onAdd &&
                        !readOnly && (
                            <ElementButton
                                className="fa fa-plus"
                                onClick={onAdd}
                            />
                        )}
                    {onRemove &&
                        !readOnly && (
                            <ElementButton
                                className="fa fa-minus"
                                onClick={onRemove}
                            />
                        )}
                    {onMove &&
                        !readOnly && (
                            <ElementButton
                                className="fa fa-arrows"
                                onClick={onMove}
                            />
                        )}
                    {onDelete &&
                        !readOnly && (
                            <ElementButton
                                className="fa fa-times"
                                onClick={onDelete}
                            />
                        )}
                    {dropdown &&
                        !readOnly && (
                            <DropdownMenu
                                align="left"
                                close={this.handleClose} // eslint-disable-line react/jsx-handler-names
                                closeOnInsideClick={false}
                                isOpen={this.state.open}
                                toggle={
                                    <ElementButton
                                        className="fa fa-ellipsis-v"
                                        onClick={this.handleOpen}
                                    />
                                }
                            >
                                {dropdown}
                            </DropdownMenu>
                        )}
                </StyledFlex>
                {children}
            </Container>
        );
    }
}

Element.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    dropdown: PropTypes.node,
    onAdd: PropTypes.func,
    onClick: PropTypes.func,
    onDelete: PropTypes.func,
    onMove: PropTypes.func,
    onRemove: PropTypes.func,
    onReset: PropTypes.func,
    readOnly: PropTypes.bool
};

export default Element;
