import Label from './label.component';
import styled from 'styled-components';

const Operator = styled(Label)`width: ${(props) => props.theme.operatorWidth}px;`;

export default Operator;
