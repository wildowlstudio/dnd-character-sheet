import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';

const StyledInput = styled.input`
    background: transparent;
    height: ${(props) => props.theme.inputHeight}px;
    border: none;
    text-align: ${({align, field}) => align || (field && 'center') || 'left'};
    ${({field, theme, multiply, operators, widthFudgeFactor, grow}) => {
        if (grow) {
            return `flex-grow: 1; flex-basis: ${estimateWidth({
                theme,
                multiply,
                operators,
                widthFudgeFactor
            })}px`;
        } else if (field) {
            return `width: ${estimateWidth({
                theme,
                multiply,
                operators,
                widthFudgeFactor
            })}px `;
        } else if (widthFudgeFactor) {
            return `width: calc(100% + ${widthFudgeFactor}px)`;
        }
        return 'width: 100%';
    }};
    ${(props) => {
        const borderColor = props.temporary
            ? props.theme.temporaryItemOutline
            : props.theme.itemOutline;
        if (props.border === 'bottom') {
            return `border-bottom: ${borderColor} ${props.theme.borderWidth}px solid;`;
        } else if (props.border === 'full') {
            return `border: ${borderColor} ${props.theme.borderWidth}px solid;`;
        }
        return '';
    }};
`;

const StyledLabel = styled.label`
    color: ${(props) => props.theme.labelColor};
    display: inline-block;
    font-size: ${(props) => props.theme.labelFontSize};
    width: 100%;
`;

class Input extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            value: props.value || ''
        };

        this.handleBlur = this.handleBlur.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (
            nextProps.value !== this.state.value &&
            !(!this.state.value && !nextProps.value)
        ) {
            this.setState({value: nextProps.value || ''});
        }
    }

    handleBlur(event) {
        if (
            this.state.value !== this.props.value &&
            !(!this.state.value && !this.props.value)
        ) {
            if (this.props.type === 'number') {
                this.props.onChange(+this.state.value, event);
            } else {
                this.props.onChange(this.state.value, event);
            }
        }
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleKeyDown(event) {
        if (event.keyCode === 13) {
            if (this.props.onEnter) {
                this.props.onEnter(event, event.target.value);
            } else {
                let target = document.activeElement.nextSibling;
                if (!target || target.tagName !== 'INPUT') {
                    const elements = document.activeElement.parentElement.parentElement.parentElement.parentElement.getElementsByTagName(
                        'input'
                    );
                    for (let i = elements.length - 2; i >= 0; i--) {
                        if (elements[i] === document.activeElement) {
                            elements[i + 1].focus();
                            break;
                        }
                    }
                } else {
                    target.focus();
                }
            }
        }
    }

    render() {
        const {
            border,
            className,
            field,
            grow,
            label,
            multiply,
            onChange, // eslint-disable-line no-unused-vars
            onEnter, // eslint-disable-line no-unused-vars
            operators,
            temporary,
            value, // eslint-disable-line no-unused-vars
            widthFudgeFactor,
            ...other
        } = this.props;

        if (label) {
            return (
                <StyledLabel className={className}>
                    <StyledInput
                        border={border}
                        field={field}
                        grow={grow}
                        multiply={multiply}
                        onBlur={this.handleBlur}
                        onChange={this.handleChange}
                        onKeyDown={this.handleKeyDown}
                        operators={operators}
                        size="1"
                        temporary={temporary}
                        value={this.state.value}
                        widthFudgeFactor={widthFudgeFactor}
                        {...other}
                    />
                    {label}
                </StyledLabel>
            );
        }
        return (
            <StyledInput
                border={border}
                className={className}
                field={field}
                grow={grow}
                multiply={multiply}
                onBlur={this.handleBlur}
                onChange={this.handleChange}
                onKeyDown={this.handleKeyDown}
                operators={operators}
                size="1"
                temporary={temporary}
                value={this.state.value}
                widthFudgeFactor={widthFudgeFactor}
                {...other}
            />
        );
    }
}

function estimateWidth({multiply, operators, theme, widthFudgeFactor}) {
    return (
        theme.fieldWidth * multiply +
        theme.itemHorizontalSpacing * (multiply - 1) * 2 +
        theme.borderWidth * 2 * (multiply - 1) +
        theme.operatorWidth * operators +
        theme.itemHorizontalSpacing * operators * 2 +
        theme.borderWidth * 2 * operators +
        widthFudgeFactor
    );
}

Input.propTypes = {
    border: PropTypes.string,
    className: PropTypes.string,
    field: PropTypes.bool,
    grow: PropTypes.bool,
    label: PropTypes.string,
    multiply: PropTypes.number,
    onChange: PropTypes.func.isRequired,
    onEnter: PropTypes.func,
    operators: PropTypes.number,
    temporary: PropTypes.bool,
    type: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    widthFudgeFactor: PropTypes.number
};

Input.defaultProps = {
    border: 'bottom',
    multiply: 1,
    operators: 0,
    widthFudgeFactor: 0
};

export default Input;
