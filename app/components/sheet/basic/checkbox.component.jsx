import PropTypes from 'prop-types';
import React from 'react';

class Checkbox extends React.PureComponent {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.props.onChange(event.target.checked);
    }

    render() {
        const {label} = this.props;
        if (label) {
            return (
                <label // eslint-disable-line
                    htmlFor={label}
                    onClick={stopPropagation}
                >
                    <input
                        checked={this.props.value || false}
                        id={label}
                        onChange={this.handleChange}
                        type="checkbox"
                    />
                    {label}
                </label>
            );
        }
        return (
            <input
                checked={this.props.value || false}
                onChange={this.handleChange}
                type="checkbox"
            />
        );
    }
}

function stopPropagation(event) {
    event.stopPropagation();
}

Checkbox.propTypes = {
    label: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.bool
};

export default Checkbox;
