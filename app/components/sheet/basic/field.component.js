import styled from 'styled-components';

const Field = styled.div`
    border: ${(props) => `${props.theme.itemOutline} ${props.theme.borderWidth}`}px solid;
    text-align: ${({align}) => align || 'center'};
    font-size: ${(props) => props.theme.labelFontSize};
    height: ${(props) => props.theme.fieldHeight + (props.heightFudgeFactor || 0)}px;
    line-height: ${(props) => props.theme.fieldHeight + (props.heightFudgeFactor || 0)}px;
    ${({grow, fullWidth, theme, multiply, widthFudgeFactor, noWidth}) => {
        if (noWidth) {
            return '';
        } else if (fullWidth) {
            return 'width: 100%';
        } else if (grow) {
            return 'flex-grow: 1';
        }
        return `width: ${theme.fieldWidth * (multiply || 1) +
            theme.itemHorizontalSpacing * ((multiply || 1) - 1) +
            (widthFudgeFactor || 0)}px`;
    }};
    ${(props) => {
        if (props.inverted) {
            return `background-color: ${props.theme.fieldBackgroundInvertColor};
            color: ${props.theme.fieldInvertColor};
            text-transform: uppercase;`;
        }
    }};
    ${(props) => {
        if (props.ornate) {
            return `border-radius: 5px 5px 0 0;
            height: ${props.theme.higherFieldHeight}px;
            line-height: ${props.theme.higherFieldHeight}px;
            font-weight: bold;`;
        }
    }};
    ${(props) => {
        if (props.valid !== undefined) {
            if (props.valid) {
                return `color: ${props.theme.validColor};`;
            } else {
                return `color: ${props.theme.invalidColor};`;
            }
        }
    }};
    ${(props) => {
        if (props.onClick) {
            return 'cursor: pointer;';
        }
    }};
    > span {
        display: inline-block;
        vertical-align: middle;
        line-height: normal;
    }
`;

export default Field;
