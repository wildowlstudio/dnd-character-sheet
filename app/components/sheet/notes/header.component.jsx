import {Field} from '../basic';
import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

const NotesHeader = ({t}) => {
    return (
        <Field
            inverted
            noWidth
        >
            {t('NOTES')}
        </Field>
    );
};

NotesHeader.propTypes = {
    t: PropTypes.func.isRequired
};

export default translate(['notes'])(NotesHeader);
