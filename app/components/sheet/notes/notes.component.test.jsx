import Notes from './notes.component';
import React from 'react';
import {mount} from 'enzyme';
import renderer from 'react-test-renderer';

/* eslint-disable react/jsx-no-bind */
/* eslint-disable react/jsx-handler-names */
describe('Sheet notes tests', () => {
    test('Should match previous Notes snapshot', () => {
        const Rendered = renderer.create(
            <Notes
                notes=""
                onNotesChange={() => null}
                onNotesReset={() => null}
            />
        );
        expect(Rendered).toMatchSnapshot();
    });

    test('Should check notes component integration', () => {
        const mockResetClick = jest.fn();
        const fields = [{text: 'Notes', fun: jest.fn(), value: 'Notes'}];
        const component = mount(
            <Notes
                notes=""
                onNotesChange={fields[0].fun}
                onNotesReset={mockResetClick}
            />
        );
        component
            .find('.fa-eraser')
            .last()
            .simulate('click');
        expect(mockResetClick).toBeCalled();

        component.find('input').forEach((input, i) => {
            input.simulate('change', {
                target: {name: 'change', value: fields[i].value}
            });
            input.simulate('blur', {target: {name: 'change'}});
            expect(fields[i].fun).toBeCalled();
            expect(fields[i].fun.mock.calls[0][0]).toBe(fields[i].value);
        });
    });
});
