import {Element, TextArea} from '../basic';

import Header from './header.component';
import PropTypes from 'prop-types';
import React from 'react';
import media from '../../../utils/style.utils';
import styled from 'styled-components';

const StyledElement = styled(Element)`
    width: 407px;

    ${media.oneRow`
        width: 608px;
    `};
`;

const Notes = ({className, notes, onNotesChange, onNotesReset}) => {
    return (
        <StyledElement
            className={className}
            onReset={onNotesReset}
        >
            <Header />
            <TextArea
                onChange={onNotesChange}
                value={notes}
            />
        </StyledElement>
    );
};

Notes.propTypes = {
    className: PropTypes.string,
    notes: PropTypes.string,
    onNotesChange: PropTypes.func.isRequired,
    onNotesReset: PropTypes.func.isRequired
};

export default Notes;
