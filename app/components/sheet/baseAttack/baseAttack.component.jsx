import {Element, Field, Flex, Input} from '../basic';

import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';
import {translate} from 'react-i18next';

const StyledElement = styled(Element)`
    margin-top: -2px;
    /* position: absolute;
    top: 272px;
    left: 256px; */
`;

const BaseAttack = ({onBaseAttackChange, onBaseAttackReset, baseAttack, t}) => (
    <StyledElement onReset={onBaseAttackReset}>
        <Flex
            align="center"
            spaced
        >
            <Field
                inverted
                multiply={3}
            >
                {t('BASE_ATTACK')}
            </Field>
            <Input
                border="full"
                field
                multiply={3}
                onChange={onBaseAttackChange}
                value={baseAttack}
            />
        </Flex>
    </StyledElement>
);

BaseAttack.propTypes = {
    baseAttack: PropTypes.string,
    onBaseAttackChange: PropTypes.func.isRequired,
    onBaseAttackReset: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired
};

export default translate(['baseAttack'])(BaseAttack);
