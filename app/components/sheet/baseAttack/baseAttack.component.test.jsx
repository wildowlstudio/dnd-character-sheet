import BaseAttack from './baseAttack.component';
import React from 'react';
import {mount} from 'enzyme';
import renderer from 'react-test-renderer';

/* eslint-disable react/jsx-no-bind */
/* eslint-disable react/jsx-handler-names */
describe('Sheet base attack tests', () => {
    test('Should match previous BaseAttack snapshot', () => {
        const Rendered = renderer.create(
            <BaseAttack
                baseAttack=""
                onBaseAttackChange={() => null}
                onBaseAttackReset={() => null}
            />
        );
        expect(Rendered).toMatchSnapshot();
    });

    test('Should check base attack component integration', () => {
        const mockResetClick = jest.fn();
        const fields = [{text: 'BASE_ATTACK', fun: jest.fn(), value: 'BASE_ATTACK'}];
        const component = mount(
            <BaseAttack
                baseAttack=""
                onBaseAttackChange={fields[0].fun}
                onBaseAttackReset={mockResetClick}
            />
        );
        component
            .find('.fa-eraser')
            .last()
            .simulate('click');
        expect(mockResetClick).toBeCalled();

        component.find('input').forEach((input, i) => {
            input.simulate('change', {
                target: {name: 'change', value: fields[i].value}
            });
            input.simulate('blur', {target: {name: 'change'}});
            expect(fields[i].fun).toBeCalled();
            expect(fields[i].fun.mock.calls[0][0]).toBe(fields[i].value);
        });
    });
});
