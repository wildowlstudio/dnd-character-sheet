import {Element, Flex} from '../basic';

import Header from './header.component';
import PropTypes from 'prop-types';
import React from 'react';
import Stat from './stat.component';
import styled from 'styled-components';

const StyledElement = styled(Element)`
    width: 234px;
    order: 1;
`;

const Stats = ({
    onStatModChange,
    onStatReset,
    onStatTempModChange,
    onStatTempScoreChange,
    stats
}) => (
    <StyledElement onReset={onStatReset}>
        <Flex
            direction="column"
            spaced
        >
            <Header />
            {['str', 'dex', 'con', 'int', 'wis', 'cha'].map((stat) => (
                <Stat
                    key={stat}
                    onStatModChange={onStatModChange}
                    onStatTempModChange={onStatTempModChange}
                    onStatTempScoreChange={onStatTempScoreChange}
                    stat={stat}
                    values={stats[stat]}
                />
            ))}
        </Flex>
    </StyledElement>
);

Stats.propTypes = {
    onStatModChange: PropTypes.func.isRequired,
    onStatReset: PropTypes.func.isRequired,
    onStatTempModChange: PropTypes.func.isRequired,
    onStatTempScoreChange: PropTypes.func.isRequired,
    stats: PropTypes.object.isRequired
};

export default Stats;
