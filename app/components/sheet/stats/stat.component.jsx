import {Field, Flex, Input} from '../basic';

import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

class Stats extends React.Component {
    constructor(props) {
        super(props);

        this.handleStatModChange = this.handleStatModChange.bind(this);
        this.handleStatTempModChange = this.handleStatTempModChange.bind(this);
        this.handleStatTempScoreChange = this.handleStatTempScoreChange.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.values !== this.props.values;
    }

    handleStatModChange(value) {
        const {onStatModChange, stat} = this.props;
        onStatModChange(stat, value);
    }

    handleStatTempModChange(value) {
        const {onStatTempModChange, stat} = this.props;
        onStatTempModChange(stat, value);
    }

    handleStatTempScoreChange(value) {
        const {onStatTempScoreChange, stat} = this.props;
        onStatTempScoreChange(stat, value);
    }

    render() {
        const {stat, values, t} = this.props;

        return (
            <Flex spaced>
                <Field inverted>{t(stat.toUpperCase())}</Field>
                <Field>
                    {(values.mod || 0) + (values.tmpScore || 0) + (values.tmpMod || 0)}
                </Field>
                <Input
                    border="full"
                    field
                    onChange={this.handleStatModChange}
                    type="number"
                    value={values.mod}
                />
                <Input
                    border="full"
                    field
                    onChange={this.handleStatTempScoreChange}
                    temporary
                    type="number"
                    value={values.tmpScore}
                />
                <Input
                    border="full"
                    field
                    onChange={this.handleStatTempModChange}
                    temporary
                    type="number"
                    value={values.tmpMod}
                />
            </Flex>
        );
    }
}

Stats.propTypes = {
    onStatModChange: PropTypes.func.isRequired,
    onStatTempModChange: PropTypes.func.isRequired,
    onStatTempScoreChange: PropTypes.func.isRequired,
    stat: PropTypes.string.isRequired,
    t: PropTypes.func.isRequired,
    values: PropTypes.shape({
        mod: PropTypes.number,
        tmpScore: PropTypes.number,
        tmpMod: PropTypes.number
    }).isRequired
};

export default translate(['stats'])(Stats);
