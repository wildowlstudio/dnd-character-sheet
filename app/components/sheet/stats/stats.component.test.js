import React from 'react';
import Stats from './stats.component';
import renderer from 'react-test-renderer';

/* eslint-disable react/jsx-no-bind */
describe('Sheet stats tests', () => {
    test('Should match previous Stats snapshot', () => {
        const Rendered = renderer.create(
            <Stats
                onStatModChange={() => null}
                onStatTempModChange={() => null}
                onStatTempScoreChange={() => null}
                stats={{
                    str: {},
                    dex: {},
                    con: {},
                    int: {},
                    wis: {},
                    cha: {}
                }}
            />
        );
        expect(Rendered).toMatchSnapshot();
    });
});
