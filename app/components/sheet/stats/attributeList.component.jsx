import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

const AttributeList = ({t}) => {
    return (
        <datalist id="attributeList">
            {['STR', 'DEX', 'CON', 'INT', 'WIS', 'CHA']
                .map((attribute) => t(attribute).toUpperCase())
                .sort()
                .map((attribute) => <option
                    key={attribute}
                    value={attribute}
                                    />)}
        </datalist>
    );
};

AttributeList.propTypes = {
    t: PropTypes.func.isRequired
};

export default translate(['stats'])(AttributeList);
