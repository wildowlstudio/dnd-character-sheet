import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';
import {Flex, Label} from '../basic';

const Stats = ({t}) => (
    <Flex
        align="flex-end"
        spaced
    >
        <Label field>{t('ABILITY')}</Label>
        <Label field>{t('SCORE')}</Label>
        <Label field>{t('MOD')}</Label>
        <Label field>{t('TEMP_SCORE')}</Label>
        <Label field>{t('TEMP_MOD')}</Label>
    </Flex>
);

Stats.propTypes = {
    t: PropTypes.func.isRequired
};

export default translate(['stats'])(Stats);
