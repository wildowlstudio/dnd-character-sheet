import Armor from './armor.component';
import React from 'react';
import {mount} from 'enzyme';
import renderer from 'react-test-renderer';

/* eslint-disable react/jsx-no-bind */
/* eslint-disable react/jsx-handler-names */
describe('Sheet armor tests', () => {
    test('Should match previous Armor snapshot', () => {
        const Rendered = renderer.create(
            <Armor
                armor={{id: 0}}
                onArmorAdd={() => null}
                onArmorBonusChange={() => null}
                onArmorDelete={() => null}
                onArmorMaxDexChange={() => null}
                onArmorMaxSpeedChange={() => null}
                onArmorNameChange={() => null}
                onArmorNotesChange={() => null}
                onArmorPenaltyChange={() => null}
                onArmorReset={() => null}
                onArmorSpellFailureChange={() => null}
                onArmorTypeChange={() => null}
                onArmorWeightChange={() => null}
            />
        );
        expect(Rendered).toMatchSnapshot();
    });
    test('Should check armor component integration', () => {
        const mockResetClick = jest.fn();
        const mockAddClick = jest.fn();
        const mockDeleteClick = jest.fn();
        const fields = [
            {text: 'ARMOR', fun: jest.fn()},
            {text: 'BONUS', fun: jest.fn()},
            {text: 'MAX_DEX', fun: jest.fn()},
            {text: 'PENALTY', fun: jest.fn()},
            {text: 'MAX_SPEED', fun: jest.fn()},
            {text: 'WEIGHT', fun: jest.fn()},
            {text: 'TYPE', fun: jest.fn()},
            {text: 'SPELL_FAILURE', fun: jest.fn()},
            {text: 'NOTES', fun: jest.fn()}
        ];
        const component = mount(
            <Armor
                armor={{id: 0}}
                onArmorAdd={mockAddClick}
                onArmorBonusChange={fields[1].fun}
                onArmorDelete={mockDeleteClick}
                onArmorMaxDexChange={fields[2].fun}
                onArmorMaxSpeedChange={fields[4].fun}
                onArmorNameChange={fields[0].fun}
                onArmorNotesChange={fields[8].fun}
                onArmorPenaltyChange={fields[3].fun}
                onArmorReset={mockResetClick}
                onArmorSpellFailureChange={fields[7].fun}
                onArmorTypeChange={fields[6].fun}
                onArmorWeightChange={fields[5].fun}
            />
        );
        component
            .find('.fa-plus')
            .last()
            .simulate('click');
        expect(mockAddClick).toBeCalled();

        component
            .find('.fa-times')
            .last()
            .simulate('click');
        expect(mockDeleteClick).toBeCalled();
        expect(mockDeleteClick.mock.calls[0][0]).toBe(0);

        component.find('input').forEach((input, i) => {
            input.simulate('change', {
                target: {name: 'change', value: fields[i].text}
            });
            input.simulate('blur', {target: {name: 'change'}});
            expect(fields[i].fun).toBeCalled();
            expect(fields[i].fun.mock.calls[0][0]).toBe(0);
            expect(fields[i].fun.mock.calls[0][1]).toBe(fields[i].text);
        });
    });
});
