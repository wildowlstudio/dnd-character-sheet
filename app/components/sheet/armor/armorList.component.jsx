import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

const ArmorList = ({t}) => {
    return (
        <datalist id="armorList">
            {[
                'PADDED',
                'LEATHER',
                'STUDDED_LEATHER',
                'CHAIN_SHIRT',
                'HIDE',
                'SCALE_MAIL',
                'CHAINMAIL',
                'BREASTPLATE',
                'SPLINT_MAIL',
                'BANDED_MAIL',
                'HALF',
                'FULL_PLATE',
                'BUCKLER',
                'SHIELD_LIGHT_WOODEN',
                'SHIELD_LIGHT_STEEL',
                'SHIELD_HEAVY_WOODEN',
                'SHIELD_HEAVY_STEEL',
                'SHIELD_TOWER'
            ]
                .map((armorType) => t(armorType))
                .sort()
                .map((armorType) => <option
                    key={armorType}
                    value={armorType}
                                    />)}
        </datalist>
    );
};

ArmorList.propTypes = {
    t: PropTypes.func.isRequired
};

export default translate(['armors'])(ArmorList);
