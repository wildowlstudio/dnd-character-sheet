import {Element, Field, Input, OrnateFlex} from '../basic';

import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

class Armor extends React.Component {
    constructor(props) {
        super(props);
        this.handleArmorBonusChange = this.handleArmorBonusChange.bind(this);
        this.handleArmorDelete = this.handleArmorDelete.bind(this);
        this.handleArmorMaxDexChange = this.handleArmorMaxDexChange.bind(this);
        this.handleArmorMaxSpeedChange = this.handleArmorMaxSpeedChange.bind(this);
        this.handleArmorNameChange = this.handleArmorNameChange.bind(this);
        this.handleArmorNotesChange = this.handleArmorNotesChange.bind(this);
        this.handleArmorPenaltyChange = this.handleArmorPenaltyChange.bind(this);
        this.handleArmorReset = this.handleArmorReset.bind(this);
        this.handleArmorSpellFailureChange = this.handleArmorSpellFailureChange.bind(
            this
        );
        this.handleArmorTypeChange = this.handleArmorTypeChange.bind(this);
        this.handleArmorWeightChange = this.handleArmorWeightChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.armor !== this.props.armor;
    }

    handleArmorBonusChange(value) {
        this.props.onArmorBonusChange(this.props.armor.id, value);
    }

    handleArmorDelete(value) {
        this.props.onArmorDelete(this.props.armor.id, value);
    }

    handleArmorMaxDexChange(value) {
        this.props.onArmorMaxDexChange(this.props.armor.id, value);
    }

    handleArmorMaxSpeedChange(value) {
        this.props.onArmorMaxSpeedChange(this.props.armor.id, value);
    }

    handleArmorNameChange(value) {
        this.props.onArmorNameChange(this.props.armor.id, value);
    }

    handleArmorNotesChange(value) {
        this.props.onArmorNotesChange(this.props.armor.id, value);
    }

    handleArmorPenaltyChange(value) {
        this.props.onArmorPenaltyChange(this.props.armor.id, value);
    }

    handleArmorReset(value) {
        this.props.onArmorReset(this.props.armor.id, value);
    }

    handleArmorSpellFailureChange(value) {
        this.props.onArmorSpellFailureChange(this.props.armor.id, value);
    }

    handleArmorTypeChange(value) {
        this.props.onArmorTypeChange(this.props.armor.id, value);
    }

    handleArmorWeightChange(value) {
        this.props.onArmorWeightChange(this.props.armor.id, value);
    }

    handleClick() {
        this.props.onClick('armor');
    }

    render() {
        const {armor, onArmorAdd, readOnly, t} = this.props;

        return (
            <Element
                onAdd={onArmorAdd}
                onClick={readOnly ? this.handleClick : null}
                onDelete={this.handleArmorDelete}
                onReset={this.handleArmorReset}
                readOnly={readOnly}
                width={595}
            >
                <OrnateFlex>
                    <div>
                        <Field
                            inverted
                            multiply={7}
                            ornate
                            widthFudgeFactor={12}
                        >
                            {t('ARMOR')}
                        </Field>
                        <Input
                            border="full"
                            list="armorList"
                            onChange={this.handleArmorNameChange}
                            readOnly={readOnly}
                            value={armor.name}
                            widthFudgeFactor={-2}
                        />
                    </div>
                    <div>
                        <Field
                            inverted
                            multiply={3}
                        >
                            {t('BONUS')}
                        </Field>
                        <Input
                            border="full"
                            onChange={this.handleArmorBonusChange}
                            readOnly={readOnly}
                            value={armor.bonus}
                            widthFudgeFactor={-1}
                        />
                    </div>
                    <div>
                        <Field
                            inverted
                            multiply={2}
                        >
                            {t('MAX_DEX')}
                        </Field>
                        <Input
                            border="full"
                            onChange={this.handleArmorMaxDexChange}
                            readOnly={readOnly}
                            value={armor.maxDex}
                            widthFudgeFactor={-1}
                        />
                    </div>
                    <div>
                        <Field
                            inverted
                            multiply={2}
                        >
                            {t('PENALTY')}
                        </Field>
                        <Input
                            border="full"
                            onChange={this.handleArmorPenaltyChange}
                            readOnly={readOnly}
                            value={armor.penalty}
                            widthFudgeFactor={-1}
                        />
                    </div>
                </OrnateFlex>
                <OrnateFlex>
                    <div>
                        <Field
                            inverted
                            multiply={2}
                        >
                            {t('MAX_SPEED')}
                        </Field>
                        <Input
                            border="full"
                            onChange={this.handleArmorMaxSpeedChange}
                            readOnly={readOnly}
                            value={armor.maxSpeed}
                            widthFudgeFactor={-2}
                        />
                    </div>
                    <div>
                        <Field
                            inverted
                            multiply={2}
                        >
                            {t('WEIGHT')}
                        </Field>
                        <Input
                            border="full"
                            onChange={this.handleArmorWeightChange}
                            readOnly={readOnly}
                            value={armor.weight}
                            widthFudgeFactor={-1}
                        />
                    </div>
                    <div>
                        <Field
                            inverted
                            multiply={2}
                        >
                            {t('TYPE')}
                        </Field>
                        <Input
                            border="full"
                            onChange={this.handleArmorTypeChange}
                            readOnly={readOnly}
                            value={armor.type}
                            widthFudgeFactor={-1}
                        />
                    </div>
                    <div>
                        <Field
                            inverted
                            multiply={3}
                        >
                            {t('SPELL_FAILURE')}
                        </Field>
                        <Input
                            border="full"
                            onChange={this.handleArmorSpellFailureChange}
                            readOnly={readOnly}
                            value={armor.spellFailure}
                            widthFudgeFactor={-1}
                        />
                    </div>
                    <div>
                        <Field
                            inverted
                            multiply={5}
                            widthFudgeFactor={14}
                        >
                            {t('NOTES')}
                        </Field>
                        <Input
                            border="full"
                            onChange={this.handleArmorNotesChange}
                            readOnly={readOnly}
                            value={armor.notes}
                            widthFudgeFactor={-1}
                        />
                    </div>
                </OrnateFlex>
            </Element>
        );
    }
}
Armor.propTypes = {
    armor: PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string,
        bonus: PropTypes.string,
        maxDex: PropTypes.string,
        penalty: PropTypes.string,
        maxSpeed: PropTypes.string,
        weight: PropTypes.string,
        type: PropTypes.string,
        spellFailure: PropTypes.string,
        notes: PropTypes.string
    }).isRequired,
    onArmorAdd: PropTypes.func.isRequired,
    onArmorBonusChange: PropTypes.func.isRequired,
    onArmorDelete: PropTypes.func.isRequired,
    onArmorMaxDexChange: PropTypes.func.isRequired,
    onArmorMaxSpeedChange: PropTypes.func.isRequired,
    onArmorNameChange: PropTypes.func.isRequired,
    onArmorNotesChange: PropTypes.func.isRequired,
    onArmorPenaltyChange: PropTypes.func.isRequired,
    onArmorReset: PropTypes.func.isRequired,
    onArmorSpellFailureChange: PropTypes.func.isRequired,
    onArmorTypeChange: PropTypes.func.isRequired,
    onArmorWeightChange: PropTypes.func.isRequired,
    onClick: PropTypes.func,
    readOnly: PropTypes.bool,
    t: PropTypes.func.isRequired
};

export default translate(['armor'])(Armor);
