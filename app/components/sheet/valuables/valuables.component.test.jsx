import React from 'react';
import Valuables from './valuables.component';
import {mount} from 'enzyme';
import renderer from 'react-test-renderer';

/* eslint-disable react/jsx-no-bind */
/* eslint-disable react/jsx-handler-names */
describe('Sheet valuables tests', () => {
    test('Should match previous Valuables snapshot', () => {
        const Rendered = renderer.create(
            <Valuables
                onBronzeChange={() => null}
                onCopperChange={() => null}
                onGoldChange={() => null}
                onIronChange={() => null}
                onPlatinumChange={() => null}
                onReset={() => null}
                onSilverChange={() => null}
                valuables={{
                    bronze: true,
                    iron: true,
                    copper: true,
                    silver: true,
                    gold: true,
                    platinum: true,
                    money: 0
                }}
            />
        );
        expect(Rendered).toMatchSnapshot();
    });

    test.skip('Should check valuables component integration', () => {
        const mockResetClick = jest.fn();
        const fields = [
            {text: 'BRONZE', fun: jest.fn(), value: true},
            {text: 'IRON', fun: jest.fn(), value: true},
            {text: 'COPPER', fun: jest.fn(), value: true},
            {text: 'SILVER', fun: jest.fn(), value: true},
            {text: 'GOLD', fun: jest.fn(), value: true},
            {text: 'PLATINUM', fun: jest.fn(), value: true}
        ];

        const component = mount(
            <Valuables
                onBronzeChange={fields[0].fun}
                onCopperChange={fields[2].fun}
                onGoldChange={fields[4].fun}
                onIronChange={fields[1].fun}
                onPlatinumChange={fields[5].fun}
                onReset={mockResetClick}
                onSilverChange={fields[3].fun}
                valuables={{
                    bronze: true,
                    iron: true,
                    copper: true,
                    silver: true,
                    gold: true,
                    platinum: true,
                    money: 0
                }}
            />
        );
        component
            .find('.fa-eraser')
            .last()
            .simulate('click');
        expect(mockResetClick).toBeCalled();

        component.find('input').forEach((input, i) => {
            if (input.props().type === 'checkbox') {
                input.simulate('change', {
                    target: {name: 'change', checked: fields[i].value}
                });
            } else {
                input.simulate('change', {
                    target: {name: 'change', value: fields[i].value}
                });
                input.simulate('blur', {target: {name: 'blur'}});
            }
        });
        fields.forEach((field, i) => {
            expect(field.fun).toBeCalled();
            expect(field.fun.mock.calls[0][0]).toBe(true);
        });
    });
});
