import {Field, Flex, Input} from '../basic';

import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

const ValuableInput = ({onChange, type, value, t}) => (
    <Flex>
        <Field
            align="left"
            fullWidth
        >
            {t(type.toUpperCase())}
        </Field>
        <Input
            border="full"
            onChange={onChange}
            type="number"
            value={value}
        />
    </Flex>
);

ValuableInput.propTypes = {
    onChange: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired,
    type: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired
};

export default translate(['valuables'])(ValuableInput);
