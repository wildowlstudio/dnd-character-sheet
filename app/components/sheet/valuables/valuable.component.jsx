import {Field, Flex} from '../basic';

import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

const Valuables = ({type, valuable, t}) => (
    <Flex>
        <Field
            align="left"
            fullWidth
        >
            {t(type.toUpperCase())}
        </Field>
        <Field
            align="left"
            fullWidth
        >
            {valuable || ''}
        </Field>
    </Flex>
);

Valuables.propTypes = {
    t: PropTypes.func.isRequired,
    type: PropTypes.string.isRequired,
    valuable: PropTypes.number.isRequired
};

export default translate(['valuables'])(Valuables);
