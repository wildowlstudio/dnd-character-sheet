import Button from '../../button/button.component';
import Modal from '../../modal/index';
import PropTypes from 'prop-types';
import React from 'react';
import ValuableInput from './valuableInput.component';
import {translate} from 'react-i18next';

class ValuablesModal extends React.PureComponent {
    constructor(props) {
        super(props);

        this.getMoney = this.getMoney.bind(this);
        this.handleBronzeChange = this.handleBronzeChange.bind(this);
        this.handleCopperChange = this.handleCopperChange.bind(this);
        this.handleGoldChange = this.handleGoldChange.bind(this);
        this.handleIronChange = this.handleIronChange.bind(this);
        this.handleMoneyAdd = this.handleMoneyAdd.bind(this);
        this.handleMoneySubtract = this.handleMoneySubtract.bind(this);
        this.handlePlatinumChange = this.handlePlatinumChange.bind(this);
        this.handleSilverChange = this.handleSilverChange.bind(this);
    }

    getMoney() {
        return (
            (this.bronze || 0) * 0.0001 +
            (this.iron || 0) * 0.001 +
            (this.copper || 0) * 0.01 +
            (this.silver || 0) * 0.1 +
            (this.gold || 0) +
            (this.platinum || 0) * 10
        );
    }

    handleBronzeChange(value) {
        this.bronze = value;
    }

    handleIronChange(value) {
        this.iron = value;
    }

    handleCopperChange(value) {
        this.copper = value;
    }

    handleSilverChange(value) {
        this.silver = value;
    }

    handleGoldChange(value) {
        this.gold = value;
    }

    handlePlatinumChange(value) {
        this.platinum = value;
    }

    handleMoneySubtract() {
        this.props.onMoneySubtract(this.getMoney());
    }
    handleMoneyAdd() {
        this.props.onMoneyAdd(this.getMoney());
    }

    render() {
        const {open, onClose, t} = this.props;
        return (
            <Modal
                footer={
                    <React.Fragment>
                        <Button onClick={this.handleMoneySubtract}>
                            {t('SUBTRACT')}
                        </Button>
                        <Button onClick={this.handleMoneyAdd}>{t('ADD')}</Button>
                    </React.Fragment>
                }
                header={t('EDITION')}
                onClose={onClose}
                open={open}
            >
                <ValuableInput
                    onChange={this.handleBronzeChange}
                    type="bronze"
                    value={0}
                />
                <ValuableInput
                    onChange={this.handleIronChange}
                    type="iron"
                    value={0}
                />
                <ValuableInput
                    onChange={this.handleCopperChange}
                    type="copper"
                    value={0}
                />
                <ValuableInput
                    onChange={this.handleSilverChange}
                    type="silver"
                    value={0}
                />
                <ValuableInput
                    onChange={this.handleGoldChange}
                    type="gold"
                    value={0}
                />
                <ValuableInput
                    onChange={this.handlePlatinumChange}
                    type="platinum"
                    value={0}
                />
            </Modal>
        );
    }
}

ValuablesModal.propTypes = {
    onClose: PropTypes.func.isRequired,
    onMoneyAdd: PropTypes.func.isRequired,
    onMoneySubtract: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    t: PropTypes.func.isRequired
};

export default translate(['common'])(ValuablesModal);
