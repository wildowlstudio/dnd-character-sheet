import {Field} from '../basic';
import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

const ValuablesHeader = ({t}) => {
    return (
        <Field
            inverted
            noWidth
        >
            {t('VALUABLES')}
        </Field>
    );
};

ValuablesHeader.propTypes = {
    t: PropTypes.func.isRequired
};

export default translate(['valuables'])(ValuablesHeader);
