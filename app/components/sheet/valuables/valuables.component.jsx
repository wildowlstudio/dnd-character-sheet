import {Checkbox, Element} from '../basic';

import Header from './header.component';
import PropTypes from 'prop-types';
import React from 'react';
import Valuable from './valuable.component';
import ValuablesModal from './valuablesModal.component';
import styled from 'styled-components';
import {translate} from 'react-i18next';

const StyledElement = styled(Element)`
    width: 367px;
`;

class Valuables extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false
        };

        this.handleClose = this.handleClose.bind(this);
        this.handleModalOpen = this.handleModalOpen.bind(this);
        this.handleMoneyAdd = this.handleMoneyAdd.bind(this);
        this.handleMoneySubtract = this.handleMoneySubtract.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (
            nextProps.valuables !== this.props.valuables ||
            nextState.open !== this.state.open
        );
    }

    handleModalOpen() {
        this.setState({open: true});
    }

    handleClose() {
        this.setState({open: false});
    }

    handleMoneySubtract(value) {
        if (value) {
            this.props.onMoneyChange(-value);
        }
        this.handleClose();
    }

    handleMoneyAdd(value) {
        if (value) {
            this.props.onMoneyChange(value);
        }
        this.handleClose();
    }

    render() {
        const {
            onBronzeChange,
            onCopperChange,
            onGoldChange,
            onIronChange,
            onPlatinumChange,
            onReset,
            onSilverChange,
            valuables,
            t
        } = this.props;
        const denomination = getDenominations(valuables);
        return (
            <StyledElement
                dropdown={
                    <React.Fragment>
                        <li>
                            <Checkbox
                                label={t('BRONZE')}
                                onChange={onBronzeChange}
                                value={valuables.bronze}
                            />
                        </li>
                        <li>
                            <Checkbox
                                label={t('IRON')}
                                onChange={onIronChange}
                                value={valuables.iron}
                            />
                        </li>
                        <li>
                            <Checkbox
                                label={t('COPPER')}
                                onChange={onCopperChange}
                                value={valuables.copper}
                            />
                        </li>
                        <li>
                            <Checkbox
                                label={t('SILVER')}
                                onChange={onSilverChange}
                                value={valuables.silver}
                            />
                        </li>
                        <li>
                            <Checkbox
                                label={t('GOLD')}
                                onChange={onGoldChange}
                                value={valuables.gold}
                            />
                        </li>
                        <li>
                            <Checkbox
                                label={t('PLATINUM')}
                                onChange={onPlatinumChange}
                                value={valuables.platinum}
                            />
                        </li>
                    </React.Fragment>
                }
                onClick={this.handleModalOpen}
                onReset={onReset}
                width={234}
            >
                <Header />
                {valuables.bronze ? (
                    <Valuable
                        type="bronze"
                        valuable={denomination.bronze}
                    />
                ) : null}
                {valuables.iron ? (
                    <Valuable
                        type="iron"
                        valuable={denomination.iron}
                    />
                ) : null}
                {valuables.copper ? (
                    <Valuable
                        type="copper"
                        valuable={denomination.copper}
                    />
                ) : null}
                {valuables.silver ? (
                    <Valuable
                        type="silver"
                        valuable={denomination.silver}
                    />
                ) : null}
                {valuables.gold ? (
                    <Valuable
                        type="gold"
                        valuable={denomination.gold}
                    />
                ) : null}
                {valuables.platinum ? (
                    <Valuable
                        type="platinum"
                        valuable={denomination.platinum}
                    />
                ) : null}
                <ValuablesModal
                    onClose={this.handleClose}
                    onMoneyAdd={this.handleMoneyAdd}
                    onMoneySubtract={this.handleMoneySubtract}
                    open={this.state.open}
                />
            </StyledElement>
        );
    }
}

function getDenominations(valuables) {
    let split = valuables.money.toString().match(/(-)?(\d*)(\d)(\.(\d)(\d)?(\d)?(\d)?)?/);
    const negative = split[1] ? -1 : 1;
    const denominations = [
        {name: 'bronze', value: +split[8] * negative || 0},
        {name: 'iron', value: +split[7] * negative || 0},
        {name: 'copper', value: +split[6] * negative || 0},
        {name: 'silver', value: +split[5] * negative || 0},
        {name: 'gold', value: +split[3] * negative || 0},
        {name: 'platinum', value: +split[2] * negative || 0}
    ];

    const output = {};

    denominations.forEach((denomination, i) => {
        if (valuables[denomination.name]) {
            output[denomination.name] = denomination.value;
            for (let j = i - 1, multiplier = 0.1; j >= 0; j--, multiplier *= 0.1) {
                if (valuables[denominations[j].name]) {
                    break;
                } else {
                    output[denomination.name] += multiplier * denominations[j].value;
                }
            }
        }
    });

    let valueToAdd = 0;
    for (
        let i = denominations.length - 1, multiplier = 10;
        i >= 0;
        i--, multiplier *= 10
    ) {
        if (valuables[denominations[i].name]) {
            output[denominations[i].name] += valueToAdd;
            break;
        } else {
            valueToAdd += multiplier * denominations[i].value;
        }
    }

    return output;
}

Valuables.propTypes = {
    onBronzeChange: PropTypes.func.isRequired,
    onCopperChange: PropTypes.func.isRequired,
    onGoldChange: PropTypes.func.isRequired,
    onIronChange: PropTypes.func.isRequired,
    onMoneyChange: PropTypes.func.isRequired,
    onPlatinumChange: PropTypes.func.isRequired,
    onReset: PropTypes.func.isRequired,
    onSilverChange: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired,
    valuables: PropTypes.shape({
        bronze: PropTypes.bool,
        iron: PropTypes.bool,
        copper: PropTypes.bool,
        silver: PropTypes.bool,
        gold: PropTypes.bool,
        platinum: PropTypes.bool,
        money: PropTypes.number
    }).isRequired
};

export default translate(['valuables'])(Valuables);
