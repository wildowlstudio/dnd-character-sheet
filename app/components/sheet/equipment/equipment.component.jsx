import {Checkbox, Element} from '../basic';

import Count from './count.component';
import Header from './header.component';
import Item from './item.component';
import PropTypes from 'prop-types';
import React from 'react';
import media from '../../../utils/style.utils';
import styled from 'styled-components';
import {translate} from 'react-i18next';

const StyledElement = styled(Element)`
    width: 407px;

    ${media.oneRow`
        width: 608px;
    `};
`;

const Equipment = ({
    className,
    items,
    onItemAdd,
    onItemCapacityChange,
    onItemDelete,
    onItemMove,
    onItemNameChange,
    onItemsDelete,
    onItemValueChange,
    onItemWeightChange,
    onOptionCapacityChange,
    onOptionSumChange,
    onOptionValueChange,
    onOptionWeightChange,
    onReset,
    onSort,
    options,
    t
}) => (
    <StyledElement
        className={className}
        dropdown={
            <React.Fragment>
                <li>
                    <Checkbox
                        label={t('WEIGHT')}
                        onChange={onOptionWeightChange}
                        value={options.weight}
                    />
                </li>
                <li>
                    <Checkbox
                        label={t('CAPACITY')}
                        onChange={onOptionCapacityChange}
                        value={options.capacity}
                    />
                </li>
                <li>
                    <Checkbox
                        label={t('VALUE')}
                        onChange={onOptionValueChange}
                        value={options.value}
                    />
                </li>
                <li>
                    <Checkbox
                        label={t('SUM')}
                        onChange={onOptionSumChange}
                        value={options.sum}
                    />
                </li>
            </React.Fragment>
        }
        onAdd={onItemAdd}
        onRemove={onItemsDelete}
        onReset={onReset}
    >
        <Header
            onSort={onSort}
            options={options}
        />
        {items
            .concat({id: 'new'})
            .map((item) => (
                <Item
                    item={item}
                    key={item.id}
                    onCapacityChange={onItemCapacityChange}
                    onDelete={onItemDelete}
                    onMove={onItemMove}
                    onNameChange={onItemNameChange}
                    onValueChange={onItemValueChange}
                    onWeightChange={onItemWeightChange}
                    options={options}
                />
            ))}
        {options.sum && <Count
            items={items}
            options={options}
                        />}
    </StyledElement>
);

Equipment.propTypes = {
    className: PropTypes.string,
    items: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            name: PropTypes.string,
            weight: PropTypes.number,
            capacity: PropTypes.number,
            value: PropTypes.number
        })
    ).isRequired,
    onItemAdd: PropTypes.func.isRequired,
    onItemCapacityChange: PropTypes.func.isRequired,
    onItemDelete: PropTypes.func.isRequired,
    onItemMove: PropTypes.func.isRequired,
    onItemNameChange: PropTypes.func.isRequired,
    onItemsDelete: PropTypes.func.isRequired,
    onItemValueChange: PropTypes.func.isRequired,
    onItemWeightChange: PropTypes.func.isRequired,
    onOptionCapacityChange: PropTypes.func.isRequired,
    onOptionSumChange: PropTypes.func.isRequired,
    onOptionValueChange: PropTypes.func.isRequired,
    onOptionWeightChange: PropTypes.func.isRequired,
    onReset: PropTypes.func.isRequired,
    onSort: PropTypes.func.isRequired,
    options: PropTypes.shape({
        sum: PropTypes.bool.isRequired,
        weight: PropTypes.bool.isRequired,
        capacity: PropTypes.bool.isRequired,
        value: PropTypes.bool.isRequired
    }).isRequired,
    t: PropTypes.func.isRequired
};

export default translate(['equipment'])(Equipment);
