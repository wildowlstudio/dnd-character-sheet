import {Field, Flex} from '../basic';

import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

const StyledHeader = Field.extend`
    width: auto;
`;

class EquipmentHeader extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {};

        this.handleSort = this.handleSort.bind(this);
        this.handleSortName = this.handleSortName.bind(this);
        this.handleSortWeight = this.handleSortWeight.bind(this);
        this.handleSortCapacity = this.handleSortCapacity.bind(this);
        this.handleSortValue = this.handleSortValue.bind(this);
    }

    handleSort(field) {
        if (this.orderBy === field) {
            this.desc = !this.desc;
        } else {
            this.desc = false;
        }
        this.orderBy = field;
        this.props.onSort(field, this.desc);
    }

    handleSortName() {
        this.handleSort('name');
    }

    handleSortWeight() {
        this.handleSort('weight');
    }

    handleSortCapacity() {
        this.handleSort('capacity');
    }

    handleSortValue() {
        this.handleSort('value');
    }

    render() {
        const {options, t} = this.props;
        return (
            <Flex direction="column">
                <StyledHeader inverted>{t('EQUIPMENT')}</StyledHeader>
                <Flex>
                    <Field
                        grow
                        onClick={this.handleSortName}
                    >
                        {t('NAME')}
                    </Field>
                    {options.weight && (
                        <Field
                            multiply={2}
                            onClick={this.handleSortWeight}
                        >
                            {t('WEIGHT')}
                        </Field>
                    )}
                    {options.capacity && (
                        <Field
                            multiply={2}
                            onClick={this.handleSortCapacity}
                        >
                            {t('CAPACITY')}
                        </Field>
                    )}
                    {options.value && (
                        <Field
                            multiply={2}
                            onClick={this.handleSortValue}
                        >
                            {t('VALUE')}
                        </Field>
                    )}
                </Flex>
            </Flex>
        );
    }
}

EquipmentHeader.propTypes = {
    onSort: PropTypes.func.isRequired,
    options: PropTypes.shape({
        weight: PropTypes.bool.isRequired,
        capacity: PropTypes.bool.isRequired,
        value: PropTypes.bool.isRequired
    }).isRequired,
    t: PropTypes.func.isRequired
};

export default translate(['equipment'])(EquipmentHeader);
