import {Flex, Input, TrashIcon} from '../basic';
import {handleDragOver, isDroppedBelow} from '../../../utils/dnd.util';

import Count from './count.component';
import PropTypes from 'prop-types';
import React from 'react';
import {findDOMNode} from 'react-dom';
import styled from 'styled-components';

const Icon = styled.button`
    width: 20px;
    border: none;
    cursor: pointer;
    background-color: transparent;
`;

const Children = styled.div`
    margin-left: 30px;
`;

const StyledInput = styled(Input)`
    margin-left: ${({theme}) => theme.itemHorizontalSpacing}px;
`;

class Item extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            open: true
        };

        this.handleCapacityChange = this.handleCapacityChange.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleDragStart = this.handleDragStart.bind(this);
        this.handleDrop = this.handleDrop.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleOpen = this.handleOpen.bind(this);
        this.handleValueChange = this.handleValueChange.bind(this);
        this.handleWeightChange = this.handleWeightChange.bind(this);
    }

    componentDidMount() {
        if (window.catchFocus) {
            const elements = findDOMNode(this).getElementsByTagName('input'); // eslint-disable-line react/no-find-dom-node
            switch (window.catchFocus) {
                case 'name':
                    elements[1].focus();
                    break;
                case 'weight':
                    elements[2].focus();
                    break;
                case 'capacity':
                    elements[3].focus();
                    break;
            }
            window.catchFocus = null;
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (
            nextProps.item !== this.props.item ||
            nextProps.options !== this.props.options ||
            nextProps.childOf !== this.props.childOf ||
            nextState.open !== this.state.open
        );
    }

    getIndicatorClass() {
        if (this.state.open) {
            return 'fa fa-caret-down';
        } else {
            return 'fa fa-caret-right';
        }
    }

    handleCapacityChange(value) {
        const {item, childOf} = this.props;
        if (item.id === 'new') {
            window.catchFocus = 'capacity';
        }
        this.props.onCapacityChange(item.id, value, childOf);
    }

    handleNameChange(value) {
        const {item, childOf} = this.props;
        if (item.id === 'new') {
            window.catchFocus = 'name';
        }
        this.props.onNameChange(item.id, value, childOf);
    }

    handleValueChange(value, event) {
        const {item, childOf} = this.props;
        if (item.id === 'new') {
            findDOMNode(this) // eslint-disable-line react/no-find-dom-node
                .getElementsByTagName('input')[0]
                .focus();
        }
        this.props.onValueChange(this.props.item.id, value, childOf);
    }

    handleWeightChange(value) {
        const {item, childOf} = this.props;
        if (item.id === 'new') {
            window.catchFocus = 'weight';
        }
        this.props.onWeightChange(item.id, value, childOf);
    }

    handleOpen() {
        this.setState((state) => ({open: !state.open}));
    }

    handleDragStart(event) {
        event.dataTransfer.setData('id', this.props.item.id);
        event.dataTransfer.setData('parentId', this.props.childOf);
    }

    handleDrop(event) {
        event.preventDefault();
        const id = +event.dataTransfer.getData('id');
        const {childOf, item} = this.props;
        if (id !== item.id) {
            let parentId = event.dataTransfer.getData('parentId');
            if (parentId === 'undefined') {
                parentId = undefined;
            } else {
                parentId = +parentId;
            }
            const below = isDroppedBelow(event);
            this.props.onMove(id, parentId, below, item.id, childOf);
        }
    }

    handleDelete() {
        const {childOf, item} = this.props;
        this.props.onDelete(item.id, childOf);
    }

    render() {
        const {childOf, item, options} = this.props;
        const {open} = this.state;
        return (
            <div>
                <Flex
                    draggable={item.id !== 'new'}
                    onDragOver={handleDragOver}
                    onDragStart={this.handleDragStart}
                    onDrop={this.handleDrop}
                    trash
                >
                    {item.capacity && childOf === undefined ? (
                        <Icon
                            aria-label="collapse"
                            className={this.getIndicatorClass()}
                            onClick={this.handleOpen}
                        />
                    ) : null}
                    <Input
                        grow
                        onChange={this.handleNameChange}
                        value={item.name}
                    />
                    {options.weight && (
                        <StyledInput
                            field
                            multiply={2}
                            onChange={this.handleWeightChange}
                            type="number"
                            value={item.weight}
                            widthFudgeFactor={-6}
                        />
                    )}
                    {options.capacity && (
                        <StyledInput
                            field
                            multiply={2}
                            onChange={this.handleCapacityChange}
                            type="number"
                            value={item.capacity}
                            widthFudgeFactor={-6}
                        />
                    )}
                    {options.value && (
                        <StyledInput
                            field
                            multiply={2}
                            onChange={this.handleValueChange}
                            type="number"
                            value={item.value}
                            widthFudgeFactor={-6}
                        />
                    )}
                    {item.id !== 'new' ? (
                        <TrashIcon
                            aria-label="delete item"
                            onClick={this.handleDelete}
                        />
                    ) : null}
                </Flex>
                {item.capacity && open && childOf === undefined ? (
                    <Children>
                        {(item.items || [])
                            .concat({id: 'new', childOf: item.id})
                            .map((child) => (
                                <Item
                                    childOf={item.id}
                                    item={child}
                                    key={child.id}
                                    onCapacityChange={this.props.onCapacityChange}
                                    onDelete={this.props.onDelete}
                                    onMove={this.props.onMove}
                                    onNameChange={this.props.onNameChange}
                                    onValueChange={this.props.onValueChange}
                                    onWeightChange={this.props.onWeightChange}
                                    options={options}
                                />
                            ))}
                        {options.sum && (
                            <Count
                                capacity={item.capacity}
                                items={item.items}
                                options={options}
                                weight={item.weight}
                            />
                        )}
                    </Children>
                ) : null}
            </div>
        );
    }
}

Item.propTypes = {
    childOf: PropTypes.number,
    item: PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string,
        weight: PropTypes.number,
        capacity: PropTypes.number,
        value: PropTypes.number
    }).isRequired,
    onCapacityChange: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    onMove: PropTypes.func.isRequired,
    onNameChange: PropTypes.func.isRequired,
    onValueChange: PropTypes.func.isRequired,
    onWeightChange: PropTypes.func.isRequired,
    options: PropTypes.shape({
        weight: PropTypes.bool.isRequired,
        capacity: PropTypes.bool.isRequired,
        value: PropTypes.bool.isRequired
    }).isRequired
};

export default Item;
