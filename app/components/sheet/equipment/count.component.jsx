import {Field, Flex} from '../basic';

import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

const StyledField = Field.extend`
    border-left: none;
    border-right: none;
    font-weight: bold;
    margin-left: ${({theme}) => theme.itemHorizontalSpacing}px;
`;

const NameField = StyledField.extend`
    margin-left: 0;
`;

const Item = ({capacity, items, options, t, weight}) => {
    const values = items.reduce(
        (acc, item) =>
            (item.items || []).reduce(
                (acc, item) => ({
                    weight: acc.weight + (+item.weight || 0),
                    capacity: acc.capacity + (+item.capacity || 0),
                    value: acc.value + (+item.value || 0)
                }),
                {
                    weight: acc.weight + (+item.weight || 0),
                    capacity: acc.capacity + (+item.capacity || 0),
                    value: acc.value + (+item.value || 0)
                }
            ),
        {
            weight: 0,
            capacity: 0,
            value: 0
        }
    );
    return (
        <Flex>
            <NameField
                align="left"
                grow
            >
                {t('TOTAL')}
            </NameField>
            {options.weight && (
                <StyledField
                    multiply={2}
                    valid={capacity === undefined ? undefined : values.weight <= capacity}
                    widthFudgeFactor={-1}
                >
                    {renderWeight(values, capacity, weight)}
                </StyledField>
            )}
            {options.capacity && (
                <StyledField
                    multiply={2}
                    widthFudgeFactor={-1}
                >
                    {values.capacity}
                </StyledField>
            )}
            {options.value && (
                <StyledField
                    multiply={2}
                    widthFudgeFactor={-1}
                >
                    {values.value}
                </StyledField>
            )}
        </Flex>
    );
};

function renderWeight(values, capacity, weight) {
    if (capacity) {
        let output = `${values.weight} / ${capacity}`;
        if (weight) {
            output += ` + ${weight}`;
        }
        return output;
    }
    return values.weight;
}

Item.propTypes = {
    capacity: PropTypes.number,
    items: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            name: PropTypes.string,
            weight: PropTypes.number,
            capacity: PropTypes.number,
            value: PropTypes.number
        })
    ),
    options: PropTypes.shape({
        weight: PropTypes.bool.isRequired,
        capacity: PropTypes.bool.isRequired,
        value: PropTypes.bool.isRequired
    }).isRequired,
    t: PropTypes.func.isRequired,
    weight: PropTypes.number
};

Item.defaultProps = {
    items: []
};

export default translate(['common'])(Item);
