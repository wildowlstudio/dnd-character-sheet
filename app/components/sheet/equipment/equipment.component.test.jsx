import Equipment from './equipment.component';
import Header from './header.component';
import React from 'react';
import {mount} from 'enzyme';
import renderer from 'react-test-renderer';

/* eslint-disable react/jsx-no-bind */
/* eslint-disable react/jsx-handler-names */
describe('Sheet equipment tests', () => {
    test('Should match previous Equipment snapshot', () => {
        const Rendered = renderer.create(
            <Equipment
                items={[]}
                onItemAdd={() => null}
                onItemCapacityChange={() => null}
                onItemDelete={() => null}
                onItemMove={() => null}
                onItemNameChange={() => null}
                onItemsDelete={() => null}
                onItemValueChange={() => null}
                onItemWeightChange={() => null}
                onOptionCapacityChange={() => null}
                onOptionSumChange={() => null}
                onOptionValueChange={() => null}
                onOptionWeightChange={() => null}
                onReset={() => null}
                onSort={() => null}
                options={{}}
            />
        );
        expect(Rendered).toMatchSnapshot();
    });

    test('Should check equipment component integration', () => {
        const mockResetClick = jest.fn();
        const mockAddClick = jest.fn();
        const mockRemoveClick = jest.fn();
        const mockSortClick = jest.fn();
        const mockDeleteClick = jest.fn();

        const fields = [
            {text: 'NAME', fun: jest.fn(), value: 'NAME', param: 1},
            {text: 'WEIGHT', fun: jest.fn(), value: Math.random(), param: 1},
            {text: 'CAPACITY', fun: jest.fn(), value: Math.random(), param: 1},
            {text: 'VALUE', fun: jest.fn(), value: Math.random(), param: 1}
        ];
        const component = mount(
            <Equipment
                items={[{id: 0}]}
                onItemAdd={mockAddClick}
                onItemCapacityChange={fields[2].fun}
                onItemDelete={mockDeleteClick}
                onItemMove={() => null}
                onItemNameChange={fields[0].fun}
                onItemsDelete={mockRemoveClick}
                onItemValueChange={fields[3].fun}
                onItemWeightChange={fields[1].fun}
                onOptionCapacityChange={() => null}
                onOptionSumChange={() => null}
                onOptionValueChange={() => null}
                onOptionWeightChange={() => null}
                onReset={mockResetClick}
                onSort={mockSortClick}
                options={{
                    weight: true,
                    capacity: true,
                    value: true
                }}
            />
        );
        component
            .find('.fa-eraser')
            .last()
            .simulate('click');
        expect(mockResetClick).toBeCalled();

        component
            .find('.fa-plus')
            .last()
            .simulate('click');
        expect(mockAddClick).toBeCalled();

        component
            .find('.fa-minus')
            .last()
            .simulate('click');
        expect(mockRemoveClick).toBeCalled();

        component
            .find('.fa-trash')
            .last()
            .simulate('click');
        expect(mockDeleteClick).toBeCalled();

        component
            .find(Header)
            .find('div')
            .forEach((div, i) => div.simulate('click'));

        expect(mockSortClick).toHaveBeenCalledTimes(4);

        component.find('input').forEach((input, i) => {
            if (i < fields.length) {
                if (input.props().type === 'checkbox') {
                    input.simulate('change', {
                        target: {name: 'change', checked: fields[i].value}
                    });
                } else {
                    input.simulate('change', {
                        target: {name: 'change', value: fields[i].value}
                    });
                    input.simulate('blur', {target: {name: 'blur'}});
                }
            }
        });
        fields.forEach((field, i) => {
            expect(field.fun).toBeCalled();
            expect(field.fun.mock.calls[0][field.param]).toBe(field.value);
        });
    });
});
