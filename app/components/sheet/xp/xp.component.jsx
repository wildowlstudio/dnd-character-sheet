import {Element, Field, Flex, Input} from '../basic';

import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';
import {translate} from 'react-i18next';

const StyledElement = styled(Element)`
    /* position: absolute;
    top: 249px;
    left: 587px; */
    width: 272px;
    margin-top: 23px;
`;

const Xp = ({onXpChange, onXpReset, xp, t}) => (
    <StyledElement onReset={onXpReset}>
        <Flex
            align="center"
            spaced
        >
            <Field
                inverted
                multiply={3}
            >
                {t('XP')}
            </Field>
            <Input
                border="full"
                field
                multiply={3}
                onChange={onXpChange}
                type="number"
                value={xp}
            />
        </Flex>
    </StyledElement>
);

Xp.propTypes = {
    onXpChange: PropTypes.func.isRequired,
    onXpReset: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired,
    xp: PropTypes.string
};

export default translate(['common'])(Xp);
