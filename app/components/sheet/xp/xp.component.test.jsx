import React from 'react';
import Xp from './xp.component';
import {mount} from 'enzyme';
import renderer from 'react-test-renderer';

/* eslint-disable react/jsx-no-bind */
/* eslint-disable react/jsx-handler-names */
describe('Sheet xp tests', () => {
    test('Should match previous Xp snapshot', () => {
        const Rendered = renderer.create(
            <Xp
                onXpChange={() => null}
                onXpReset={() => null}
                xp=""
            />
        );
        expect(Rendered).toMatchSnapshot();
    });

    test('Should check xp component integration', () => {
        const mockResetClick = jest.fn();
        const fields = [{text: 'XP', fun: jest.fn(), value: Math.random()}];
        const component = mount(
            <Xp
                onXpChange={fields[0].fun}
                onXpReset={mockResetClick}
                xp=""
            />
        );
        component
            .find('.fa-eraser')
            .last()
            .simulate('click');
        expect(mockResetClick).toBeCalled();

        component.find('input').forEach((input, i) => {
            input.simulate('change', {
                target: {name: 'change', value: fields[i].value}
            });
            input.simulate('blur', {target: {name: 'change'}});
            expect(fields[i].fun).toBeCalled();
            expect(fields[i].fun.mock.calls[0][0]).toBe(fields[i].value);
        });
    });
});
