import {Field, Flex, Input, Operator} from '../basic';

import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

const Ac = ({
    ac,
    onAcArmorChange,
    onAcDexChange,
    onAcMiscChange,
    onAcShieldChange,
    onAcSizeChange,
    t
}) => (
    <Flex
        align="center"
        spaced
    >
        <Field inverted>{t('AC')}</Field>
        <Field>
            {10 +
                (+ac.armor || 0) +
                (+ac.shield || 0) +
                (+ac.dex || 0) +
                (+ac.size || 0) +
                (+ac.misc || 0)}
        </Field>
        <Operator>{'='}</Operator>
        <Field>{'10'}</Field>
        <Operator>{'+'}</Operator>
        <Input
            border="full"
            field
            onChange={onAcArmorChange}
            type="number"
            value={ac.armor}
        />
        <Operator>{'+'}</Operator>
        <Input
            border="full"
            field
            onChange={onAcShieldChange}
            type="number"
            value={ac.shield}
        />
        <Operator>{'+'}</Operator>
        <Input
            border="full"
            field
            onChange={onAcDexChange}
            type="number"
            value={ac.dex}
        />
        <Operator>{'+'}</Operator>
        <Input
            border="full"
            field
            onChange={onAcSizeChange}
            type="number"
            value={ac.size}
        />
        <Operator>{'+'}</Operator>
        <Input
            border="full"
            field
            onChange={onAcMiscChange}
            type="number"
            value={ac.misc}
        />
    </Flex>
);

Ac.propTypes = {
    ac: PropTypes.shape({
        armor: PropTypes.number,
        shield: PropTypes.number,
        dex: PropTypes.number,
        size: PropTypes.number,
        misc: PropTypes.number
    }).isRequired,
    onAcArmorChange: PropTypes.func.isRequired,
    onAcDexChange: PropTypes.func.isRequired,
    onAcMiscChange: PropTypes.func.isRequired,
    onAcShieldChange: PropTypes.func.isRequired,
    onAcSizeChange: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired
};

export default translate(['header'])(Ac);
