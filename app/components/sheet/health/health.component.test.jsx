import Health from './health.component';
import React from 'react';
import {mount} from 'enzyme';
import renderer from 'react-test-renderer';

/* eslint-disable react/jsx-no-bind */
/* eslint-disable react/jsx-handler-names */
describe('Sheet health tests', () => {
    test('Should match previous Health snapshot', () => {
        const Rendered = renderer.create(
            <Health
                ac={{}}
                hp={{}}
                onAcArmorChange={() => null}
                onAcDexChange={() => null}
                onAcMiscChange={() => null}
                onAcShieldChange={() => null}
                onAcSizeChange={() => null}
                onCurrentHpChange={() => null}
                onHpChange={() => null}
                onSubdualDamageChange={() => null}
            />
        );
        expect(Rendered).toMatchSnapshot();
    });

    test('Should check health component integration', () => {
        const mockResetClick = jest.fn();
        const fields = [
            {text: 'HP', fun: jest.fn(), value: Math.random()},
            {text: 'CURRENT_HP', fun: jest.fn(), value: 'CURRENT_HP'},
            {text: 'SUBDUAL_DAMAGE', fun: jest.fn(), value: 'SUBDUAL_DAMAGE'},
            {text: 'ARMOR', fun: jest.fn(), value: Math.random()},
            {text: 'SHIELD', fun: jest.fn(), value: Math.random()},
            {text: 'DEX', fun: jest.fn(), value: Math.random()},
            {text: 'SIZE', fun: jest.fn(), value: Math.random()},
            {text: 'MISC', fun: jest.fn(), value: Math.random()}
        ];
        const component = mount(
            <Health
                ac={{}}
                hp={{}}
                onAcArmorChange={fields[3].fun}
                onAcDexChange={fields[5].fun}
                onAcMiscChange={fields[7].fun}
                onAcShieldChange={fields[4].fun}
                onAcSizeChange={fields[6].fun}
                onCurrentHpChange={fields[1].fun}
                onHealthReset={mockResetClick}
                onHpChange={fields[0].fun}
                onSubdualDamageChange={fields[2].fun}
            />
        );
        component
            .find('.fa-eraser')
            .last()
            .simulate('click');
        expect(mockResetClick).toBeCalled();

        component.find('input').forEach((input, i) => {
            input.simulate('change', {
                target: {name: 'change', value: fields[i].value}
            });
            input.simulate('blur', {target: {name: 'change'}});
            expect(fields[i].fun).toBeCalled();
            expect(fields[i].fun.mock.calls[0][0]).toBe(fields[i].value);
        });
    });
});
