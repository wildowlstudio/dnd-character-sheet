import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';
import {Flex, Label} from '../basic';

const HpHeader = ({t}) => (
    <Flex
        align="flex-end"
        spaced
    >
        <Label field />
        <Label field>{t('common:TOTAL')}</Label>
        <Label
            field
            multiply={3}
            operators={3}
        >
            {t('WOUNDS/CURRENT_HP')}
        </Label>
        <Label
            field
            multiply={3}
            operators={3}
        >
            {t('SUBDUAL_DAMAGE')}
        </Label>
    </Flex>
);

HpHeader.propTypes = {
    t: PropTypes.func.isRequired
};

export default translate(['health', 'common'])(HpHeader);
