import {Element, Flex} from '../basic';

import Ac from './ac.component';
import AcHeader from './acHeader.component';
import Hp from './hp.component';
import HpHeader from './hpHeader.component';
import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';

const StyledElement = styled(Element)`
    width: 474px;
    margin-top: 15px;

    /* position: absolute;
    top: 134px;
    left: 256px; */
`;

const Health = ({
    ac,
    hp,
    onAcArmorChange,
    onAcDexChange,
    onAcMiscChange,
    onAcShieldChange,
    onAcSizeChange,
    onCurrentHpChange,
    onHealthReset,
    onHpChange,
    onSubdualDamageChange
}) => (
    <StyledElement onReset={onHealthReset}>
        <Flex
            direction="column"
            spaced
        >
            <HpHeader />
            <Hp
                hp={hp}
                onCurrentHpChange={onCurrentHpChange}
                onHpChange={onHpChange}
                onSubdualDamageChange={onSubdualDamageChange}
            />
            <Ac
                ac={ac}
                onAcArmorChange={onAcArmorChange}
                onAcDexChange={onAcDexChange}
                onAcMiscChange={onAcMiscChange}
                onAcShieldChange={onAcShieldChange}
                onAcSizeChange={onAcSizeChange}
            />
            <AcHeader />
        </Flex>
    </StyledElement>
);

Health.propTypes = {
    ac: PropTypes.shape({
        armor: PropTypes.number,
        shield: PropTypes.number,
        dex: PropTypes.number,
        size: PropTypes.number,
        misc: PropTypes.number
    }).isRequired,
    hp: PropTypes.shape({
        hp: PropTypes.number,
        current: PropTypes.number,
        subdualDamage: PropTypes.string
    }).isRequired,
    onAcArmorChange: PropTypes.func.isRequired,
    onAcDexChange: PropTypes.func.isRequired,
    onAcMiscChange: PropTypes.func.isRequired,
    onAcShieldChange: PropTypes.func.isRequired,
    onAcSizeChange: PropTypes.func.isRequired,
    onCurrentHpChange: PropTypes.func.isRequired,
    onHealthReset: PropTypes.func.isRequired,
    onHpChange: PropTypes.func.isRequired,
    onSubdualDamageChange: PropTypes.func.isRequired
};

export default Health;
