import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';
import {Flex, Label, Operator} from '../basic';

const AcHeader = ({t}) => (
    <Flex
        align="flex-end"
        spaced
    >
        <Label field />
        <Label field>{t('common:TOTAL')}</Label>
        <Operator />
        <Label field>{t('BASE')}</Label>
        <Operator />
        <Label field>{t('ARMOR')}</Label>
        <Operator />
        <Label field>{t('SHIELD')}</Label>
        <Operator />
        <Label field>{t('stats:DEX')}</Label>
        <Operator />
        <Label field>{t('SIZE')}</Label>
        <Operator />
        <Label field>{t('Misc')}</Label>
    </Flex>
);

AcHeader.propTypes = {
    t: PropTypes.func.isRequired
};

export default translate(['health', 'stats', 'common'])(AcHeader);
