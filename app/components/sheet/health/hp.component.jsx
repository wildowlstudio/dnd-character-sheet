import {Field, Flex, Input} from '../basic';

import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

const Hp = ({hp, onCurrentHpChange, onHpChange, onSubdualDamageChange, t}) => (
    <Flex spaced>
        <Field inverted>{t('HP')}</Field>
        <Input
            border="full"
            field
            onChange={onHpChange}
            type="number"
            value={hp.hp}
        />
        <Input
            border="full"
            field
            multiply={3}
            onChange={onCurrentHpChange}
            operators={3}
            value={hp.current}
        />
        <Input
            border="full"
            field
            multiply={3}
            onChange={onSubdualDamageChange}
            operators={3}
            value={hp.subdualDamage}
        />
    </Flex>
);

Hp.propTypes = {
    hp: PropTypes.shape({
        hp: PropTypes.number,
        current: PropTypes.number,
        subdualDamage: PropTypes.string
    }).isRequired,
    onCurrentHpChange: PropTypes.func.isRequired,
    onHpChange: PropTypes.func.isRequired,
    onSubdualDamageChange: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired
};

export default translate(['health'])(Hp);
