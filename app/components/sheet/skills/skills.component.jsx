import {Element} from '../basic';
import PropTypes from 'prop-types';
import React from 'react';
import Skill from './skill.component';
import SkillList from './skillList.component';
import SkillsHeader from './skillsHeader.component';
import media from '../../../utils/style.utils';
import styled from 'styled-components';

const StyledElement = styled(Element)`
    width: 407px;
    margin-top: -71px;

    ${media.oneRow`
        width: 608px;
        margin-top: 0;
    `};
`;

const Skills = ({
    onSkillAbilityModChange,
    onSkillAdd,
    onSkillAttributeChange,
    onSkillClassChange,
    onSkillDelete,
    onSkillsDelete,
    onSkillMaxClassRankChange,
    onSkillMaxCrossClassRankChange,
    onSkillMiscModChange,
    onSkillNameChange,
    onSkillReset,
    onSkillSkillRankChange,
    onSkillSort,
    skill,
    skills
}) => (
    <StyledElement
        onAdd={onSkillAdd}
        onRemove={onSkillsDelete}
        onReset={onSkillReset}
    >
        <SkillsHeader
            onSkillMaxClassRankChange={onSkillMaxClassRankChange}
            onSkillMaxCrossClassRankChange={onSkillMaxCrossClassRankChange}
            onSort={onSkillSort}
            skill={skill}
        />
        {skills
            .concat({id: 'new'})
            .map((skill) => (
                <Skill
                    key={skill.id}
                    onDelete={onSkillDelete}
                    onSkillAbilityModChange={onSkillAbilityModChange}
                    onSkillAttributeChange={onSkillAttributeChange}
                    onSkillClassChange={onSkillClassChange}
                    onSkillMiscModChange={onSkillMiscModChange}
                    onSkillNameChange={onSkillNameChange}
                    onSkillSkillRankChange={onSkillSkillRankChange}
                    skill={skill}
                />
            ))}
        <SkillList />
    </StyledElement>
);

Skills.propTypes = {
    onSkillAbilityModChange: PropTypes.func.isRequired,
    onSkillAdd: PropTypes.func.isRequired,
    onSkillAttributeChange: PropTypes.func.isRequired,
    onSkillClassChange: PropTypes.func.isRequired,
    onSkillDelete: PropTypes.func.isRequired,
    onSkillMaxClassRankChange: PropTypes.func.isRequired,
    onSkillMaxCrossClassRankChange: PropTypes.func.isRequired,
    onSkillMiscModChange: PropTypes.func.isRequired,
    onSkillNameChange: PropTypes.func.isRequired,
    onSkillReset: PropTypes.func.isRequired,
    onSkillsDelete: PropTypes.func.isRequired,
    onSkillSkillRankChange: PropTypes.func.isRequired,
    onSkillSort: PropTypes.func.isRequired,
    skill: PropTypes.shape({
        maxClassRank: PropTypes.number,
        maxCrossClassRank: PropTypes.number
    }).isRequired,
    skills: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string,
            attribute: PropTypes.string,
            class: PropTypes.bool,
            abilityMod: PropTypes.number,
            skillRank: PropTypes.number,
            miscMod: PropTypes.number
        })
    ).isRequired
};

export default Skills;
