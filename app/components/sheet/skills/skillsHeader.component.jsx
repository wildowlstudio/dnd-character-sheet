import {Field, Flex, Input, VerticalText} from '../basic';

import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';
import {translate} from 'react-i18next';

const Title = styled.div`
    flex-grow: 1;
`;

const InputContainer = styled.div`
    background-color: #ffffff;
    color: #000000;
    border: solid 1px #ffffff;
    margin-left: 5px;

    > input {
        width: 25px;
    }
`;

class SkillHeader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};

        this.handleAbilityModSort = this.handleAbilityModSort.bind(this);
        this.handleAttributeSort = this.handleAttributeSort.bind(this);
        this.handleClassSort = this.handleClassSort.bind(this);
        this.handleMiscModSort = this.handleMiscModSort.bind(this);
        this.handleNameSort = this.handleNameSort.bind(this);
        this.handleSkillRankSort = this.handleSkillRankSort.bind(this);
        this.handleSort = this.handleSort.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return false;
    }

    handleSort(field) {
        if (this.orderBy === field) {
            this.desc = !this.desc;
        } else {
            this.desc = false;
        }
        this.orderBy = field;
        this.props.onSort(field, this.desc);
    }

    handleClassSort() {
        this.handleSort('class');
    }

    handleNameSort() {
        this.handleSort('name');
    }

    handleAttributeSort() {
        this.handleSort('attribute');
    }

    handleAbilityModSort() {
        this.handleSort('abilityMod');
    }

    handleSkillRankSort() {
        this.handleSort('skillRank');
    }

    handleMiscModSort() {
        this.handleSort('miscMod');
    }

    render() {
        const {
            onSkillMaxClassRankChange,
            onSkillMaxCrossClassRankChange,
            skill,
            t
        } = this.props;

        return (
            <Flex>
                <VerticalText onClick={this.handleClassSort}>{t('CLASS')}</VerticalText>
                <Flex
                    direction="column"
                    grow
                >
                    <Field
                        grow
                        inverted
                    >
                        <Flex>
                            <Title>{t('SKILLS')}</Title>
                            <Flex>
                                <div>{t('MAX_RANKS')}</div>
                                <InputContainer>
                                    <Input
                                        field
                                        max="999"
                                        min="0"
                                        onChange={onSkillMaxClassRankChange}
                                        step="1"
                                        type="number"
                                        value={skill.maxClassRank}
                                    />
                                    <span>{'/'}</span>
                                    <Input
                                        field
                                        max="999"
                                        min="0"
                                        onChange={onSkillMaxCrossClassRankChange}
                                        step="1"
                                        type="number"
                                        value={skill.maxCrossClassRank}
                                    />
                                </InputContainer>
                            </Flex>
                        </Flex>
                    </Field>
                    <Flex>
                        <Field
                            grow
                            heightFudgeFactor={13}
                            onClick={this.handleNameSort}
                        >
                            <span>{t('SKILL_NAME')}</span>
                        </Field>
                        <Field
                            heightFudgeFactor={13}
                            onClick={this.handleAttributeSort}
                            widthFudgeFactor={10}
                        >
                            <span>{t('ATTRIBUTE')}</span>
                        </Field>
                        <Field
                            heightFudgeFactor={13}
                            widthFudgeFactor={10}
                        >
                            {t('common:TOTAL')}
                        </Field>
                        <Field
                            heightFudgeFactor={13}
                            onClick={this.handleAbilityModSort}
                            widthFudgeFactor={10}
                        >
                            <span>{t('ABILITY_MODIFIER')}</span>
                        </Field>
                        <Field
                            heightFudgeFactor={13}
                            onClick={this.handleSkillRankSort}
                            widthFudgeFactor={10}
                        >
                            <span>{t('SKILL_RANKS')}</span>
                        </Field>
                        <Field
                            heightFudgeFactor={13}
                            onClick={this.handleMiscModSort}
                            widthFudgeFactor={10}
                        >
                            <span>{t('MISC_MODIFIER')}</span>
                        </Field>
                    </Flex>
                </Flex>
            </Flex>
        );
    }
}

SkillHeader.propTypes = {
    onSkillMaxClassRankChange: PropTypes.func.isRequired,
    onSkillMaxCrossClassRankChange: PropTypes.func.isRequired,
    onSort: PropTypes.func.isRequired,
    skill: PropTypes.shape({
        maxClassRank: PropTypes.number,
        maxCrossClassRank: PropTypes.number
    }).isRequired,
    t: PropTypes.func.isRequired
};

export default translate(['skills'])(SkillHeader);
