import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

const SkillList = ({t}) => {
    return (
        <datalist id="skillList">
            {[
                'APPRAISE',
                'AUTOHYPNOSIS',
                'BALANCE',
                'BLUFF',
                'CLIMB',
                'CONCENTRATION',
                'CONTROL_SHAPE',
                'CRAFT',
                'DECIPHER_SCRIPT',
                'DIPLOMACY',
                'DISABLE_DEVICE',
                'DISGUISE',
                'ESCAPE_ARTIST',
                'FORGERY',
                'GATHER_INFORMATION',
                'HANDLE_ANIMAL',
                'HEAL',
                'HIDE',
                'INTIMIDATE',
                'JUMP',
                'KNOWLEDGE',
                'LISTEN',
                'MOVE_SILENTLY',
                'OPEN_LOCK',
                'PERFORM',
                'PSICRAFT',
                'PROFESSION',
                'RIDE',
                'SEARCH',
                'SENSE_MOTIVE',
                'SLEIGHT_OF_HAND',
                'SPEAK_LANGUAGE',
                'SPELLCRAFT',
                'SPOT',
                'SURVIVAL',
                'SWIM',
                'TUMBLE',
                'USE_MAGIC_DEVICE',
                'USE_PSIONIC_DEVICE',
                'USE_ROPE'
            ]
                .map((attribute) => t(attribute))
                .sort()
                .map((attribute) => <option
                    key={attribute}
                    value={attribute}
                                    />)}
        </datalist>
    );
};

SkillList.propTypes = {
    t: PropTypes.func.isRequired
};

export default translate(['skills'])(SkillList);
