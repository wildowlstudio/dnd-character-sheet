import Header from './skillsHeader.component';
import React from 'react';
import Skills from './skills.component';
import {mount} from 'enzyme';
import renderer from 'react-test-renderer';

/* eslint-disable react/jsx-no-bind */
/* eslint-disable react/jsx-handler-names */
describe('Sheet skills tests', () => {
    test('Should match previous Skills snapshot', () => {
        const Rendered = renderer.create(
            <Skills
                onSkillAbilityModChange={() => null}
                onSkillAdd={() => null}
                onSkillAttributeChange={() => null}
                onSkillClassChange={() => null}
                onSkillDelete={() => null}
                onSkillMaxClassRankChange={() => null}
                onSkillMaxCrossClassRankChange={() => null}
                onSkillMiscModChange={() => null}
                onSkillNameChange={() => null}
                onSkillReset={() => null}
                onSkillSkillRankChange={() => null}
                onSkillSort={() => null}
                skill={{}}
                skills={[{id: 0}]}
            />
        );
        expect(Rendered).toMatchSnapshot();
    });

    test('Should check skills component integration', () => {
        const mockResetClick = jest.fn();
        const mockAddClick = jest.fn();
        const mockRemoveClick = jest.fn();
        const mockSortClick = jest.fn();
        const fields = [
            {text: 'MAX_CLASS_RANK', fun: jest.fn(), value: Math.random()},
            {text: 'MAX_CROSS_CLASS_RANK', fun: jest.fn(), value: Math.random()},
            {text: 'CLASS', fun: jest.fn(), value: true},
            {text: 'NAME', fun: jest.fn(), value: 'NAME'},
            {text: 'ATTRIBUTE', fun: jest.fn(), value: 'ATTRIBUTE'},
            {text: 'ABILITY_MOD', fun: jest.fn(), value: Math.random()},
            {text: 'SKILL_RANK', fun: jest.fn(), value: Math.random()},
            {text: 'MISC_MOD', fun: jest.fn(), value: Math.random()}
        ];
        const component = mount(
            <Skills
                onSkillAbilityModChange={fields[5].fun}
                onSkillAdd={mockAddClick}
                onSkillAttributeChange={fields[4].fun}
                onSkillClassChange={fields[2].fun}
                onSkillDelete={mockRemoveClick}
                onSkillMaxClassRankChange={fields[0].fun}
                onSkillMaxCrossClassRankChange={fields[1].fun}
                onSkillMiscModChange={fields[7].fun}
                onSkillNameChange={fields[3].fun}
                onSkillReset={mockResetClick}
                onSkillSkillRankChange={fields[6].fun}
                onSkillSort={mockSortClick}
                skill={{}}
                skills={[{id: 0}]}
            />
        );

        component.find('.fa').forEach((element) => element.simulate('click'));
        expect(mockResetClick).toBeCalled();
        expect(mockAddClick).toBeCalled();
        expect(mockRemoveClick).toBeCalled();

        component
            .find(Header)
            .find('div')
            .forEach((div, i) => div.simulate('click'));
        expect(mockSortClick).toHaveBeenCalledTimes(6);

        component.find('input').forEach((input, i) => {
            if (i < fields.length) {
                if (input.props().type === 'checkbox') {
                    input.simulate('change', {
                        target: {name: 'change', checked: fields[i].value}
                    });
                    expect(fields[i].fun).toBeCalled();
                    expect(fields[i].fun.mock.calls[0][1]).toBe(fields[i].value);
                } else {
                    input.simulate('change', {
                        target: {name: 'change', value: fields[i].value}
                    });
                    input.simulate('blur', {target: {name: 'change'}});
                    expect(fields[i].fun).toBeCalled();
                    const paramIndex =
                        fields[i].text === 'MAX_CLASS_RANK' ||
                        fields[i].text === 'MAX_CROSS_CLASS_RANK'
                            ? 0
                            : 1;
                    expect(fields[i].fun.mock.calls[0][paramIndex]).toBe(fields[i].value);
                }
            }
        });
    });
});
