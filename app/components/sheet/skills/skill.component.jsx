import {Checkbox, Field, Flex, Input, Operator, TrashIcon} from '../basic';

import PropTypes from 'prop-types';
import React from 'react';
import {findDOMNode} from 'react-dom';
import styled from 'styled-components';

const StyledInput = styled(Input)`
    margin-left: 8px;
`;

const StyledField = Field.extend`
    border-right: transparent;
    border-left: transparent;
    border-top: transparent;
    margin-left: 8px;
`;

class Skill extends React.Component {
    constructor(props) {
        super(props);

        this.handleDelete = this.handleDelete.bind(this);
        this.handleSkillAbilityModChange = this.handleSkillAbilityModChange.bind(this);
        this.handleSkillAttributeChange = this.handleSkillAttributeChange.bind(this);
        this.handleSkillClassChange = this.handleSkillClassChange.bind(this);
        this.handleSkillMiscModChange = this.handleSkillMiscModChange.bind(this);
        this.handleSkillNameChange = this.handleSkillNameChange.bind(this);
        this.handleSkillSkillRankChange = this.handleSkillSkillRankChange.bind(this);
    }

    componentDidMount() {
        if (window.catchFocus) {
            const elements = findDOMNode(this).getElementsByTagName('input'); // eslint-disable-line react/no-find-dom-node
            switch (window.catchFocus) {
                case 'class':
                    elements[1].focus();
                    break;
                case 'name':
                    elements[2].focus();
                    break;
                case 'attribute':
                    elements[3].focus();
                    break;
                case 'abilityMod':
                    elements[4].focus();
                    break;
                case 'skillRank':
                    elements[5].focus();
                    break;
            }
            window.catchFocus = null;
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.skill !== this.props.skill;
    }

    handleSkillAbilityModChange(value) {
        const {skill} = this.props;
        if (skill.id === 'new') {
            window.catchFocus = 'abilityMod';
        }
        this.props.onSkillAbilityModChange(skill.id, value);
    }

    handleSkillAttributeChange(value) {
        const {skill} = this.props;
        if (skill.id === 'new') {
            window.catchFocus = 'attribute';
        }
        this.props.onSkillAttributeChange(skill.id, value);
    }

    handleSkillClassChange(value) {
        const {skill} = this.props;
        if (skill.id === 'new') {
            window.catchFocus = 'class';
        }
        this.props.onSkillClassChange(skill.id, value);
    }

    handleSkillMiscModChange(value) {
        const {skill} = this.props;
        if (skill.id === 'new') {
            findDOMNode(this) // eslint-disable-line react/no-find-dom-node
                .getElementsByTagName('input')[0]
                .focus();
        }
        this.props.onSkillMiscModChange(skill.id, value);
    }

    handleSkillNameChange(value) {
        const {skill} = this.props;
        if (skill.id === 'new') {
            window.catchFocus = 'name';
        }
        this.props.onSkillNameChange(skill.id, value);
    }

    handleSkillSkillRankChange(value) {
        const {skill} = this.props;
        if (skill.id === 'new') {
            window.catchFocus = 'skillRank';
        }
        this.props.onSkillSkillRankChange(skill.id, value);
    }

    handleDelete() {
        this.props.onDelete(this.props.skill.id);
    }

    render() {
        const {skill} = this.props;
        return (
            <Flex trash>
                <Checkbox
                    onChange={this.handleSkillClassChange}
                    value={skill.class}
                />
                <Input
                    field
                    grow
                    list="skillList"
                    onChange={this.handleSkillNameChange}
                    value={skill.name}
                />
                <StyledInput
                    field
                    list="attributeList"
                    onChange={this.handleSkillAttributeChange}
                    value={skill.attribute}
                    widthFudgeFactor={3}
                />
                <StyledField
                    field
                    widthFudgeFactor={3}
                >
                    {(+skill.abilityMod || 0) +
                        (+skill.skillRank || 0) +
                        (+skill.miscMod || 0)}
                </StyledField>
                <Operator>{'='}</Operator>
                <Input
                    field
                    onChange={this.handleSkillAbilityModChange}
                    type="number"
                    value={skill.abilityMod}
                    widthFudgeFactor={3}
                />
                <Operator>{'+'}</Operator>
                <Input
                    field
                    onChange={this.handleSkillSkillRankChange}
                    type="number"
                    value={skill.skillRank}
                    widthFudgeFactor={3}
                />
                <Operator>{'+'}</Operator>
                <Input
                    field
                    onChange={this.handleSkillMiscModChange}
                    type="number"
                    value={skill.miscMod}
                    widthFudgeFactor={5}
                />
                {skill.id !== 'new' ? (
                    <TrashIcon
                        aria-label="delete skill"
                        onClick={this.handleDelete}
                    />
                ) : null}
            </Flex>
        );
    }
}

Skill.propTypes = {
    onDelete: PropTypes.func.isRequired,
    onSkillAbilityModChange: PropTypes.func.isRequired,
    onSkillAttributeChange: PropTypes.func.isRequired,
    onSkillClassChange: PropTypes.func.isRequired,
    onSkillMiscModChange: PropTypes.func.isRequired,
    onSkillNameChange: PropTypes.func.isRequired,
    onSkillSkillRankChange: PropTypes.func.isRequired,
    skill: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string,
            attribute: PropTypes.string,
            class: PropTypes.bool,
            abilityMod: PropTypes.number,
            skillRank: PropTypes.number,
            miscMod: PropTypes.number
        })
    ).isRequired
};

export default Skill;
