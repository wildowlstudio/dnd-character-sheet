import {Element, Field, Flex, Input, Operator} from '../basic';

import Header from './header.component';
import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';
import {translate} from 'react-i18next';

const StyledElement = styled(Element)`
    margin-top: -9px;

    /* position: absolute;
    top: 217px;
    left: 256px; */
`;

const Initiative = ({
    initiative,
    onDexModChange,
    onInitiativeReset,
    onMiscModChange,
    t
}) => (
    <StyledElement onReset={onInitiativeReset}>
        <Header />
        <Flex
            align="center"
            spaced
        >
            <Field
                inverted
                multiply={3}
            >
                {t('INITIATIVE')}
            </Field>
            <Field>{(+initiative.dexMod || 0) + (+initiative.miscMod || 0)}</Field>
            <Operator>{' = '}</Operator>
            <Input
                border="full"
                field
                onChange={onDexModChange}
                type="number"
                value={initiative.dexMod}
            />
            <Operator>{'+'}</Operator>
            <Input
                border="full"
                field
                onChange={onMiscModChange}
                type="number"
                value={initiative.miscMod}
            />
        </Flex>
    </StyledElement>
);

Initiative.propTypes = {
    initiative: PropTypes.shape({
        dexMod: PropTypes.number,
        miscMod: PropTypes.number
    }).isRequired,
    onDexModChange: PropTypes.func.isRequired,
    onInitiativeReset: PropTypes.func.isRequired,
    onMiscModChange: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired
};

export default translate(['initiative'])(Initiative);
