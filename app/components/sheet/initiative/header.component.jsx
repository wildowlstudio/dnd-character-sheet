import {Flex, Label, Operator} from '../basic';

import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

const InitiativeHeader = ({t}) => (
    <Flex
        align="flex-end"
        spaced
    >
        <Label
            field
            multiply={3}
        />
        <Label field>{t('common:TOTAL')}</Label>
        <Operator />
        <Label field>{t('DEX_MOD')}</Label>
        <Operator />
        <Label field>{t('MISC_MOD')}</Label>
    </Flex>
);

InitiativeHeader.propTypes = {
    t: PropTypes.func.isRequired
};

export default translate(['initiative', 'common'])(InitiativeHeader);
