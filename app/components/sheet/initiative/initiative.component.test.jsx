import Initiative from './initiative.component';
import React from 'react';
import {mount} from 'enzyme';
import renderer from 'react-test-renderer';

/* eslint-disable react/jsx-no-bind */
/* eslint-disable react/jsx-handler-names */
describe('Sheet initiative tests', () => {
    test('Should match previous Initiative snapshot', () => {
        const Rendered = renderer.create(
            <Initiative
                initiative={{}}
                onDexModChange={() => null}
                onInitiativeReset={() => null}
                onMiscModChange={() => null}
            />
        );
        expect(Rendered).toMatchSnapshot();
    });

    test('Should check initiative component integration', () => {
        const mockResetClick = jest.fn();
        const fields = [
            {text: 'DEX_MOD', fun: jest.fn(), value: Math.random()},
            {text: 'MISC_MOD', fun: jest.fn(), value: Math.random()}
        ];
        const component = mount(
            <Initiative
                initiative={{}}
                onDexModChange={fields[0].fun}
                onInitiativeReset={mockResetClick}
                onMiscModChange={fields[1].fun}
            />
        );
        component
            .find('.fa-eraser')
            .last()
            .simulate('click');
        expect(mockResetClick).toBeCalled();

        component.find('input').forEach((input, i) => {
            input.simulate('change', {
                target: {name: 'change', value: fields[i].value}
            });
            input.simulate('blur', {target: {name: 'change'}});
            expect(fields[i].fun).toBeCalled();
            expect(fields[i].fun.mock.calls[0][0]).toBe(fields[i].value);
        });
    });
});
