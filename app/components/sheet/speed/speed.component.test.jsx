import React from 'react';
import Speed from './speed.component';
import {mount} from 'enzyme';
import renderer from 'react-test-renderer';

/* eslint-disable react/jsx-no-bind */
/* eslint-disable react/jsx-handler-names */
describe('Sheet speed tests', () => {
    test('Should match previous Speed snapshot', () => {
        const Rendered = renderer.create(
            <Speed
                onSpeedChange={() => null}
                onSpeedReset={() => null}
                speed=""
            />
        );
        expect(Rendered).toMatchSnapshot();
    });

    test('Should check speed component integration', () => {
        const mockResetClick = jest.fn();
        const fields = [{text: 'SPEED', fun: jest.fn(), value: 'SPEED'}];
        const component = mount(
            <Speed
                onSpeedChange={fields[0].fun}
                onSpeedReset={mockResetClick}
                speed=""
            />
        );
        component
            .find('.fa-eraser')
            .last()
            .simulate('click');
        expect(mockResetClick).toBeCalled();

        component.find('input').forEach((input, i) => {
            input.simulate('change', {
                target: {name: 'change', value: fields[i].value}
            });
            input.simulate('blur', {target: {name: 'change'}});
            expect(fields[i].fun).toBeCalled();
            expect(fields[i].fun.mock.calls[0][0]).toBe(fields[i].value);
        });
    });
});
