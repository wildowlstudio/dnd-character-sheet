import {Element, Field, Flex, Input} from '../basic';

import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';
import {translate} from 'react-i18next';

const StyledElement = styled(Element)`
    /* position: absolute;
    top: 272px;
    left: 587px; */
    width: 272px;
    margin-top: -2px;
`;

const Speed = ({onSpeedChange, onSpeedReset, speed, t}) => (
    <StyledElement onReset={onSpeedReset}>
        <Flex
            align="center"
            spaced
        >
            <Field
                inverted
                multiply={3}
            >
                {t('SPEED')}
            </Field>
            <Input
                border="full"
                field
                multiply={3}
                onChange={onSpeedChange}
                value={speed}
            />
        </Flex>
    </StyledElement>
);

Speed.propTypes = {
    onSpeedChange: PropTypes.func.isRequired,
    onSpeedReset: PropTypes.func.isRequired,
    speed: PropTypes.string,
    t: PropTypes.func.isRequired
};

export default translate(['speed'])(Speed);
