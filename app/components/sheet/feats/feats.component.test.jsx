import Feats from './feats.component';
import React from 'react';
import {mount} from 'enzyme';
import renderer from 'react-test-renderer';

/* eslint-disable react/jsx-no-bind */
/* eslint-disable react/jsx-handler-names */
describe('Sheet feats tests', () => {
    test('Should match previous Feats snapshot', () => {
        const Rendered = renderer.create(
            <Feats
                feats={[{id: 0}]}
                onFeatAdd={() => null}
                onFeatChange={() => null}
                onFeatClean={() => null}
                onFeatDelete={() => null}
                onFeatMove={() => null}
                onFeatReset={() => null}
            />
        );
        expect(Rendered).toMatchSnapshot();
    });

    test('Should check feats component integration', () => {
        const mockResetClick = jest.fn();
        const mockAddClick = jest.fn();
        const mockRemoveClick = jest.fn();
        const mockDeleteClick = jest.fn();
        const mockChange = jest.fn();

        const component = mount(
            <Feats
                feats={[{id: 0}]}
                onFeatAdd={mockAddClick}
                onFeatChange={mockChange}
                onFeatClean={mockRemoveClick}
                onFeatDelete={mockDeleteClick}
                onFeatMove={() => null}
                onFeatReset={mockResetClick}
            />
        );

        component.find('.fa').forEach((element) => element.simulate('click'));
        expect(mockResetClick).toBeCalled();
        expect(mockAddClick).toBeCalled();
        expect(mockRemoveClick).toBeCalled();

        const valueField = component.find('input').first();
        valueField.simulate('change', {
            target: {name: 'change', value: 'test'}
        });
        valueField.simulate('blur', {target: {name: 'change'}});
        expect(mockChange).toBeCalled();
        expect(mockChange.mock.calls[0][0]).toBe(0);
        expect(mockChange.mock.calls[0][1]).toBe('test');
    });
});
