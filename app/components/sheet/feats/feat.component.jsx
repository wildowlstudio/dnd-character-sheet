import {Flex, Input, TrashIcon} from '../basic';
import {handleDragOver, isDroppedBelow} from '../../../utils/dnd.util';

import PropTypes from 'prop-types';
import React from 'react';
import {findDOMNode} from 'react-dom';

class Feat extends React.PureComponent {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleDragStart = this.handleDragStart.bind(this);
        this.handleDrop = this.handleDrop.bind(this);
    }

    handleChange(value) {
        const {feat} = this.props;
        if (feat.id === 'new') {
            findDOMNode(this) // eslint-disable-line react/no-find-dom-node
                .getElementsByTagName('input')[0]
                .focus();
        }
        this.props.onChange(feat.id, value);
    }

    handleDelete() {
        this.props.onDelete(this.props.feat.id);
    }

    handleDragStart(event) {
        event.dataTransfer.setData('id', this.props.feat.id);
    }

    handleDrop(event) {
        event.preventDefault();
        const id = +event.dataTransfer.getData('id');
        const {feat} = this.props;
        if (id !== feat.id) {
            const below = isDroppedBelow(event);
            this.props.onMove(id, below, feat.id);
        }
    }

    render() {
        const {feat} = this.props;

        return (
            <Flex
                draggable={feat.id !== 'new'}
                grow
                maxWidth="33%"
                minWidth="30%"
                onDragOver={handleDragOver}
                onDragStart={this.handleDragStart}
                onDrop={this.handleDrop}
                trash
            >
                <Input
                    align="left"
                    key={feat.id}
                    onChange={this.handleChange}
                    value={feat.value}
                />
                {feat.id !== 'new' ? (
                    <TrashIcon
                        aria-label="delete feat"
                        onClick={this.handleDelete}
                    />
                ) : null}
            </Flex>
        );
    }
}

Feat.propTypes = {
    feat: PropTypes.shape({
        id: PropTypes.number.isRequired,
        value: PropTypes.string
    }).isRequired,
    onChange: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    onMove: PropTypes.func.isRequired
};

export default Feat;
