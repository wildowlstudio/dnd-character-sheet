import {Element, Flex} from '../basic';

import Feat from './feat.component';
import Header from './header.component';
import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';

const StyledElement = styled(Element)`
    width: 608px;
`;

const Feats = ({
    className,
    feats,
    onFeatAdd,
    onFeatClean,
    onFeatDelete,
    onFeatMove,
    onFeatReset,
    onFeatChange
}) => {
    return (
        <StyledElement
            className={className}
            onAdd={onFeatAdd}
            onRemove={onFeatClean}
            onReset={onFeatReset}
        >
            <Header />
            <Flex
                spaced
                wrap="true"
            >
                {feats
                    .concat({id: 'new'})
                    .map((feat) => (
                        <Feat
                            feat={feat}
                            key={feat.id}
                            onChange={onFeatChange}
                            onDelete={onFeatDelete}
                            onMove={onFeatMove}
                        />
                    ))}
            </Flex>
        </StyledElement>
    );
};

Feats.propTypes = {
    className: PropTypes.string,
    feats: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            value: PropTypes.string
        })
    ).isRequired,
    onFeatAdd: PropTypes.func.isRequired,
    onFeatChange: PropTypes.func.isRequired,
    onFeatClean: PropTypes.func.isRequired,
    onFeatDelete: PropTypes.func.isRequired,
    onFeatMove: PropTypes.func.isRequired,
    onFeatReset: PropTypes.func.isRequired
};

export default Feats;
