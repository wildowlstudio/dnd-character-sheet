import KnownSpells from './knownSpells.component';
import React from 'react';
import {mount} from 'enzyme';
import renderer from 'react-test-renderer';

/* eslint-disable react/jsx-no-bind */
/* eslint-disable react/jsx-handler-names */
describe('Sheet knownSpells tests', () => {
    test('Should match previous KnownSpells snapshot', () => {
        const Rendered = renderer.create(
            <KnownSpells
                onSpellAdd={() => null}
                onSpellChange={() => null}
                onSpellClean={() => null}
                onSpellDelete={() => null}
                onSpellMove={() => null}
                onSpellReset={() => null}
                spells={[{id: 0}]}
            />
        );
        expect(Rendered).toMatchSnapshot();
    });

    test('Should check knownSpells component integration', () => {
        const mockResetClick = jest.fn();
        const mockAddClick = jest.fn();
        const mockRemoveClick = jest.fn();
        const mockDeleteClick = jest.fn();
        const mockChange = jest.fn();

        const component = mount(
            <KnownSpells
                onSpellAdd={mockAddClick}
                onSpellChange={mockChange}
                onSpellClean={mockRemoveClick}
                onSpellDelete={mockDeleteClick}
                onSpellMove={() => null}
                onSpellReset={mockResetClick}
                spells={[{id: 0}]}
            />
        );

        component.find('.fa').forEach((element) => element.simulate('click'));
        expect(mockResetClick).toBeCalled();
        expect(mockAddClick).toBeCalled();
        expect(mockRemoveClick).toBeCalled();

        const valueField = component.find('input').first();
        valueField.simulate('change', {
            target: {name: 'change', value: 'test'}
        });
        valueField.simulate('blur', {target: {name: 'change'}});
        expect(mockChange).toBeCalled();
        expect(mockChange.mock.calls[0][0]).toBe(0);
        expect(mockChange.mock.calls[0][1]).toBe('test');
    });
});
