import {Field} from '../basic';
import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

const FeatsHeader = ({t}) => {
    return (
        <Field
            inverted
            noWidth
        >
            {t('KNOWN_SPELLS')}
        </Field>
    );
};

FeatsHeader.propTypes = {
    t: PropTypes.func.isRequired
};

export default translate(['spells'])(FeatsHeader);
