import {Element, Flex} from '../basic';

import Header from './header.component';
import KnownSpell from './knownSpell.component';
import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';

const StyledElement = styled(Element)`
    width: 608px;
`;

const KnownSpells = ({
    className,
    spells,
    onSpellAdd,
    onSpellClean,
    onSpellDelete,
    onSpellMove,
    onSpellReset,
    onSpellChange
}) => {
    return (
        <StyledElement
            className={className}
            onAdd={onSpellAdd}
            onRemove={onSpellClean}
            onReset={onSpellReset}
        >
            <Header />
            <Flex
                spaced
                wrap="true"
            >
                {spells
                    .concat({id: 'new'})
                    .map((knownSpell) => (
                        <KnownSpell
                            key={knownSpell.id}
                            onChange={onSpellChange}
                            onDelete={onSpellDelete}
                            onMove={onSpellMove}
                            spell={knownSpell}
                        />
                    ))}
            </Flex>
        </StyledElement>
    );
};

KnownSpells.propTypes = {
    className: PropTypes.string,
    onSpellAdd: PropTypes.func.isRequired,
    onSpellChange: PropTypes.func.isRequired,
    onSpellClean: PropTypes.func.isRequired,
    onSpellDelete: PropTypes.func.isRequired,
    onSpellMove: PropTypes.func.isRequired,
    onSpellReset: PropTypes.func.isRequired,
    spells: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            value: PropTypes.string
        })
    ).isRequired
};

export default KnownSpells;
