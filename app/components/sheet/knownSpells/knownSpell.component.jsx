import {Flex, Input, TrashIcon} from '../basic';
import {handleDragOver, isDroppedBelow} from '../../../utils/dnd.util';

import PropTypes from 'prop-types';
import React from 'react';
import {findDOMNode} from 'react-dom';

class KnownSpell extends React.PureComponent {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleDragStart = this.handleDragStart.bind(this);
        this.handleDrop = this.handleDrop.bind(this);
    }

    handleChange(value) {
        const {spell} = this.props;
        if (spell.id === 'new') {
            findDOMNode(this) // eslint-disable-line react/no-find-dom-node
                .getElementsByTagName('input')[0]
                .focus();
        }
        this.props.onChange(spell.id, value);
    }

    handleDelete() {
        this.props.onDelete(this.props.spell.id);
    }

    handleDragStart(event) {
        event.dataTransfer.setData('id', this.props.spell.id);
    }

    handleDrop(event) {
        event.preventDefault();
        const id = +event.dataTransfer.getData('id');
        const {spell} = this.props;
        if (id !== spell.id) {
            const below = isDroppedBelow(event);
            this.props.onMove(id, below, spell.id);
        }
    }

    render() {
        const {spell} = this.props;

        return (
            <Flex
                draggable={spell.id !== 'new'}
                grow
                maxWidth="33%"
                minWidth="30%"
                onDragOver={handleDragOver}
                onDragStart={this.handleDragStart}
                onDrop={this.handleDrop}
                trash
            >
                <Input
                    align="left"
                    key={spell.id}
                    onChange={this.handleChange}
                    value={spell.value}
                />
                {spell.id !== 'new' ? (
                    <TrashIcon
                        aria-label="delete knownSpell"
                        onClick={this.handleDelete}
                    />
                ) : null}
            </Flex>
        );
    }
}

KnownSpell.propTypes = {
    onChange: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    onMove: PropTypes.func.isRequired,
    spell: PropTypes.shape({
        id: PropTypes.number.isRequired,
        value: PropTypes.string
    }).isRequired
};

export default KnownSpell;
