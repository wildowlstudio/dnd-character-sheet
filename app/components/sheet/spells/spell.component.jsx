import {Field, Flex, Input} from '../basic';

import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

class Spell extends React.Component {
    constructor(props) {
        super(props);

        this.handleKnownChange = this.handleKnownChange.bind(this);
        this.handlePerDayChange = this.handlePerDayChange.bind(this);
        this.handleBonusChange = this.handleBonusChange.bind(this);
        this.handleSaveDcChange = this.handleSaveDcChange.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.spell !== this.props.spell;
    }

    handleKnownChange(value) {
        this.props.onKnownChange(this.props.level, value);
    }

    handlePerDayChange(value) {
        this.props.onPerDayChange(this.props.level, value);
    }

    handleBonusChange(value) {
        this.props.onBonusChange(this.props.level, value);
    }

    handleSaveDcChange(value) {
        this.props.onSaveDcChange(this.props.level, value);
    }

    render() {
        const {level, t, spell} = this.props;

        return (
            <Flex spaced>
                <Field>{t(level)}</Field>
                <Input
                    border="full"
                    field
                    onChange={this.handleKnownChange}
                    type="number"
                    value={spell.known}
                />
                <Input
                    border="full"
                    field
                    onChange={this.handlePerDayChange}
                    type="number"
                    value={spell.perDay}
                />
                <Input
                    border="full"
                    field
                    onChange={this.handleBonusChange}
                    type="number"
                    value={spell.bonus}
                />
                <Input
                    border="full"
                    field
                    onChange={this.handleSaveDcChange}
                    type="number"
                    value={spell.saveDc}
                />
            </Flex>
        );
    }
}

Spell.propTypes = {
    level: PropTypes.string.isRequired,
    onBonusChange: PropTypes.func.isRequired,
    onKnownChange: PropTypes.func.isRequired,
    onPerDayChange: PropTypes.func.isRequired,
    onSaveDcChange: PropTypes.func.isRequired,
    spell: PropTypes.shape({
        known: PropTypes.string,
        perDay: PropTypes.string,
        bonus: PropTypes.string,
        saveDc: PropTypes.string
    }).isRequired,
    t: PropTypes.func.isRequired
};

export default translate(['spells'])(Spell);
