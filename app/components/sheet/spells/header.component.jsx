import {Field, Flex, Label} from '../basic';

import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

const SpellsHeader = ({t}) => (
    <Flex direction="column">
        <Field
            inverted
            noWidth
        >
            {t('SPELLS')}
        </Field>
        <Flex
            align="flex-end"
            spaced
        >
            <Label field>{t('LEVEL')}</Label>
            <Label field>{t('KNOWN')}</Label>
            <Label field>{t('PER_DAY')}</Label>
            <Label field>{t('BONUS')}</Label>
            <Label field>{t('SAVE_DC')}</Label>
        </Flex>
    </Flex>
);

SpellsHeader.propTypes = {
    t: PropTypes.func.isRequired
};

export default translate(['spells'])(SpellsHeader);
