import {Element, Flex} from '../basic';

import Header from './header.component';
import PropTypes from 'prop-types';
import React from 'react';
import Spell from './spell.component';
import styled from 'styled-components';

const StyledElement = styled(Element)`
    width: 234px;
`;

const Spells = ({
    onBonusChange,
    onKnownChange,
    onPerDayChange,
    onSaveDcChange,
    onSpellsReset,
    spells
}) => {
    return (
        <StyledElement onReset={onSpellsReset}>
            <Flex
                direction="column"
                spaced
            >
                <Header />
                {spells.map((spell, i) => (
                    <Spell
                        key={i}
                        level={i}
                        onBonusChange={onBonusChange}
                        onKnownChange={onKnownChange}
                        onPerDayChange={onPerDayChange}
                        onSaveDcChange={onSaveDcChange}
                        spell={spell}
                    />
                ))}
            </Flex>
        </StyledElement>
    );
};

Spells.propTypes = {
    onBonusChange: PropTypes.func.isRequired,
    onKnownChange: PropTypes.func.isRequired,
    onPerDayChange: PropTypes.func.isRequired,
    onSaveDcChange: PropTypes.func.isRequired,
    onSpellsReset: PropTypes.func.isRequired,
    spells: PropTypes.arrayOf(
        PropTypes.shape({
            known: PropTypes.string,
            perDay: PropTypes.string,
            bonus: PropTypes.string,
            saveDc: PropTypes.string
        })
    ).isRequired
};

export default Spells;
