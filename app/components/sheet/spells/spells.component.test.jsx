import React from 'react';
import Spells from './spells.component';
import {mount} from 'enzyme';
import renderer from 'react-test-renderer';

/* eslint-disable react/jsx-no-bind */
/* eslint-disable react/jsx-handler-names */
describe('Sheet spells tests', () => {
    test('Should match previous Spells snapshot', () => {
        const Rendered = renderer.create(
            <Spells
                onBonusChange={() => null}
                onKnownChange={() => null}
                onPerDayChange={() => null}
                onSaveDcChange={() => null}
                onSpellsReset={() => null}
                spells={[...Array(10)].map(() => ({}))}
            />
        );
        expect(Rendered).toMatchSnapshot();
    });

    test('Should check spells component integration', () => {
        const mockResetClick = jest.fn();
        const fields = [
            {text: 'KNOWN', fun: jest.fn(), value: Math.random()},
            {text: 'PER_DAY', fun: jest.fn(), value: Math.random()},
            {text: 'BONUS', fun: jest.fn(), value: Math.random()},
            {text: 'SAVE_DC', fun: jest.fn(), value: Math.random()}
        ];

        const component = mount(
            <Spells
                onBonusChange={fields[2].fun}
                onKnownChange={fields[0].fun}
                onPerDayChange={fields[1].fun}
                onSaveDcChange={fields[3].fun}
                onSpellsReset={mockResetClick}
                spells={[...Array(10)].map(() => ({}))}
            />
        );
        component
            .find('.fa-eraser')
            .last()
            .simulate('click');
        expect(mockResetClick).toBeCalled();

        component.find('input').forEach((input, i) => {
            const index = i % 4;
            input.simulate('change', {
                target: {name: 'change', value: fields[index].value + index}
            });
            input.simulate('blur', {target: {name: 'blur'}});
        });
        fields.forEach((field, i) => {
            expect(field.fun).toHaveBeenCalledTimes(10);
            for (let j = 0; j < 10; j++) {
                expect(field.fun.mock.calls[j][0]).toBe(j);
                expect(field.fun.mock.calls[j][1]).toBe(field.value + i);
            }
        });
    });
});
