import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

const RaceList = ({t}) => {
    return (
        <datalist id="raceList">
            {['DWARF', 'ELF', 'GNOME', 'HALF_ELF', 'HALF_ORC', 'HALFLING', 'HUMAN']
                .map((race) => t(race))
                .sort()
                .map((race) => <option
                    key={race}
                    value={race}
                               />)}
        </datalist>
    );
};

RaceList.propTypes = {
    t: PropTypes.func.isRequired
};

export default translate(['races'])(RaceList);
