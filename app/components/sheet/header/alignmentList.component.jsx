import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

const AlignmentList = ({t}) => {
    return (
        <datalist id="alignmentList">
            {[
                'LAWFUL_GOOD',
                'NEUTRAL_GOOD',
                'CHAOTIC_GOOD',
                'LAWFUL_NEUTRAL',
                'NEUTRAL',
                'CHAOTIC_NEUTRAL',
                'LAWFUL_EVIL',
                'NEUTRAL_EVIL',
                'CHAOTIC_EVIL'
            ].map((race) => <option
                key={race}
                value={t(race)}
                            />)}
        </datalist>
    );
};

AlignmentList.propTypes = {
    t: PropTypes.func.isRequired
};

export default translate(['alignments'])(AlignmentList);
