import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

const ClassList = ({t}) => {
    return (
        <datalist id="classList">
            {[
                'BARBARIAN',
                'BARD',
                'CLERIC',
                'DRUID',
                'FIGHTER',
                'MAGE',
                'MONK',
                'PALADIN',
                'RANGER',
                'SORCERER',
                'ROGUE',
                'WARLOCK'
            ]
                .map((className) => t(className))
                .sort()
                .map((className) => <option
                    key={className}
                    value={className}
                                    />)}
        </datalist>
    );
};

ClassList.propTypes = {
    t: PropTypes.func.isRequired
};

export default translate(['classes'])(ClassList);
