import {Flex, Input} from '../basic';

import AlignmentList from './alignmentList.component';
import ClassList from './classList.component';
import GenderList from './genderList.component';
import PropTypes from 'prop-types';
import RaceList from './raceList.component';
import React from 'react';
import SizeList from './sizeList.component';
import styled from 'styled-components';
import {translate} from 'react-i18next';

const Container = styled.div`
    padding: 10px;
    grid-column: span 5;
    grid-row: span 3;
`;

const Header = ({
    basic,
    onUpdateAge,
    onUpdateAlignment,
    onUpdateClass,
    onUpdateDeity,
    onUpdateEyes,
    onUpdateGender,
    onUpdateHair,
    onUpdateHeight,
    onUpdateLevel,
    onUpdateName,
    onUpdatePlayer,
    onUpdateRace,
    onUpdateSize,
    onUpdateWeight,
    t
}) => (
    <Container>
        <Flex spaced>
            <Input
                label={t('NAME')}
                onChange={onUpdateName}
                value={basic.name}
            />
            <Input
                label={t('PLAYER')}
                onChange={onUpdatePlayer}
                value={basic.player}
            />
        </Flex>
        <Flex spaced>
            <Input
                label={t('CLASS')}
                list="classList"
                onChange={onUpdateClass}
                value={basic.class}
            />
            <Input
                label={t('RACE')}
                list="raceList"
                onChange={onUpdateRace}
                value={basic.race}
            />
            <Input
                label={t('ALIGNMENT')}
                list="alignmentList"
                onChange={onUpdateAlignment}
                value={basic.alignment}
            />
            <Input
                label={t('DEITY')}
                onChange={onUpdateDeity}
                value={basic.deity}
            />
        </Flex>
        <Flex spaced>
            <Input
                label={t('LEVEL')}
                onChange={onUpdateLevel}
                value={basic.level}
            />
            <Input
                label={t('SIZE')}
                list="sizeList"
                onChange={onUpdateSize}
                value={basic.size}
            />
            <Input
                label={t('AGE')}
                onChange={onUpdateAge}
                value={basic.age}
            />
            <Input
                label={t('GENDER')}
                list="genderList"
                onChange={onUpdateGender}
                value={basic.gender}
            />
            <Input
                label={t('HEIGHT')}
                onChange={onUpdateHeight}
                value={basic.height}
            />
            <Input
                label={t('WEIGHT')}
                onChange={onUpdateWeight}
                value={basic.weight}
            />
            <Input
                label={t('EYES')}
                onChange={onUpdateEyes}
                value={basic.eyes}
            />
            <Input
                label={t('HAIR')}
                onChange={onUpdateHair}
                value={basic.hair}
            />
        </Flex>
        <RaceList />
        <AlignmentList />
        <GenderList />
        <SizeList />
        <ClassList />
    </Container>
);

Header.propTypes = {
    basic: PropTypes.shape({
        name: PropTypes.string,
        player: PropTypes.string,
        class: PropTypes.string,
        race: PropTypes.string,
        alignment: PropTypes.string,
        deity: PropTypes.string,
        level: PropTypes.number,
        size: PropTypes.string,
        age: PropTypes.string,
        gender: PropTypes.string,
        height: PropTypes.string,
        weight: PropTypes.string,
        eyes: PropTypes.string,
        hair: PropTypes.string
    }).isRequired,
    onUpdateAge: PropTypes.func.isRequired,
    onUpdateAlignment: PropTypes.func.isRequired,
    onUpdateClass: PropTypes.func.isRequired,
    onUpdateDeity: PropTypes.func.isRequired,
    onUpdateEyes: PropTypes.func.isRequired,
    onUpdateGender: PropTypes.func.isRequired,
    onUpdateHair: PropTypes.func.isRequired,
    onUpdateHeight: PropTypes.func.isRequired,
    onUpdateLevel: PropTypes.func.isRequired,
    onUpdateName: PropTypes.func.isRequired,
    onUpdatePlayer: PropTypes.func.isRequired,
    onUpdateRace: PropTypes.func.isRequired,
    onUpdateSize: PropTypes.func.isRequired,
    onUpdateWeight: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired
};

export default translate(['header'])(Header);
