import Header from './header.component';
import React from 'react';
import renderer from 'react-test-renderer';

/* eslint-disable react/jsx-no-bind */
describe('Sheet header tests', () => {
    test('Should match previous Header snapshot', () => {
        const Rendered = renderer.create(
            <Header
                basic={{}}
                onUpdateAge={() => null}
                onUpdateAlignment={() => null}
                onUpdateClass={() => null}
                onUpdateDeity={() => null}
                onUpdateEyes={() => null}
                onUpdateGender={() => null}
                onUpdateHair={() => null}
                onUpdateHeight={() => null}
                onUpdateLevel={() => null}
                onUpdateName={() => null}
                onUpdatePlayer={() => null}
                onUpdateRace={() => null}
                onUpdateSize={() => null}
                onUpdateWeight={() => null}
            />
        );
        expect(Rendered).toMatchSnapshot();
    });
});
