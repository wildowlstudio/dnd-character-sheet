import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

const SizeList = ({t}) => {
    return (
        <datalist id="sizeList">
            {[
                'FINE',
                'DIMINUTIVE',
                'TINY',
                'SMALL',
                'MEDIUM',
                'LARGE',
                'HUGE',
                'GARGANTUAN',
                'COLOSSAL'
            ].map((race) => <option
                key={race}
                value={t(race)}
                            />)}
        </datalist>
    );
};

SizeList.propTypes = {
    t: PropTypes.func.isRequired
};

export default translate(['sizes'])(SizeList);
