import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

const GenderList = ({t}) => {
    return (
        <datalist id="genderList">
            {['MALE', 'FEMALE']
                .map((gender) => t(gender))
                .sort()
                .map((gender) => <option
                    key={gender}
                    value={gender}
                                 />)}
        </datalist>
    );
};

GenderList.propTypes = {
    t: PropTypes.func.isRequired
};

export default translate(['genders'])(GenderList);
