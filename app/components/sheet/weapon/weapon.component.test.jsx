import React from 'react';
import Weapon from './weapon.component';
import {mount} from 'enzyme';
import renderer from 'react-test-renderer';

/* eslint-disable react/jsx-no-bind */
/* eslint-disable react/jsx-handler-names */
describe('Sheet weapon tests', () => {
    test('Should match previous Weapon snapshot', () => {
        const Rendered = renderer.create(
            <Weapon
                onWeaponAdd={() => null}
                onWeaponAttackBonusChange={() => null}
                onWeaponCriticalChange={() => null}
                onWeaponDamageChange={() => null}
                onWeaponDelete={() => null}
                onWeaponKindChange={() => null}
                onWeaponNameChange={() => null}
                onWeaponNotesChange={() => null}
                onWeaponRangeChange={() => null}
                onWeaponReset={() => null}
                onWeaponSizeChange={() => null}
                onWeaponWeightChange={() => null}
                weapon={{id: 0}}
            />
        );
        expect(Rendered).toMatchSnapshot();
    });
    test('Should check weapon component integration', () => {
        const mockResetClick = jest.fn();
        const mockAddClick = jest.fn();
        const mockDeleteClick = jest.fn();
        const fields = [
            {text: 'WEAPON', fun: jest.fn()},
            {text: 'ATTACK_BONUS', fun: jest.fn()},
            {text: 'DAMAGE', fun: jest.fn()},
            {text: 'CRITICAL', fun: jest.fn()},
            {text: 'RANGE', fun: jest.fn()},
            {text: 'WEIGHT', fun: jest.fn()},
            {text: 'SIZE', fun: jest.fn()},
            {text: 'KIND', fun: jest.fn()},
            {text: 'NOTES', fun: jest.fn()}
        ];
        const component = mount(
            <Weapon
                onWeaponAdd={mockAddClick}
                onWeaponAttackBonusChange={fields[1].fun}
                onWeaponCriticalChange={fields[3].fun}
                onWeaponDamageChange={fields[2].fun}
                onWeaponDelete={mockDeleteClick}
                onWeaponKindChange={fields[7].fun}
                onWeaponNameChange={fields[0].fun}
                onWeaponNotesChange={fields[8].fun}
                onWeaponRangeChange={fields[4].fun}
                onWeaponReset={mockResetClick}
                onWeaponSizeChange={fields[6].fun}
                onWeaponWeightChange={fields[5].fun}
                weapon={{id: 0}}
            />
        );
        component
            .find('.fa-plus')
            .last()
            .simulate('click');
        expect(mockAddClick).toBeCalled();

        component
            .find('.fa-times')
            .last()
            .simulate('click');
        expect(mockDeleteClick).toBeCalled();
        expect(mockDeleteClick.mock.calls[0][0]).toBe(0);

        component.find('input').forEach((input, i) => {
            input.simulate('change', {
                target: {name: 'change', value: fields[i].text}
            });
            input.simulate('blur', {target: {name: 'change'}});
            expect(fields[i].fun).toBeCalled();
            expect(fields[i].fun.mock.calls[0][0]).toBe(0);
            expect(fields[i].fun.mock.calls[0][1]).toBe(fields[i].text);
        });
    });
});
