import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

const WeaponList = ({t}) => {
    return (
        <datalist id="weaponList">
            {[
                'GAUNTLET',
                'DAGGER',
                'DAGGER_PUNCHING',
                'GAUNTLET_SPIKED',
                'MACE_LIGHT',
                'SICKLE',
                'CLUB',
                'MACE_HEAVY',
                'MORNINGSTAR',
                'SHORTSPEAR',
                'LONGSPEAR',
                'QUARTERSTAFF',
                'SPEAR',
                'CROSSBOW_HEAVY',
                'CROSSBOW_LIGHT',
                'DART',
                'JAVELIN',
                'SLING',
                'AXE_THROWING',
                'HAMMER_LIGHT',
                'HANDAXE',
                'KUKRI',
                'PICK_LIGHT',
                'SAP',
                'SHIELD_LIGHT',
                'SPIKED_ARMOR',
                'SPIKED_SHIELD_LIGHT',
                'SWORD_SHORT',
                'BATTLEAXE',
                'FLAIL',
                'LONGSWORD',
                'PICK_HEAVY',
                'RAPIER',
                'SCIMITAR',
                'SHIELD_HEAVY',
                'SPIKED_SHIELD_HEAVY',
                'TRIDENT',
                'WARHAMMER',
                'FALCHION',
                'GLAIVE',
                'GREATAXE',
                'GREATCLUB',
                'FLAIL_HEAVY',
                'GREATSWORD',
                'GUISARME',
                'HALBERD',
                'LANCE',
                'RANSEUR',
                'SCYTHE',
                'LONGBOW',
                'LONGBOW_COMPOSITE',
                'SHORTBOW',
                'SHORTBOW_COMPOSITE',
                'KAMA',
                'NUNCHAKU',
                'SAI',
                'SIANGHAM',
                'SWORD_BASTARD',
                'WARAXE_DWARVEN',
                'WHIP',
                'AXE_ORC_DOUBLE',
                'CHAIN_SPIKED',
                'FLAIL_DIRE',
                'HAMMER_GNOME_HOOKED',
                'SWORD_TWO',
                'URGROSH_DWARVEN',
                'BOLAS',
                'CROSSBOW_HAND',
                'CROSSBOW_REPEATING_HEAVY',
                'CROSSBOW_REPEATING_LIGHT',
                'NET',
                'SHURIKEN'
            ]
                .map((weaponType) => t(weaponType))
                .sort()
                .map((weaponType) => <option
                    key={weaponType}
                    value={weaponType}
                                     />)}
        </datalist>
    );
};

WeaponList.propTypes = {
    t: PropTypes.func.isRequired
};

export default translate(['weapons'])(WeaponList);
