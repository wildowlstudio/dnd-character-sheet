import PropTypes from 'prop-types';
import React from 'react';
import {translate} from 'react-i18next';

const WeaponTypes = ({t}) => {
    return (
        <datalist id="weaponTypes">
            {['BLUDGEONING', 'PIERCING', 'SLASHING']
                .map((weaponType) => t(weaponType))
                .sort()
                .map((weaponType) => <option
                    key={weaponType}
                    value={weaponType}
                                     />)}
        </datalist>
    );
};

WeaponTypes.propTypes = {
    t: PropTypes.func.isRequired
};

export default translate(['weaponTypes'])(WeaponTypes);
