import Weapon from './weapon.component.jsx';
import WeaponList from './weaponList.component';
import WeaponTypes from './weaponTypes.component';

export default Weapon;
export {WeaponTypes, WeaponList};
