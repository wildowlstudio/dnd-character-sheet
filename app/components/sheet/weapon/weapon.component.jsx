import {Element, Field, Input, OrnateFlex} from '../basic';

import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';
import {translate} from 'react-i18next';

const StyledElement = styled(Element)`
    width: 608px;
`;

class Weapon extends React.Component {
    constructor(props) {
        super(props);

        this.handleWeaponAttackBonusChange = this.handleWeaponAttackBonusChange.bind(
            this
        );
        this.handleWeaponCriticalChange = this.handleWeaponCriticalChange.bind(this);
        this.handleWeaponDamageChange = this.handleWeaponDamageChange.bind(this);
        this.handleWeaponKindChange = this.handleWeaponKindChange.bind(this);
        this.handleWeaponNameChange = this.handleWeaponNameChange.bind(this);
        this.handleWeaponNotesChange = this.handleWeaponNotesChange.bind(this);
        this.handleWeaponRangeChange = this.handleWeaponRangeChange.bind(this);
        this.handleWeaponSizeChange = this.handleWeaponSizeChange.bind(this);
        this.handleWeaponWeightChange = this.handleWeaponWeightChange.bind(this);
        this.handleWeaponDelete = this.handleWeaponDelete.bind(this);
        this.handleWeaponReset = this.handleWeaponReset.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.weapon !== this.props.weapon;
    }

    handleWeaponAttackBonusChange(value) {
        this.props.onWeaponAttackBonusChange(this.props.weapon.id, value);
    }

    handleWeaponCriticalChange(value) {
        this.props.onWeaponCriticalChange(this.props.weapon.id, value);
    }

    handleWeaponDamageChange(value) {
        this.props.onWeaponDamageChange(this.props.weapon.id, value);
    }

    handleWeaponKindChange(value) {
        this.props.onWeaponKindChange(this.props.weapon.id, value);
    }

    handleWeaponNameChange(value) {
        this.props.onWeaponNameChange(this.props.weapon.id, value);
    }

    handleWeaponNotesChange(value) {
        this.props.onWeaponNotesChange(this.props.weapon.id, value);
    }

    handleWeaponRangeChange(value) {
        this.props.onWeaponRangeChange(this.props.weapon.id, value);
    }

    handleWeaponSizeChange(value) {
        this.props.onWeaponSizeChange(this.props.weapon.id, value);
    }

    handleWeaponWeightChange(value) {
        this.props.onWeaponWeightChange(this.props.weapon.id, value);
    }

    handleWeaponDelete() {
        this.props.onWeaponDelete(this.props.weapon.id);
    }

    handleWeaponReset() {
        this.props.onWeaponReset(this.props.weapon.id);
    }

    handleClick() {
        this.props.onClick('weapon');
    }

    render() {
        const {onWeaponAdd, readOnly, weapon, t} = this.props;

        return (
            <StyledElement
                onAdd={onWeaponAdd}
                onClick={readOnly ? this.handleClick : null}
                onDelete={this.handleWeaponDelete}
                onReset={this.handleWeaponReset}
                readOnly={readOnly}
            >
                <OrnateFlex>
                    <div>
                        <Field
                            inverted
                            multiply={7}
                            ornate
                            widthFudgeFactor={12}
                        >
                            {t('WEAPON')}
                        </Field>
                        <Input
                            border="full"
                            list="weaponList"
                            onChange={this.handleWeaponNameChange}
                            readOnly={readOnly}
                            value={weapon.name}
                            widthFudgeFactor={-2}
                        />
                    </div>
                    <div>
                        <Field
                            inverted
                            multiply={3}
                        >
                            {t('ATTACK_BONUS')}
                        </Field>
                        <Input
                            border="full"
                            onChange={this.handleWeaponAttackBonusChange}
                            readOnly={readOnly}
                            value={weapon.attackBonus}
                            widthFudgeFactor={-1}
                        />
                    </div>
                    <div>
                        <Field
                            inverted
                            multiply={2}
                        >
                            {t('DAMAGE')}
                        </Field>
                        <Input
                            border="full"
                            onChange={this.handleWeaponDamageChange}
                            readOnly={readOnly}
                            value={weapon.damage}
                            widthFudgeFactor={-1}
                        />
                    </div>
                    <div>
                        <Field
                            inverted
                            multiply={2}
                        >
                            {t('CRITICAL')}
                        </Field>
                        <Input
                            border="full"
                            onChange={this.handleWeaponCriticalChange}
                            readOnly={readOnly}
                            value={weapon.critical}
                            widthFudgeFactor={-1}
                        />
                    </div>
                </OrnateFlex>
                <OrnateFlex>
                    <div>
                        <Field
                            inverted
                            multiply={2}
                        >
                            {t('RANGE')}
                        </Field>
                        <Input
                            border="full"
                            onChange={this.handleWeaponRangeChange}
                            readOnly={readOnly}
                            value={weapon.range}
                            widthFudgeFactor={-2}
                        />
                    </div>
                    <div>
                        <Field
                            inverted
                            multiply={2}
                        >
                            {t('WEIGHT')}
                        </Field>
                        <Input
                            border="full"
                            onChange={this.handleWeaponWeightChange}
                            readOnly={readOnly}
                            value={weapon.weight}
                            widthFudgeFactor={-1}
                        />
                    </div>
                    <div>
                        <Field
                            inverted
                            multiply={2}
                        >
                            {t('SIZE')}
                        </Field>
                        <Input
                            border="full"
                            onChange={this.handleWeaponSizeChange}
                            readOnly={readOnly}
                            value={weapon.size}
                            widthFudgeFactor={-1}
                        />
                    </div>
                    <div>
                        <Field
                            inverted
                            multiply={3}
                        >
                            {t('KIND')}
                        </Field>
                        <Input
                            border="full"
                            list="weaponTypes"
                            onChange={this.handleWeaponKindChange}
                            readOnly={readOnly}
                            value={weapon.kind}
                            widthFudgeFactor={-1}
                        />
                    </div>
                    <div>
                        <Field
                            inverted
                            multiply={5}
                            widthFudgeFactor={14}
                        >
                            {t('NOTES')}
                        </Field>
                        <Input
                            border="full"
                            onChange={this.handleWeaponNotesChange}
                            readOnly={readOnly}
                            value={weapon.notes}
                            widthFudgeFactor={-1}
                        />
                    </div>
                </OrnateFlex>
            </StyledElement>
        );
    }
}

Weapon.propTypes = {
    onClick: PropTypes.func,
    onWeaponAdd: PropTypes.func.isRequired,
    onWeaponAttackBonusChange: PropTypes.func.isRequired,
    onWeaponCriticalChange: PropTypes.func.isRequired,
    onWeaponDamageChange: PropTypes.func.isRequired,
    onWeaponDelete: PropTypes.func.isRequired,
    onWeaponKindChange: PropTypes.func.isRequired,
    onWeaponNameChange: PropTypes.func.isRequired,
    onWeaponNotesChange: PropTypes.func.isRequired,
    onWeaponRangeChange: PropTypes.func.isRequired,
    onWeaponReset: PropTypes.func.isRequired,
    onWeaponSizeChange: PropTypes.func.isRequired,
    onWeaponWeightChange: PropTypes.func.isRequired,
    readOnly: PropTypes.bool,
    t: PropTypes.func.isRequired,
    weapon: PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string,
        attackBonus: PropTypes.string,
        damage: PropTypes.string,
        critical: PropTypes.string,
        range: PropTypes.string,
        weight: PropTypes.string,
        size: PropTypes.string,
        kind: PropTypes.string,
        nodes: PropTypes.string
    }).isRequired
};

export default translate(['weapon'])(Weapon);
