import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';

const StyledButton = styled.button`
    background-color: ${(props) => props.theme.buttonBackgroundColor};
    border: none;
    color: ${(props) => props.theme.buttonColor};
    cursor: pointer;
    font-size: ${(props) => props.theme.buttonFontSize};
    margin: ${(props) => props.theme.buttonMargin};
    padding: ${(props) => props.theme.buttonPadding};
    text-align: center;
    text-decoration: none;
    ${({noUpper}) => (noUpper ? '' : 'text-transform: uppercase')};
    transition: ${(props) => props.theme.activeTransition};

    &:hover,
    &:focus {
        color: ${(props) => props.theme.activeColor};
    }
`;

const Button = ({children, ...others}) => {
    return <StyledButton {...others}>{children}</StyledButton>;
};

Button.propTypes = {
    children: PropTypes.node.isRequired,
    noUpper: PropTypes.bool
};

export default Button;
