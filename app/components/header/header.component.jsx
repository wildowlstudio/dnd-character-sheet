import DiceModal from './diceModal.component';
import DropdownMenu from 'react-dd-menu';
import FileSaver from 'file-saver';
import PropTypes from 'prop-types';
import React from 'react';
import {loadSheet} from '../../actions/global.actions';
import media from '../../utils/style.utils';
import store from '../../utils/store.util';
import styled from 'styled-components';
import {translate} from 'react-i18next';

const HeaderWrapper = styled.header`
    display: flex;
    background-color: ${(props) => props.theme.navbarBackgroundColor};
    align-items: center;
    justify-content: space-between;

    > a {
        color: ${(props) => props.theme.navbarColor};
        transition: ${(props) => props.theme.activeTransition};
        cursor: pointer;
        padding: 14px 16px;
        text-decoration: none;
    }

    > a:hover,
    > a:focus-within {
        color: ${(props) => props.theme.activeColor};
    }

    > a > span {
        color: #a1a9b0;
    }

    > ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        display: flex;
    }

    > ul > li {
        display: block;
        padding: 14px 16px;
        color: white;
        button {
            color: ${(props) => props.theme.navbarColor};
            text-decoration: none;
            transition: ${(props) => props.theme.activeTransition};
            background-color: transparent;
            border: none;
            cursor: pointer;
            font-size: initial;
        }
    }

    > ul > li button:hover,
    > ul > li button:focus,
    > ul > li:hover,
    > ul > li:focus {
        color: ${(props) => props.theme.activeColor};
    }

    ${media.print`
        display: none;
    `};
`;

const HiddenInput = styled.input`
    display: none;
`;

const Header = class extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            dropdownOpen: false
        };

        this.inputRef = this.inputRef.bind(this);
        this.handleFileClick = this.handleFileClick.bind(this);
        this.handleFileLoad = this.handleFileLoad.bind(this);
        this.handleDiceRoll = this.handleDiceRoll.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleDropdownOpen = this.handleDropdownOpen.bind(this);
    }

    inputRef(ref) {
        if (ref) {
            this.input = ref;
        }
    }

    handleFileSave() {
        const sheet = store.getState().sheet;
        const blob = new Blob([JSON.stringify(sheet)], {type: 'application/json'});
        FileSaver.saveAs(
            blob,
            ((sheet && sheet.basic && sheet.basic.name) || 'character') + '.json'
        );
    }

    handleFileClick() {
        this.input.click();
    }

    handleFileLoad(event) {
        const reader = new FileReader();
        reader.onload = function() {
            const sheet = JSON.parse(reader.result);
            store.dispatch(loadSheet(sheet));
        };
        reader.readAsText(event.target.files[0]);
    }

    handleDiceRoll() {
        this.setState({open: true});
    }

    handleClose() {
        this.setState({open: false, dropdownOpen: false});
    }

    handleDropdownOpen() {
        this.setState({dropdownOpen: true});
    }

    render() {
        const {onResetSheet, t} = this.props;

        return (
            <HeaderWrapper className="not-printable">
                <a href="https://www.wild-owl.com">
                    {'WILD '}
                    <span>{'OWL '}</span>
                    {'STUDIO'}
                </a>
                <ul>
                    <li>
                        <button onClick={onResetSheet}>{t('NEW')}</button>
                    </li>
                    <li>
                        <button onClick={this.handleFileClick}>{t('LOAD')}</button>
                        <HiddenInput
                            innerRef={this.inputRef}
                            onChange={this.handleFileLoad}
                            type="file"
                        />
                    </li>
                    <li>
                        <button onClick={this.handleFileSave}>{t('SAVE')}</button>
                    </li>
                    <li>
                        <button onClick={this.handleDiceRoll}>{t('ROLL_DICE')}</button>
                    </li>
                    <li>
                        <DropdownMenu
                            align="left"
                            close={this.handleClose} // eslint-disable-line react/jsx-handler-names
                            isOpen={this.state.dropdownOpen}
                            toggle={
                                <button
                                    aria-label={t('LANGUAGE')}
                                    className="fa fa-globe"
                                    onClick={this.handleDropdownOpen}
                                />
                            }
                        >
                            <li>
                                <button>{'English'}</button>
                            </li>
                            <li>
                                <button>{'Polski'}</button>
                            </li>
                        </DropdownMenu>
                    </li>
                    <DiceModal
                        onClose={this.handleClose}
                        open={this.state.open}
                    />
                </ul>
            </HeaderWrapper>
        );
    }
};

Header.propTypes = {
    onResetSheet: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired
};

export default translate(['common'])(Header);
