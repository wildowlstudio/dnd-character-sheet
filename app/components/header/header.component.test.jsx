import Header from './header.component';
import React from 'react';
import {mount} from 'enzyme';
import renderer from 'react-test-renderer';

/* eslint-disable react/jsx-no-bind */
describe('Header tests', () => {
    test('Should match previous Header snapshot', () => {
        const Rendered = renderer.create(<Header onResetSheet={() => null} />);
        expect(Rendered).toMatchSnapshot();
    });
    test('Should check header component integration', () => {
        const mockResetClick = jest.fn();
        const component = mount(<Header onResetSheet={mockResetClick} />);
        component.find('button').forEach((element) => {
            const props = element.props();
            switch (props.children) {
                case 'NEW':
                    element.simulate('click');
                    expect(mockResetClick).toHaveBeenCalledTimes(1);
            }
        });
    });
});
