import {Flex, Input} from '../sheet/basic';

import Button from '../button';
import Modal from '../modal';
import PropTypes from 'prop-types';
import React from 'react';
import diceRoll from '../../utils/dice.util';
import styled from 'styled-components';
import {translate} from 'react-i18next';

const RecentList = styled.ul`
    list-style: none;
    padding: 0;
`;

let recentDice =
    (window.localStorage &&
        window.localStorage.recentDice &&
        JSON.parse(window.localStorage.recentDice)) ||
    [];

class Dice extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            result: []
        };

        this.handleClick = this.handleClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleRoll = this.handleRoll.bind(this);
    }

    handleClick(event) {
        this.handleRoll(event, event.target.innerText);
    }

    handleChange(value) {
        this.query = value;
    }

    handleRoll(event, value) {
        const query = value || this.query;
        try {
            const result = diceRoll(query);
            recentDice = recentDice.filter((value) => value !== query);
            recentDice.unshift(query);
            if (recentDice.length > 5) {
                recentDice.length = 5;
            }
            this.setState({result, query, error: false});
            window.localStorage.recentDice = JSON.stringify(recentDice);
        } catch (err) {
            this.setState({query, error: true, result: []});
        }
    }

    render() {
        const {t, open, onClose} = this.props;
        const {query, result, error} = this.state;
        return (
            <Modal
                footer={<Button onClick={this.handleRoll}>{t('ROLL')}</Button>}
                header={t('ROLL_DICE')}
                onClose={onClose}
                open={open}
            >
                <Flex>
                    <div>
                        <Input
                            onChange={this.handleChange}
                            onEnter={this.handleRoll}
                            value={query}
                        />
                        <RecentList>
                            {recentDice.map((recent) => (
                                <li key={recent}>
                                    <Button
                                        noUpper
                                        onClick={this.handleClick}
                                    >
                                        {recent}
                                    </Button>
                                </li>
                            ))}
                        </RecentList>
                    </div>
                    <div>
                        {query ? <span>{query + ':'}</span> : null}
                        <br />
                        {error ? <span>{t('UNKNOWN_QUERY')}</span> : null}
                        <ol>
                            {result.map((result, i) => (
                                <li key={i.toString() + result}>{result}</li>
                            ))}
                        </ol>
                    </div>
                </Flex>
            </Modal>
        );
    }
}

Dice.propTypes = {
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
    t: PropTypes.func.isRequired
};

Dice.defaultProps = {
    result: []
};

export default translate(['common'])(Dice);
