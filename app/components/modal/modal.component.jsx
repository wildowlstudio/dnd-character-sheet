import PropTypes from 'prop-types';
import React from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';

const ModalWrapper = styled.div`
    position: fixed;
    z-index: 1;
    padding-top: 100px;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    overflow: auto;
    background-color: rgba(0, 0, 0, 0.4);
`;

const Content = styled.div`
    position: relative;
    background-color: #fefefe;
    margin: auto;
    padding: 0;
    border: 1px solid #888;
    max-width: 80%;
    max-height: 80%;
    display: flex;
    flex-direction: column;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
    animation-name: animateEntry;
    animation-duration: 0.5s;
    ${({width}) => (width ? `width: ${width}px` : '')};
`;

const Header = styled.div`
    padding: 2px 16px;
    background-color: ${(props) => props.theme.modalHeaderBackgroundColor};
    color: ${(props) => props.theme.modalHeaderColor};

    > h2 {
        margin: 7px 0px;
    }
`;

const Body = styled.div`
    padding: 2px 16px;
    position: relative;
    overflow: auto;
    flex-grow: 1;
`;

const Footer = styled.div`
    padding: 2px 16px;
    background-color: ${(props) => props.theme.modalFooterBackgroundColor};
    position: relative;
    display: flex;
    align-items: center;
    min-height: 36px;
    justify-content: flex-end;
`;

const Close = styled.span`
    color: ${(props) => props.theme.modalHeaderColor};
    float: right;
    font-size: 28px;
    margin-top: 5px;

    &:hover,
    &:focus {
        color: ${(props) => props.theme.activeColor};
        text-decoration: none;
        cursor: pointer;
    }
`;

const modalRoot = document.getElementById('modal-root');

class Modal extends React.PureComponent {
    constructor(props) {
        super(props);
        this.el = document.createElement('div');

        this.handleClose = this.handleClose.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.open !== this.props.open) {
            if (nextProps.open) {
                modalRoot.appendChild(this.el);
            } else {
                modalRoot.removeChild(this.el);
            }
        }
    }

    componentWillUnmount() {
        if (this.props.open) {
            modalRoot.removeChild(this.el);
        }
    }

    handlePropagation(event) {
        event.stopPropagation();
    }

    handleClose(event) {
        event.stopPropagation();
        this.props.onClose();
    }

    render() {
        const {footer, children, header} = this.props;
        return ReactDOM.createPortal(
            <ModalWrapper onClick={this.handleClose}>
                <Content onClick={this.handlePropagation}>
                    <Header>
                        <Close
                            className="fa fa-times"
                            onClick={this.handleClose}
                        />
                        <h2>{header}</h2>
                    </Header>
                    <Body>{children}</Body>
                    <Footer>{footer}</Footer>
                </Content>
            </ModalWrapper>,
            this.el
        );
    }
}

Modal.propTypes = {
    children: PropTypes.node.isRequired,
    footer: PropTypes.node.isRequired,
    header: PropTypes.string.isRequired,
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired
};

export default Modal;
