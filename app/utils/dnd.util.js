export function isDroppedBelow(event) {
    const hoverBoundingRect = event.target.getBoundingClientRect();
    const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;
    const hoverClientY = event.clientY - hoverBoundingRect.top;
    return hoverClientY > hoverMiddleY;
}

export function isDroppedAfter(event) {
    const hoverBoundingRect = event.target.getBoundingClientRect();
    const hoverMiddleX = (hoverBoundingRect.right - hoverBoundingRect.left) / 2;
    const hoverClientX = event.clientX - hoverBoundingRect.left;
    return hoverClientX > hoverMiddleX;
}

export function handleDragOver(event) {
    event.preventDefault();
}
