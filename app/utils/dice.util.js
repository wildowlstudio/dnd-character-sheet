const diceRegexp = /^(\d*)[dDkK](\d+)([+-]\d+)?$/;

export default function rollDice(string) {
    const parsed = string.match(diceRegexp);
    if (!parsed) {
        throw new Error();
    }
    const dices = +parsed[1] || 1;
    const dice = +parsed[2];
    const plus = +parsed[3] || 0;
    if (!dice) {
        throw new Error();
    }
    return [...Array(dices)].map(() => Math.floor(Math.random() * dice) + 1 + plus);
}
