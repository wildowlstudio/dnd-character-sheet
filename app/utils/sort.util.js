export default function sortBy(orderBy, source, desc) {
    const array = Object.assign([], source);
    array.sort((a, b) => {
        if (!a[orderBy]) {
            if (!b[orderBy]) {
                return 0;
            }
            return 1;
        } else if (!b[orderBy]) {
            return -1;
        } else if (!desc && typeof a[orderBy] === 'string') {
            return a[orderBy].localeCompare(b[orderBy]);
        } else if (desc && typeof b[orderBy] === 'string') {
            return b[orderBy].localeCompare(a[orderBy]);
        } else {
            return desc ? b[orderBy] - a[orderBy] : a[orderBy] - b[orderBy];
        }
    });
    return array;
}
