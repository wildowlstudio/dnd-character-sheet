import {css} from 'styled-components';

const media = {
    oneRow: (...args) => css`
        @media only screen and (max-width: 1050px) {
            ${css(...args)};
        }
    `,
    avatarWrap: (...args) => css`
        @media only screen and (max-width: 1019px) {
            ${css(...args)};
        }
    `,
    print: (...args) => css`
        @media print {
            ${css(...args)};
        }
    `
};

export default media;
