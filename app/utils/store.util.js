import appReducer from '../reducers/app.reducer';
import {createStore} from 'redux';
import {loadSheet} from '../actions/global.actions';

const store = createStore(
    appReducer,
    window.devToolsExtension && window.devToolsExtension()
);

if (window.localStorage && window.localStorage.getItem('sheet')) {
    store.dispatch(loadSheet(JSON.parse(localStorage.getItem('sheet'))));
}

export default store;
