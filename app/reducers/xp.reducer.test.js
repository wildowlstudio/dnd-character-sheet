import {loadSheet, resetSheet} from '../actions/global.actions';
import {resetXp, updateXp} from '../actions/xp.actions';

import reducer from './xp.reducer';

describe('Reducer tests', () => {
    describe('Xp reducer tests', () => {
        test('Should return the initial state', () => {
            expect(reducer(undefined, {})).toEqual('');
        });
        test('Should handle RESET_SHEET', () => {
            const state = 'Test';
            const newState = reducer(state, resetSheet());
            expect(newState).toEqual('');
            expect(newState).not.toBe(state);
        });
        test('Should handle RESET_XP', () => {
            const state = 'Test';
            const newState = reducer(state, resetXp());
            expect(newState).toEqual('');
            expect(newState).not.toBe(state);
        });
        test('Should handle LOAD_SHEET', () => {
            const state = 'test1';
            const sheetToLoad = {
                xp: 'test2'
            };
            const newState = reducer(state, loadSheet(sheetToLoad));
            expect(newState).toEqual(sheetToLoad.xp);
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_XP', () => {
            const state = '';
            const newState = reducer(state, updateXp('Test'));
            expect(newState).toEqual('Test');
            expect(reducer('Test', updateXp('Test2'))).toEqual('Test2');
            expect(newState).not.toBe(state);
        });
    });
});
