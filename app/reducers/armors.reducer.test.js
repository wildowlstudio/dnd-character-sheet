import {
    addArmor,
    deleteArmor,
    resetArmor,
    updateArmorBonus,
    updateArmorMaxDex,
    updateArmorMaxSpeed,
    updateArmorName,
    updateArmorNotes,
    updateArmorPenalty,
    updateArmorSpellFailure,
    updateArmorType,
    updateArmorWeight
} from '../actions/armor.actions';
import {loadSheet, resetSheet} from '../actions/global.actions';

import reducer from './armors.reducer';

describe('Reducer tests', () => {
    describe('Armors reducer tests', () => {
        test('Should return the initial state', () => {
            expect(reducer(undefined, {})).toEqual([{id: 0}]);
        });
        test('Should handle RESET_SHEET', () => {
            const state = [{test: 'Test'}];
            const newState = reducer(state, resetSheet());
            expect(newState).toEqual([{id: 0}]);
            expect(newState).not.toBe(state);
        });
        test('Should handle LOAD_SHEET', () => {
            const state = [];
            const sheetToLoad = {
                armors: [{id: 1, test: 'test'}]
            };
            const newState = reducer(state, loadSheet(sheetToLoad));
            expect(newState).toEqual(sheetToLoad.armors);
            expect(reducer(state, loadSheet([]))).toEqual([{id: 0}]);
            expect(newState).not.toBe(state);
            expect(newState).not.toBe(sheetToLoad.armors);
        });
        test('Should handle UPDATE_ARMOR_NAME', () => {
            const state = [{id: 0, name: 'test1'}];
            const newState = reducer(state, updateArmorName(0, 'test2'));
            expect(newState).toEqual([{id: 0, name: 'test2'}]);
            expect(
                reducer([{id: 0, name: 'test3'}], updateArmorName(0, 'test4'))
            ).toEqual([{id: 0, name: 'test4'}]);
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_ARMOR_BONUS', () => {
            const state = [{id: 0, bonus: 'test1'}];
            const newState = reducer(state, updateArmorBonus(0, 'test2'));
            expect(newState).toEqual([{id: 0, bonus: 'test2'}]);
            expect(
                reducer([{id: 0, bonus: 'test3'}], updateArmorBonus(0, 'test4'))
            ).toEqual([{id: 0, bonus: 'test4'}]);
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_ARMOR_MAX_DEX', () => {
            const state = [{id: 0, maxDex: 'test1'}];
            const newState = reducer(state, updateArmorMaxDex(0, 'test2'));
            expect(newState).toEqual([{id: 0, maxDex: 'test2'}]);
            expect(
                reducer([{id: 0, maxDex: 'test3'}], updateArmorMaxDex(0, 'test4'))
            ).toEqual([{id: 0, maxDex: 'test4'}]);
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_ARMOR_PENALTY', () => {
            const state = [{id: 0, penalty: 'test1'}];
            const newState = reducer(state, updateArmorPenalty(0, 'test2'));
            expect(newState).toEqual([{id: 0, penalty: 'test2'}]);
            expect(
                reducer([{id: 0, penalty: 'test3'}], updateArmorPenalty(0, 'test4'))
            ).toEqual([{id: 0, penalty: 'test4'}]);
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_ARMOR_MAX_SPEED', () => {
            const state = [{id: 0, maxSpeed: 'test1'}];
            const newState = reducer(state, updateArmorMaxSpeed(0, 'test2'));
            expect(newState).toEqual([{id: 0, maxSpeed: 'test2'}]);
            expect(
                reducer([{id: 0, maxSpeed: 'test3'}], updateArmorMaxSpeed(0, 'test4'))
            ).toEqual([{id: 0, maxSpeed: 'test4'}]);
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_ARMOR_WEIGHT', () => {
            const state = [{id: 0, weight: 'test1'}];
            const newState = reducer(state, updateArmorWeight(0, 'test2'));
            expect(newState).toEqual([{id: 0, weight: 'test2'}]);
            expect(
                reducer([{id: 0, weight: 'test3'}], updateArmorWeight(0, 'test4'))
            ).toEqual([{id: 0, weight: 'test4'}]);
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_ARMOR_TYPE', () => {
            const state = [{id: 0, type: 'test1'}];
            const newState = reducer(state, updateArmorType(0, 'test2'));
            expect(newState).toEqual([{id: 0, type: 'test2'}]);
            expect(
                reducer([{id: 0, type: 'test3'}], updateArmorType(0, 'test4'))
            ).toEqual([{id: 0, type: 'test4'}]);
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_ARMOR_SPELL_FAILURE', () => {
            const state = [{id: 0, spellFailure: 'test1'}];
            const newState = reducer(state, updateArmorSpellFailure(0, 'test2'));
            expect(newState).toEqual([{id: 0, spellFailure: 'test2'}]);
            expect(
                reducer(
                    [{id: 0, spellFailure: 'test3'}],
                    updateArmorSpellFailure(0, 'test4')
                )
            ).toEqual([{id: 0, spellFailure: 'test4'}]);
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_ARMOR_NOTES', () => {
            const state = [{id: 0, notes: 'test1'}];
            const newState = reducer(state, updateArmorNotes(0, 'test2'));
            expect(newState).toEqual([{id: 0, notes: 'test2'}]);
            expect(
                reducer([{id: 0, notes: 'test3'}], updateArmorNotes(0, 'test4'))
            ).toEqual([{id: 0, notes: 'test4'}]);
            expect(newState).not.toBe(state);
        });
        test('Should handle ADD_ARMOR', () => {
            const state = [{id: 0, notes: 'test'}];
            const newState = reducer(state, addArmor());
            expect(newState).toEqual([{id: 0, notes: 'test'}, {id: 1}]);
            expect(reducer([{id: 0, notes: 'test'}], addArmor())).toEqual([
                {id: 0, notes: 'test'},
                {id: 1}
            ]);
            expect(newState).not.toBe(state);
        });
        test('Should handle RESET_ARMOR', () => {
            const state = [{id: 0, notes: 'test'}];
            const newState = reducer(state, resetArmor(0));
            expect(newState).toEqual([{id: 0}]);
            expect(
                reducer(
                    [{id: 0, notes: 'test'}, {id: 1, notes: 'test2'}],
                    resetArmor(0)
                )
            ).toEqual([{id: 0}, {id: 1, notes: 'test2'}]);
            expect(newState).not.toBe(state);
        });
        test('Should handle DELETE_ARMOR', () => {
            const state = [{id: 0, notes: 'test'}];
            const newState = reducer(state, deleteArmor(0));
            expect(newState).toEqual([{id: 0}]);
            expect(
                reducer(
                    [{id: 0, notes: 'test'}, {id: 1, notes: 'test2'}],
                    deleteArmor(0)
                )
            ).toEqual([{id: 1, notes: 'test2'}]);
            expect(newState).not.toBe(state);
        });
    });
});
