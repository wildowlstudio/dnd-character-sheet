import {actionTypes} from '../actions/weapon.actions';
import {actionTypes as globalActionTypes} from '../actions/global.actions';

export default function reducer(state = [{id: 0}], action) {
    switch (action.type) {
        case globalActionTypes.RESET_SHEET:
            return [{id: 0}];
        case globalActionTypes.LOAD_SHEET:
            return (
                (action.sheet &&
                    action.sheet.weapons &&
                    action.sheet.weapons.length &&
                    Object.assign([], action.sheet.weapons)) || [{id: 0}]
            );
        case actionTypes.UPDATE_WEAPON_NAME:
            return state.map((weapon) => {
                if (weapon.id === action.id) {
                    return Object.assign({}, weapon, {name: action.value});
                }
                return weapon;
            });
        case actionTypes.UPDATE_WEAPON_ATTACK_BONUS:
            return state.map((weapon) => {
                if (weapon.id === action.id) {
                    return Object.assign({}, weapon, {attackBonus: action.value});
                }
                return weapon;
            });
        case actionTypes.UPDATE_WEAPON_DAMAGE:
            return state.map((weapon) => {
                if (weapon.id === action.id) {
                    return Object.assign({}, weapon, {damage: action.value});
                }
                return weapon;
            });
        case actionTypes.UPDATE_WEAPON_CRITICAL:
            return state.map((weapon) => {
                if (weapon.id === action.id) {
                    return Object.assign({}, weapon, {critical: action.value});
                }
                return weapon;
            });
        case actionTypes.UPDATE_WEAPON_RANGE:
            return state.map((weapon) => {
                if (weapon.id === action.id) {
                    return Object.assign({}, weapon, {range: action.value});
                }
                return weapon;
            });
        case actionTypes.UPDATE_WEAPON_WEIGHT:
            return state.map((weapon) => {
                if (weapon.id === action.id) {
                    return Object.assign({}, weapon, {weight: action.value});
                }
                return weapon;
            });
        case actionTypes.UPDATE_WEAPON_SIZE:
            return state.map((weapon) => {
                if (weapon.id === action.id) {
                    return Object.assign({}, weapon, {size: action.value});
                }
                return weapon;
            });
        case actionTypes.UPDATE_WEAPON_KIND:
            return state.map((weapon) => {
                if (weapon.id === action.id) {
                    return Object.assign({}, weapon, {kind: action.value});
                }
                return weapon;
            });
        case actionTypes.UPDATE_WEAPON_NOTES:
            return state.map((weapon) => {
                if (weapon.id === action.id) {
                    return Object.assign({}, weapon, {notes: action.value});
                }
                return weapon;
            });
        case actionTypes.ADD_WEAPON:
            return state.concat({
                id: state.reduce((acc, weapon) => {
                    if (weapon.id >= acc) {
                        return weapon.id + 1;
                    }
                    return acc;
                }, 0)
            });
        case actionTypes.RESET_WEAPON:
            return state.map((weapon) => {
                if (weapon.id === action.id) {
                    return {id: weapon.id};
                }
                return weapon;
            });
        case actionTypes.DELETE_WEAPON: {
            const newState = state.filter((weapon) => weapon.id !== action.id);
            if (!newState.length) {
                newState.push({id: 0});
            }
            return newState;
        }
        default:
            return state;
    }
}
