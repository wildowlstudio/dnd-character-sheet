import {
    addLanguage,
    cleanLanguages,
    deleteLanguage,
    moveLanguage,
    resetLanguages,
    updateLanguage
} from '../actions/language.actions';
import {loadSheet, resetSheet} from '../actions/global.actions';

import reducer from './languages.reducer';

describe('Reducer tests', () => {
    describe('Languages reducer tests', () => {
        test('Should return the initial state', () => {
            expect(reducer(undefined, {})).toEqual([]);
        });
        test('Should handle RESET_SHEET', () => {
            const state = [{name: 'test'}];
            const newState = reducer(state, resetSheet());
            expect(newState).toEqual([]);
            expect(newState).not.toBe(state);
        });
        test('Should handle RESET_LANGUAGES', () => {
            const state = [{name: 'test'}];
            const newState = reducer(state, resetLanguages());
            expect(newState).toEqual([]);
            expect(newState).not.toBe(state);
        });
        test('Should handle LOAD_SHEET', () => {
            const state = [{name: 'test'}];
            const sheetToLoad = {
                languages: [{name: 'test2'}]
            };
            const newState = reducer(state, loadSheet(sheetToLoad));
            expect(newState).toEqual(sheetToLoad.languages);
            expect(newState).not.toBe(state);
            expect(newState).not.toBe(sheetToLoad.languages);
        });
        test('Should handle ADD_LANGUAGE', () => {
            const state = [{id: 0, name: 'test'}];
            const newState = reducer(state, addLanguage());
            expect(newState).toEqual([{id: 0, name: 'test'}, {id: 1}]);
            expect(newState).not.toBe(state);
            expect(reducer([], addLanguage())).toEqual([{id: 0}]);
        });

        test('Should handle CLEAN_LANGUAGES', () => {
            const state = [
                {id: 0, value: 'test'},
                {id: 2},
                {id: 1, value: 'test2'},
                {id: 3},
                {id: 4}
            ];
            const newState = reducer(state, cleanLanguages(1));
            expect(newState).toEqual([
                {id: 0, value: 'test'},
                {id: 1, value: 'test2'}
            ]);
            expect(newState).not.toBe(state);
        });

        test('Should handle DELETE_LANGUAGE', () => {
            const state = [{id: 0, name: 'test'}, {id: 1, name: 'test2'}];
            const newState = reducer(state, deleteLanguage(1));
            expect(newState).toEqual([{id: 0, name: 'test'}]);
            expect(newState).not.toBe(state);
        });

        test('Should handle MOVE_LANGUAGE', () => {
            const state = [
                {id: 0, name: 'test'},
                {id: 1, name: 'test2'},
                {id: 2, name: 'aTest'}
            ];
            const newState = reducer(state, moveLanguage(0, true, 2));
            expect(newState).not.toBe(state);
            expect(newState).toEqual([
                {id: 1, name: 'test2'},
                {id: 2, name: 'aTest'},
                {id: 0, name: 'test'}
            ]);
            expect(reducer(state, moveLanguage(0, false, 2))).toEqual([
                {id: 1, name: 'test2'},
                {id: 0, name: 'test'},
                {id: 2, name: 'aTest'}
            ]);
            expect(reducer(state, moveLanguage(0, true, 'new'))).toEqual([
                {id: 1, name: 'test2'},
                {id: 2, name: 'aTest'},
                {id: 0, name: 'test'}
            ]);
            expect(reducer(state, moveLanguage(0, false, 'new'))).toEqual([
                {id: 1, name: 'test2'},
                {id: 2, name: 'aTest'},
                {id: 0, name: 'test'}
            ]);
        });

        test('Should handle UPDATE_LANGUAGE', () => {
            const state = [{id: 0, name: 'test'}, {id: 1, name: 'test2'}];
            const newState = reducer(state, updateLanguage(1, 'test3'));
            expect(newState).not.toBe(state);
            expect(reducer(state, updateLanguage(1, 'test3'))).toEqual([
                {id: 0, name: 'test'},
                {id: 1, name: 'test2', value: 'test3'}
            ]);
            expect(reducer(state, updateLanguage('new', 'test3'))).toEqual([
                {id: 0, name: 'test'},
                {id: 1, name: 'test2'},
                {id: 2, value: 'test3'}
            ]);
        });
    });
});
