import {
    addWeapon,
    deleteWeapon,
    resetWeapon,
    updateWeaponAttackBonus,
    updateWeaponCritical,
    updateWeaponDamage,
    updateWeaponKind,
    updateWeaponName,
    updateWeaponNotes,
    updateWeaponRange,
    updateWeaponSize,
    updateWeaponWeight
} from '../actions/weapon.actions';
import {loadSheet, resetSheet} from '../actions/global.actions';

import reducer from './weapons.reducer';

describe('Reducer tests', () => {
    describe('Weapons reducer tests', () => {
        test('Should return the initial state', () => {
            expect(reducer(undefined, {})).toEqual([{id: 0}]);
        });
        test('Should handle RESET_SHEET', () => {
            const state = [{test: 'Test'}];
            const newState = reducer(state, resetSheet());
            expect(newState).toEqual([{id: 0}]);
            expect(newState).not.toBe(state);
        });
        test('Should handle LOAD_SHEET', () => {
            const state = {};
            const sheetToLoad = {
                weapons: [{test: 'test'}]
            };
            const newState = reducer(state, loadSheet(sheetToLoad));
            expect(newState).toEqual(sheetToLoad.weapons);
            expect(reducer(state, loadSheet([]))).toEqual([{id: 0}]);
            expect(newState).not.toBe(state);
            expect(newState).not.toBe(sheetToLoad.weapons);
        });
        test('Should handle UPDATE_WEAPON_NAME', () => {
            const state = [{id: 0, name: 'test1'}];
            const newState = reducer(state, updateWeaponName(0, 'test2'));
            expect(newState).toEqual([{id: 0, name: 'test2'}]);
            expect(
                reducer([{id: 0, name: 'test3'}], updateWeaponName(0, 'test4'))
            ).toEqual([{id: 0, name: 'test4'}]);
            expect(newState).not.toBe(state);
        });

        test('Should handle UPDATE_WEAPON_ATTACK_BONUS', () => {
            const state = [{id: 0, attackBonus: 'test1'}];
            const newState = reducer(state, updateWeaponAttackBonus(0, 'test2'));
            expect(newState).toEqual([{id: 0, attackBonus: 'test2'}]);
            expect(
                reducer(
                    [{id: 0, attackBonus: 'test3'}],
                    updateWeaponAttackBonus(0, 'test4')
                )
            ).toEqual([{id: 0, attackBonus: 'test4'}]);
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_WEAPON_DAMAGE', () => {
            const state = [{id: 0, damage: 'test1'}];
            const newState = reducer(state, updateWeaponDamage(0, 'test2'));
            expect(newState).toEqual([{id: 0, damage: 'test2'}]);
            expect(
                reducer([{id: 0, damage: 'test3'}], updateWeaponDamage(0, 'test4'))
            ).toEqual([{id: 0, damage: 'test4'}]);
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_WEAPON_CRITICAL', () => {
            const state = [{id: 0, critical: 'test1'}];
            const newState = reducer(state, updateWeaponCritical(0, 'test2'));
            expect(newState).toEqual([{id: 0, critical: 'test2'}]);
            expect(
                reducer([{id: 0, critical: 'test3'}], updateWeaponCritical(0, 'test4'))
            ).toEqual([{id: 0, critical: 'test4'}]);
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_WEAPON_RANGE', () => {
            const state = [{id: 0, range: 'test1'}];
            const newState = reducer(state, updateWeaponRange(0, 'test2'));
            expect(newState).toEqual([{id: 0, range: 'test2'}]);
            expect(
                reducer([{id: 0, range: 'test3'}], updateWeaponRange(0, 'test4'))
            ).toEqual([{id: 0, range: 'test4'}]);
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_WEAPON_WEIGHT', () => {
            const state = [{id: 0, weight: 'test1'}];
            const newState = reducer(state, updateWeaponWeight(0, 'test2'));
            expect(newState).toEqual([{id: 0, weight: 'test2'}]);
            expect(
                reducer([{id: 0, weight: 'test3'}], updateWeaponWeight(0, 'test4'))
            ).toEqual([{id: 0, weight: 'test4'}]);
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_WEAPON_SIZE', () => {
            const state = [{id: 0, size: 'test1'}];
            const newState = reducer(state, updateWeaponSize(0, 'test2'));
            expect(newState).toEqual([{id: 0, size: 'test2'}]);
            expect(
                reducer([{id: 0, size: 'test3'}], updateWeaponSize(0, 'test4'))
            ).toEqual([{id: 0, size: 'test4'}]);
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_WEAPON_KIND', () => {
            const state = [{id: 0, kind: 'test1'}];
            const newState = reducer(state, updateWeaponKind(0, 'test2'));
            expect(newState).toEqual([{id: 0, kind: 'test2'}]);
            expect(
                reducer([{id: 0, kind: 'test3'}], updateWeaponKind(0, 'test4'))
            ).toEqual([{id: 0, kind: 'test4'}]);
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_WEAPON_NOTES', () => {
            const state = [{id: 0, notes: 'test1'}];
            const newState = reducer(state, updateWeaponNotes(0, 'test2'));
            expect(newState).toEqual([{id: 0, notes: 'test2'}]);
            expect(
                reducer([{id: 0, notes: 'test3'}], updateWeaponNotes(0, 'test4'))
            ).toEqual([{id: 0, notes: 'test4'}]);
            expect(newState).not.toBe(state);
        });
        test('Should handle ADD_WEAPON', () => {
            const state = [{id: 0, notes: 'test'}];
            const newState = reducer(state, addWeapon());
            expect(newState).toEqual([{id: 0, notes: 'test'}, {id: 1}]);
            expect(reducer([{id: 0, notes: 'test'}], addWeapon())).toEqual([
                {id: 0, notes: 'test'},
                {id: 1}
            ]);
            expect(newState).not.toBe(state);
        });
        test('Should handle RESET_WEAPON', () => {
            const state = [{id: 0, notes: 'test'}];
            const newState = reducer(state, resetWeapon(0));
            expect(newState).toEqual([{id: 0}]);
            expect(
                reducer(
                    [{id: 0, notes: 'test'}, {id: 1, notes: 'test2'}],
                    resetWeapon(0)
                )
            ).toEqual([{id: 0}, {id: 1, notes: 'test2'}]);
            expect(newState).not.toBe(state);
        });
        test('Should handle DELETE_WEAPON', () => {
            const state = [{id: 0, notes: 'test'}];
            const newState = reducer(state, deleteWeapon(0));
            expect(newState).toEqual([{id: 0}]);
            expect(
                reducer(
                    [{id: 0, notes: 'test'}, {id: 1, notes: 'test2'}],
                    deleteWeapon(0)
                )
            ).toEqual([{id: 1, notes: 'test2'}]);
            expect(newState).not.toBe(state);
        });
    });
});
