import {actionTypes} from '../actions/baseAttack.actions';
import {actionTypes as globalActionTypes} from '../actions/global.actions';

export default function reducer(state = '', action) {
    switch (action.type) {
        case globalActionTypes.RESET_SHEET:
        case actionTypes.RESET_BASE_ATTACK:
            return '';
        case globalActionTypes.LOAD_SHEET:
            return (action.sheet && action.sheet.baseAttack) || '';
        case actionTypes.UPDATE_BASE_ATTACK:
            return action.value;
        default:
            return state;
    }
}
