import {loadSheet, resetSheet} from '../actions/global.actions';
import {resetNotes, updateNotes} from '../actions/notes.actions';

import reducer from './notes.reducer';

describe('Reducer tests', () => {
    describe('Notes reducer tests', () => {
        test('Should return the initial state', () => {
            expect(reducer(undefined, {})).toEqual('');
        });
        test('Should handle RESET_SHEET', () => {
            const state = 'Test';
            const newState = reducer(state, resetSheet());
            expect(newState).toEqual('');
            expect(newState).not.toBe(state);
        });
        test('Should handle RESET_NOTES', () => {
            const state = 'Test';
            const newState = reducer(state, resetNotes());
            expect(newState).toEqual('');
            expect(newState).not.toBe(state);
        });
        test('Should handle LOAD_SHEET', () => {
            const state = {};
            const sheetToLoad = {
                notes: 'test'
            };
            const newState = reducer(state, loadSheet(sheetToLoad));
            expect(newState).toEqual(sheetToLoad.notes);
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_NOTES', () => {
            const newState = reducer('', updateNotes('Test'));
            expect(newState).toEqual('Test');
        });
    });
});
