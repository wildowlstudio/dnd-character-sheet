import {
    addSkill,
    deleteSkill,
    deleteSkills,
    resetSkills,
    sortSkills,
    updateSkillAbilityMod,
    updateSkillAttribute,
    updateSkillClass,
    updateSkillMiscMod,
    updateSkillName,
    updateSkillSkillRank
} from '../actions/skill.actions';
import {loadSheet, resetSheet} from '../actions/global.actions';

import reducer from './skills.reducer';

describe('Reducer tests', () => {
    describe('Skill reducer tests', () => {
        test('Should return the initial state', () => {
            expect(reducer(undefined, {})).toEqual([]);
        });
        test('Should handle RESET_SHEET', () => {
            const state = [{name: 'test'}];
            const newState = reducer(state, resetSheet());
            expect(newState).toEqual([]);
            expect(newState).not.toBe(state);
        });
        test('Should handle RESET_SKILLS', () => {
            const state = [{name: 'test'}];
            const newState = reducer(state, resetSkills());
            expect(newState).toEqual([]);
            expect(newState).not.toBe(state);
        });
        test('Should handle LOAD_SHEET', () => {
            const state = [{name: 'test'}];
            const sheetToLoad = {
                skills: [{name: 'test2'}]
            };
            const newState = reducer(state, loadSheet(sheetToLoad));
            expect(newState).toEqual(sheetToLoad.skills);
            expect(newState).not.toBe(state);
            expect(newState).not.toBe(sheetToLoad.skills);
        });
        test('Should handle ADD_SKILL', () => {
            const state = [{id: 0, name: 'test'}];
            const newState = reducer(state, addSkill());
            expect(newState).toEqual([{id: 0, name: 'test'}, {id: 1}]);
            expect(newState).not.toBe(state);
            expect(reducer([], addSkill())).toEqual([{id: 0}]);
        });

        test('Should handle DELETE_SKILLS', () => {
            const state = [
                {id: 0, name: 'test'},
                {id: 2},
                {id: 1, name: 'test2'},
                {id: 3},
                {id: 4}
            ];
            const newState = reducer(state, deleteSkills(1));
            expect(newState).toEqual([{id: 0, name: 'test'}, {id: 1, name: 'test2'}]);
            expect(newState).not.toBe(state);
        });

        test('Should handle DELETE_SKILL', () => {
            const state = [{id: 0, name: 'test'}, {id: 1, name: 'test2'}];
            const newState = reducer(state, deleteSkill(1));
            expect(newState).toEqual([{id: 0, name: 'test'}]);
            expect(newState).not.toBe(state);
        });

        test('Should handle SORT_SKILLS', () => {
            const state = [
                {id: 3},
                {id: 0, name: 'test'},
                {id: 1, name: 'test2'},
                {id: 2, name: 'aTest'}
            ];
            const newState = reducer(state, sortSkills('name', false));
            expect(newState).not.toBe(state);
            expect(newState).toEqual([
                {id: 2, name: 'aTest'},
                {id: 0, name: 'test'},
                {id: 1, name: 'test2'},
                {id: 3}
            ]);
            expect(reducer(state, sortSkills('name', true))).toEqual([
                {id: 1, name: 'test2'},
                {id: 0, name: 'test'},
                {id: 2, name: 'aTest'},
                {id: 3}
            ]);
        });

        test('Should handle UPDATE_SKILL_CLASS', () => {
            const state = [{id: 0, name: 'test'}, {id: 1, name: 'test2'}];
            const newState = reducer(state, updateSkillClass(1, 'test3'));
            expect(newState).not.toBe(state);
            expect(reducer(state, updateSkillClass(1, 'test3'))).toEqual([
                {id: 0, name: 'test'},
                {id: 1, name: 'test2', class: 'test3'}
            ]);
            expect(reducer(state, updateSkillClass('new', 'test3'))).toEqual([
                {id: 0, name: 'test'},
                {id: 1, name: 'test2'},
                {id: 2, class: 'test3'}
            ]);
        });

        test('Should handle UPDATE_SKILL_NAME', () => {
            const state = [{id: 0, name: 'test'}, {id: 1, name: 'test2'}];
            const newState = reducer(state, updateSkillName(1, 'test3'));
            expect(newState).not.toBe(state);
            expect(reducer(state, updateSkillName(1, 'test3'))).toEqual([
                {id: 0, name: 'test'},
                {id: 1, name: 'test3'}
            ]);
            expect(reducer(state, updateSkillName('new', 'test3'))).toEqual([
                {id: 0, name: 'test'},
                {id: 1, name: 'test2'},
                {id: 2, name: 'test3'}
            ]);
        });

        test('Should handle UPDATE_SKILL_ATTRIBUTE', () => {
            const state = [{id: 0, name: 'test'}, {id: 1, name: 'test2'}];
            const newState = reducer(state, updateSkillAttribute(1, 'test3'));
            expect(newState).not.toBe(state);
            expect(reducer(state, updateSkillAttribute(1, 'test3'))).toEqual([
                {id: 0, name: 'test'},
                {id: 1, name: 'test2', attribute: 'test3'}
            ]);
            expect(reducer(state, updateSkillAttribute('new', 'test3'))).toEqual([
                {id: 0, name: 'test'},
                {id: 1, name: 'test2'},
                {id: 2, attribute: 'test3'}
            ]);
        });

        test('Should handle UPDATE_SKILL_ABILITY_MOD', () => {
            const state = [{id: 0, name: 'test'}, {id: 1, name: 'test2'}];
            const newState = reducer(state, updateSkillAbilityMod(1, 2));
            expect(newState).not.toBe(state);
            expect(reducer(state, updateSkillAbilityMod(1, 2))).toEqual([
                {id: 0, name: 'test'},
                {id: 1, name: 'test2', abilityMod: 2}
            ]);
            expect(reducer(state, updateSkillAbilityMod('new', 2))).toEqual([
                {id: 0, name: 'test'},
                {id: 1, name: 'test2'},
                {id: 2, abilityMod: 2}
            ]);
        });

        test('Should handle UPDATE_SKILL_SKILL_RANK', () => {
            const state = [{id: 0, name: 'test'}, {id: 1, name: 'test2'}];
            const newState = reducer(state, updateSkillSkillRank(1, 2));
            expect(newState).not.toBe(state);
            expect(reducer(state, updateSkillSkillRank(1, 2))).toEqual([
                {id: 0, name: 'test'},
                {id: 1, name: 'test2', skillRank: 2}
            ]);
            expect(reducer(state, updateSkillSkillRank('new', 2))).toEqual([
                {id: 0, name: 'test'},
                {id: 1, name: 'test2'},
                {id: 2, skillRank: 2}
            ]);
        });

        test('Should handle UPDATE_SKILL_MISC_MOD', () => {
            const state = [{id: 0, name: 'test'}, {id: 1, name: 'test2'}];
            const newState = reducer(state, updateSkillMiscMod(1, 2));
            expect(newState).not.toBe(state);
            expect(reducer(state, updateSkillMiscMod(1, 2))).toEqual([
                {id: 0, name: 'test'},
                {id: 1, name: 'test2', miscMod: 2}
            ]);
            expect(reducer(state, updateSkillMiscMod('new', 2))).toEqual([
                {id: 0, name: 'test'},
                {id: 1, name: 'test2'},
                {id: 2, miscMod: 2}
            ]);
        });
    });
});
