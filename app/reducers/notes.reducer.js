import {actionTypes} from '../actions/notes.actions';
import {actionTypes as globalActionTypes} from '../actions/global.actions';

export default function reducer(state = '', action) {
    switch (action.type) {
        case globalActionTypes.RESET_SHEET:
        case actionTypes.RESET_NOTES:
            return '';
        case globalActionTypes.LOAD_SHEET:
            return (action.sheet && action.sheet.notes) || '';
        case actionTypes.UPDATE_NOTES:
            return action.value;
        default:
            return state;
    }
}
