import sheetReducer from './sheet.reducer';

export default function appReducer(state = {}, action) {
    const sheet = sheetReducer(state.sheet, action);
    window.setTimeout(() => {
        if (window.localStorage) {
            window.localStorage.sheet = JSON.stringify(sheet);
        }
    });
    return {
        sheet
    };
}
