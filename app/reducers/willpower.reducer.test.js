import {loadSheet, resetSheet} from '../actions/global.actions';
import {
    resetSavingThrows,
    updateWillpowerAbilityMod,
    updateWillpowerBase,
    updateWillpowerMagicMod,
    updateWillpowerMiscMod,
    updateWillpowerOtherMod,
    updateWillpowerTempMod
} from '../actions/savingThrow.actions';

import reducer from './willpower.reducer';

describe('Reducer tests', () => {
    describe('Willpower reducer tests', () => {
        test('Should return the initial state', () => {
            expect(reducer(undefined, {})).toEqual({});
        });
        test('Should handle RESET_SHEET', () => {
            const state = {test: 'Test'};
            const newState = reducer(state, resetSheet());
            expect(newState).toEqual({});
            expect(newState).not.toBe(state);
        });
        test('Should handle RESET_SAVING_THROWS', () => {
            const state = {test: 'Test'};
            const newState = reducer(state, resetSavingThrows());
            expect(newState).toEqual({});
            expect(newState).not.toBe(state);
        });
        test('Should handle LOAD_SHEET', () => {
            const state = {};
            const sheetToLoad = {
                willpower: {test: 'test'}
            };
            const newState = reducer(state, loadSheet(sheetToLoad));
            expect(newState).toEqual(sheetToLoad.willpower);
            expect(newState).not.toBe(state);
            expect(newState).not.toBe(sheetToLoad.willpower);
        });

        test('Should handle UPDATE_WILLPOWER_BASE', () => {
            const state = {};
            const newState = reducer(state, updateWillpowerBase('Test'));
            expect(newState).toEqual({base: 'Test'});
            expect(reducer({test: 'Test'}, updateWillpowerBase('Test'))).toEqual({
                test: 'Test',
                base: 'Test'
            });
            expect(newState).not.toBe(state);
        });

        test('Should handle UPDATE_WILLPOWER_ABILITY_MOD', () => {
            const state = {};
            const newState = reducer(state, updateWillpowerAbilityMod('Test'));
            expect(newState).toEqual({abilityMod: 'Test'});
            expect(reducer({test: 'Test'}, updateWillpowerAbilityMod('Test'))).toEqual({
                test: 'Test',
                abilityMod: 'Test'
            });
            expect(newState).not.toBe(state);
        });

        test('Should handle UPDATE_WILLPOWER_MAGIC_MOD', () => {
            const state = {};
            const newState = reducer(state, updateWillpowerMagicMod('Test'));
            expect(newState).toEqual({magicMod: 'Test'});
            expect(reducer({test: 'Test'}, updateWillpowerMagicMod('Test'))).toEqual({
                test: 'Test',
                magicMod: 'Test'
            });
            expect(newState).not.toBe(state);
        });

        test('Should handle UPDATE_WILLPOWER_MISC_MOD', () => {
            const state = {};
            const newState = reducer(state, updateWillpowerMiscMod('Test'));
            expect(newState).toEqual({miscMod: 'Test'});
            expect(reducer({test: 'Test'}, updateWillpowerMiscMod('Test'))).toEqual({
                test: 'Test',
                miscMod: 'Test'
            });
            expect(newState).not.toBe(state);
        });

        test('Should handle UPDATE_WILLPOWER_TEMP_MOD', () => {
            const state = {};
            const newState = reducer(state, updateWillpowerTempMod('Test'));
            expect(newState).toEqual({tempMod: 'Test'});
            expect(reducer({test: 'Test'}, updateWillpowerTempMod('Test'))).toEqual({
                test: 'Test',
                tempMod: 'Test'
            });
            expect(newState).not.toBe(state);
        });

        test('Should handle UPDATE_WILLPOWER_OTHER_MOD', () => {
            const state = {};
            const newState = reducer(state, updateWillpowerOtherMod('Test'));
            expect(newState).toEqual({otherMod: 'Test'});
            expect(reducer({test: 'Test'}, updateWillpowerOtherMod('Test'))).toEqual({
                test: 'Test',
                otherMod: 'Test'
            });
            expect(newState).not.toBe(state);
        });
    });
});
