import {actionTypes} from '../actions/health.actions';
import {actionTypes as globalActionTypes} from '../actions/global.actions';

export default function reducer(state = {}, action) {
    switch (action.type) {
        case actionTypes.RESET_HEALTH:
        case globalActionTypes.RESET_SHEET:
            return {};
        case globalActionTypes.LOAD_SHEET:
            return Object.assign({}, state, (action.sheet || {hp: {}}).hp);
        case actionTypes.UPDATE_HP:
            return Object.assign({}, state, {hp: action.value});
        case actionTypes.UPDATE_CURRENT_HP:
            return Object.assign({}, state, {current: action.value});
        case actionTypes.UPDATE_SUBDUAL_DAMAGE:
            return Object.assign({}, state, {subdualDamage: action.value});
        default:
            return state;
    }
}
