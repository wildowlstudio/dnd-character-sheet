import {loadSheet, resetSheet} from '../actions/global.actions';
import {
    resetHealth,
    updateCurrentHp,
    updateHp,
    updateSubdualDamage
} from '../actions/health.actions';

import reducer from './hp.reducer';

describe('Reducer tests', () => {
    describe('Hp reducer tests', () => {
        test('Should return the initial state', () => {
            expect(reducer(undefined, {})).toEqual({});
        });
        test('Should handle RESET_SHEET', () => {
            const state = {test: 'Test'};
            const newState = reducer(state, resetSheet());
            expect(newState).toEqual({});
            expect(newState).not.toBe(state);
        });
        test('Should handle LOAD_SHEET', () => {
            const state = {};
            const sheetToLoad = {
                hp: {test: 'test'}
            };
            const newState = reducer(state, loadSheet(sheetToLoad));
            expect(newState).toEqual(sheetToLoad.hp);
            expect(newState).not.toBe(state);
            expect(newState).not.toBe(sheetToLoad.hp);
        });
        test('Should handle RESET_HEALTH', () => {
            const state = {test: 'Test'};
            const newState = reducer(state, resetHealth());
            expect(newState).toEqual({});
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_HP', () => {
            const state = {};
            const newState = reducer(state, updateHp('Test'));
            expect(newState).toEqual({hp: 'Test'});
            expect(reducer({test: 'Test'}, updateHp('Test'))).toEqual({
                test: 'Test',
                hp: 'Test'
            });
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_CURRENT_HP', () => {
            const state = {};
            const newState = reducer(state, updateCurrentHp('Test'));
            expect(newState).toEqual({current: 'Test'});
            expect(reducer({test: 'Test'}, updateCurrentHp('Test'))).toEqual({
                test: 'Test',
                current: 'Test'
            });
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_SUBDUAL_DAMAGE', () => {
            const state = {};
            const newState = reducer(state, updateSubdualDamage('Test'));
            expect(newState).toEqual({subdualDamage: 'Test'});
            expect(reducer({test: 'Test'}, updateSubdualDamage('Test'))).toEqual({
                test: 'Test',
                subdualDamage: 'Test'
            });
            expect(newState).not.toBe(state);
        });
    });
});
