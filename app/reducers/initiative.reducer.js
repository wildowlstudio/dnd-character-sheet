import {actionTypes} from '../actions/initiative.actions';
import {actionTypes as globalActionTypes} from '../actions/global.actions';

export default function reducer(state = {}, action) {
    switch (action.type) {
        case globalActionTypes.RESET_SHEET:
        case actionTypes.RESET_INITIATIVE:
            return {};
        case globalActionTypes.LOAD_SHEET:
            return Object.assign(
                {},
                state,
                (action.sheet || {initiative: {}}).initiative
            );
        case actionTypes.UPDATE_INITIATIVE_DEX_MOD:
            return Object.assign({}, state, {dexMod: +action.value});
        case actionTypes.UPDATE_INITIATIVE_MISC_MOD:
            return Object.assign({}, state, {miscMod: +action.value});
        default:
            return state;
    }
}
