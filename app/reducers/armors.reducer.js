import {actionTypes} from '../actions/armor.actions';
import {actionTypes as globalActionTypes} from '../actions/global.actions';

export default function reducer(state = [{id: 0}], action) {
    switch (action.type) {
        case globalActionTypes.RESET_SHEET:
            return [{id: 0}];
        case globalActionTypes.LOAD_SHEET:
            return (
                (action.sheet &&
                    action.sheet.armors &&
                    action.sheet.armors.length &&
                    Object.assign([], action.sheet.armors)) || [{id: 0}]
            );
        case actionTypes.UPDATE_ARMOR_NAME:
            return state.map((armor) => {
                if (armor.id === action.id) {
                    return Object.assign({}, armor, {name: action.value});
                }
                return armor;
            });
        case actionTypes.UPDATE_ARMOR_BONUS:
            return state.map((armor) => {
                if (armor.id === action.id) {
                    return Object.assign({}, armor, {bonus: action.value});
                }
                return armor;
            });
        case actionTypes.UPDATE_ARMOR_MAX_DEX:
            return state.map((armor) => {
                if (armor.id === action.id) {
                    return Object.assign({}, armor, {maxDex: action.value});
                }
                return armor;
            });
        case actionTypes.UPDATE_ARMOR_PENALTY:
            return state.map((armor) => {
                if (armor.id === action.id) {
                    return Object.assign({}, armor, {penalty: action.value});
                }
                return armor;
            });
        case actionTypes.UPDATE_ARMOR_MAX_SPEED:
            return state.map((armor) => {
                if (armor.id === action.id) {
                    return Object.assign({}, armor, {maxSpeed: action.value});
                }
                return armor;
            });
        case actionTypes.UPDATE_ARMOR_WEIGHT:
            return state.map((armor) => {
                if (armor.id === action.id) {
                    return Object.assign({}, armor, {weight: action.value});
                }
                return armor;
            });
        case actionTypes.UPDATE_ARMOR_TYPE:
            return state.map((armor) => {
                if (armor.id === action.id) {
                    return Object.assign({}, armor, {type: action.value});
                }
                return armor;
            });
        case actionTypes.UPDATE_ARMOR_SPELL_FAILURE:
            return state.map((armor) => {
                if (armor.id === action.id) {
                    return Object.assign({}, armor, {spellFailure: action.value});
                }
                return armor;
            });
        case actionTypes.UPDATE_ARMOR_NOTES:
            return state.map((armor) => {
                if (armor.id === action.id) {
                    return Object.assign({}, armor, {notes: action.value});
                }
                return armor;
            });
        case actionTypes.ADD_ARMOR:
            return state.concat({
                id: state.reduce((acc, armor) => {
                    if (armor.id >= acc) {
                        return armor.id + 1;
                    }
                    return acc;
                }, 0)
            });
        case actionTypes.RESET_ARMOR:
            return state.map((armor) => {
                if (armor.id === action.id) {
                    return {id: armor.id};
                }
                return armor;
            });
        case actionTypes.DELETE_ARMOR: {
            const newState = state.filter((armor) => armor.id !== action.id);
            if (!newState.length) {
                newState.push({id: 0});
            }
            return newState;
        }
        default:
            return state;
    }
}
