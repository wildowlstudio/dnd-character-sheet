import {actionTypes} from '../actions/speed.actions';
import {actionTypes as globalActionTypes} from '../actions/global.actions';

export default function reducer(state = '', action) {
    switch (action.type) {
        case globalActionTypes.RESET_SHEET:
        case actionTypes.RESET_SPEED:
            return '';
        case globalActionTypes.LOAD_SHEET:
            return (action.sheet && action.sheet.speed) || '';
        case actionTypes.UPDATE_SPEED:
            return action.value;
        default:
            return state;
    }
}
