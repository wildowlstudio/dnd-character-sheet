import {loadSheet, resetSheet} from '../actions/global.actions';
import {resetAvatar, updateAvatar} from '../actions/avatar.actions';

import reducer from './avatar.reducer';

describe('Reducer tests', () => {
    describe('Avatar reducer tests', () => {
        test('Should return the initial state', () => {
            expect(reducer(undefined, {})).toEqual(null);
        });
        test('Should handle RESET_SHEET', () => {
            const state = 'Test';
            const newState = reducer(state, resetSheet());
            expect(newState).toEqual(null);
            expect(newState).not.toBe(state);
        });
        test('Should handle RESET_AVATAR', () => {
            const state = 'Test';
            const newState = reducer(state, resetAvatar());
            expect(newState).toEqual(null);
            expect(newState).not.toBe(state);
        });
        test('Should handle LOAD_SHEET', () => {
            const state = 'test1';
            const sheetToLoad = {
                avatar: 'test2'
            };
            const newState = reducer(state, loadSheet(sheetToLoad));
            expect(newState).toEqual(sheetToLoad.avatar);
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_AVATAR', () => {
            const state = '';
            const newState = reducer(state, updateAvatar('Test'));
            expect(newState).toEqual('Test');
            expect(reducer('Test', updateAvatar('Test2'))).toEqual('Test2');
            expect(newState).not.toBe(state);
        });
    });
});
