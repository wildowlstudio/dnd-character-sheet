import {loadSheet, resetSheet} from '../actions/global.actions';
import {
    resetInitiative,
    updateInitiativeDexMod,
    updateInitiativeMiscMod
} from '../actions/initiative.actions';

import reducer from './initiative.reducer';

describe('Reducer tests', () => {
    describe('Initiative reducer tests', () => {
        test('Should return the initial state', () => {
            expect(reducer(undefined, {})).toEqual({});
        });
        test('Should handle RESET_SHEET', () => {
            const state = {test: 'Test'};
            const newState = reducer(state, resetSheet());
            expect(newState).toEqual({});
            expect(newState).not.toBe(state);
        });
        test('Should handle RESET_INITIATIVE', () => {
            const state = {test: 'Test'};
            const newState = reducer(state, resetInitiative());
            expect(newState).toEqual({});
            expect(newState).not.toBe(state);
        });
        test('Should handle LOAD_SHEET', () => {
            const state = {};
            const sheetToLoad = {
                initiative: {test: 'test'}
            };
            const newState = reducer(state, loadSheet(sheetToLoad));
            expect(newState).toEqual(sheetToLoad.initiative);
            expect(newState).not.toBe(state);
            expect(newState).not.toBe(sheetToLoad.initiative);
        });
        test('Should handle UPDATE_INITIATIVE_DEX_MOD', () => {
            const state = {};
            const newState = reducer(state, updateInitiativeDexMod(1));
            expect(newState).not.toBe(state);
            expect(newState).toEqual({dexMod: 1});
            expect(reducer(state, updateInitiativeDexMod('test'))).toEqual({
                dexMod: NaN
            });
            expect(reducer({test: 'Test'}, updateInitiativeDexMod(1))).toEqual({
                test: 'Test',
                dexMod: 1
            });
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_INITIATIVE_MISC_MOD', () => {
            const state = {};
            const newState = reducer(state, updateInitiativeMiscMod(1));
            expect(newState).not.toBe(state);
            expect(newState).toEqual({miscMod: 1});
            expect(reducer(state, updateInitiativeMiscMod('Test'))).toEqual({
                miscMod: NaN
            });
            expect(reducer({test: 'Test'}, updateInitiativeMiscMod(1))).toEqual({
                test: 'Test',
                miscMod: 1
            });
            expect(newState).not.toBe(state);
        });
    });
});
