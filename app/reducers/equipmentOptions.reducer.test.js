import {
    updateEquipmentOptionCapacity,
    updateEquipmentOptionSum,
    updateEquipmentOptionValue,
    updateEquipmentOptionWeight
} from '../actions/equipment.actions';

import {loadSheet} from '../actions/global.actions';
import reducer from './equipmentOptions.reducer';

describe('Reducer tests', () => {
    describe('Equipment options reducer tests', () => {
        test('Should return the initial state', () => {
            expect(reducer(undefined, {})).toEqual({
                weight: true,
                capacity: true,
                value: true,
                sum: true
            });
        });
        test('Should handle LOAD_SHEET', () => {
            const state = {};
            const sheetToLoad = {
                options: {equipment: {test: 'test'}}
            };
            const newState = reducer(state, loadSheet(sheetToLoad));
            expect(newState).toEqual(sheetToLoad.options.equipment);
            expect(newState).not.toBe(state);
            expect(newState).not.toBe(sheetToLoad.options.equipment);
        });
        test('Should handle UPDATE_EQUIPMENT_OPTION_WEIGHT', () => {
            const state = {};
            const newState = reducer(state, updateEquipmentOptionWeight(false));
            expect(newState).toEqual({weight: false});
            expect(reducer({test: 'Test'}, updateEquipmentOptionWeight(false))).toEqual(
                {
                    test: 'Test',
                    weight: false
                }
            );
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_EQUIPMENT_OPTION_CAPACITY', () => {
            const state = {};
            const newState = reducer(state, updateEquipmentOptionCapacity(false));
            expect(newState).toEqual({capacity: false});
            expect(
                reducer({test: 'Test'}, updateEquipmentOptionCapacity(false))
            ).toEqual({
                test: 'Test',
                capacity: false
            });
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_EQUIPMENT_OPTION_VALUE', () => {
            const state = {};
            const newState = reducer(state, updateEquipmentOptionValue(false));
            expect(newState).toEqual({value: false});
            expect(reducer({test: 'Test'}, updateEquipmentOptionValue(false))).toEqual({
                test: 'Test',
                value: false
            });
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_EQUIPMENT_OPTION_SUM', () => {
            const state = {};
            const newState = reducer(state, updateEquipmentOptionSum(false));
            expect(newState).toEqual({sum: false});
            expect(reducer({test: 'Test'}, updateEquipmentOptionSum(false))).toEqual({
                test: 'Test',
                sum: false
            });
            expect(newState).not.toBe(state);
        });
    });
});
