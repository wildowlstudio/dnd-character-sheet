import {loadSheet, resetSheet} from '../actions/global.actions';
import {resetSpeed, updateSpeed} from '../actions/speed.actions';

import reducer from './speed.reducer';

describe('Reducer tests', () => {
    describe('Speed reducer tests', () => {
        test('Should return the initial state', () => {
            expect(reducer(undefined, {})).toEqual('');
        });
        test('Should handle RESET_SHEET', () => {
            const state = 'Test';
            const newState = reducer(state, resetSheet());
            expect(newState).toEqual('');
            expect(newState).not.toBe(state);
        });
        test('Should handle RESET_SPEED', () => {
            const state = 'Test';
            const newState = reducer(state, resetSpeed());
            expect(newState).toEqual('');
            expect(newState).not.toBe(state);
        });
        test('Should handle LOAD_SHEET', () => {
            const state = 'test1';
            const sheetToLoad = {
                speed: 'test2'
            };
            const newState = reducer(state, loadSheet(sheetToLoad));
            expect(newState).toEqual(sheetToLoad.speed);
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_SPEED', () => {
            const state = '';
            const newState = reducer(state, updateSpeed('Test'));
            expect(newState).toEqual('Test');
            expect(reducer('Test', updateSpeed('Test2'))).toEqual('Test2');
            expect(newState).not.toBe(state);
        });
    });
});
