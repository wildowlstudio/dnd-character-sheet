import {
    addKnownSpell,
    cleanKnownSpells,
    deleteKnownSpell,
    moveKnownSpell,
    resetKnownSpells,
    updateKnownSpell
} from '../actions/knownSpell.actions';
import {loadSheet, resetSheet} from '../actions/global.actions';

import reducer from './knownSpells.reducer';

describe('Reducer tests', () => {
    describe('KnownSpells reducer tests', () => {
        test('Should return the initial state', () => {
            expect(reducer(undefined, {})).toEqual([]);
        });
        test('Should handle RESET_SHEET', () => {
            const state = [{name: 'test'}];
            const newState = reducer(state, resetSheet());
            expect(newState).toEqual([]);
            expect(newState).not.toBe(state);
        });
        test('Should handle RESET_KNOWN_SPELLS', () => {
            const state = [{name: 'test'}];
            const newState = reducer(state, resetKnownSpells());
            expect(newState).toEqual([]);
            expect(newState).not.toBe(state);
        });
        test('Should handle LOAD_SHEET', () => {
            const state = [{name: 'test'}];
            const sheetToLoad = {
                knownSpells: [{name: 'test2'}]
            };
            const newState = reducer(state, loadSheet(sheetToLoad));
            expect(newState).toEqual(sheetToLoad.knownSpells);
            expect(newState).not.toBe(state);
            expect(newState).not.toBe(sheetToLoad.feats);
        });
        test('Should handle ADD_FEAT', () => {
            const state = [{id: 0, name: 'test'}];
            const newState = reducer(state, addKnownSpell());
            expect(newState).toEqual([{id: 0, name: 'test'}, {id: 1}]);
            expect(newState).not.toBe(state);
            expect(reducer([], addKnownSpell())).toEqual([{id: 0}]);
        });

        test('Should handle CLEAN_KNOWN_SPELLS', () => {
            const state = [
                {id: 0, value: 'test'},
                {id: 2},
                {id: 1, value: 'test2'},
                {id: 3},
                {id: 4}
            ];
            const newState = reducer(state, cleanKnownSpells(1));
            expect(newState).toEqual([
                {id: 0, value: 'test'},
                {id: 1, value: 'test2'}
            ]);
            expect(newState).not.toBe(state);
        });

        test('Should handle DELETE_FEAT', () => {
            const state = [{id: 0, name: 'test'}, {id: 1, name: 'test2'}];
            const newState = reducer(state, deleteKnownSpell(1));
            expect(newState).toEqual([{id: 0, name: 'test'}]);
            expect(newState).not.toBe(state);
        });

        test('Should handle MOVE_FEAT', () => {
            const state = [
                {id: 0, name: 'test'},
                {id: 1, name: 'test2'},
                {id: 2, name: 'aTest'}
            ];
            const newState = reducer(state, moveKnownSpell(0, true, 2));
            expect(newState).not.toBe(state);
            expect(newState).toEqual([
                {id: 1, name: 'test2'},
                {id: 2, name: 'aTest'},
                {id: 0, name: 'test'}
            ]);
            expect(reducer(state, moveKnownSpell(0, false, 2))).toEqual([
                {id: 1, name: 'test2'},
                {id: 0, name: 'test'},
                {id: 2, name: 'aTest'}
            ]);
            expect(reducer(state, moveKnownSpell(0, true, 'new'))).toEqual([
                {id: 1, name: 'test2'},
                {id: 2, name: 'aTest'},
                {id: 0, name: 'test'}
            ]);
            expect(reducer(state, moveKnownSpell(0, false, 'new'))).toEqual([
                {id: 1, name: 'test2'},
                {id: 2, name: 'aTest'},
                {id: 0, name: 'test'}
            ]);
        });

        test('Should handle UPDATE_FEAT', () => {
            const state = [{id: 0, name: 'test'}, {id: 1, name: 'test2'}];
            const newState = reducer(state, updateKnownSpell(1, 'test3'));
            expect(newState).not.toBe(state);
            expect(reducer(state, updateKnownSpell(1, 'test3'))).toEqual([
                {id: 0, name: 'test'},
                {id: 1, name: 'test2', value: 'test3'}
            ]);
            expect(reducer(state, updateKnownSpell('new', 'test3'))).toEqual([
                {id: 0, name: 'test'},
                {id: 1, name: 'test2'},
                {id: 2, value: 'test3'}
            ]);
        });
    });
});
