import {actionTypes} from '../actions/equipment.actions';
import {actionTypes as globalActionTypes} from '../actions/global.actions';
import sortBy from '../utils/sort.util';

export default function reducer(state = [], action) {
    switch (action.type) {
        case globalActionTypes.RESET_SHEET:
        case actionTypes.RESET_EQUIPMENT:
            return [];
        case globalActionTypes.LOAD_SHEET:
            return Object.assign([], (action.sheet || {equipment: []}).equipment);
        case actionTypes.ADD_EQUIPMENT_ITEM:
            return addEquipmentItem(state);
        case actionTypes.DELETE_EQUIPMENT_ITEMS:
            return deleteItems(state);
        case actionTypes.DELETE_EQUIPMENT_ITEM:
            return deleteEquipmentItem(state, action);
        case actionTypes.SORT_EQUIPMENT:
            return sortItems(state, action);
        case actionTypes.UPDATE_EQUIPMENT_ITEM_NAME:
            return updateOrAdd(state, action, {name: action.value});
        case actionTypes.UPDATE_EQUIPMENT_ITEM_WEIGHT:
            return updateOrAdd(state, action, {weight: action.value});
        case actionTypes.UPDATE_EQUIPMENT_ITEM_CAPACITY:
            return updateOrAdd(state, action, {capacity: action.value});
        case actionTypes.UPDATE_EQUIPMENT_ITEM_VALUE:
            return updateOrAdd(state, action, {value: action.value});
        case actionTypes.MOVE_EQUIPMENT_ITEM:
            return moveEquipmentItem(state, action);
        default:
            return state;
    }
}

function addEquipmentItem(state, array = state, item = {}) {
    return array.concat(
        Object.assign({}, item, {
            id: state.reduce(
                (acc, item) =>
                    (item.items || []).reduce(
                        (acc, item) => (item.id >= acc ? item.id + 1 : acc),
                        item.id >= acc ? item.id + 1 : acc
                    ),
                0
            )
        })
    );
}

function updateOrAdd(state, action, obj) {
    if (action.id === 'new') {
        if (action.parentId !== undefined) {
            return state.map((item) => {
                if (item.id === action.parentId) {
                    return Object.assign({}, item, {
                        items: addEquipmentItem(state, item.items || [], obj)
                    });
                }
                return item;
            });
        }
        return addEquipmentItem(state, state, obj);
    } else {
        if (action.parentId !== undefined) {
            return state.map((item) => {
                if (item.id === action.parentId) {
                    return Object.assign({}, item, {
                        items: item.items.map((item) => {
                            if (item.id === action.id) {
                                return Object.assign({}, item, obj);
                            }
                            return item;
                        })
                    });
                }
                return item;
            });
        }
        return state.map((item) => {
            if (item.id === action.id) {
                return Object.assign({}, item, obj);
            }
            return item;
        });
    }
}

function deleteItems(state) {
    return state.reduce((acc, item) => {
        if (item.name || item.weight || item.capacity || item.value) {
            if (item.items && item.items.length > 0) {
                return acc.concat(
                    Object.assign({}, item, {
                        items: item.items.filter(
                            (item) =>
                                item.name || item.weight || item.capacity || item.value
                        )
                    })
                );
            }
            return acc.concat(item);
        }
        return acc;
    }, []);
}

function deleteEquipmentItem(state, action) {
    if (action.parentId === undefined) {
        return state.filter((item) => item.id !== action.id);
    }
    return state.map((item) => {
        if (item.id === action.parentId) {
            return Object.assign({}, item, {
                items: item.items.filter((item) => item.id !== action.id)
            });
        }
        return item;
    });
}

function sortItems(state, action) {
    const array = sortBy(action.value, state, action.desc);
    return array.map((item) => {
        if (item.items) {
            return Object.assign({}, item, {
                items: sortBy(action.value, item.items, action.desc)
            });
        }
        return item;
    });
}

function moveEquipmentItem(state, action) {
    let source;
    let newState;
    // remove item from state and store into source
    if (action.parentId === undefined) {
        newState = state.filter((item) => {
            if (item.id === action.id) {
                source = item;
                return false;
            }
            return true;
        });
    } else {
        newState = state.map((item) => {
            if (item.id === action.parentId) {
                return Object.assign({}, item, {
                    items: item.items.filter((item) => {
                        if (item.id === action.id) {
                            source = item;
                            return false;
                        }
                        return true;
                    })
                });
            }
            return item;
        });
    }
    // add source to the correct place
    if (action.targetParentId === undefined) {
        if (action.targetId === 'new') {
            return newState.concat(source);
        }
        for (let i = newState.length - 1; i >= 0; i--) {
            if (newState[i].id === action.targetId) {
                newState.splice(i + (action.below ? 1 : 0), 0, source);
                break;
            }
        }
    } else {
        newState = newState.map((item) => {
            if (item.id === action.targetParentId) {
                if (action.targetId === 'new') {
                    return Object.assign({}, item, {
                        items: item.items.concat(source)
                    });
                }
                const items = Object.assign([], item.items);
                for (let i = items.length - 1; i >= 0; i--) {
                    if (items[i].id === action.targetId) {
                        items.splice(i + (action.below ? 1 : 0), 0, source);
                        break;
                    }
                }
                return Object.assign({}, item, {
                    items
                });
            }
            return item;
        });
    }
    return newState;
}
