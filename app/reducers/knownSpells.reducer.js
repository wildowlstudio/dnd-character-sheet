import {actionTypes} from '../actions/knownSpell.actions';
import {actionTypes as globalActionTypes} from '../actions/global.actions';

export default function reducer(state = [], action) {
    switch (action.type) {
        case globalActionTypes.RESET_SHEET:
        case actionTypes.RESET_KNOWN_SPELLS:
            return [];
        case globalActionTypes.LOAD_SHEET:
            return Object.assign([], (action.sheet || {knownSpells: []}).knownSpells);
        case actionTypes.ADD_KNOWN_SPELL:
            return addSpell(state);
        case actionTypes.CLEAN_KNOWN_SPELLS:
            return cleanSpells(state);
        case actionTypes.DELETE_KNOWN_SPELL:
            return state.filter((skill) => skill.id !== action.id);
        case actionTypes.UPDATE_KNOWN_SPELL:
            return updateOrAdd(state, action, {value: action.value});
        case actionTypes.MOVE_KNOWN_SPELL:
            return moveSpell(state, action);
        default:
            return state;
    }
}

function addSpell(state, obj = {}) {
    return state.concat(
        Object.assign({}, obj, {
            id: state.reduce((acc, spell) => (spell.id >= acc ? spell.id + 1 : acc), 0)
        })
    );
}

function updateOrAdd(state, action, obj) {
    if (action.id === 'new') {
        return addSpell(state, obj);
    } else {
        return state.map((spell) => {
            if (spell.id === action.id) {
                return Object.assign({}, spell, obj);
            }
            return spell;
        });
    }
}

function cleanSpells(state) {
    return state.filter((skill) => skill.value);
}

function moveSpell(state, action) {
    let source;
    let targetIndex;
    const newState = state.filter((spell, i) => {
        if (spell.id === action.id) {
            source = spell;
            return false;
        }
        return true;
    });
    if (action.targetId === 'new') {
        return newState.concat(source);
    }
    newState.find((spell, i) => {
        if (spell.id === action.targetId) {
            targetIndex = i;
            return true;
        }
        return false;
    });
    if (source && targetIndex !== undefined) {
        newState.splice(targetIndex + (action.below ? 1 : 0), 0, source);
        return newState;
    }
    return state;
}
