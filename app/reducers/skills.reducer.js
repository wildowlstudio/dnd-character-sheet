import {actionTypes} from '../actions/skill.actions';
import {actionTypes as globalActionTypes} from '../actions/global.actions';
import sortBy from '../utils/sort.util';

export default function reducer(state = [], action) {
    switch (action.type) {
        case globalActionTypes.RESET_SHEET:
        case actionTypes.RESET_SKILLS:
            return [];
        case globalActionTypes.LOAD_SHEET:
            return Object.assign([], (action.sheet || {skills: []}).skills);
        case actionTypes.ADD_SKILL:
            return addSkill(state);
        case actionTypes.DELETE_SKILLS:
            return deleteSkills(state);
        case actionTypes.DELETE_SKILL:
            return state.filter((skill) => skill.id !== action.id);
        case actionTypes.SORT_SKILLS:
            return sortBy(action.value, state, action.desc);
        case actionTypes.UPDATE_SKILL_CLASS:
            return updateOrAdd(state, action, {class: action.value});
        case actionTypes.UPDATE_SKILL_NAME:
            return updateOrAdd(state, action, {name: action.value});
        case actionTypes.UPDATE_SKILL_ATTRIBUTE:
            return updateOrAdd(state, action, {attribute: action.value});
        case actionTypes.UPDATE_SKILL_ABILITY_MOD:
            return updateOrAdd(state, action, {abilityMod: action.value});
        case actionTypes.UPDATE_SKILL_SKILL_RANK:
            return updateOrAdd(state, action, {skillRank: action.value});
        case actionTypes.UPDATE_SKILL_MISC_MOD:
            return updateOrAdd(state, action, {miscMod: action.value});
        default:
            return state;
    }
}

function addSkill(state, obj = {}) {
    return state.concat(
        Object.assign({}, obj, {
            id: state.reduce((acc, skill) => (skill.id >= acc ? skill.id + 1 : acc), 0)
        })
    );
}

function updateOrAdd(state, action, obj) {
    if (action.id === 'new') {
        return addSkill(state, obj);
    } else {
        return state.map((item) => {
            if (item.id === action.id) {
                return Object.assign({}, item, obj);
            }
            return item;
        });
    }
}

function deleteSkills(state) {
    return state.filter(
        (skill) =>
            skill.name ||
            skill.attribute ||
            skill.abilityMod ||
            skill.skillRank ||
            skill.miscMod
    );
}
