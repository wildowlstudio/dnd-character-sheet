import {loadSheet, resetSheet} from '../actions/global.actions';
import {
    resetSavingThrows,
    updateReflexAbilityMod,
    updateReflexBase,
    updateReflexMagicMod,
    updateReflexMiscMod,
    updateReflexOtherMod,
    updateReflexTempMod
} from '../actions/savingThrow.actions';

import reducer from './reflex.reducer';

describe('Reducer tests', () => {
    describe('Reflex reducer tests', () => {
        test('Should return the initial state', () => {
            expect(reducer(undefined, {})).toEqual({});
        });
        test('Should handle RESET_SHEET', () => {
            const state = {test: 'Test'};
            const newState = reducer(state, resetSheet());
            expect(newState).toEqual({});
            expect(newState).not.toBe(state);
        });
        test('Should handle RESET_SAVING_THROWS', () => {
            const state = {test: 'Test'};
            const newState = reducer(state, resetSavingThrows());
            expect(newState).toEqual({});
            expect(newState).not.toBe(state);
        });
        test('Should handle LOAD_SHEET', () => {
            const state = {};
            const sheetToLoad = {
                reflex: {test: 'test'}
            };
            const newState = reducer(state, loadSheet(sheetToLoad));
            expect(newState).toEqual(sheetToLoad.reflex);
            expect(newState).not.toBe(state);
            expect(newState).not.toBe(sheetToLoad.reflex);
        });

        test('Should handle UPDATE_REFLEX_BASE', () => {
            const state = {};
            const newState = reducer(state, updateReflexBase('Test'));
            expect(newState).toEqual({base: 'Test'});
            expect(reducer({test: 'Test'}, updateReflexBase('Test'))).toEqual({
                test: 'Test',
                base: 'Test'
            });
            expect(newState).not.toBe(state);
        });

        test('Should handle UPDATE_REFLEX_ABILITY_MOD', () => {
            const state = {};
            const newState = reducer(state, updateReflexAbilityMod('Test'));
            expect(newState).toEqual({abilityMod: 'Test'});
            expect(reducer({test: 'Test'}, updateReflexAbilityMod('Test'))).toEqual({
                test: 'Test',
                abilityMod: 'Test'
            });
            expect(newState).not.toBe(state);
        });

        test('Should handle UPDATE_REFLEX_MAGIC_MOD', () => {
            const state = {};
            const newState = reducer(state, updateReflexMagicMod('Test'));
            expect(newState).toEqual({magicMod: 'Test'});
            expect(reducer({test: 'Test'}, updateReflexMagicMod('Test'))).toEqual({
                test: 'Test',
                magicMod: 'Test'
            });
            expect(newState).not.toBe(state);
        });

        test('Should handle UPDATE_REFLEX_MISC_MOD', () => {
            const state = {};
            const newState = reducer(state, updateReflexMiscMod('Test'));
            expect(newState).toEqual({miscMod: 'Test'});
            expect(reducer({test: 'Test'}, updateReflexMiscMod('Test'))).toEqual({
                test: 'Test',
                miscMod: 'Test'
            });
            expect(newState).not.toBe(state);
        });

        test('Should handle UPDATE_REFLEX_TEMP_MOD', () => {
            const state = {};
            const newState = reducer(state, updateReflexTempMod('Test'));
            expect(newState).toEqual({tempMod: 'Test'});
            expect(reducer({test: 'Test'}, updateReflexTempMod('Test'))).toEqual({
                test: 'Test',
                tempMod: 'Test'
            });
            expect(newState).not.toBe(state);
        });

        test('Should handle UPDATE_REFLEX_OTHER_MOD', () => {
            const state = {};
            const newState = reducer(state, updateReflexOtherMod('Test'));
            expect(newState).toEqual({otherMod: 'Test'});
            expect(reducer({test: 'Test'}, updateReflexOtherMod('Test'))).toEqual({
                test: 'Test',
                otherMod: 'Test'
            });
            expect(newState).not.toBe(state);
        });
    });
});
