import {actionTypes} from '../actions/basic.actions';
import {actionTypes as globalActionTypes} from '../actions/global.actions';

export default function reducer(state = {}, action) {
    switch (action.type) {
        case globalActionTypes.RESET_SHEET:
            return {};
        case globalActionTypes.LOAD_SHEET:
            return Object.assign({}, state, (action.sheet || {basic: {}}).basic);
        case actionTypes.UPDATE_NAME:
            return Object.assign({}, state, {name: action.value});
        case actionTypes.UPDATE_PLAYER:
            return Object.assign({}, state, {player: action.value});
        case actionTypes.UPDATE_CLASS:
            return Object.assign({}, state, {class: action.value});
        case actionTypes.UPDATE_RACE:
            return Object.assign({}, state, {race: action.value});
        case actionTypes.UPDATE_ALIGNMENT:
            return Object.assign({}, state, {alignment: action.value});
        case actionTypes.UPDATE_DEITY:
            return Object.assign({}, state, {deity: action.value});
        case actionTypes.UPDATE_LEVEL:
            return Object.assign({}, state, {level: action.value});
        case actionTypes.UPDATE_SIZE:
            return Object.assign({}, state, {size: action.value});
        case actionTypes.UPDATE_AGE:
            return Object.assign({}, state, {age: action.value});
        case actionTypes.UPDATE_GENDER:
            return Object.assign({}, state, {gender: action.value});
        case actionTypes.UPDATE_HEIGHT:
            return Object.assign({}, state, {height: action.value});
        case actionTypes.UPDATE_WEIGHT:
            return Object.assign({}, state, {weight: action.value});
        case actionTypes.UPDATE_EYES:
            return Object.assign({}, state, {eyes: action.value});
        case actionTypes.UPDATE_HAIR:
            return Object.assign({}, state, {hair: action.value});
        default:
            return state;
    }
}
