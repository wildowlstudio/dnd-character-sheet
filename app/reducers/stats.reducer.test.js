import {loadSheet, resetSheet} from '../actions/global.actions';
import {
    resetStats,
    updateStatMod,
    updateStatTempMod,
    updateStatTempScore
} from '../actions/stat.actions';

import reducer from './stats.reducer';

describe('Reducer tests', () => {
    describe('Stats reducer tests', () => {
        test('Should return the initial state', () => {
            expect(reducer(undefined, {})).toEqual({
                str: {},
                dex: {},
                con: {},
                int: {},
                wis: {},
                cha: {}
            });
        });
        test('Should handle RESET_SHEET', () => {
            const state = {test: 'Test', test2: {mod: 'Test3'}};
            const newState = reducer(state, resetSheet());
            expect(newState).toEqual({
                str: {},
                dex: {},
                con: {},
                int: {},
                wis: {},
                cha: {}
            });
        });
        test('Should handle RESET_STATS', () => {
            const state = {test: 'Test', test2: {mod: 'Test3'}};
            const newState = reducer(state, resetStats());
            expect(newState).toEqual({
                str: {},
                dex: {},
                con: {},
                int: {},
                wis: {},
                cha: {}
            });
        });
        test('Should handle LOAD_SHEET', () => {
            const state = {};
            const sheetToLoad = {
                stats: {test: {mod: 'test'}}
            };
            const newState = reducer(state, loadSheet(sheetToLoad));
            expect(newState).toEqual(sheetToLoad.stats);
            expect(newState).not.toBe(state);
            expect(newState).not.toBe(sheetToLoad.stats);
        });
        test('Should handle UPDATE_STAT_MOD', () => {
            const state = {};
            const newState = reducer(state, updateStatMod('str', '5'));
            expect(newState).toEqual({str: {mod: 5}});
            expect(reducer({dex: {mod: 3}}, updateStatMod('str', '5'))).toEqual({
                dex: {mod: 3},
                str: {mod: 5}
            });
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_STAT_TEMP_MOD', () => {
            const state = {};
            const newState = reducer(state, updateStatTempMod('str', '5'));
            expect(newState).toEqual({str: {tmpMod: 5}});
            expect(
                reducer({dex: {tmpMod: 3}}, updateStatTempMod('str', '5'))
            ).toEqual({
                dex: {tmpMod: 3},
                str: {tmpMod: 5}
            });
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_STAT_TEMP_SCORE', () => {
            const state = {};
            const newState = reducer(state, updateStatTempScore('str', '5'));
            expect(newState).toEqual({str: {tmpScore: 5}});
            expect(
                reducer({dex: {tmpScore: 3}}, updateStatTempScore('str', '5'))
            ).toEqual({
                dex: {tmpScore: 3},
                str: {tmpScore: 5}
            });
            expect(newState).not.toBe(state);
        });
    });
});
