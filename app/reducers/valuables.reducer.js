import {actionTypes} from '../actions/valuables.actions';
import {actionTypes as globalActionTypes} from '../actions/global.actions';

function initialState() {
    return {
        money: 0,
        copper: true,
        silver: true,
        gold: true
    };
}

export default function reducer(state = initialState(), action) {
    switch (action.type) {
        case globalActionTypes.RESET_SHEET:
        case actionTypes.RESET_VALUABLES:
            return initialState();
        case globalActionTypes.LOAD_SHEET:
            return Object.assign(
                {},
                (action.sheet && action.sheet.valuables) || initialState()
            );
        case actionTypes.UPDATE_VALUABLES_MONEY:
            return Object.assign({}, state, {money: state.money + action.value});
        case actionTypes.UPDATE_VALUABLES_BRONZE_OPTION:
            return Object.assign({}, state, {bronze: action.value});
        case actionTypes.UPDATE_VALUABLES_IRON_OPTION:
            return Object.assign({}, state, {iron: action.value});
        case actionTypes.UPDATE_VALUABLES_COPPER_OPTION:
            return Object.assign({}, state, {copper: action.value});
        case actionTypes.UPDATE_VALUABLES_SILVER_OPTION:
            return Object.assign({}, state, {silver: action.value});
        case actionTypes.UPDATE_VALUABLES_GOLD_OPTION:
            return Object.assign({}, state, {gold: action.value});
        case actionTypes.UPDATE_VALUABLES_PLATINUM_OPTION:
            return Object.assign({}, state, {platinum: action.value});
        default:
            return state;
    }
}
