import {loadSheet, resetSheet} from '../actions/global.actions';
import {
    resetSpells,
    updateSpellBonus,
    updateSpellKnown,
    updateSpellPerDay,
    updateSpellSaveDc
} from '../actions/spell.actions';

import reducer from './spells.reducer';

describe('Reducer tests', () => {
    describe('Spells reducer tests', () => {
        test('Should return the initial state', () => {
            expect(reducer(undefined, {})).toEqual([...Array(10)].map(() => ({})));
        });
        test('Should handle RESET_SHEET', () => {
            const state = [{test: 'Test'}];
            const newState = reducer(state, resetSheet());
            expect(newState).toEqual([...Array(10)].map(() => ({})));
            expect(newState).not.toBe(state);
        });
        test('Should handle RESET_SPELLS', () => {
            const state = [{test: 'Test'}];
            const newState = reducer(state, resetSpells());
            expect(newState).toEqual([...Array(10)].map(() => ({})));
            expect(newState).not.toBe(state);
        });
        test('Should handle LOAD_SHEET', () => {
            const state = [];
            const sheetToLoad = {
                spells: [...Array(10)].map((_, i) => ({known: i}))
            };
            const newState = reducer(state, loadSheet(sheetToLoad));
            expect(newState).toEqual(sheetToLoad.spells);
            expect(newState).not.toBe(state);
            expect(newState).not.toBe(sheetToLoad.spells);
        });
        test('Should handle UPDATE_SPELL_KNOWN', () => {
            const state = [...Array(10)].map(() => ({}));
            const newState = reducer(state, updateSpellKnown(1, 'Test'));
            expect(newState).toEqual([
                {},
                {known: 'Test'},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {}
            ]);
            expect(
                reducer([...Array(10)].map(() => ({})), updateSpellKnown(2, 'Test'))
            ).toEqual([{}, {}, {known: 'Test'}, {}, {}, {}, {}, {}, {}, {}]);
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_SPELL_PER_DAY', () => {
            const state = [...Array(10)].map(() => ({}));
            const newState = reducer(state, updateSpellPerDay(1, 'Test'));
            expect(newState).toEqual([
                {},
                {perDay: 'Test'},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {}
            ]);
            expect(
                reducer([...Array(10)].map(() => ({})), updateSpellPerDay(2, 'Test'))
            ).toEqual([{}, {}, {perDay: 'Test'}, {}, {}, {}, {}, {}, {}, {}]);
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_SPELL_BONUS', () => {
            const state = [...Array(10)].map(() => ({}));
            const newState = reducer(state, updateSpellBonus(1, 'Test'));
            expect(newState).toEqual([
                {},
                {bonus: 'Test'},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {}
            ]);
            expect(
                reducer([...Array(10)].map(() => ({})), updateSpellBonus(2, 'Test'))
            ).toEqual([{}, {}, {bonus: 'Test'}, {}, {}, {}, {}, {}, {}, {}]);
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_SPELL_SAVE_DC', () => {
            const state = [...Array(10)].map(() => ({}));
            const newState = reducer(state, updateSpellSaveDc(1, 'Test'));
            expect(newState).toEqual([
                {},
                {saveDc: 'Test'},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {}
            ]);
            expect(
                reducer([...Array(10)].map(() => ({})), updateSpellSaveDc(2, 'Test'))
            ).toEqual([{}, {}, {saveDc: 'Test'}, {}, {}, {}, {}, {}, {}, {}]);
            expect(newState).not.toBe(state);
        });
    });
});
