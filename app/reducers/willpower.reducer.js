import {actionTypes} from '../actions/savingThrow.actions';
import {actionTypes as globalActionTypes} from '../actions/global.actions';

export default function reducer(state = {}, action) {
    switch (action.type) {
        case globalActionTypes.RESET_SHEET:
        case actionTypes.RESET_SAVING_THROWS:
            return {};
        case globalActionTypes.LOAD_SHEET:
            return Object.assign(
                {},
                state,
                (action.sheet || {willpower: {}}).willpower
            );
        case actionTypes.UPDATE_WILLPOWER_BASE:
            return Object.assign({}, state, {base: action.value});
        case actionTypes.UPDATE_WILLPOWER_ABILITY_MOD:
            return Object.assign({}, state, {abilityMod: action.value});
        case actionTypes.UPDATE_WILLPOWER_MAGIC_MOD:
            return Object.assign({}, state, {magicMod: action.value});
        case actionTypes.UPDATE_WILLPOWER_MISC_MOD:
            return Object.assign({}, state, {miscMod: action.value});
        case actionTypes.UPDATE_WILLPOWER_TEMP_MOD:
            return Object.assign({}, state, {tempMod: action.value});
        case actionTypes.UPDATE_WILLPOWER_OTHER_MOD:
            return Object.assign({}, state, {otherMod: action.value});
        default:
            return state;
    }
}
