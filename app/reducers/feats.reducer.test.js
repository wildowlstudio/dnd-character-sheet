import {
    addFeat,
    cleanFeats,
    deleteFeat,
    moveFeat,
    resetFeats,
    updateFeat
} from '../actions/feat.actions';
import {loadSheet, resetSheet} from '../actions/global.actions';

import reducer from './feats.reducer';

describe('Reducer tests', () => {
    describe('Feats reducer tests', () => {
        test('Should return the initial state', () => {
            expect(reducer(undefined, {})).toEqual([]);
        });
        test('Should handle RESET_SHEET', () => {
            const state = [{name: 'test'}];
            const newState = reducer(state, resetSheet());
            expect(newState).toEqual([]);
            expect(newState).not.toBe(state);
        });
        test('Should handle RESET_FEATS', () => {
            const state = [{name: 'test'}];
            const newState = reducer(state, resetFeats());
            expect(newState).toEqual([]);
            expect(newState).not.toBe(state);
        });
        test('Should handle LOAD_SHEET', () => {
            const state = [{name: 'test'}];
            const sheetToLoad = {
                feats: [{name: 'test2'}]
            };
            const newState = reducer(state, loadSheet(sheetToLoad));
            expect(newState).toEqual(sheetToLoad.feats);
            expect(newState).not.toBe(state);
            expect(newState).not.toBe(sheetToLoad.feats);
        });
        test('Should handle ADD_FEAT', () => {
            const state = [{id: 0, name: 'test'}];
            const newState = reducer(state, addFeat());
            expect(newState).toEqual([{id: 0, name: 'test'}, {id: 1}]);
            expect(newState).not.toBe(state);
            expect(reducer([], addFeat())).toEqual([{id: 0}]);
        });

        test('Should handle CLEAN_FEATS', () => {
            const state = [
                {id: 0, value: 'test'},
                {id: 2},
                {id: 1, value: 'test2'},
                {id: 3},
                {id: 4}
            ];
            const newState = reducer(state, cleanFeats(1));
            expect(newState).toEqual([
                {id: 0, value: 'test'},
                {id: 1, value: 'test2'}
            ]);
            expect(newState).not.toBe(state);
        });

        test('Should handle DELETE_FEAT', () => {
            const state = [{id: 0, name: 'test'}, {id: 1, name: 'test2'}];
            const newState = reducer(state, deleteFeat(1));
            expect(newState).toEqual([{id: 0, name: 'test'}]);
            expect(newState).not.toBe(state);
        });

        test('Should handle MOVE_FEAT', () => {
            const state = [
                {id: 0, name: 'test'},
                {id: 1, name: 'test2'},
                {id: 2, name: 'aTest'}
            ];
            const newState = reducer(state, moveFeat(0, true, 2));
            expect(newState).not.toBe(state);
            expect(newState).toEqual([
                {id: 1, name: 'test2'},
                {id: 2, name: 'aTest'},
                {id: 0, name: 'test'}
            ]);
            expect(reducer(state, moveFeat(0, false, 2))).toEqual([
                {id: 1, name: 'test2'},
                {id: 0, name: 'test'},
                {id: 2, name: 'aTest'}
            ]);
            expect(reducer(state, moveFeat(0, true, 'new'))).toEqual([
                {id: 1, name: 'test2'},
                {id: 2, name: 'aTest'},
                {id: 0, name: 'test'}
            ]);
            expect(reducer(state, moveFeat(0, false, 'new'))).toEqual([
                {id: 1, name: 'test2'},
                {id: 2, name: 'aTest'},
                {id: 0, name: 'test'}
            ]);
        });

        test('Should handle UPDATE_FEAT', () => {
            const state = [{id: 0, name: 'test'}, {id: 1, name: 'test2'}];
            const newState = reducer(state, updateFeat(1, 'test3'));
            expect(newState).not.toBe(state);
            expect(reducer(state, updateFeat(1, 'test3'))).toEqual([
                {id: 0, name: 'test'},
                {id: 1, name: 'test2', value: 'test3'}
            ]);
            expect(reducer(state, updateFeat('new', 'test3'))).toEqual([
                {id: 0, name: 'test'},
                {id: 1, name: 'test2'},
                {id: 2, value: 'test3'}
            ]);
        });
    });
});
