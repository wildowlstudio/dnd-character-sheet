import {loadSheet, resetSheet} from '../actions/global.actions';
import {
    resetSkills,
    updateSkillMaxClassRank,
    updateSkillMaxCrossClassRank
} from '../actions/skill.actions';

import reducer from './skill.reducer';

describe('Reducer tests', () => {
    describe('Skill reducer tests', () => {
        test('Should return the initial state', () => {
            expect(reducer(undefined, {})).toEqual({});
        });
        test('Should handle RESET_SHEET', () => {
            const state = {};
            const newState = reducer(state, resetSheet());
            expect(newState).toEqual({});
            expect(newState).not.toBe(state);
        });
        test('Should handle RESET_SKILLS', () => {
            const state = {};
            const newState = reducer(state, resetSkills());
            expect(newState).toEqual({});
            expect(newState).not.toBe(state);
        });
        test('Should handle LOAD_SHEET', () => {
            const state = [{name: 'test'}];
            const sheetToLoad = {
                skill: {name: 'test2'}
            };
            const newState = reducer(state, loadSheet(sheetToLoad));
            expect(newState).toEqual(sheetToLoad.skill);
            expect(newState).not.toBe(state);
            expect(newState).not.toBe(sheetToLoad.skill);
        });

        test('Should handle UPDATE_SKILL_MAX_CLASS_RANK', () => {
            const state = {test: 'test'};
            const newState = reducer(state, updateSkillMaxClassRank('test2'));
            expect(newState).toEqual({test: 'test', maxClassRank: 'test2'});
            expect(newState).not.toBe(state);
        });

        test('Should handle UPDATE_SKILL_MAX_CROSS_CLASS_RANK', () => {
            const state = {test: 'test'};
            const newState = reducer(state, updateSkillMaxCrossClassRank('test2'));
            expect(newState).toEqual({test: 'test', maxCrossClassRank: 'test2'});
            expect(newState).not.toBe(state);
        });
    });
});
