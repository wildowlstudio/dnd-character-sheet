import {actionTypes} from '../actions/feat.actions';
import {actionTypes as globalActionTypes} from '../actions/global.actions';

export default function reducer(state = [], action) {
    switch (action.type) {
        case globalActionTypes.RESET_SHEET:
        case actionTypes.RESET_FEATS:
            return [];
        case globalActionTypes.LOAD_SHEET:
            return Object.assign([], (action.sheet || {feats: []}).feats);
        case actionTypes.ADD_FEAT:
            return addFeat(state);
        case actionTypes.CLEAN_FEATS:
            return cleanFeats(state);
        case actionTypes.DELETE_FEAT:
            return state.filter((skill) => skill.id !== action.id);
        case actionTypes.UPDATE_FEAT:
            return updateOrAdd(state, action, {value: action.value});
        case actionTypes.MOVE_FEAT:
            return moveFeat(state, action);
        default:
            return state;
    }
}

function addFeat(state, obj = {}) {
    return state.concat(
        Object.assign({}, obj, {
            id: state.reduce((acc, feat) => (feat.id >= acc ? feat.id + 1 : acc), 0)
        })
    );
}

function updateOrAdd(state, action, obj) {
    if (action.id === 'new') {
        return addFeat(state, obj);
    } else {
        return state.map((feat) => {
            if (feat.id === action.id) {
                return Object.assign({}, feat, obj);
            }
            return feat;
        });
    }
}

function cleanFeats(state) {
    return state.filter((skill) => skill.value);
}

function moveFeat(state, action) {
    let source;
    let targetIndex;
    const newState = state.filter((feat, i) => {
        if (feat.id === action.id) {
            source = feat;
            return false;
        }
        return true;
    });
    if (action.targetId === 'new') {
        return newState.concat(source);
    }
    newState.find((feat, i) => {
        if (feat.id === action.targetId) {
            targetIndex = i;
            return true;
        }
        return false;
    });
    if (source && targetIndex !== undefined) {
        newState.splice(targetIndex + (action.below ? 1 : 0), 0, source);
        return newState;
    }
    return state;
}
