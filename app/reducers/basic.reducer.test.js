import {loadSheet, resetSheet} from '../actions/global.actions';
import {
    updateAge,
    updateAlignment,
    updateClass,
    updateDeity,
    updateEyes,
    updateGender,
    updateHair,
    updateHeight,
    updateLevel,
    updateName,
    updatePlayer,
    updateRace,
    updateSize,
    updateWeight
} from '../actions/basic.actions';

import reducer from './basic.reducer';

describe('Reducer tests', () => {
    describe('Basic reducer tests', () => {
        test('Should return the initial state', () => {
            expect(reducer(undefined, {})).toEqual({});
        });
        test('Should handle RESET_SHEET', () => {
            const state = {test: 'Test'};
            const newState = reducer(state, resetSheet());
            expect(newState).toEqual({});
            expect(newState).not.toBe(state);
        });
        test('Should handle LOAD_SHEET', () => {
            const state = {};
            const sheetToLoad = {
                basic: {test: 'test'}
            };
            const newState = reducer(state, loadSheet(sheetToLoad));
            expect(newState).toEqual(sheetToLoad.basic);
            expect(newState).not.toBe(state);
            expect(newState).not.toBe(sheetToLoad.basic);
        });
        test('Should handle UPDATE_NAME', () => {
            const state = {};
            const newState = reducer(state, updateName('Test'));
            expect(newState).toEqual({name: 'Test'});
            expect(reducer({test: 'Test'}, updateName('Test'))).toEqual({
                test: 'Test',
                name: 'Test'
            });
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_PLAYER', () => {
            const state = {};
            const newState = reducer(state, updatePlayer('Test'));
            expect(newState).toEqual({player: 'Test'});
            expect(reducer({test: 'Test'}, updatePlayer('Test'))).toEqual({
                test: 'Test',
                player: 'Test'
            });
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_CLASS', () => {
            const state = {};
            const newState = reducer(state, updateClass('Test'));
            expect(newState).toEqual({class: 'Test'});
            expect(reducer({test: 'Test'}, updateClass('Test'))).toEqual({
                test: 'Test',
                class: 'Test'
            });
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_RACE', () => {
            const state = {};
            const newState = reducer(state, updateRace('Test'));
            expect(newState).toEqual({race: 'Test'});
            expect(reducer({test: 'Test'}, updateRace('Test'))).toEqual({
                test: 'Test',
                race: 'Test'
            });
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_ALIGNMENT', () => {
            const state = {};
            const newState = reducer(state, updateAlignment('Test'));
            expect(newState).toEqual({alignment: 'Test'});
            expect(reducer({test: 'Test'}, updateAlignment('Test'))).toEqual({
                test: 'Test',
                alignment: 'Test'
            });
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_DEITY', () => {
            const state = {};
            const newState = reducer(state, updateDeity('Test'));
            expect(newState).toEqual({deity: 'Test'});
            expect(reducer({test: 'Test'}, updateDeity('Test'))).toEqual({
                test: 'Test',
                deity: 'Test'
            });
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_LEVEL', () => {
            const state = {};
            const newState = reducer(state, updateLevel('Test'));
            expect(newState).toEqual({level: 'Test'});
            expect(reducer({test: 'Test'}, updateLevel('Test'))).toEqual({
                test: 'Test',
                level: 'Test'
            });
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_SIZE', () => {
            const state = {};
            const newState = reducer(state, updateSize('Test'));
            expect(newState).toEqual({size: 'Test'});
            expect(reducer({test: 'Test'}, updateSize('Test'))).toEqual({
                test: 'Test',
                size: 'Test'
            });
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_AGE', () => {
            const state = {};
            const newState = reducer(state, updateAge('Test'));
            expect(newState).toEqual({age: 'Test'});
            expect(reducer({test: 'Test'}, updateAge('Test'))).toEqual({
                test: 'Test',
                age: 'Test'
            });
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_GENDER', () => {
            const state = {};
            const newState = reducer(state, updateGender('Test'));
            expect(newState).toEqual({gender: 'Test'});
            expect(reducer({test: 'Test'}, updateGender('Test'))).toEqual({
                test: 'Test',
                gender: 'Test'
            });
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_HEIGHT', () => {
            const state = {};
            const newState = reducer(state, updateHeight('Test'));
            expect(newState).toEqual({height: 'Test'});
            expect(reducer({test: 'Test'}, updateHeight('Test'))).toEqual({
                test: 'Test',
                height: 'Test'
            });
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_WEIGHT', () => {
            const state = {};
            const newState = reducer(state, updateWeight('Test'));
            expect(newState).toEqual({weight: 'Test'});
            expect(reducer({test: 'Test'}, updateWeight('Test'))).toEqual({
                test: 'Test',
                weight: 'Test'
            });
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_EYES', () => {
            const state = {};
            const newState = reducer(state, updateEyes('Test'));
            expect(newState).toEqual({eyes: 'Test'});
            expect(reducer({test: 'Test'}, updateEyes('Test'))).toEqual({
                test: 'Test',
                eyes: 'Test'
            });
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_HAIR', () => {
            const state = {};
            const newState = reducer(state, updateHair('Test'));
            expect(newState).toEqual({hair: 'Test'});
            expect(reducer({test: 'Test'}, updateHair('Test'))).toEqual({
                test: 'Test',
                hair: 'Test'
            });
            expect(newState).not.toBe(state);
        });
    });
});
