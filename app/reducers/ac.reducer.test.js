import {loadSheet, resetSheet} from '../actions/global.actions';
import {
    resetHealth,
    updateAcArmor,
    updateAcDex,
    updateAcMisc,
    updateAcShield,
    updateAcSize
} from '../actions/health.actions';

import reducer from './ac.reducer';

describe('Reducer tests', () => {
    describe('Ac reducer tests', () => {
        test('Should return the initial state', () => {
            expect(reducer(undefined, {})).toEqual({});
        });
        test('Should handle RESET_SHEET', () => {
            const state = {test: 'Test'};
            const newState = reducer(state, resetSheet());
            expect(newState).toEqual({});
            expect(newState).not.toBe(state);
        });
        test('Should handle LOAD_SHEET', () => {
            const state = {};
            const sheetToLoad = {
                ac: {test: 'test'}
            };
            const newState = reducer(state, loadSheet(sheetToLoad));
            expect(newState).toEqual(sheetToLoad.ac);
            expect(newState).not.toBe(state);
            expect(newState).not.toBe(sheetToLoad.ac);
        });
        test('Should handle RESET_HEALTH', () => {
            const state = {test: 'Test'};
            const newState = reducer(state, resetHealth());
            expect(newState).toEqual({});
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_AC_ARMOR', () => {
            const state = {};
            const newState = reducer(state, updateAcArmor('Test'));
            expect(newState).toEqual({armor: 'Test'});
            expect(reducer({test: 'Test'}, updateAcArmor('Test'))).toEqual({
                test: 'Test',
                armor: 'Test'
            });
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_AC_SHIELD', () => {
            const state = {};
            const newState = reducer(state, updateAcShield('Test'));
            expect(newState).toEqual({shield: 'Test'});
            expect(reducer({test: 'Test'}, updateAcShield('Test'))).toEqual({
                test: 'Test',
                shield: 'Test'
            });
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_AC_DEX', () => {
            const state = {};
            const newState = reducer(state, updateAcDex('Test'));
            expect(newState).toEqual({dex: 'Test'});
            expect(reducer({test: 'Test'}, updateAcDex('Test'))).toEqual({
                test: 'Test',
                dex: 'Test'
            });
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_AC_SIZE', () => {
            const state = {};
            const newState = reducer(state, updateAcSize('Test'));
            expect(newState).toEqual({size: 'Test'});
            expect(reducer({test: 'Test'}, updateAcSize('Test'))).toEqual({
                test: 'Test',
                size: 'Test'
            });
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_AC_MISC', () => {
            const state = {};
            const newState = reducer(state, updateAcMisc('Test'));
            expect(newState).toEqual({misc: 'Test'});
            expect(reducer({test: 'Test'}, updateAcMisc('Test'))).toEqual({
                test: 'Test',
                misc: 'Test'
            });
            expect(newState).not.toBe(state);
        });
    });
});
