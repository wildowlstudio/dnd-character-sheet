import {actionTypes} from '../actions/health.actions';
import {actionTypes as globalActionTypes} from '../actions/global.actions';

export default function reducer(state = {}, action) {
    switch (action.type) {
        case globalActionTypes.RESET_SHEET:
        case actionTypes.RESET_HEALTH:
            return {};
        case globalActionTypes.LOAD_SHEET:
            return Object.assign({}, state, (action.sheet || {ac: {}}).ac);
        case actionTypes.UPDATE_NAME:
            return Object.assign({}, state, {name: action.value});
        case actionTypes.UPDATE_AC_ARMOR:
            return Object.assign({}, state, {armor: action.value});
        case actionTypes.UPDATE_AC_SHIELD:
            return Object.assign({}, state, {shield: action.value});
        case actionTypes.UPDATE_AC_DEX:
            return Object.assign({}, state, {dex: action.value});
        case actionTypes.UPDATE_AC_SIZE:
            return Object.assign({}, state, {size: action.value});
        case actionTypes.UPDATE_AC_MISC:
            return Object.assign({}, state, {misc: action.value});
        default:
            return state;
    }
}
