import {actionTypes} from '../actions/xp.actions';
import {actionTypes as globalActionTypes} from '../actions/global.actions';

export default function reducer(state = '', action) {
    switch (action.type) {
        case globalActionTypes.RESET_SHEET:
        case actionTypes.RESET_XP:
            return '';
        case globalActionTypes.LOAD_SHEET:
            return (action.sheet && action.sheet.xp) || '';
        case actionTypes.UPDATE_XP:
            return action.value;
        default:
            return state;
    }
}
