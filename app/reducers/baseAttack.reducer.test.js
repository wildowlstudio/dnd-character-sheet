import {loadSheet, resetSheet} from '../actions/global.actions';
import {resetBaseAttack, updateBaseAttack} from '../actions/baseAttack.actions';

import reducer from './baseAttack.reducer';

describe('Reducer tests', () => {
    describe('Base attack reducer tests', () => {
        test('Should return the initial state', () => {
            expect(reducer(undefined, {})).toEqual('');
        });
        test('Should handle RESET_SHEET', () => {
            const state = 'Test';
            const newState = reducer(state, resetSheet());
            expect(newState).toEqual('');
            expect(newState).not.toBe(state);
        });
        test('Should handle RESET_SPEED', () => {
            const state = 'Test';
            const newState = reducer(state, resetBaseAttack());
            expect(newState).toEqual('');
            expect(newState).not.toBe(state);
        });
        test('Should handle LOAD_SHEET', () => {
            const state = 'test1';
            const sheetToLoad = {
                baseAttack: 'test2'
            };
            const newState = reducer(state, loadSheet(sheetToLoad));
            expect(newState).toEqual(sheetToLoad.baseAttack);
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_BASE_ATTACK', () => {
            const state = '';
            const newState = reducer(state, updateBaseAttack('Test'));
            expect(newState).toEqual('Test');
            expect(reducer('Test', updateBaseAttack('Test2'))).toEqual('Test2');
            expect(newState).not.toBe(state);
        });
    });
});
