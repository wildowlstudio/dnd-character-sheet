import {actionTypes} from '../actions/language.actions';
import {actionTypes as globalActionTypes} from '../actions/global.actions';

export default function reducer(state = [], action) {
    switch (action.type) {
        case globalActionTypes.RESET_SHEET:
        case actionTypes.RESET_LANGUAGES:
            return [];
        case globalActionTypes.LOAD_SHEET:
            return Object.assign([], (action.sheet || {languages: []}).languages);
        case actionTypes.ADD_LANGUAGE:
            return addLanguage(state);
        case actionTypes.CLEAN_LANGUAGES:
            return cleanLanguages(state);
        case actionTypes.DELETE_LANGUAGE:
            return state.filter((skill) => skill.id !== action.id);
        case actionTypes.UPDATE_LANGUAGE:
            return updateOrAdd(state, action, {value: action.value});
        case actionTypes.MOVE_LANGUAGE:
            return moveLanguage(state, action);
        default:
            return state;
    }
}

function addLanguage(state, obj = {}) {
    return state.concat(
        Object.assign({}, obj, {
            id: state.reduce(
                (acc, language) => (language.id >= acc ? language.id + 1 : acc),
                0
            )
        })
    );
}

function updateOrAdd(state, action, obj) {
    if (action.id === 'new') {
        return addLanguage(state, obj);
    } else {
        return state.map((language) => {
            if (language.id === action.id) {
                return Object.assign({}, language, obj);
            }
            return language;
        });
    }
}

function cleanLanguages(state) {
    return state.filter((skill) => skill.value);
}

function moveLanguage(state, action) {
    let source;
    let targetIndex;
    const newState = state.filter((language, i) => {
        if (language.id === action.id) {
            source = language;
            return false;
        }
        return true;
    });
    if (action.targetId === 'new') {
        return newState.concat(source);
    }
    newState.find((language, i) => {
        if (language.id === action.targetId) {
            targetIndex = i;
            return true;
        }
        return false;
    });
    if (source && targetIndex !== undefined) {
        newState.splice(targetIndex + (action.below ? 1 : 0), 0, source);
        return newState;
    }
    return state;
}
