import {loadSheet, resetSheet} from '../actions/global.actions';
import {
    resetSavingThrows,
    updateFortitudeAbilityMod,
    updateFortitudeBase,
    updateFortitudeMagicMod,
    updateFortitudeMiscMod,
    updateFortitudeOtherMod,
    updateFortitudeTempMod
} from '../actions/savingThrow.actions';

import reducer from './fortitude.reducer';

describe('Reducer tests', () => {
    describe('Fortitude reducer tests', () => {
        test('Should return the initial state', () => {
            expect(reducer(undefined, {})).toEqual({});
        });
        test('Should handle RESET_SHEET', () => {
            const state = {test: 'Test'};
            const newState = reducer(state, resetSheet());
            expect(newState).toEqual({});
            expect(newState).not.toBe(state);
        });
        test('Should handle RESET_SAVING_THROWS', () => {
            const state = {test: 'Test'};
            const newState = reducer(state, resetSavingThrows());
            expect(newState).toEqual({});
            expect(newState).not.toBe(state);
        });
        test('Should handle LOAD_SHEET', () => {
            const state = {};
            const sheetToLoad = {
                fortitude: {test: 'test'}
            };
            const newState = reducer(state, loadSheet(sheetToLoad));
            expect(newState).toEqual(sheetToLoad.fortitude);
            expect(newState).not.toBe(state);
            expect(newState).not.toBe(sheetToLoad.fortitude);
        });

        test('Should handle UPDATE_FORTITUDE_BASE', () => {
            const state = {};
            const newState = reducer(state, updateFortitudeBase('Test'));
            expect(newState).toEqual({base: 'Test'});
            expect(reducer({test: 'Test'}, updateFortitudeBase('Test'))).toEqual({
                test: 'Test',
                base: 'Test'
            });
            expect(newState).not.toBe(state);
        });

        test('Should handle UPDATE_FORTITUDE_ABILITY_MOD', () => {
            const state = {};
            const newState = reducer(state, updateFortitudeAbilityMod('Test'));
            expect(newState).toEqual({abilityMod: 'Test'});
            expect(reducer({test: 'Test'}, updateFortitudeAbilityMod('Test'))).toEqual({
                test: 'Test',
                abilityMod: 'Test'
            });
            expect(newState).not.toBe(state);
        });

        test('Should handle UPDATE_FORTITUDE_MAGIC_MOD', () => {
            const state = {};
            const newState = reducer(state, updateFortitudeMagicMod('Test'));
            expect(newState).toEqual({magicMod: 'Test'});
            expect(reducer({test: 'Test'}, updateFortitudeMagicMod('Test'))).toEqual({
                test: 'Test',
                magicMod: 'Test'
            });
            expect(newState).not.toBe(state);
        });

        test('Should handle UPDATE_FORTITUDE_MISC_MOD', () => {
            const state = {};
            const newState = reducer(state, updateFortitudeMiscMod('Test'));
            expect(newState).toEqual({miscMod: 'Test'});
            expect(reducer({test: 'Test'}, updateFortitudeMiscMod('Test'))).toEqual({
                test: 'Test',
                miscMod: 'Test'
            });
            expect(newState).not.toBe(state);
        });

        test('Should handle UPDATE_FORTITUDE_TEMP_MOD', () => {
            const state = {};
            const newState = reducer(state, updateFortitudeTempMod('Test'));
            expect(newState).toEqual({tempMod: 'Test'});
            expect(reducer({test: 'Test'}, updateFortitudeTempMod('Test'))).toEqual({
                test: 'Test',
                tempMod: 'Test'
            });
            expect(newState).not.toBe(state);
        });

        test('Should handle UPDATE_FORTITUDE_OTHER_MOD', () => {
            const state = {};
            const newState = reducer(state, updateFortitudeOtherMod('Test'));
            expect(newState).toEqual({otherMod: 'Test'});
            expect(reducer({test: 'Test'}, updateFortitudeOtherMod('Test'))).toEqual({
                test: 'Test',
                otherMod: 'Test'
            });
            expect(newState).not.toBe(state);
        });
    });
});
