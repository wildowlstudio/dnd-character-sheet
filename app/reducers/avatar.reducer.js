import {actionTypes} from '../actions/avatar.actions';
import {actionTypes as globalActionTypes} from '../actions/global.actions';

export default function reducer(state = null, action) {
    switch (action.type) {
        case globalActionTypes.RESET_SHEET:
        case actionTypes.RESET_AVATAR:
            return null;
        case globalActionTypes.LOAD_SHEET:
            return (action.sheet && action.sheet.avatar) || null;
        case actionTypes.UPDATE_AVATAR:
            return action.value;
        default:
            return state;
    }
}
