import equipmentOptionsReducer from './equipmentOptions.reducer';

export default function reducer(state = {}, action) {
    return {
        equipment: equipmentOptionsReducer(state.equipment, action)
    };
}
