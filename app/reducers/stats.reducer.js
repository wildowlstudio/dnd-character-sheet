import {actionTypes} from '../actions/stat.actions';
import {actionTypes as globalActionTypes} from '../actions/global.actions';

const initialState = () => ({
    str: {},
    dex: {},
    con: {},
    int: {},
    wis: {},
    cha: {}
});

export default function reducer(state = initialState(), action) {
    switch (action.type) {
        case globalActionTypes.LOAD_SHEET:
            return Object.assign({}, action.sheet.stats);
        case globalActionTypes.RESET_SHEET:
        case actionTypes.RESET_STATS:
            return Object.assign({}, initialState());
        case actionTypes.UPDATE_STAT_MOD: {
            const obj = {};
            obj[action.stat] = Object.assign({}, state[action.stat], {
                mod: +action.value
            });
            return Object.assign({}, state, obj);
        }
        case actionTypes.UPDATE_STAT_TEMP_MOD: {
            const obj = {};
            obj[action.stat] = Object.assign({}, state[action.stat], {
                tmpMod: +action.value
            });
            return Object.assign({}, state, obj);
        }
        case actionTypes.UPDATE_STAT_TEMP_SCORE: {
            const obj = {};
            obj[action.stat] = Object.assign({}, state[action.stat], {
                tmpScore: +action.value
            });
            return Object.assign({}, state, obj);
        }
        default:
            return state;
    }
}
