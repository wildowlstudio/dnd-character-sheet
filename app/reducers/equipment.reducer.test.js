import {
    addEquipmentItem,
    deleteEquipmentItem,
    deleteEquipmentItems,
    moveEquipmentItem,
    resetEquipment,
    sortEquipment,
    updateEquipmentItemCapacity,
    updateEquipmentItemName,
    updateEquipmentItemValue,
    updateEquipmentItemWeight
} from '../actions/equipment.actions';
import {loadSheet, resetSheet} from '../actions/global.actions';

import reducer from './equipment.reducer';

describe('Reducer tests', () => {
    describe('Equipment reducer tests', () => {
        test('Should return the initial state', () => {
            expect(reducer(undefined, {})).toEqual([]);
        });
        test('Should handle RESET_SHEET', () => {
            const state = [{name: 'test'}];
            const newState = reducer(state, resetSheet());
            expect(newState).toEqual([]);
            expect(newState).not.toBe(state);
        });
        test('Should handle RESET_EQUIPMENT', () => {
            const state = [{name: 'test'}];
            const newState = reducer(state, resetEquipment());
            expect(newState).toEqual([]);
            expect(newState).not.toBe(state);
        });
        test('Should handle LOAD_SHEET', () => {
            const state = [{name: 'test'}];
            const sheetToLoad = {
                equipment: [{name: 'test2'}]
            };
            const newState = reducer(state, loadSheet(sheetToLoad));
            expect(newState).toEqual(sheetToLoad.equipment);
            expect(newState).not.toBe(state);
            expect(newState).not.toBe(sheetToLoad.equipment);
        });
        test('Should handle ADD_EQUIPMENT_ITEM', () => {
            const state = [{id: 0, name: 'test'}];
            const newState = reducer(state, addEquipmentItem());
            expect(newState).toEqual([{id: 0, name: 'test'}, {id: 1}]);
            expect(newState).not.toBe(state);
            expect(reducer([], addEquipmentItem())).toEqual([{id: 0}]);
        });

        test('Should handle DELETE_EQUIPMENT_ITEMS', () => {
            const state = [
                {id: 0, name: 'test'},
                {id: 1, name: 'test2'},
                {id: 2},
                {id: 3},
                {id: 4}
            ];
            const newState = reducer(state, deleteEquipmentItems());
            expect(newState).toEqual([{id: 0, name: 'test'}, {id: 1, name: 'test2'}]);
            expect(newState).not.toBe(state);
        });

        test('Should handle DELETE_EQUIPMENT_ITEM', () => {
            const state = [{id: 0, name: 'test'}, {id: 1, name: 'test2'}, {id: 2}];
            const newState = reducer(state, deleteEquipmentItem(1));
            expect(newState).toEqual([{id: 0, name: 'test'}, {id: 2}]);
            expect(newState).not.toBe(state);
        });

        test('Should handle SORT_EQUIPMENT', () => {
            const state = [
                {id: 4},
                {
                    id: 0,
                    name: 'test',
                    items: [{id: 6, name: 'test6'}, {id: 5, name: 'test5'}]
                },
                {id: 1, name: 'test2'},
                {id: 2, name: 'aTest'}
            ];
            const newState = reducer(state, sortEquipment('name', false));
            expect(newState).not.toBe(state);
            expect(newState).toEqual([
                {id: 2, name: 'aTest'},
                {
                    id: 0,
                    name: 'test',
                    items: [{id: 5, name: 'test5'}, {id: 6, name: 'test6'}]
                },
                {id: 1, name: 'test2'},
                {id: 4}
            ]);
            expect(reducer(state, sortEquipment('name', true))).toEqual([
                {id: 1, name: 'test2'},
                {
                    id: 0,
                    name: 'test',
                    items: [{id: 6, name: 'test6'}, {id: 5, name: 'test5'}]
                },
                {id: 2, name: 'aTest'},
                {id: 4}
            ]);
        });

        test('Should handle UPDATE_EQUIPMENT_ITEM_NAME', () => {
            const state = [{id: 0, name: 'test'}, {id: 1, name: 'test2'}];
            const newState = reducer(state, updateEquipmentItemName(1, 'test3'));
            expect(reducer(state, updateEquipmentItemName(1, 'test3'))).toEqual([
                {id: 0, name: 'test'},
                {id: 1, name: 'test3'}
            ]);
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_EQUIPMENT_ITEM_WEIGHT', () => {
            const state = [{id: 0, name: 'test'}, {id: 1, name: 'test2'}];
            const variable = Math.random;
            const newState = reducer(state, updateEquipmentItemWeight(1, variable));
            expect(reducer(state, updateEquipmentItemWeight(1, variable))).toEqual([
                {id: 0, name: 'test'},
                {id: 1, name: 'test2', weight: variable}
            ]);
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_EQUIPMENT_ITEM_CAPACITY', () => {
            const state = [{id: 0, name: 'test'}, {id: 1, name: 'test2'}];
            const variable = Math.random;
            const newState = reducer(state, updateEquipmentItemCapacity(1, variable));
            expect(reducer(state, updateEquipmentItemCapacity(1, variable))).toEqual([
                {id: 0, name: 'test'},
                {id: 1, name: 'test2', capacity: variable}
            ]);
            expect(newState).not.toBe(state);
        });
        test('Should handle UPDATE_EQUIPMENT_ITEM_VALUE', () => {
            const state = [{id: 0, name: 'test'}, {id: 1, name: 'test2'}];
            const variable = Math.random;
            const newState = reducer(state, updateEquipmentItemValue(1, variable));
            expect(reducer(state, updateEquipmentItemValue(1, variable))).toEqual([
                {id: 0, name: 'test'},
                {id: 1, name: 'test2', value: variable}
            ]);
            expect(newState).not.toBe(state);
        });
        test('Should handle MOVE_EQUIPMENT_ITEM', () => {
            const state = [
                {id: 0, name: 'test0', items: [{id: 4, name: 'test4'}]},
                {id: 1, name: 'test1'},
                {id: 3, name: 'test1', items: [{id: 2, name: 'test2'}]}
            ];
            const newState = reducer(
                state,
                moveEquipmentItem(0, undefined, true, 3, undefined)
            );
            expect(newState).not.toBe(state);
            expect(newState).toEqual([
                {id: 1, name: 'test1'},
                {id: 3, name: 'test1', items: [{id: 2, name: 'test2'}]},
                {id: 0, name: 'test0', items: [{id: 4, name: 'test4'}]}
            ]);
            expect(
                reducer(state, moveEquipmentItem(0, undefined, false, 3, undefined))
            ).toEqual([
                {id: 1, name: 'test1'},
                {id: 0, name: 'test0', items: [{id: 4, name: 'test4'}]},
                {id: 3, name: 'test1', items: [{id: 2, name: 'test2'}]}
            ]);
            expect(reducer(state, moveEquipmentItem(1, undefined, true, 4, 0))).toEqual([
                {
                    id: 0,
                    name: 'test0',
                    items: [{id: 4, name: 'test4'}, {id: 1, name: 'test1'}]
                },
                {id: 3, name: 'test1', items: [{id: 2, name: 'test2'}]}
            ]);
            expect(reducer(state, moveEquipmentItem(1, undefined, false, 4, 0))).toEqual([
                {
                    id: 0,
                    name: 'test0',
                    items: [{id: 1, name: 'test1'}, {id: 4, name: 'test4'}]
                },
                {id: 3, name: 'test1', items: [{id: 2, name: 'test2'}]}
            ]);
            expect(reducer(state, moveEquipmentItem(4, 0, true, 1, undefined))).toEqual([
                {
                    id: 0,
                    name: 'test0',
                    items: []
                },
                {id: 1, name: 'test1'},
                {id: 4, name: 'test4'},
                {id: 3, name: 'test1', items: [{id: 2, name: 'test2'}]}
            ]);
            expect(reducer(state, moveEquipmentItem(4, 0, false, 1, undefined))).toEqual([
                {
                    id: 0,
                    name: 'test0',
                    items: []
                },
                {id: 4, name: 'test4'},
                {id: 1, name: 'test1'},
                {id: 3, name: 'test1', items: [{id: 2, name: 'test2'}]}
            ]);
            expect(reducer(state, moveEquipmentItem(4, 0, true, 2, 3))).toEqual([
                {
                    id: 0,
                    name: 'test0',
                    items: []
                },
                {id: 1, name: 'test1'},
                {
                    id: 3,
                    name: 'test1',
                    items: [{id: 2, name: 'test2'}, {id: 4, name: 'test4'}]
                }
            ]);
            expect(reducer(state, moveEquipmentItem(4, 0, false, 2, 3))).toEqual([
                {
                    id: 0,
                    name: 'test0',
                    items: []
                },
                {id: 1, name: 'test1'},
                {
                    id: 3,
                    name: 'test1',
                    items: [{id: 4, name: 'test4'}, {id: 2, name: 'test2'}]
                }
            ]);
            expect(
                reducer(state, moveEquipmentItem(1, undefined, false, 'new', undefined))
            ).toEqual([
                {
                    id: 0,
                    name: 'test0',
                    items: [{id: 4, name: 'test4'}]
                },
                {
                    id: 3,
                    name: 'test1',
                    items: [{id: 2, name: 'test2'}]
                },
                {id: 1, name: 'test1'}
            ]);
            expect(
                reducer(state, moveEquipmentItem(1, undefined, true, 'new', undefined))
            ).toEqual([
                {
                    id: 0,
                    name: 'test0',
                    items: [{id: 4, name: 'test4'}]
                },
                {
                    id: 3,
                    name: 'test1',
                    items: [{id: 2, name: 'test2'}]
                },
                {id: 1, name: 'test1'}
            ]);
            expect(
                reducer(state, moveEquipmentItem(1, undefined, true, 'new', 0))
            ).toEqual([
                {
                    id: 0,
                    name: 'test0',
                    items: [{id: 4, name: 'test4'}, {id: 1, name: 'test1'}]
                },
                {
                    id: 3,
                    name: 'test1',
                    items: [{id: 2, name: 'test2'}]
                }
            ]);
            expect(
                reducer(state, moveEquipmentItem(1, undefined, false, 'new', 0))
            ).toEqual([
                {
                    id: 0,
                    name: 'test0',
                    items: [{id: 4, name: 'test4'}, {id: 1, name: 'test1'}]
                },
                {
                    id: 3,
                    name: 'test1',
                    items: [{id: 2, name: 'test2'}]
                }
            ]);
            expect(
                reducer(state, moveEquipmentItem(4, 0, true, 'new', undefined))
            ).toEqual([
                {
                    id: 0,
                    name: 'test0',
                    items: []
                },
                {id: 1, name: 'test1'},
                {
                    id: 3,
                    name: 'test1',
                    items: [{id: 2, name: 'test2'}]
                },
                {id: 4, name: 'test4'}
            ]);
            expect(
                reducer(state, moveEquipmentItem(4, 0, false, 'new', undefined))
            ).toEqual([
                {
                    id: 0,
                    name: 'test0',
                    items: []
                },
                {id: 1, name: 'test1'},
                {
                    id: 3,
                    name: 'test1',
                    items: [{id: 2, name: 'test2'}]
                },
                {id: 4, name: 'test4'}
            ]);
            expect(reducer(state, moveEquipmentItem(4, 0, true, 'new', 3))).toEqual([
                {
                    id: 0,
                    name: 'test0',
                    items: []
                },
                {id: 1, name: 'test1'},
                {
                    id: 3,
                    name: 'test1',
                    items: [{id: 2, name: 'test2'}, {id: 4, name: 'test4'}]
                }
            ]);
            expect(reducer(state, moveEquipmentItem(4, 0, false, 'new', 3))).toEqual([
                {
                    id: 0,
                    name: 'test0',
                    items: []
                },
                {id: 1, name: 'test1'},
                {
                    id: 3,
                    name: 'test1',
                    items: [{id: 2, name: 'test2'}, {id: 4, name: 'test4'}]
                }
            ]);
        });
    });
});
