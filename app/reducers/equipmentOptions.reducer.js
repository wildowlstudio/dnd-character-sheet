import {actionTypes} from '../actions/equipment.actions';
import {actionTypes as globalActionTypes} from '../actions/global.actions';

export default function reducer(
    state = {
        weight: true,
        capacity: true,
        value: true,
        sum: true
    },
    action
) {
    switch (action.type) {
        case globalActionTypes.LOAD_SHEET:
            return Object.assign(
                {},
                state,
                (action.sheet &&
                    action.sheet.options &&
                    action.sheet.options.equipment) || {
                    weight: true,
                    capacity: true,
                    value: true,
                    sum: true
                }
            );
        case actionTypes.UPDATE_EQUIPMENT_OPTION_WEIGHT:
            return Object.assign({}, state, {weight: !!action.value});
        case actionTypes.UPDATE_EQUIPMENT_OPTION_CAPACITY:
            return Object.assign({}, state, {capacity: !!action.value});
        case actionTypes.UPDATE_EQUIPMENT_OPTION_VALUE:
            return Object.assign({}, state, {value: !!action.value});
        case actionTypes.UPDATE_EQUIPMENT_OPTION_SUM:
            return Object.assign({}, state, {sum: !!action.value});

        default:
            return state;
    }
}
