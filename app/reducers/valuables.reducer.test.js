import {loadSheet, resetSheet} from '../actions/global.actions';
import {
    resetValuables,
    updateValuablesBronzeOption,
    updateValuablesCopperOption,
    updateValuablesGoldOption,
    updateValuablesIronOption,
    updateValuablesMoney,
    updateValuablesPlatinumOption,
    updateValuablesSilverOption
} from '../actions/valuables.actions';

import reducer from './valuables.reducer';

describe('Reducer tests', () => {
    describe('Basic reducer tests', () => {
        test('Should return the initial state', () => {
            expect(reducer(undefined, {})).toEqual({
                money: 0,
                copper: true,
                silver: true,
                gold: true
            });
        });
        test('Should handle RESET_SHEET', () => {
            const state = {test: 'Test'};
            const newState = reducer(state, resetSheet());
            expect(newState).toEqual({
                money: 0,
                copper: true,
                silver: true,
                gold: true
            });
            expect(newState).not.toBe(state);
        });
        test('Should handle RESET_VALUABLES', () => {
            const state = {test: 'Test'};
            const newState = reducer(state, resetValuables());
            expect(newState).toEqual({
                money: 0,
                copper: true,
                silver: true,
                gold: true
            });
            expect(newState).not.toBe(state);
        });
        test('Should handle LOAD_SHEET', () => {
            const state = {};
            const sheetToLoad = {
                valuables: {money: 102, test: 'test'}
            };
            const newState = reducer(state, loadSheet(sheetToLoad));
            expect(newState).toEqual(sheetToLoad.valuables);
            expect(newState).not.toBe(state);
            expect(newState).not.toBe(sheetToLoad.valuables);
        });
        test('Should handle UPDATE_VALUABLES_MONEY', () => {
            const state = {money: 0};
            const newState = reducer(state, updateValuablesMoney(102));
            expect(newState).toEqual({money: 102});
            expect(
                reducer({test: 'Test', money: 3}, updateValuablesMoney(102))
            ).toEqual({
                test: 'Test',
                money: 105
            });
            expect(newState).not.toBe(state);
        });

        test('Should handle UPDATE_VALUABLES_BRONZE_OPTION', () => {
            const state = {};
            const newState = reducer(state, updateValuablesBronzeOption(true));
            expect(newState).toEqual({bronze: true});
            expect(reducer({test: 'Test'}, updateValuablesBronzeOption(true))).toEqual({
                test: 'Test',
                bronze: true
            });
            expect(newState).not.toBe(state);
        });

        test('Should handle UPDATE_VALUABLES_IRON_OPTION', () => {
            const state = {};
            const newState = reducer(state, updateValuablesIronOption(true));
            expect(newState).toEqual({iron: true});
            expect(reducer({test: 'Test'}, updateValuablesIronOption(true))).toEqual({
                test: 'Test',
                iron: true
            });
            expect(newState).not.toBe(state);
        });

        test('Should handle UPDATE_VALUABLES_COPPER_OPTION', () => {
            const state = {};
            const newState = reducer(state, updateValuablesCopperOption(true));
            expect(newState).toEqual({copper: true});
            expect(reducer({test: 'Test'}, updateValuablesCopperOption(true))).toEqual({
                test: 'Test',
                copper: true
            });
            expect(newState).not.toBe(state);
        });

        test('Should handle UPDATE_VALUABLES_SILVER_OPTION', () => {
            const state = {};
            const newState = reducer(state, updateValuablesSilverOption(true));
            expect(newState).toEqual({silver: true});
            expect(reducer({test: 'Test'}, updateValuablesSilverOption(true))).toEqual({
                test: 'Test',
                silver: true
            });
            expect(newState).not.toBe(state);
        });

        test('Should handle UPDATE_VALUABLES_GOLD_OPTION', () => {
            const state = {};
            const newState = reducer(state, updateValuablesGoldOption(true));
            expect(newState).toEqual({gold: true});
            expect(reducer({test: 'Test'}, updateValuablesGoldOption(true))).toEqual({
                test: 'Test',
                gold: true
            });
            expect(newState).not.toBe(state);
        });

        test('Should handle UPDATE_VALUABLES_PLATINUM_OPTION', () => {
            const state = {};
            const newState = reducer(state, updateValuablesPlatinumOption(true));
            expect(newState).toEqual({platinum: true});
            expect(
                reducer({test: 'Test'}, updateValuablesPlatinumOption(true))
            ).toEqual({
                test: 'Test',
                platinum: true
            });
            expect(newState).not.toBe(state);
        });
    });
});
