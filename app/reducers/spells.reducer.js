import {actionTypes} from '../actions/spell.actions';
import {actionTypes as globalActionTypes} from '../actions/global.actions';

export default function reducer(state = [...Array(10)].map(() => ({})), action) {
    switch (action.type) {
        case globalActionTypes.RESET_SHEET:
        case actionTypes.RESET_SPELLS:
            return [...Array(10)].map(() => ({}));
        case globalActionTypes.LOAD_SHEET:
            return (
                (action.sheet &&
                    action.sheet.spells &&
                    Object.assign([], action.sheet.spells)) ||
                [...Array(10)].map(() => ({}))
            );
        case actionTypes.UPDATE_SPELL_KNOWN:
            return state.map((spell, i) => {
                if (i === action.id) {
                    return Object.assign({}, spell, {known: action.value});
                }
                return spell;
            });
        case actionTypes.UPDATE_SPELL_PER_DAY:
            return state.map((spell, i) => {
                if (i === action.id) {
                    return Object.assign({}, spell, {perDay: action.value});
                }
                return spell;
            });
        case actionTypes.UPDATE_SPELL_BONUS:
            return state.map((spell, i) => {
                if (i === action.id) {
                    return Object.assign({}, spell, {bonus: action.value});
                }
                return spell;
            });
        case actionTypes.UPDATE_SPELL_SAVE_DC:
            return state.map((spell, i) => {
                if (i === action.id) {
                    return Object.assign({}, spell, {saveDc: action.value});
                }
                return spell;
            });
        default:
            return state;
    }
}
