import {actionTypes} from '../actions/skill.actions';
import {actionTypes as globalActionTypes} from '../actions/global.actions';

export default function reducer(state = {}, action) {
    switch (action.type) {
        case globalActionTypes.RESET_SHEET:
        case actionTypes.RESET_SKILLS:
            return {};
        case globalActionTypes.LOAD_SHEET:
            return Object.assign({}, (action.sheet || {skill: {}}).skill);
        case actionTypes.UPDATE_SKILL_MAX_CLASS_RANK:
            return Object.assign({}, state, {maxClassRank: action.value});
        case actionTypes.UPDATE_SKILL_MAX_CROSS_CLASS_RANK:
            return Object.assign({}, state, {maxCrossClassRank: action.value});
        default:
            return state;
    }
}
