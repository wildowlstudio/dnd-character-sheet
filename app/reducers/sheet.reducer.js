import acReducer from './ac.reducer';
import armorsReducer from './armors.reducer';
import avatarReducer from './avatar.reducer';
import baseAttackReducer from './baseAttack.reducer';
import basicReducer from './basic.reducer';
import equipmentReducer from './equipment.reducer';
import featsReducer from './feats.reducer';
import fortitudeReducer from './fortitude.reducer';
import hpReducer from './hp.reducer';
import initiativeReducer from './initiative.reducer';
import knownSpellsReducer from './knownSpells.reducer';
import languagesReducer from './languages.reducer';
import notesReducer from './notes.reducer';
import optionsReducer from './options.reducer';
import reflexReducer from './reflex.reducer';
import skillReducer from './skill.reducer';
import skillsReducer from './skills.reducer';
import speedReducer from './speed.reducer';
import spellsReducer from './spells.reducer';
import statsReducer from './stats.reducer';
import valuablesReducer from './valuables.reducer';
import weaponsReducer from './weapons.reducer';
import willpowerReducer from './willpower.reducer';
import xpReducer from './xp.reducer';

export default function reducer(state = {}, action) {
    return {
        ac: acReducer(state.ac, action),
        avatar: avatarReducer(state.avatar, action),
        armors: armorsReducer(state.armors, action),
        baseAttack: baseAttackReducer(state.baseAttack, action),
        basic: basicReducer(state.basic, action),
        equipment: equipmentReducer(state.equipment, action),
        feats: featsReducer(state.feats, action),
        fortitude: fortitudeReducer(state.fortitude, action),
        hp: hpReducer(state.hp, action),
        initiative: initiativeReducer(state.initiative, action),
        knownSpells: knownSpellsReducer(state.knownSpells, action),
        languages: languagesReducer(state.languages, action),
        notes: notesReducer(state.notes, action),
        options: optionsReducer(state.options, action),
        reflex: reflexReducer(state.reflex, action),
        skill: skillReducer(state.skill, action),
        skills: skillsReducer(state.skills, action),
        speed: speedReducer(state.speed, action),
        spells: spellsReducer(state.spells, action),
        stats: statsReducer(state.stats, action),
        valuables: valuablesReducer(state.valuables, action),
        weapons: weaponsReducer(state.weapons, action),
        willpower: willpowerReducer(state.willpower, action),
        xp: xpReducer(state.xp, action)
    };
}
