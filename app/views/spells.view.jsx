import PropTypes from 'prop-types';
import React from 'react';
import Spells from '../components/sheet/spells';

const SpellsView = ({
    resetSpells,
    spells,
    updateSpellBonus,
    updateSpellKnown,
    updateSpellPerDay,
    updateSpellSaveDc
}) => (
    <Spells
        onBonusChange={updateSpellBonus}
        onKnownChange={updateSpellKnown}
        onPerDayChange={updateSpellPerDay}
        onSaveDcChange={updateSpellSaveDc}
        onSpellsReset={resetSpells}
        spells={spells}
    />
);

SpellsView.propTypes = {
    resetSpells: PropTypes.func.isRequired,
    spells: PropTypes.arrayOf(
        PropTypes.shape({
            known: PropTypes.string,
            perDay: PropTypes.string,
            bonus: PropTypes.string,
            saveDc: PropTypes.string
        })
    ).isRequired,
    updateSpellBonus: PropTypes.func.isRequired,
    updateSpellKnown: PropTypes.func.isRequired,
    updateSpellPerDay: PropTypes.func.isRequired,
    updateSpellSaveDc: PropTypes.func.isRequired
};

export default SpellsView;
