import Feats from '../components/sheet/feats';
import PropTypes from 'prop-types';
import React from 'react';

const FeatsView = ({
    addFeat,
    className,
    cleanFeats,
    deleteFeat,
    feats,
    moveFeat,
    resetFeats,
    updateFeat
}) => (
    <Feats
        className={className}
        feats={feats}
        onFeatAdd={addFeat}
        onFeatChange={updateFeat}
        onFeatClean={cleanFeats}
        onFeatDelete={deleteFeat}
        onFeatMove={moveFeat}
        onFeatReset={resetFeats}
    />
);

FeatsView.propTypes = {
    addFeat: PropTypes.func.isRequired,
    className: PropTypes.string,
    cleanFeats: PropTypes.func.isRequired,
    deleteFeat: PropTypes.func.isRequired,
    feats: PropTypes.shape({
        id: PropTypes.number.isRequired,
        value: PropTypes.string
    }).isRequired,
    moveFeat: PropTypes.func.isRequired,
    resetFeats: PropTypes.func.isRequired,
    updateFeat: PropTypes.func.isRequired
};

export default FeatsView;
