import Equipment from '../components/sheet/equipment';
import PropTypes from 'prop-types';
import React from 'react';

const EquipmentView = ({
    addEquipmentItem,
    className,
    deleteEquipmentItem,
    deleteEquipmentItems,
    equipment,
    moveEquipmentItem,
    options,
    resetEquipment,
    sortEquipment,
    updateEquipmentItemCapacity,
    updateEquipmentItemName,
    updateEquipmentItemValue,
    updateEquipmentItemWeight,
    updateEquipmentOptionCapacity,
    updateEquipmentOptionSum,
    updateEquipmentOptionValue,
    updateEquipmentOptionWeight
}) => (
    <Equipment
        className={className}
        items={equipment}
        onItemAdd={addEquipmentItem}
        onItemCapacityChange={updateEquipmentItemCapacity}
        onItemDelete={deleteEquipmentItem}
        onItemMove={moveEquipmentItem}
        onItemNameChange={updateEquipmentItemName}
        onItemsDelete={deleteEquipmentItems}
        onItemValueChange={updateEquipmentItemValue}
        onItemWeightChange={updateEquipmentItemWeight}
        onOptionCapacityChange={updateEquipmentOptionCapacity}
        onOptionSumChange={updateEquipmentOptionSum}
        onOptionValueChange={updateEquipmentOptionValue}
        onOptionWeightChange={updateEquipmentOptionWeight}
        onReset={resetEquipment}
        onSort={sortEquipment}
        options={options}
    />
);

EquipmentView.propTypes = {
    addEquipmentItem: PropTypes.func.isRequired,
    className: PropTypes.string,
    deleteEquipmentItem: PropTypes.func.isRequired,
    deleteEquipmentItems: PropTypes.func.isRequired,
    equipment: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            name: PropTypes.string,
            weight: PropTypes.number,
            capacity: PropTypes.number,
            value: PropTypes.number
        })
    ).isRequired,
    moveEquipmentItem: PropTypes.func.isRequired,
    options: PropTypes.shape({
        sum: PropTypes.bool.isRequired,
        weight: PropTypes.bool.isRequired,
        capacity: PropTypes.bool.isRequired,
        value: PropTypes.bool.isRequired
    }).isRequired,
    resetEquipment: PropTypes.func.isRequired,
    sortEquipment: PropTypes.func.isRequired,
    updateEquipmentItemCapacity: PropTypes.func.isRequired,
    updateEquipmentItemName: PropTypes.func.isRequired,
    updateEquipmentItemValue: PropTypes.func.isRequired,
    updateEquipmentItemWeight: PropTypes.func.isRequired,
    updateEquipmentOptionCapacity: PropTypes.func.isRequired,
    updateEquipmentOptionSum: PropTypes.func.isRequired,
    updateEquipmentOptionValue: PropTypes.func.isRequired,
    updateEquipmentOptionWeight: PropTypes.func.isRequired
};

export default EquipmentView;
