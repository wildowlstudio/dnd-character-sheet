import PropTypes from 'prop-types';
import React from 'react';
import Skills from '../components/sheet/skills';

const SkillsView = ({
    addSkill,
    deleteSkill,
    deleteSkills,
    resetSkills,
    skill,
    skills,
    sortSkills,
    updateSkillAbilityMod,
    updateSkillAttribute,
    updateSkillClass,
    updateSkillMaxClassRank,
    updateSkillMaxCrossClassRank,
    updateSkillMiscMod,
    updateSkillName,
    updateSkillSkillRank
}) => (
    <Skills
        onSkillAbilityModChange={updateSkillAbilityMod}
        onSkillAdd={addSkill}
        onSkillAttributeChange={updateSkillAttribute}
        onSkillClassChange={updateSkillClass}
        onSkillDelete={deleteSkill}
        onSkillMaxClassRankChange={updateSkillMaxClassRank}
        onSkillMaxCrossClassRankChange={updateSkillMaxCrossClassRank}
        onSkillMiscModChange={updateSkillMiscMod}
        onSkillNameChange={updateSkillName}
        onSkillReset={resetSkills}
        onSkillsDelete={deleteSkills}
        onSkillSkillRankChange={updateSkillSkillRank}
        onSkillSort={sortSkills}
        skill={skill}
        skills={skills}
    />
);

SkillsView.propTypes = {
    addSkill: PropTypes.func.isRequired,
    deleteSkill: PropTypes.func.isRequired,
    deleteSkills: PropTypes.func.isRequired,
    resetSkills: PropTypes.func.isRequired,
    skill: PropTypes.shape({
        maxClassRank: PropTypes.number,
        maxCrossClassRank: PropTypes.number
    }).isRequired,
    skills: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string,
            attribute: PropTypes.string,
            class: PropTypes.bool,
            abilityMod: PropTypes.number,
            skillRank: PropTypes.number,
            miscMod: PropTypes.number
        })
    ).isRequired,
    sortSkills: PropTypes.func.isRequired,
    updateSkillAbilityMod: PropTypes.func.isRequired,
    updateSkillAttribute: PropTypes.func.isRequired,
    updateSkillClass: PropTypes.func.isRequired,
    updateSkillMaxClassRank: PropTypes.func.isRequired,
    updateSkillMaxCrossClassRank: PropTypes.func.isRequired,
    updateSkillMiscMod: PropTypes.func.isRequired,
    updateSkillName: PropTypes.func.isRequired,
    updateSkillSkillRank: PropTypes.func.isRequired
};

export default SkillsView;
