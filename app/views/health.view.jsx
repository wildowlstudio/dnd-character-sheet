import Health from '../components/sheet/health';
import PropTypes from 'prop-types';
import React from 'react';

const HealthView = ({
    ac,
    hp,
    resetHealth,
    updateAcArmor,
    updateAcDex,
    updateAcMisc,
    updateAcShield,
    updateAcSize,
    updateCurrentHp,
    updateHp,
    updateSubdualDamage
}) => (
    <Health
        ac={ac}
        hp={hp}
        onAcArmorChange={updateAcArmor}
        onAcDexChange={updateAcDex}
        onAcMiscChange={updateAcMisc}
        onAcShieldChange={updateAcShield}
        onAcSizeChange={updateAcSize}
        onCurrentHpChange={updateCurrentHp}
        onHealthReset={resetHealth}
        onHpChange={updateHp}
        onSubdualDamageChange={updateSubdualDamage}
    />
);

HealthView.propTypes = {
    ac: PropTypes.shape({
        armor: PropTypes.number,
        shield: PropTypes.number,
        dex: PropTypes.number,
        size: PropTypes.number,
        misc: PropTypes.number
    }).isRequired,
    hp: PropTypes.shape({
        hp: PropTypes.number,
        current: PropTypes.number,
        subdualDamage: PropTypes.string
    }).isRequired,
    resetHealth: PropTypes.func.isRequired,
    updateAcArmor: PropTypes.func.isRequired,
    updateAcDex: PropTypes.func.isRequired,
    updateAcMisc: PropTypes.func.isRequired,
    updateAcShield: PropTypes.func.isRequired,
    updateAcSize: PropTypes.func.isRequired,
    updateCurrentHp: PropTypes.func.isRequired,
    updateHp: PropTypes.func.isRequired,
    updateSubdualDamage: PropTypes.func.isRequired
};

export default HealthView;
