import PropTypes from 'prop-types';
import React from 'react';
import Xp from '../components/sheet/xp';

const XpView = ({resetXp, xp, updateXp}) => (
    <Xp
        onXpChange={updateXp}
        onXpReset={resetXp}
        xp={xp}
    />
);

XpView.propTypes = {
    resetXp: PropTypes.func.isRequired,
    updateXp: PropTypes.func.isRequired,
    xp: PropTypes.string
};

export default XpView;
