import PropTypes from 'prop-types';
import React from 'react';
import Stats from '../components/sheet/stats';

const StatsView = ({
    resetStats,
    stats,
    updateStatMod,
    updateStatTempMod,
    updateStatTempScore
}) => (
    <Stats
        onStatModChange={updateStatMod}
        onStatReset={resetStats}
        onStatTempModChange={updateStatTempMod}
        onStatTempScoreChange={updateStatTempScore}
        stats={stats}
    />
);

StatsView.propTypes = {
    resetStats: PropTypes.func.isRequired,
    stats: PropTypes.object.isRequired,
    updateStatMod: PropTypes.func.isRequired,
    updateStatTempMod: PropTypes.func.isRequired,
    updateStatTempScore: PropTypes.func.isRequired
};

export default StatsView;
