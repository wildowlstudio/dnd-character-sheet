import Notes from '../components/sheet/notes';
import PropTypes from 'prop-types';
import React from 'react';

const NotesView = ({className, resetNotes, notes, updateNotes}) => (
    <Notes
        className={className}
        notes={notes}
        onNotesChange={updateNotes}
        onNotesReset={resetNotes}
    />
);

NotesView.propTypes = {
    className: PropTypes.string,
    notes: PropTypes.string,
    resetNotes: PropTypes.func.isRequired,
    updateNotes: PropTypes.func.isRequired
};

export default NotesView;
