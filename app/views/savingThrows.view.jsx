import PropTypes from 'prop-types';
import React from 'react';
import SavingThrows from '../components/sheet/savingThrows';

const SavingThrowsView = ({
    fortitude,
    reflex,
    resetSavingThrows,
    updateFortitudeAbilityMod,
    updateFortitudeBase,
    updateFortitudeMagicMod,
    updateFortitudeMiscMod,
    updateFortitudeOtherMod,
    updateFortitudeTempMod,
    updateReflexAbilityMod,
    updateReflexBase,
    updateReflexMagicMod,
    updateReflexMiscMod,
    updateReflexOtherMod,
    updateReflexTempMod,
    updateWillpowerAbilityMod,
    updateWillpowerBase,
    updateWillpowerMagicMod,
    updateWillpowerMiscMod,
    updateWillpowerOtherMod,
    updateWillpowerTempMod,
    willpower
}) => (
    <SavingThrows
        fortitude={fortitude}
        onFortitudeAbilityModChange={updateFortitudeAbilityMod}
        onFortitudeBaseChange={updateFortitudeBase}
        onFortitudeMagicModChange={updateFortitudeMagicMod}
        onFortitudeMiscModChange={updateFortitudeMiscMod}
        onFortitudeOtherModChange={updateFortitudeOtherMod}
        onFortitudeTempModChange={updateFortitudeTempMod}
        onReflexAbilityModChange={updateReflexAbilityMod}
        onReflexBaseChange={updateReflexBase}
        onReflexMagicModChange={updateReflexMagicMod}
        onReflexMiscModChange={updateReflexMiscMod}
        onReflexOtherModChange={updateReflexOtherMod}
        onReflexTempModChange={updateReflexTempMod}
        onSavingThrowsReset={resetSavingThrows}
        onWillpowerAbilityModChange={updateWillpowerAbilityMod}
        onWillpowerBaseChange={updateWillpowerBase}
        onWillpowerMagicModChange={updateWillpowerMagicMod}
        onWillpowerMiscModChange={updateWillpowerMiscMod}
        onWillpowerOtherModChange={updateWillpowerOtherMod}
        onWillpowerTempModChange={updateWillpowerTempMod}
        reflex={reflex}
        willpower={willpower}
    />
);

SavingThrowsView.propTypes = {
    fortitude: PropTypes.shape({
        base: PropTypes.number,
        abilityMod: PropTypes.number,
        magicMod: PropTypes.number,
        miscMod: PropTypes.number,
        tempMod: PropTypes.number,
        otherMod: PropTypes.number
    }).isRequired,
    reflex: PropTypes.shape({
        base: PropTypes.number,
        abilityMod: PropTypes.number,
        magicMod: PropTypes.number,
        miscMod: PropTypes.number,
        tempMod: PropTypes.number,
        otherMod: PropTypes.number
    }).isRequired,
    resetSavingThrows: PropTypes.func.isRequired,
    updateFortitudeAbilityMod: PropTypes.func.isRequired,
    updateFortitudeBase: PropTypes.func.isRequired,
    updateFortitudeMagicMod: PropTypes.func.isRequired,
    updateFortitudeMiscMod: PropTypes.func.isRequired,
    updateFortitudeOtherMod: PropTypes.func.isRequired,
    updateFortitudeTempMod: PropTypes.func.isRequired,
    updateReflexAbilityMod: PropTypes.func.isRequired,
    updateReflexBase: PropTypes.func.isRequired,
    updateReflexMagicMod: PropTypes.func.isRequired,
    updateReflexMiscMod: PropTypes.func.isRequired,
    updateReflexOtherMod: PropTypes.func.isRequired,
    updateReflexTempMod: PropTypes.func.isRequired,
    updateWillpowerAbilityMod: PropTypes.func.isRequired,
    updateWillpowerBase: PropTypes.func.isRequired,
    updateWillpowerMagicMod: PropTypes.func.isRequired,
    updateWillpowerMiscMod: PropTypes.func.isRequired,
    updateWillpowerOtherMod: PropTypes.func.isRequired,
    updateWillpowerTempMod: PropTypes.func.isRequired,
    willpower: PropTypes.shape({
        base: PropTypes.number,
        abilityMod: PropTypes.number,
        magicMod: PropTypes.number,
        miscMod: PropTypes.number,
        tempMod: PropTypes.number,
        otherMod: PropTypes.number
    }).isRequired
};

export default SavingThrowsView;
