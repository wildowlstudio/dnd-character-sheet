import Languages from '../components/sheet/languages';
import PropTypes from 'prop-types';
import React from 'react';

const LanguagesView = ({
    addLanguage,
    cleanLanguages,
    deleteLanguage,
    languages,
    moveLanguage,
    resetLanguages,
    updateLanguage
}) => (
    <Languages
        languages={languages}
        onLanguageAdd={addLanguage}
        onLanguageChange={updateLanguage}
        onLanguageClean={cleanLanguages}
        onLanguageDelete={deleteLanguage}
        onLanguageMove={moveLanguage}
        onLanguageReset={resetLanguages}
    />
);

LanguagesView.propTypes = {
    addLanguage: PropTypes.func.isRequired,
    cleanLanguages: PropTypes.func.isRequired,
    deleteLanguage: PropTypes.func.isRequired,
    languages: PropTypes.shape({
        id: PropTypes.number.isRequired,
        value: PropTypes.string
    }).isRequired,
    moveLanguage: PropTypes.func.isRequired,
    resetLanguages: PropTypes.func.isRequired,
    updateLanguage: PropTypes.func.isRequired
};

export default LanguagesView;
