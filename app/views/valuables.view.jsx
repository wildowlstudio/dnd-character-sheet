import PropTypes from 'prop-types';
import React from 'react';
import Valuables from '../components/sheet/valuables';

const ValuablesView = ({
    resetValuables,
    updateValuablesBronzeOption,
    updateValuablesCopperOption,
    updateValuablesGoldOption,
    updateValuablesIronOption,
    updateValuablesMoney,
    updateValuablesPlatinumOption,
    updateValuablesSilverOption,
    valuables
}) => (
    <Valuables
        onBronzeChange={updateValuablesBronzeOption}
        onCopperChange={updateValuablesCopperOption}
        onGoldChange={updateValuablesGoldOption}
        onIronChange={updateValuablesIronOption}
        onMoneyChange={updateValuablesMoney}
        onPlatinumChange={updateValuablesPlatinumOption}
        onReset={resetValuables}
        onSilverChange={updateValuablesSilverOption}
        valuables={valuables}
    />
);

ValuablesView.propTypes = {
    resetValuables: PropTypes.func.isRequired,
    updateValuablesBronzeOption: PropTypes.func.isRequired,
    updateValuablesCopperOption: PropTypes.func.isRequired,
    updateValuablesGoldOption: PropTypes.func.isRequired,
    updateValuablesIronOption: PropTypes.func.isRequired,
    updateValuablesMoney: PropTypes.func.isRequired,
    updateValuablesPlatinumOption: PropTypes.func.isRequired,
    updateValuablesSilverOption: PropTypes.func.isRequired,
    valuables: PropTypes.shape({
        bronze: PropTypes.bool,
        iron: PropTypes.bool,
        copper: PropTypes.bool,
        silver: PropTypes.bool,
        gold: PropTypes.bool,
        platinum: PropTypes.bool,
        money: PropTypes.number
    }).isRequired
};

export default ValuablesView;
