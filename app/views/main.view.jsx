import Header from '../components/header';
import PropTypes from 'prop-types';
import React from 'react';
import Sheet from '../containers/sheet.container';

const Main = ({resetSheet}) => (
    <div>
        <Header onResetSheet={resetSheet} />
        <Sheet />
    </div>
);

Main.propTypes = {
    resetSheet: PropTypes.func.isRequired
};

export default Main;
