import KnownSpells from '../components/sheet/knownSpells';
import PropTypes from 'prop-types';
import React from 'react';

const KnownSpellsView = ({
    addKnownSpell,
    className,
    cleanKnownSpells,
    deleteKnownSpell,
    moveKnownSpell,
    resetKnownSpells,
    spells,
    updateKnownSpell
}) => (
    <KnownSpells
        className={className}
        onSpellAdd={addKnownSpell}
        onSpellChange={updateKnownSpell}
        onSpellClean={cleanKnownSpells}
        onSpellDelete={deleteKnownSpell}
        onSpellMove={moveKnownSpell}
        onSpellReset={resetKnownSpells}
        spells={spells}
    />
);

KnownSpellsView.propTypes = {
    addKnownSpell: PropTypes.func.isRequired,
    className: PropTypes.string,
    cleanKnownSpells: PropTypes.func.isRequired,
    deleteKnownSpell: PropTypes.func.isRequired,
    moveKnownSpell: PropTypes.func.isRequired,
    resetKnownSpells: PropTypes.func.isRequired,
    spells: PropTypes.shape({
        id: PropTypes.number.isRequired,
        value: PropTypes.string
    }).isRequired,
    updateKnownSpell: PropTypes.func.isRequired
};

export default KnownSpellsView;
