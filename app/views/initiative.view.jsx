import Initiative from '../components/sheet/initiative';
import PropTypes from 'prop-types';
import React from 'react';

const InitiativeView = ({
    initiative,
    resetInitiative,
    updateInitiativeDexMod,
    updateInitiativeMiscMod
}) => (
    <Initiative
        initiative={initiative}
        onDexModChange={updateInitiativeDexMod}
        onInitiativeReset={resetInitiative}
        onMiscModChange={updateInitiativeMiscMod}
    />
);

InitiativeView.propTypes = {
    initiative: PropTypes.shape({
        dexMod: PropTypes.number,
        miscMod: PropTypes.number
    }).isRequired,
    resetInitiative: PropTypes.func.isRequired,
    updateInitiativeDexMod: PropTypes.func.isRequired,
    updateInitiativeMiscMod: PropTypes.func.isRequired
};

export default InitiativeView;
