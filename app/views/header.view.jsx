import Header from '../components/sheet/header';
import PropTypes from 'prop-types';
import React from 'react';

const HeaderView = ({
    basic,
    updateAge,
    updateAlignment,
    updateClass,
    updateDeity,
    updateEyes,
    updateGender,
    updateHair,
    updateHeight,
    updateLevel,
    updateName,
    updatePlayer,
    updateRace,
    updateSize,
    updateWeight
}) => (
    <Header
        basic={basic}
        onUpdateAge={updateAge}
        onUpdateAlignment={updateAlignment}
        onUpdateClass={updateClass}
        onUpdateDeity={updateDeity}
        onUpdateEyes={updateEyes}
        onUpdateGender={updateGender}
        onUpdateHair={updateHair}
        onUpdateHeight={updateHeight}
        onUpdateLevel={updateLevel}
        onUpdateName={updateName}
        onUpdatePlayer={updatePlayer}
        onUpdateRace={updateRace}
        onUpdateSize={updateSize}
        onUpdateWeight={updateWeight}
    />
);

HeaderView.propTypes = {
    basic: PropTypes.shape({
        name: PropTypes.string,
        player: PropTypes.string,
        class: PropTypes.string,
        race: PropTypes.string,
        alignment: PropTypes.string,
        deity: PropTypes.string,
        level: PropTypes.number,
        size: PropTypes.string,
        age: PropTypes.string,
        gender: PropTypes.string,
        height: PropTypes.string,
        weight: PropTypes.string,
        eyes: PropTypes.string,
        hair: PropTypes.string
    }).isRequired,
    updateAge: PropTypes.func.isRequired,
    updateAlignment: PropTypes.func.isRequired,
    updateClass: PropTypes.func.isRequired,
    updateDeity: PropTypes.func.isRequired,
    updateEyes: PropTypes.func.isRequired,
    updateGender: PropTypes.func.isRequired,
    updateHair: PropTypes.func.isRequired,
    updateHeight: PropTypes.func.isRequired,
    updateLevel: PropTypes.func.isRequired,
    updateName: PropTypes.func.isRequired,
    updatePlayer: PropTypes.func.isRequired,
    updateRace: PropTypes.func.isRequired,
    updateSize: PropTypes.func.isRequired,
    updateWeight: PropTypes.func.isRequired
};

export default HeaderView;
