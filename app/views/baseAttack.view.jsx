import BaseAttack from '../components/sheet/baseAttack';
import PropTypes from 'prop-types';
import React from 'react';

const BaseAttackView = ({resetBaseAttack, baseAttack, updateBaseAttack}) => (
    <BaseAttack
        baseAttack={baseAttack}
        onBaseAttackChange={updateBaseAttack}
        onBaseAttackReset={resetBaseAttack}
    />
);

BaseAttackView.propTypes = {
    baseAttack: PropTypes.string,
    resetBaseAttack: PropTypes.func.isRequired,
    updateBaseAttack: PropTypes.func.isRequired
};

export default BaseAttackView;
