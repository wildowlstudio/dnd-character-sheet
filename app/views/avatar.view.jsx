import Avatar from '../components/sheet/avatar';
import PropTypes from 'prop-types';
import React from 'react';

const AvatarView = ({resetAvatar, avatar, updateAvatar}) => (
    <Avatar
        avatar={avatar}
        onChange={updateAvatar}
        onReset={resetAvatar}
    />
);

AvatarView.propTypes = {
    avatar: PropTypes.string,
    resetAvatar: PropTypes.func.isRequired,
    updateAvatar: PropTypes.func.isRequired
};

export default AvatarView;
