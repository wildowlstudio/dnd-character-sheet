import Armor, {ArmorList} from '../components/sheet/armor';
import Weapon, {WeaponList, WeaponTypes} from '../components/sheet/weapon';

import {AttributeList} from '../components/sheet/stats';
import Avatar from '../containers/avatar.container';
import BaseAttack from '../containers/baseAttack.container';
import Equipment from '../containers/equipment.container';
import Feats from '../containers/feat.container';
import {Flex} from '../components/sheet/basic';
import Header from '../containers/header.container';
import Health from '../containers/health.container';
import Initiative from '../containers/initiative.container';
import KnownSpells from '../containers/knownSpells.container';
import {LanguageList} from '../components/sheet/languages';
import Languages from '../containers/language.container';
import Notes from '../containers/notes.container';
import PropTypes from 'prop-types';
import React from 'react';
import SavingThrowsContainer from '../containers/savingThrows.container';
import Skills from '../containers/skill.container';
import Speed from '../containers/speed.container';
import Spells from '../containers/spells.container';
import Stats from '../containers/stats.container';
import Valuables from '../containers/valuables.container';
import Xp from '../containers/xp.container';
import media from '../utils/style.utils';
import styled from 'styled-components';

const LeftVerticalFlex = styled.div`
    display: flex;
    flex-direction: column;
    width: 608px;
`;

const RightVerticalFlex = styled.div`
    display: flex;
    flex-direction: column;
    margin-left: 5px;

    ${media.oneRow`
        margin-left: 0
    `};
`;

const Sheet = ({
    addArmor,
    addWeapon,
    armors,
    deleteArmor,
    deleteWeapon,
    resetArmor,
    resetWeapon,
    updateArmorBonus,
    updateArmorMaxDex,
    updateArmorMaxSpeed,
    updateArmorName,
    updateArmorNotes,
    updateArmorPenalty,
    updateArmorSpellFailure,
    updateArmorType,
    updateArmorWeight,
    updateWeaponAttackBonus,
    updateWeaponCritical,
    updateWeaponDamage,
    updateWeaponKind,
    updateWeaponName,
    updateWeaponNotes,
    updateWeaponRange,
    updateWeaponSize,
    updateWeaponWeight,
    weapons
}) => (
    <div>
        <Header />
        <Flex
            unwrapOnPrint
            wrap="true"
        >
            <Stats />
            <Flex
                direction="column"
                order="3"
            >
                <Health />
                <Flex>
                    <div>
                        <Initiative />
                        <BaseAttack />
                    </div>
                    <div>
                        <Xp />
                        <Speed />
                    </div>
                </Flex>
            </Flex>
            <Avatar />
        </Flex>
        <SavingThrowsContainer />
        <Flex
            justify="space-between"
            unwrapOnPrint
            wrap="true"
        >
            <LeftVerticalFlex>
                {weapons.map((weapon) => (
                    <Weapon
                        key={weapon.id}
                        onWeaponAdd={addWeapon}
                        onWeaponAttackBonusChange={updateWeaponAttackBonus}
                        onWeaponCriticalChange={updateWeaponCritical}
                        onWeaponDamageChange={updateWeaponDamage}
                        onWeaponDelete={deleteWeapon}
                        onWeaponKindChange={updateWeaponKind}
                        onWeaponNameChange={updateWeaponName}
                        onWeaponNotesChange={updateWeaponNotes}
                        onWeaponRangeChange={updateWeaponRange}
                        onWeaponReset={resetWeapon}
                        onWeaponSizeChange={updateWeaponSize}
                        onWeaponWeightChange={updateWeaponWeight}
                        weapon={weapon}
                    />
                ))}
                {armors.map((armor) => (
                    <Armor
                        armor={armor}
                        key={armor.id}
                        onArmorAdd={addArmor}
                        onArmorBonusChange={updateArmorBonus}
                        onArmorDelete={deleteArmor}
                        onArmorMaxDexChange={updateArmorMaxDex}
                        onArmorMaxSpeedChange={updateArmorMaxSpeed}
                        onArmorNameChange={updateArmorName}
                        onArmorNotesChange={updateArmorNotes}
                        onArmorPenaltyChange={updateArmorPenalty}
                        onArmorReset={resetArmor}
                        onArmorSpellFailureChange={updateArmorSpellFailure}
                        onArmorTypeChange={updateArmorType}
                        onArmorWeightChange={updateArmorWeight}
                    />
                ))}
                <Flex className="not-printable">
                    <Spells />
                    <Flex direction="column">
                        <Valuables />
                        <Languages />
                    </Flex>
                </Flex>
                <Feats className="not-printable" />
                <KnownSpells className="not-printable" />
            </LeftVerticalFlex>

            <RightVerticalFlex>
                <Skills />
                <Equipment className="not-printable" />
                <Notes className="not-printable" />
            </RightVerticalFlex>
        </Flex>

        <Flex
            className="print-only-flex"
            justify="space-between"
        >
            <LeftVerticalFlex>
                <Flex>
                    <Spells />
                    <Flex direction="column">
                        <Valuables />
                        <Languages />
                    </Flex>
                </Flex>
                <Feats />
                <KnownSpells />
            </LeftVerticalFlex>

            <RightVerticalFlex>
                <Equipment />
                <Notes />
            </RightVerticalFlex>
        </Flex>

        <WeaponTypes />
        <WeaponList />
        <AttributeList />
        <ArmorList />
        <LanguageList />
    </div>
);

Sheet.propTypes = {
    addArmor: PropTypes.func.isRequired,
    addWeapon: PropTypes.func.isRequired,
    armors: PropTypes.array.isRequired,
    deleteArmor: PropTypes.func.isRequired,
    deleteWeapon: PropTypes.func.isRequired,
    resetArmor: PropTypes.func.isRequired,
    resetWeapon: PropTypes.func.isRequired,
    updateArmorBonus: PropTypes.func.isRequired,
    updateArmorMaxDex: PropTypes.func.isRequired,
    updateArmorMaxSpeed: PropTypes.func.isRequired,
    updateArmorName: PropTypes.func.isRequired,
    updateArmorNotes: PropTypes.func.isRequired,
    updateArmorPenalty: PropTypes.func.isRequired,
    updateArmorSpellFailure: PropTypes.func.isRequired,
    updateArmorType: PropTypes.func.isRequired,
    updateArmorWeight: PropTypes.func.isRequired,
    updateWeaponAttackBonus: PropTypes.func.isRequired,
    updateWeaponCritical: PropTypes.func.isRequired,
    updateWeaponDamage: PropTypes.func.isRequired,
    updateWeaponKind: PropTypes.func.isRequired,
    updateWeaponName: PropTypes.func.isRequired,
    updateWeaponNotes: PropTypes.func.isRequired,
    updateWeaponRange: PropTypes.func.isRequired,
    updateWeaponSize: PropTypes.func.isRequired,
    updateWeaponWeight: PropTypes.func.isRequired,
    weapons: PropTypes.array.isRequired
};

export default Sheet;
