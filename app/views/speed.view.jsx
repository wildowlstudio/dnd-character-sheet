import PropTypes from 'prop-types';
import React from 'react';
import Speed from '../components/sheet/speed';

const SpeedView = ({resetSpeed, speed, updateSpeed}) => (
    <Speed
        onSpeedChange={updateSpeed}
        onSpeedReset={resetSpeed}
        speed={speed}
    />
);

SpeedView.propTypes = {
    resetSpeed: PropTypes.func.isRequired,
    speed: PropTypes.string,
    updateSpeed: PropTypes.func.isRequired
};

export default SpeedView;
