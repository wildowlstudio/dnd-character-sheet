export const actionTypes = {
    UPDATE_HP: 'UPDATE_HP',
    UPDATE_CURRENT_HP: 'UPDATE_CURRENT_HP',
    UPDATE_SUBDUAL_DAMAGE: 'UPDATE_SUBDUAL_DAMAGE',
    UPDATE_AC_ARMOR: 'UPDATE_AC_ARMOR',
    UPDATE_AC_SHIELD: 'UPDATE_AC_SHIELD',
    UPDATE_AC_DEX: 'UPDATE_AC_DEX',
    UPDATE_AC_SIZE: 'UPDATE_AC_SIZE',
    UPDATE_AC_MISC: 'UPDATE_AC_MISC',
    RESET_HEALTH: 'RESET_HEALTH'
};

export function updateHp(value) {
    return {type: actionTypes.UPDATE_HP, value};
}

export function updateCurrentHp(value) {
    return {type: actionTypes.UPDATE_CURRENT_HP, value};
}

export function updateSubdualDamage(value) {
    return {type: actionTypes.UPDATE_SUBDUAL_DAMAGE, value};
}

export function updateAcArmor(value) {
    return {type: actionTypes.UPDATE_AC_ARMOR, value};
}

export function updateAcShield(value) {
    return {type: actionTypes.UPDATE_AC_SHIELD, value};
}

export function updateAcDex(value) {
    return {type: actionTypes.UPDATE_AC_DEX, value};
}

export function updateAcSize(value) {
    return {type: actionTypes.UPDATE_AC_SIZE, value};
}

export function updateAcMisc(value) {
    return {type: actionTypes.UPDATE_AC_MISC, value};
}

export function resetHealth() {
    return {type: actionTypes.RESET_HEALTH};
}
