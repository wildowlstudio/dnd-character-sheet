export const actionTypes = {
    UPDATE_SKILL_CLASS: 'UPDATE_SKILL_CLASS',
    UPDATE_SKILL_NAME: 'UPDATE_SKILL_NAME',
    UPDATE_SKILL_ATTRIBUTE: 'UPDATE_SKILL_ATTRIBUTE',
    UPDATE_SKILL_ABILITY_MOD: 'UPDATE_SKILL_ABILITY_MOD',
    UPDATE_SKILL_SKILL_RANK: 'UPDATE_SKILL_SKILL_RANK',
    UPDATE_SKILL_MISC_MOD: 'UPDATE_SKILL_MISC_MOD',
    RESET_SKILLS: 'RESET_SKILLS',
    ADD_SKILL: 'ADD_SKILL',
    DELETE_SKILLS: 'DELETE_SKILLS',
    DELETE_SKILL: 'DELETE_SKILL',
    SORT_SKILLS: 'SORT_SKILLS',
    UPDATE_SKILL_MAX_CLASS_RANK: 'UPDATE_SKILL_MAX_CLASS_RANK',
    UPDATE_SKILL_MAX_CROSS_CLASS_RANK: 'UPDATE_SKILL_MAX_CROSS_CLASS_RANK'
};

export function updateSkillClass(id, value) {
    return {type: actionTypes.UPDATE_SKILL_CLASS, id, value};
}

export function updateSkillName(id, value) {
    return {type: actionTypes.UPDATE_SKILL_NAME, id, value};
}

export function updateSkillAttribute(id, value) {
    return {type: actionTypes.UPDATE_SKILL_ATTRIBUTE, id, value};
}

export function updateSkillAbilityMod(id, value) {
    return {type: actionTypes.UPDATE_SKILL_ABILITY_MOD, id, value};
}

export function updateSkillSkillRank(id, value) {
    return {type: actionTypes.UPDATE_SKILL_SKILL_RANK, id, value};
}

export function updateSkillMiscMod(id, value) {
    return {type: actionTypes.UPDATE_SKILL_MISC_MOD, id, value};
}

export function resetSkills() {
    return {type: actionTypes.RESET_SKILLS};
}

export function addSkill() {
    return {type: actionTypes.ADD_SKILL};
}

export function deleteSkills() {
    return {type: actionTypes.DELETE_SKILLS};
}

export function deleteSkill(id) {
    return {type: actionTypes.DELETE_SKILL, id};
}

export function sortSkills(value, desc) {
    return {type: actionTypes.SORT_SKILLS, value, desc};
}

export function updateSkillMaxClassRank(value) {
    return {type: actionTypes.UPDATE_SKILL_MAX_CLASS_RANK, value};
}

export function updateSkillMaxCrossClassRank(value) {
    return {type: actionTypes.UPDATE_SKILL_MAX_CROSS_CLASS_RANK, value};
}
