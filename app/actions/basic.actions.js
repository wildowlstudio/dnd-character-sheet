export const actionTypes = {
    UPDATE_NAME: 'UPDATE_NAME',
    UPDATE_PLAYER: 'UPDATE_PLAYER',
    UPDATE_CLASS: 'UPDATE_CLASS',
    UPDATE_RACE: 'UPDATE_RACE',
    UPDATE_ALIGNMENT: 'UPDATE_ALIGNMENT',
    UPDATE_DEITY: 'UPDATE_DEITY',
    UPDATE_LEVEL: 'UPDATE_LEVEL',
    UPDATE_SIZE: 'UPDATE_SIZE',
    UPDATE_AGE: 'UPDATE_AGE',
    UPDATE_GENDER: 'UPDATE_GENDER',
    UPDATE_HEIGHT: 'UPDATE_HEIGHT',
    UPDATE_WEIGHT: 'UPDATE_WEIGHT',
    UPDATE_EYES: 'UPDATE_EYES',
    UPDATE_HAIR: 'UPDATE_HAIR'
};

export function updateName(value) {
    return {type: actionTypes.UPDATE_NAME, value};
}

export function updatePlayer(value) {
    return {type: actionTypes.UPDATE_PLAYER, value};
}

export function updateClass(value) {
    return {type: actionTypes.UPDATE_CLASS, value};
}

export function updateRace(value) {
    return {type: actionTypes.UPDATE_RACE, value};
}

export function updateAlignment(value) {
    return {type: actionTypes.UPDATE_ALIGNMENT, value};
}

export function updateDeity(value) {
    return {type: actionTypes.UPDATE_DEITY, value};
}

export function updateLevel(value) {
    return {type: actionTypes.UPDATE_LEVEL, value};
}

export function updateSize(value) {
    return {type: actionTypes.UPDATE_SIZE, value};
}

export function updateAge(value) {
    return {type: actionTypes.UPDATE_AGE, value};
}

export function updateGender(value) {
    return {type: actionTypes.UPDATE_GENDER, value};
}

export function updateHeight(value) {
    return {type: actionTypes.UPDATE_HEIGHT, value};
}

export function updateWeight(value) {
    return {type: actionTypes.UPDATE_WEIGHT, value};
}

export function updateEyes(value) {
    return {type: actionTypes.UPDATE_EYES, value};
}

export function updateHair(value) {
    return {type: actionTypes.UPDATE_HAIR, value};
}
