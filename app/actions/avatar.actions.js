export const actionTypes = {
    RESET_AVATAR: 'RESET_AVATAR',
    UPDATE_AVATAR: 'UPDATE_AVATAR'
};

export function resetAvatar() {
    return {type: actionTypes.RESET_AVATAR};
}

export function updateAvatar(value) {
    return {type: actionTypes.UPDATE_AVATAR, value};
}
