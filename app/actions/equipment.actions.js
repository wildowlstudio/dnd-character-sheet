export const actionTypes = {
    RESET_EQUIPMENT: 'RESET_EQUIPMENT',
    ADD_EQUIPMENT_ITEM: 'ADD_EQUIPMENT_ITEM',
    DELETE_EQUIPMENT_ITEMS: 'DELETE_EQUIPMENT_ITEMS',
    DELETE_EQUIPMENT_ITEM: 'DELETE_EQUIPMENT_ITEM',
    SORT_EQUIPMENT: 'SORT_EQUIPMENT',
    UPDATE_EQUIPMENT_ITEM_NAME: 'UPDATE_EQUIPMENT_ITEM_NAME',
    UPDATE_EQUIPMENT_ITEM_WEIGHT: 'UPDATE_EQUIPMENT_ITEM_WEIGHT',
    UPDATE_EQUIPMENT_ITEM_CAPACITY: 'UPDATE_EQUIPMENT_ITEM_CAPACITY',
    UPDATE_EQUIPMENT_ITEM_VALUE: 'UPDATE_EQUIPMENT_ITEM_VALUE',
    UPDATE_EQUIPMENT_OPTION_WEIGHT: 'UPDATE_EQUIPMENT_OPTION_WEIGHT',
    UPDATE_EQUIPMENT_OPTION_CAPACITY: 'UPDATE_EQUIPMENT_OPTION_CAPACITY',
    UPDATE_EQUIPMENT_OPTION_VALUE: 'UPDATE_EQUIPMENT_OPTION_VALUE',
    UPDATE_EQUIPMENT_OPTION_SUM: 'UPDATE_EQUIPMENT_OPTION_SUM',
    MOVE_EQUIPMENT_ITEM: 'MOVE_EQUIPMENT_ITEM'
};

export function resetEquipment() {
    return {type: actionTypes.RESET_EQUIPMENT};
}

export function addEquipmentItem() {
    return {type: actionTypes.ADD_EQUIPMENT_ITEM};
}

export function deleteEquipmentItems() {
    return {type: actionTypes.DELETE_EQUIPMENT_ITEMS};
}

export function deleteEquipmentItem(id, parentId) {
    return {type: actionTypes.DELETE_EQUIPMENT_ITEM, id, parentId};
}

export function sortEquipment(value, desc) {
    return {type: actionTypes.SORT_EQUIPMENT, value, desc};
}

export function updateEquipmentItemName(id, value, parentId) {
    return {type: actionTypes.UPDATE_EQUIPMENT_ITEM_NAME, id, value, parentId};
}

export function updateEquipmentItemWeight(id, value, parentId) {
    return {type: actionTypes.UPDATE_EQUIPMENT_ITEM_WEIGHT, id, value, parentId};
}

export function updateEquipmentItemCapacity(id, value, parentId) {
    return {type: actionTypes.UPDATE_EQUIPMENT_ITEM_CAPACITY, id, value, parentId};
}

export function updateEquipmentItemValue(id, value, parentId) {
    return {type: actionTypes.UPDATE_EQUIPMENT_ITEM_VALUE, id, value, parentId};
}

export function updateEquipmentOptionWeight(value) {
    return {type: actionTypes.UPDATE_EQUIPMENT_OPTION_WEIGHT, value};
}

export function updateEquipmentOptionCapacity(value) {
    return {type: actionTypes.UPDATE_EQUIPMENT_OPTION_CAPACITY, value};
}

export function updateEquipmentOptionValue(value) {
    return {type: actionTypes.UPDATE_EQUIPMENT_OPTION_VALUE, value};
}

export function updateEquipmentOptionSum(value) {
    return {type: actionTypes.UPDATE_EQUIPMENT_OPTION_SUM, value};
}

export function moveEquipmentItem(id, parentId, below, targetId, targetParentId) {
    return {
        type: actionTypes.MOVE_EQUIPMENT_ITEM,
        id,
        below,
        parentId,
        targetId,
        targetParentId
    };
}
