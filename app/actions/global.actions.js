export const actionTypes = {
    LOAD_SHEET: 'LOAD_SHEET',
    RESET_SHEET: 'RESET_SHEET'
};

export function resetSheet(sheet) {
    return {type: actionTypes.RESET_SHEET};
}

export function loadSheet(sheet) {
    return {type: actionTypes.LOAD_SHEET, sheet};
}
