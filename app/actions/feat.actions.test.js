import {
    actionTypes,
    addFeat,
    cleanFeats,
    deleteFeat,
    moveFeat,
    resetFeats,
    updateFeat
} from './feat.actions';

describe('Action tests', () => {
    test('Should create an action to UPDATE_FEAT', () => {
        expect(updateFeat(0, 'test')).toEqual({
            type: actionTypes.UPDATE_FEAT,
            id: 0,
            value: 'test'
        });
    });

    test('Should create an action to RESET_FEATS', () => {
        expect(resetFeats()).toEqual({
            type: actionTypes.RESET_FEATS
        });
    });

    test('Should create an action to ADD_FEAT', () => {
        expect(addFeat()).toEqual({
            type: actionTypes.ADD_FEAT
        });
    });

    test('Should create an action to MOVE_FEAT', () => {
        expect(moveFeat(0, true, 1)).toEqual({
            type: actionTypes.MOVE_FEAT,
            id: 0,
            below: true,
            targetId: 1
        });
    });

    test('Should create an action to CLEAN_FEATS', () => {
        expect(cleanFeats()).toEqual({
            type: actionTypes.CLEAN_FEATS
        });
    });

    test('Should create an action to DELETE_FEAT', () => {
        expect(deleteFeat(0)).toEqual({
            type: actionTypes.DELETE_FEAT,
            id: 0
        });
    });
});
