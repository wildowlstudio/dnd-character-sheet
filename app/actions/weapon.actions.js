export const actionTypes = {
    UPDATE_WEAPON_NAME: 'UPDATE_WEAPON_NAME',
    UPDATE_WEAPON_ATTACK_BONUS: 'UPDATE_WEAPON_ATTACK_BONUS',
    UPDATE_WEAPON_DAMAGE: 'UPDATE_WEAPON_DAMAGE',
    UPDATE_WEAPON_CRITICAL: 'UPDATE_WEAPON_CRITICAL',
    UPDATE_WEAPON_RANGE: 'UPDATE_WEAPON_RANGE',
    UPDATE_WEAPON_WEIGHT: 'UPDATE_WEAPON_WEIGHT',
    UPDATE_WEAPON_SIZE: 'UPDATE_WEAPON_SIZE',
    UPDATE_WEAPON_KIND: 'UPDATE_WEAPON_KIND',
    UPDATE_WEAPON_NOTES: 'UPDATE_WEAPON_NOTES',
    ADD_WEAPON: 'ADD_WEAPON',
    RESET_WEAPON: 'RESET_WEAPON',
    DELETE_WEAPON: 'DELETE_WEAPON'
};

export function updateWeaponName(id, value) {
    return {type: actionTypes.UPDATE_WEAPON_NAME, id, value};
}

export function updateWeaponAttackBonus(id, value) {
    return {type: actionTypes.UPDATE_WEAPON_ATTACK_BONUS, id, value};
}

export function updateWeaponDamage(id, value) {
    return {type: actionTypes.UPDATE_WEAPON_DAMAGE, id, value};
}

export function updateWeaponCritical(id, value) {
    return {type: actionTypes.UPDATE_WEAPON_CRITICAL, id, value};
}

export function updateWeaponRange(id, value) {
    return {type: actionTypes.UPDATE_WEAPON_RANGE, id, value};
}

export function updateWeaponWeight(id, value) {
    return {type: actionTypes.UPDATE_WEAPON_WEIGHT, id, value};
}

export function updateWeaponSize(id, value) {
    return {type: actionTypes.UPDATE_WEAPON_SIZE, id, value};
}

export function updateWeaponKind(id, value) {
    return {type: actionTypes.UPDATE_WEAPON_KIND, id, value};
}

export function updateWeaponNotes(id, value) {
    return {type: actionTypes.UPDATE_WEAPON_NOTES, id, value};
}

export function addWeapon() {
    return {type: actionTypes.ADD_WEAPON};
}

export function resetWeapon(id) {
    return {type: actionTypes.RESET_WEAPON, id};
}

export function deleteWeapon(id) {
    return {type: actionTypes.DELETE_WEAPON, id};
}
