import {
    actionTypes,
    addEquipmentItem,
    deleteEquipmentItem,
    deleteEquipmentItems,
    moveEquipmentItem,
    resetEquipment,
    sortEquipment,
    updateEquipmentItemCapacity,
    updateEquipmentItemName,
    updateEquipmentItemValue,
    updateEquipmentItemWeight,
    updateEquipmentOptionCapacity,
    updateEquipmentOptionSum,
    updateEquipmentOptionValue,
    updateEquipmentOptionWeight
} from './equipment.actions';

describe('Action tests', () => {
    test('Should create an action to RESET_EQUIPMENT', () => {
        expect(resetEquipment()).toEqual({
            type: actionTypes.RESET_EQUIPMENT
        });
    });

    test('Should create an action to ADD_EQUIPMENT_ITEM', () => {
        expect(addEquipmentItem()).toEqual({
            type: actionTypes.ADD_EQUIPMENT_ITEM
        });
    });

    test('Should create an action to DELETE_EQUIPMENT_ITEMS', () => {
        expect(deleteEquipmentItems()).toEqual({
            type: actionTypes.DELETE_EQUIPMENT_ITEMS
        });
    });

    test('Should create an action to DELETE_EQUIPMENT_ITEM', () => {
        expect(deleteEquipmentItem(1, 2)).toEqual({
            type: actionTypes.DELETE_EQUIPMENT_ITEM,
            id: 1,
            parentId: 2
        });
    });

    test('Should create an action to SORT_EQUIPMENT', () => {
        expect(sortEquipment('test', true)).toEqual({
            type: actionTypes.SORT_EQUIPMENT,
            value: 'test',
            desc: true
        });
    });

    test('Should create an action to UPDATE_EQUIPMENT_ITEM_NAME', () => {
        expect(updateEquipmentItemName(0, 'test', 1)).toEqual({
            type: actionTypes.UPDATE_EQUIPMENT_ITEM_NAME,
            id: 0,
            value: 'test',
            parentId: 1
        });
    });

    test('Should create an action to UPDATE_EQUIPMENT_ITEM_WEIGHT', () => {
        expect(updateEquipmentItemWeight(0, 'test', 1)).toEqual({
            type: actionTypes.UPDATE_EQUIPMENT_ITEM_WEIGHT,
            id: 0,
            value: 'test',
            parentId: 1
        });
    });

    test('Should create an action to UPDATE_EQUIPMENT_ITEM_CAPACITY', () => {
        expect(updateEquipmentItemCapacity(0, 'test', 1)).toEqual({
            type: actionTypes.UPDATE_EQUIPMENT_ITEM_CAPACITY,
            id: 0,
            value: 'test',
            parentId: 1
        });
    });

    test('Should create an action to UPDATE_EQUIPMENT_ITEM_VALUE', () => {
        expect(updateEquipmentItemValue(0, 'test', 1)).toEqual({
            type: actionTypes.UPDATE_EQUIPMENT_ITEM_VALUE,
            id: 0,
            value: 'test',
            parentId: 1
        });
    });

    test('Should create an action to UPDATE_EQUIPMENT_OPTION_WEIGHT', () => {
        expect(updateEquipmentOptionWeight(true)).toEqual({
            type: actionTypes.UPDATE_EQUIPMENT_OPTION_WEIGHT,
            value: true
        });
    });

    test('Should create an action to UPDATE_EQUIPMENT_OPTION_CAPACITY', () => {
        expect(updateEquipmentOptionCapacity(true)).toEqual({
            type: actionTypes.UPDATE_EQUIPMENT_OPTION_CAPACITY,
            value: true
        });
    });

    test('Should create an action to UPDATE_EQUIPMENT_OPTION_VALUE', () => {
        expect(updateEquipmentOptionValue(true)).toEqual({
            type: actionTypes.UPDATE_EQUIPMENT_OPTION_VALUE,
            value: true
        });
    });

    test('Should create an action to UPDATE_EQUIPMENT_OPTION_SUM', () => {
        expect(updateEquipmentOptionSum(true)).toEqual({
            type: actionTypes.UPDATE_EQUIPMENT_OPTION_SUM,
            value: true
        });
    });

    test('Should create an action to MOVE_EQUIPMENT_ITEM', () => {
        expect(moveEquipmentItem(1, 2, true, 3, 4)).toEqual({
            type: actionTypes.MOVE_EQUIPMENT_ITEM,
            id: 1,
            parentId: 2,
            below: true,
            targetId: 3,
            targetParentId: 4
        });
    });
});
