export const actionTypes = {
    UPDATE_XP: 'UPDATE_XP',
    RESET_XP: 'RESET_XP'
};

export function updateXp(value) {
    return {type: actionTypes.UPDATE_XP, value};
}

export function resetXp() {
    return {type: actionTypes.RESET_XP};
}
