export const actionTypes = {
    UPDATE_ARMOR_NAME: 'UPDATE_ARMOR_NAME',
    UPDATE_ARMOR_BONUS: 'UPDATE_ARMOR_BONUS',
    UPDATE_ARMOR_MAX_DEX: 'UPDATE_ARMOR_MAX_DEX',
    UPDATE_ARMOR_PENALTY: 'UPDATE_ARMOR_PENALTY',
    UPDATE_ARMOR_MAX_SPEED: 'UPDATE_ARMOR_MAX_SPEED',
    UPDATE_ARMOR_WEIGHT: 'UPDATE_ARMOR_WEIGHT',
    UPDATE_ARMOR_TYPE: 'UPDATE_ARMOR_TYPE',
    UPDATE_ARMOR_SPELL_FAILURE: 'UPDATE_ARMOR_SPELL_FAILURE',
    UPDATE_ARMOR_NOTES: 'UPDATE_ARMOR_NOTES',
    ADD_ARMOR: 'ADD_ARMOR',
    RESET_ARMOR: 'RESET_ARMOR',
    DELETE_ARMOR: 'DELETE_ARMOR'
};

export function updateArmorName(id, value) {
    return {type: actionTypes.UPDATE_ARMOR_NAME, id, value};
}

export function updateArmorBonus(id, value) {
    return {type: actionTypes.UPDATE_ARMOR_BONUS, id, value};
}

export function updateArmorMaxDex(id, value) {
    return {type: actionTypes.UPDATE_ARMOR_MAX_DEX, id, value};
}

export function updateArmorPenalty(id, value) {
    return {type: actionTypes.UPDATE_ARMOR_PENALTY, id, value};
}

export function updateArmorMaxSpeed(id, value) {
    return {type: actionTypes.UPDATE_ARMOR_MAX_SPEED, id, value};
}

export function updateArmorWeight(id, value) {
    return {type: actionTypes.UPDATE_ARMOR_WEIGHT, id, value};
}

export function updateArmorType(id, value) {
    return {type: actionTypes.UPDATE_ARMOR_TYPE, id, value};
}

export function updateArmorSpellFailure(id, value) {
    return {type: actionTypes.UPDATE_ARMOR_SPELL_FAILURE, id, value};
}

export function updateArmorNotes(id, value) {
    return {type: actionTypes.UPDATE_ARMOR_NOTES, id, value};
}

export function addArmor() {
    return {type: actionTypes.ADD_ARMOR};
}

export function resetArmor(id) {
    return {type: actionTypes.RESET_ARMOR, id};
}

export function deleteArmor(id) {
    return {type: actionTypes.DELETE_ARMOR, id};
}
