import {
    actionTypes,
    addWeapon,
    deleteWeapon,
    resetWeapon,
    updateWeaponAttackBonus,
    updateWeaponCritical,
    updateWeaponDamage,
    updateWeaponKind,
    updateWeaponName,
    updateWeaponNotes,
    updateWeaponRange,
    updateWeaponSize,
    updateWeaponWeight
} from './weapon.actions';

describe('Action tests', () => {
    test('Should create an action to update weapon name', () => {
        expect(updateWeaponName(0, 'test')).toEqual({
            type: actionTypes.UPDATE_WEAPON_NAME,
            id: 0,
            value: 'test'
        });
    });
    test('Should create an action to update weapon attack bonus', () => {
        expect(updateWeaponAttackBonus(0, 'test')).toEqual({
            type: actionTypes.UPDATE_WEAPON_ATTACK_BONUS,
            id: 0,
            value: 'test'
        });
    });
    test('Should create an action to update weapon damage', () => {
        expect(updateWeaponDamage(0, 'test')).toEqual({
            type: actionTypes.UPDATE_WEAPON_DAMAGE,
            id: 0,
            value: 'test'
        });
    });
    test('Should create an action to update weapon critical', () => {
        expect(updateWeaponCritical(0, 'test')).toEqual({
            type: actionTypes.UPDATE_WEAPON_CRITICAL,
            id: 0,
            value: 'test'
        });
    });
    test('Should create an action to update weapon range', () => {
        expect(updateWeaponRange(0, 'test')).toEqual({
            type: actionTypes.UPDATE_WEAPON_RANGE,
            id: 0,
            value: 'test'
        });
    });
    test('Should create an action to update weapon weight', () => {
        expect(updateWeaponWeight(0, 'test')).toEqual({
            type: actionTypes.UPDATE_WEAPON_WEIGHT,
            id: 0,
            value: 'test'
        });
    });
    test('Should create an action to update weapon size', () => {
        expect(updateWeaponSize(0, 'test')).toEqual({
            type: actionTypes.UPDATE_WEAPON_SIZE,
            id: 0,
            value: 'test'
        });
    });
    test('Should create an action to update weapon kind', () => {
        expect(updateWeaponKind(0, 'test')).toEqual({
            type: actionTypes.UPDATE_WEAPON_KIND,
            id: 0,
            value: 'test'
        });
    });
    test('Should create an action to update weapon notes', () => {
        expect(updateWeaponNotes(0, 'test')).toEqual({
            type: actionTypes.UPDATE_WEAPON_NOTES,
            id: 0,
            value: 'test'
        });
    });
    test('Should create an action to add weapon', () => {
        expect(addWeapon()).toEqual({
            type: actionTypes.ADD_WEAPON
        });
    });
    test('Should create an action to reset weapon', () => {
        expect(resetWeapon(0)).toEqual({
            type: actionTypes.RESET_WEAPON,
            id: 0
        });
    });
    test('Should create an action to delete weapon', () => {
        expect(deleteWeapon(0)).toEqual({
            type: actionTypes.DELETE_WEAPON,
            id: 0
        });
    });
});
