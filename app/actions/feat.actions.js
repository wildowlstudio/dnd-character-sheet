export const actionTypes = {
    UPDATE_FEAT: 'UPDATE_FEAT',
    DELETE_FEAT: 'DELETE_FEAT',
    RESET_FEATS: 'RESET_FEATS',
    ADD_FEAT: 'ADD_FEAT',
    MOVE_FEAT: 'MOVE_FEAT',
    CLEAN_FEATS: 'CLEAN_FEATS'
};

export function updateFeat(id, value) {
    return {type: actionTypes.UPDATE_FEAT, id, value};
}

export function deleteFeat(id) {
    return {type: actionTypes.DELETE_FEAT, id};
}

export function resetFeats() {
    return {type: actionTypes.RESET_FEATS};
}

export function addFeat() {
    return {type: actionTypes.ADD_FEAT};
}

export function moveFeat(id, below, targetId) {
    return {type: actionTypes.MOVE_FEAT, id, below, targetId};
}

export function cleanFeats() {
    return {type: actionTypes.CLEAN_FEATS};
}
