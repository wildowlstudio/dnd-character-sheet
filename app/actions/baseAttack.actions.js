export const actionTypes = {
    UPDATE_BASE_ATTACK: 'UPDATE_BASE_ATTACK',
    RESET_BASE_ATTACK: 'RESET_BASE_ATTACK'
};

export function updateBaseAttack(value) {
    return {type: actionTypes.UPDATE_BASE_ATTACK, value};
}

export function resetBaseAttack() {
    return {type: actionTypes.RESET_BASE_ATTACK};
}
