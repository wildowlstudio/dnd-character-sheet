import {
    actionTypes,
    resetInitiative,
    updateInitiativeDexMod,
    updateInitiativeMiscMod
} from './initiative.actions';

describe('Action tests', () => {
    test('Should create an action to UPDATE_INITIATIVE_DEX_MOD', () => {
        expect(updateInitiativeDexMod('test')).toEqual({
            type: actionTypes.UPDATE_INITIATIVE_DEX_MOD,
            value: 'test'
        });
    });

    test('Should create an action to UPDATE_INITIATIVE_MISC_MOD', () => {
        expect(updateInitiativeMiscMod('test')).toEqual({
            type: actionTypes.UPDATE_INITIATIVE_MISC_MOD,
            value: 'test'
        });
    });

    test('Should create an action to RESET_INITIATIVE', () => {
        expect(resetInitiative('test')).toEqual({
            type: actionTypes.RESET_INITIATIVE
        });
    });
});
