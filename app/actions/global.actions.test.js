import {actionTypes, loadSheet, resetSheet} from './global.actions';

describe('Action tests', () => {
    test('Should create an action to reset sheet', () => {
        expect(resetSheet()).toEqual({
            type: actionTypes.RESET_SHEET
        });
    });
    test('Should create an action to load sheet', () => {
        const value = {};
        expect(loadSheet(value)).toEqual({
            type: actionTypes.LOAD_SHEET,
            sheet: {}
        });
    });
});
