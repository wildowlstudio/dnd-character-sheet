import {actionTypes, resetAvatar, updateAvatar} from './avatar.actions';

describe('Action tests', () => {
    test('Should create an action to RESET_AVATAR', () => {
        expect(resetAvatar()).toEqual({
            type: actionTypes.RESET_AVATAR
        });
    });

    test('Should create an action to UPDATE_AVATAR', () => {
        expect(updateAvatar()).toEqual({
            type: actionTypes.UPDATE_AVATAR
        });
    });
});
