export const actionTypes = {
    UPDATE_VALUABLES_MONEY: 'UPDATE_VALUABLES_MONEY',
    UPDATE_VALUABLES_BRONZE_OPTION: 'UPDATE_VALUABLES_BRONZE_OPTION',
    UPDATE_VALUABLES_IRON_OPTION: 'UPDATE_VALUABLES_IRON_OPTION',
    UPDATE_VALUABLES_COPPER_OPTION: 'UPDATE_VALUABLES_COPPER_OPTION',
    UPDATE_VALUABLES_SILVER_OPTION: 'UPDATE_VALUABLES_SILVER_OPTION',
    UPDATE_VALUABLES_GOLD_OPTION: 'UPDATE_VALUABLES_GOLD_OPTION',
    UPDATE_VALUABLES_PLATINUM_OPTION: 'UPDATE_VALUABLES_PLATINUM_OPTION',
    RESET_VALUABLES: 'RESET_VALUABLES'
};

export function updateValuablesMoney(value) {
    return {type: actionTypes.UPDATE_VALUABLES_MONEY, value};
}

export function updateValuablesBronzeOption(value) {
    return {type: actionTypes.UPDATE_VALUABLES_BRONZE_OPTION, value};
}

export function updateValuablesIronOption(value) {
    return {type: actionTypes.UPDATE_VALUABLES_IRON_OPTION, value};
}

export function updateValuablesCopperOption(value) {
    return {type: actionTypes.UPDATE_VALUABLES_COPPER_OPTION, value};
}

export function updateValuablesSilverOption(value) {
    return {type: actionTypes.UPDATE_VALUABLES_SILVER_OPTION, value};
}

export function updateValuablesGoldOption(value) {
    return {type: actionTypes.UPDATE_VALUABLES_GOLD_OPTION, value};
}

export function updateValuablesPlatinumOption(value) {
    return {type: actionTypes.UPDATE_VALUABLES_PLATINUM_OPTION, value};
}

export function resetValuables() {
    return {type: actionTypes.RESET_VALUABLES};
}
