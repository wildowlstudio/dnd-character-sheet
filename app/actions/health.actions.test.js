import {
    actionTypes,
    resetHealth,
    updateAcArmor,
    updateAcDex,
    updateAcMisc,
    updateAcShield,
    updateAcSize,
    updateCurrentHp,
    updateHp,
    updateSubdualDamage
} from './health.actions';

describe('Action tests', () => {
    test('Should create an action to update hp', () => {
        expect(updateHp(0)).toEqual({
            type: actionTypes.UPDATE_HP,
            value: 0
        });
    });

    test('Should create an action to update current hp', () => {
        expect(updateCurrentHp(0)).toEqual({
            type: actionTypes.UPDATE_CURRENT_HP,
            value: 0
        });
    });

    test('Should create an action to update subdual damage', () => {
        expect(updateSubdualDamage(0)).toEqual({
            type: actionTypes.UPDATE_SUBDUAL_DAMAGE,
            value: 0
        });
    });

    test('Should create an action to update ac armor', () => {
        expect(updateAcArmor(0)).toEqual({
            type: actionTypes.UPDATE_AC_ARMOR,
            value: 0
        });
    });

    test('Should create an action to update ac shield', () => {
        expect(updateAcShield(0)).toEqual({
            type: actionTypes.UPDATE_AC_SHIELD,
            value: 0
        });
    });

    test('Should create an action to update ac dex', () => {
        expect(updateAcDex(0)).toEqual({
            type: actionTypes.UPDATE_AC_DEX,
            value: 0
        });
    });

    test('Should create an action to update ac size', () => {
        expect(updateAcSize(0)).toEqual({
            type: actionTypes.UPDATE_AC_SIZE,
            value: 0
        });
    });

    test('Should create an action to update ac misc', () => {
        expect(updateAcMisc(0)).toEqual({
            type: actionTypes.UPDATE_AC_MISC,
            value: 0
        });
    });

    test('Should create an action to reset health', () => {
        expect(resetHealth()).toEqual({
            type: actionTypes.RESET_HEALTH
        });
    });
});
