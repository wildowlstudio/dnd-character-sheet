import {
    actionTypes,
    resetSavingThrows,
    updateFortitudeAbilityMod,
    updateFortitudeBase,
    updateFortitudeMagicMod,
    updateFortitudeMiscMod,
    updateFortitudeOtherMod,
    updateFortitudeTempMod,
    updateReflexAbilityMod,
    updateReflexBase,
    updateReflexMagicMod,
    updateReflexMiscMod,
    updateReflexOtherMod,
    updateReflexTempMod,
    updateWillpowerAbilityMod,
    updateWillpowerBase,
    updateWillpowerMagicMod,
    updateWillpowerMiscMod,
    updateWillpowerOtherMod,
    updateWillpowerTempMod
} from './savingThrow.actions';

describe('Action tests', () => {
    test('Should create an action to fortitude base', () => {
        expect(updateFortitudeBase(0)).toEqual({
            type: actionTypes.UPDATE_FORTITUDE_BASE,
            value: 0
        });
    });

    test('Should create an action to fortitude ability mod', () => {
        expect(updateFortitudeAbilityMod(0)).toEqual({
            type: actionTypes.UPDATE_FORTITUDE_ABILITY_MOD,
            value: 0
        });
    });

    test('Should create an action to fortitude magic mod', () => {
        expect(updateFortitudeMagicMod(0)).toEqual({
            type: actionTypes.UPDATE_FORTITUDE_MAGIC_MOD,
            value: 0
        });
    });

    test('Should create an action to fortitude misc mod', () => {
        expect(updateFortitudeMiscMod(0)).toEqual({
            type: actionTypes.UPDATE_FORTITUDE_MISC_MOD,
            value: 0
        });
    });

    test('Should create an action to fortitude temp mod', () => {
        expect(updateFortitudeTempMod(0)).toEqual({
            type: actionTypes.UPDATE_FORTITUDE_TEMP_MOD,
            value: 0
        });
    });

    test('Should create an action to fortitude other mod', () => {
        expect(updateFortitudeOtherMod(0)).toEqual({
            type: actionTypes.UPDATE_FORTITUDE_OTHER_MOD,
            value: 0
        });
    });

    test('Should create an action to reflex base', () => {
        expect(updateReflexBase(0)).toEqual({
            type: actionTypes.UPDATE_REFLEX_BASE,
            value: 0
        });
    });

    test('Should create an action to reflex ability mod', () => {
        expect(updateReflexAbilityMod(0)).toEqual({
            type: actionTypes.UPDATE_REFLEX_ABILITY_MOD,
            value: 0
        });
    });

    test('Should create an action to reflex magic mod', () => {
        expect(updateReflexMagicMod(0)).toEqual({
            type: actionTypes.UPDATE_REFLEX_MAGIC_MOD,
            value: 0
        });
    });

    test('Should create an action to reflex misc mod', () => {
        expect(updateReflexMiscMod(0)).toEqual({
            type: actionTypes.UPDATE_REFLEX_MISC_MOD,
            value: 0
        });
    });

    test('Should create an action to reflex temp mod', () => {
        expect(updateReflexTempMod(0)).toEqual({
            type: actionTypes.UPDATE_REFLEX_TEMP_MOD,
            value: 0
        });
    });

    test('Should create an action to reflex other mod', () => {
        expect(updateReflexOtherMod(0)).toEqual({
            type: actionTypes.UPDATE_REFLEX_OTHER_MOD,
            value: 0
        });
    });

    test('Should create an action to willpower base', () => {
        expect(updateWillpowerBase(0)).toEqual({
            type: actionTypes.UPDATE_WILLPOWER_BASE,
            value: 0
        });
    });

    test('Should create an action to willpower ability mod', () => {
        expect(updateWillpowerAbilityMod(0)).toEqual({
            type: actionTypes.UPDATE_WILLPOWER_ABILITY_MOD,
            value: 0
        });
    });

    test('Should create an action to willpower magic mod', () => {
        expect(updateWillpowerMagicMod(0)).toEqual({
            type: actionTypes.UPDATE_WILLPOWER_MAGIC_MOD,
            value: 0
        });
    });

    test('Should create an action to willpower misc mod', () => {
        expect(updateWillpowerMiscMod(0)).toEqual({
            type: actionTypes.UPDATE_WILLPOWER_MISC_MOD,
            value: 0
        });
    });

    test('Should create an action to willpower temp mod', () => {
        expect(updateWillpowerTempMod(0)).toEqual({
            type: actionTypes.UPDATE_WILLPOWER_TEMP_MOD,
            value: 0
        });
    });

    test('Should create an action to willpower other mod', () => {
        expect(updateWillpowerOtherMod(0)).toEqual({
            type: actionTypes.UPDATE_WILLPOWER_OTHER_MOD,
            value: 0
        });
    });

    test('Should create an action to reset saving throws', () => {
        expect(resetSavingThrows()).toEqual({
            type: actionTypes.RESET_SAVING_THROWS
        });
    });
});
