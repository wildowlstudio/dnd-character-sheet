export const actionTypes = {
    UPDATE_LANGUAGE: 'UPDATE_LANGUAGE',
    DELETE_LANGUAGE: 'DELETE_LANGUAGE',
    RESET_LANGUAGES: 'RESET_LANGUAGES',
    ADD_LANGUAGE: 'ADD_LANGUAGE',
    MOVE_LANGUAGE: 'MOVE_LANGUAGE',
    CLEAN_LANGUAGES: 'CLEAN_LANGUAGES'
};

export function updateLanguage(id, value) {
    return {type: actionTypes.UPDATE_LANGUAGE, id, value};
}

export function deleteLanguage(id) {
    return {type: actionTypes.DELETE_LANGUAGE, id};
}

export function resetLanguages() {
    return {type: actionTypes.RESET_LANGUAGES};
}

export function addLanguage() {
    return {type: actionTypes.ADD_LANGUAGE};
}

export function moveLanguage(id, below, targetId) {
    return {type: actionTypes.MOVE_LANGUAGE, id, below, targetId};
}

export function cleanLanguages() {
    return {type: actionTypes.CLEAN_LANGUAGES};
}
