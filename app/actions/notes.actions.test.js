import {actionTypes, resetNotes, updateNotes} from './notes.actions';

describe('Action tests', () => {
    test('Should create an action to RESET_NOTES', () => {
        expect(resetNotes()).toEqual({
            type: actionTypes.RESET_NOTES
        });
    });

    test('Should create an action to UPDATE_NOTES', () => {
        expect(updateNotes('test')).toEqual({
            type: actionTypes.UPDATE_NOTES,
            value: 'test'
        });
    });
});
