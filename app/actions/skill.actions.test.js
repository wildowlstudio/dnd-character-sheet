import {
    actionTypes,
    addSkill,
    deleteSkill,
    deleteSkills,
    resetSkills,
    sortSkills,
    updateSkillAbilityMod,
    updateSkillAttribute,
    updateSkillClass,
    updateSkillMaxClassRank,
    updateSkillMaxCrossClassRank,
    updateSkillMiscMod,
    updateSkillName,
    updateSkillSkillRank
} from './skill.actions';

describe('Action tests', () => {
    test('Should create an action to update skill class', () => {
        expect(updateSkillClass(0, 'test')).toEqual({
            type: actionTypes.UPDATE_SKILL_CLASS,
            id: 0,
            value: 'test'
        });
    });

    test('Should create an action to update skill name', () => {
        expect(updateSkillName(0, 'test')).toEqual({
            type: actionTypes.UPDATE_SKILL_NAME,
            id: 0,
            value: 'test'
        });
    });

    test('Should create an action to update skill attribute', () => {
        expect(updateSkillAttribute(0, 'test')).toEqual({
            type: actionTypes.UPDATE_SKILL_ATTRIBUTE,
            id: 0,
            value: 'test'
        });
    });

    test('Should create an action to update skill ability mod', () => {
        expect(updateSkillAbilityMod(0, 'test')).toEqual({
            type: actionTypes.UPDATE_SKILL_ABILITY_MOD,
            id: 0,
            value: 'test'
        });
    });

    test('Should create an action to update skill skill rank', () => {
        expect(updateSkillSkillRank(0, 'test')).toEqual({
            type: actionTypes.UPDATE_SKILL_SKILL_RANK,
            id: 0,
            value: 'test'
        });
    });

    test('Should create an action to update skill misc mod', () => {
        expect(updateSkillMiscMod(0, 'test')).toEqual({
            type: actionTypes.UPDATE_SKILL_MISC_MOD,
            id: 0,
            value: 'test'
        });
    });

    test('Should create an action to reset skills', () => {
        expect(resetSkills()).toEqual({
            type: actionTypes.RESET_SKILLS
        });
    });

    test('Should create an action to add skill', () => {
        expect(addSkill()).toEqual({
            type: actionTypes.ADD_SKILL
        });
    });

    test('Should create an action to delete skill', () => {
        expect(deleteSkill(0)).toEqual({
            type: actionTypes.DELETE_SKILL,
            id: 0
        });
    });

    test('Should create an action to delete skills', () => {
        expect(deleteSkills()).toEqual({
            type: actionTypes.DELETE_SKILLS
        });
    });

    test('Should create an action to sort skills', () => {
        expect(sortSkills('test', false)).toEqual({
            type: actionTypes.SORT_SKILLS,
            value: 'test',
            desc: false
        });
    });

    test('Should create an action to UPDATE_SKILL_MAX_CLASS_RANK', () => {
        expect(updateSkillMaxClassRank('test')).toEqual({
            type: actionTypes.UPDATE_SKILL_MAX_CLASS_RANK,
            value: 'test'
        });
    });

    test('Should create an action to UPDATE_SKILL_MAX_CROSS_CLASS_RANK', () => {
        expect(updateSkillMaxCrossClassRank('test')).toEqual({
            type: actionTypes.UPDATE_SKILL_MAX_CROSS_CLASS_RANK,
            value: 'test'
        });
    });
});
