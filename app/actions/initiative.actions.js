export const actionTypes = {
    UPDATE_INITIATIVE_DEX_MOD: 'UPDATE_INITIATIVE_DEX_MOD',
    UPDATE_INITIATIVE_MISC_MOD: 'UPDATE_INITIATIVE_MISC_MOD',
    RESET_INITIATIVE: 'RESET_INITIATIVE'
};

export function updateInitiativeDexMod(value) {
    return {type: actionTypes.UPDATE_INITIATIVE_DEX_MOD, value};
}

export function updateInitiativeMiscMod(value) {
    return {type: actionTypes.UPDATE_INITIATIVE_MISC_MOD, value};
}

export function resetInitiative() {
    return {type: actionTypes.RESET_INITIATIVE};
}
