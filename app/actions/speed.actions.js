export const actionTypes = {
    UPDATE_SPEED: 'UPDATE_SPEED',
    RESET_SPEED: 'RESET_SPEED'
};

export function updateSpeed(value) {
    return {type: actionTypes.UPDATE_SPEED, value};
}

export function resetSpeed() {
    return {type: actionTypes.RESET_SPEED};
}
