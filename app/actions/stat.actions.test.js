import {
    actionTypes,
    resetStats,
    updateStatMod,
    updateStatTempMod,
    updateStatTempScore
} from './stat.actions';

describe('Action tests', () => {
    test('Should create an action to update stat mod', () => {
        const value = Math.random();
        const stat = 'test';
        expect(updateStatMod(stat, value)).toEqual({
            type: actionTypes.UPDATE_STAT_MOD,
            stat,
            value
        });
    });
    test('Should create an action to update stat temp mod', () => {
        const value = Math.random();
        const stat = 'test';
        expect(updateStatTempMod(stat, value)).toEqual({
            type: actionTypes.UPDATE_STAT_TEMP_MOD,
            stat,
            value
        });
    });
    test('Should create an action to reset stats', () => {
        expect(resetStats()).toEqual({
            type: actionTypes.RESET_STATS
        });
    });
    test('Should create an action to update stat temp score', () => {
        const value = Math.random();
        const stat = 'test';
        expect(updateStatTempScore(stat, value)).toEqual({
            type: actionTypes.UPDATE_STAT_TEMP_SCORE,
            stat,
            value
        });
    });
});
