import {
    actionTypes,
    updateAge,
    updateAlignment,
    updateClass,
    updateDeity,
    updateEyes,
    updateGender,
    updateHair,
    updateHeight,
    updateLevel,
    updateName,
    updatePlayer,
    updateRace,
    updateSize,
    updateWeight
} from './basic.actions';

describe('Action tests', () => {
    test('Should create an action to update name', () => {
        const value = Math.random();
        expect(updateName(value)).toEqual({
            type: actionTypes.UPDATE_NAME,
            value
        });
    });
    test('Should create an action to update player', () => {
        const value = Math.random();
        expect(updatePlayer(value)).toEqual({type: actionTypes.UPDATE_PLAYER, value});
    });
    test('Should create an action to update class', () => {
        const value = Math.random();
        expect(updateClass(value)).toEqual({type: actionTypes.UPDATE_CLASS, value});
    });
    test('Should create an action to update race', () => {
        const value = Math.random();
        expect(updateRace(value)).toEqual({type: actionTypes.UPDATE_RACE, value});
    });
    test('Should create an action to update alignment', () => {
        const value = Math.random();
        expect(updateAlignment(value)).toEqual({
            type: actionTypes.UPDATE_ALIGNMENT,
            value
        });
    });
    test('Should create an action to update deity', () => {
        const value = Math.random();
        expect(updateDeity(value)).toEqual({type: actionTypes.UPDATE_DEITY, value});
    });
    test('Should create an action to update level', () => {
        const value = Math.random();
        expect(updateLevel(value)).toEqual({type: actionTypes.UPDATE_LEVEL, value});
    });
    test('Should create an action to update size', () => {
        const value = Math.random();
        expect(updateSize(value)).toEqual({type: actionTypes.UPDATE_SIZE, value});
    });
    test('Should create an action to update age', () => {
        const value = Math.random();
        expect(updateAge(value)).toEqual({type: actionTypes.UPDATE_AGE, value});
    });
    test('Should create an action to update gender', () => {
        const value = Math.random();
        expect(updateGender(value)).toEqual({type: actionTypes.UPDATE_GENDER, value});
    });
    test('Should create an action to update height', () => {
        const value = Math.random();
        expect(updateHeight(value)).toEqual({type: actionTypes.UPDATE_HEIGHT, value});
    });
    test('Should create an action to update weight', () => {
        const value = Math.random();
        expect(updateWeight(value)).toEqual({type: actionTypes.UPDATE_WEIGHT, value});
    });
    test('Should create an action to update eyes', () => {
        const value = Math.random();
        expect(updateEyes(value)).toEqual({type: actionTypes.UPDATE_EYES, value});
    });
    test('Should create an action to update hair', () => {
        const value = Math.random();
        expect(updateHair(value)).toEqual({type: actionTypes.UPDATE_HAIR, value});
    });
});
