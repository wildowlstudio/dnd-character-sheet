import {actionTypes, resetBaseAttack, updateBaseAttack} from './baseAttack.actions';

describe('Action tests', () => {
    test('Should create an action to UPDATE_BASE_ATTACK', () => {
        expect(updateBaseAttack('test')).toEqual({
            type: actionTypes.UPDATE_BASE_ATTACK,
            value: 'test'
        });
    });

    test('Should create an action to RESET_BASE_ATTACK', () => {
        expect(resetBaseAttack()).toEqual({
            type: actionTypes.RESET_BASE_ATTACK
        });
    });
});
