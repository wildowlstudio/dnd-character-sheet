import {actionTypes, resetSpeed, updateSpeed} from './speed.actions';

describe('Action tests', () => {
    test('Should create an action to UPDATE_SPEED', () => {
        expect(updateSpeed('test')).toEqual({
            type: actionTypes.UPDATE_SPEED,
            value: 'test'
        });
    });

    test('Should create an action to RESET_SPEED', () => {
        expect(resetSpeed()).toEqual({
            type: actionTypes.RESET_SPEED
        });
    });
});
