export const actionTypes = {
    UPDATE_KNOWN_SPELL: 'UPDATE_KNOWN_SPELL',
    RESET_KNOWN_SPELLS: 'RESET_KNOWN_SPELLS',
    ADD_KNOWN_SPELL: 'ADD_KNOWN_SPELL',
    MOVE_KNOWN_SPELL: 'MOVE_KNOWN_SPELL',
    CLEAN_KNOWN_SPELLS: 'CLEAN_KNOWN_SPELLS',
    DELETE_KNOWN_SPELL: 'DELETE_KNOWN_SPELL'
};

export function updateKnownSpell(id, value) {
    return {type: actionTypes.UPDATE_KNOWN_SPELL, id, value};
}

export function resetKnownSpells() {
    return {type: actionTypes.RESET_KNOWN_SPELLS};
}

export function addKnownSpell() {
    return {type: actionTypes.ADD_KNOWN_SPELL};
}

export function moveKnownSpell(id, below, targetId) {
    return {type: actionTypes.MOVE_KNOWN_SPELL, id, below, targetId};
}

export function cleanKnownSpells() {
    return {type: actionTypes.CLEAN_KNOWN_SPELLS};
}

export function deleteKnownSpell(id) {
    return {type: actionTypes.DELETE_KNOWN_SPELL, id};
}
