import {
    actionTypes,
    resetSpells,
    updateSpellBonus,
    updateSpellKnown,
    updateSpellPerDay,
    updateSpellSaveDc
} from './spell.actions';

describe('Action tests', () => {
    test('Should create an action to RESET_SPELLS', () => {
        expect(resetSpells()).toEqual({
            type: actionTypes.RESET_SPELLS
        });
    });

    test('Should create an action to UPDATE_SPELL_KNOWN', () => {
        expect(updateSpellKnown(0, 'test')).toEqual({
            type: actionTypes.UPDATE_SPELL_KNOWN,
            id: 0,
            value: 'test'
        });
    });

    test('Should create an action to UPDATE_SPELL_PER_DAY', () => {
        expect(updateSpellPerDay(0, 'test')).toEqual({
            type: actionTypes.UPDATE_SPELL_PER_DAY,
            id: 0,
            value: 'test'
        });
    });

    test('Should create an action to UPDATE_SPELL_BONUS', () => {
        expect(updateSpellBonus(0, 'test')).toEqual({
            type: actionTypes.UPDATE_SPELL_BONUS,
            id: 0,
            value: 'test'
        });
    });

    test('Should create an action to UPDATE_SPELL_SAVE_DC', () => {
        expect(updateSpellSaveDc(0, 'test')).toEqual({
            type: actionTypes.UPDATE_SPELL_SAVE_DC,
            id: 0,
            value: 'test'
        });
    });
});
