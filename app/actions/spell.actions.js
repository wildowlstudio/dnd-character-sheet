export const actionTypes = {
    RESET_SPELLS: 'RESET_SPELLS',
    UPDATE_SPELL_KNOWN: 'UPDATE_SPELL_KNOWN',
    UPDATE_SPELL_PER_DAY: 'UPDATE_SPELL_PER_DAY',
    UPDATE_SPELL_BONUS: 'UPDATE_SPELL_BONUS',
    UPDATE_SPELL_SAVE_DC: 'UPDATE_SPELL_SAVE_DC'
};

export function resetSpells() {
    return {type: actionTypes.RESET_SPELLS};
}

export function updateSpellKnown(id, value) {
    return {type: actionTypes.UPDATE_SPELL_KNOWN, id, value};
}

export function updateSpellPerDay(id, value) {
    return {type: actionTypes.UPDATE_SPELL_PER_DAY, id, value};
}

export function updateSpellBonus(id, value) {
    return {type: actionTypes.UPDATE_SPELL_BONUS, id, value};
}

export function updateSpellSaveDc(id, value) {
    return {type: actionTypes.UPDATE_SPELL_SAVE_DC, id, value};
}
