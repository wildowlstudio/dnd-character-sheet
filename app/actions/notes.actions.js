export const actionTypes = {
    RESET_NOTES: 'RESET_NOTES',
    UPDATE_NOTES: 'UPDATE_NOTES'
};

export function resetNotes() {
    return {type: actionTypes.RESET_NOTES};
}

export function updateNotes(value) {
    return {type: actionTypes.UPDATE_NOTES, value};
}
