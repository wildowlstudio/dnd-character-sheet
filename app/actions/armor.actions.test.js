import {
    actionTypes,
    addArmor,
    deleteArmor,
    resetArmor,
    updateArmorBonus,
    updateArmorMaxDex,
    updateArmorMaxSpeed,
    updateArmorName,
    updateArmorNotes,
    updateArmorPenalty,
    updateArmorSpellFailure,
    updateArmorType,
    updateArmorWeight
} from './armor.actions';

describe('Action tests', () => {
    test('Should create an action to update armor name', () => {
        expect(updateArmorName(0, 'test')).toEqual({
            type: actionTypes.UPDATE_ARMOR_NAME,
            id: 0,
            value: 'test'
        });
    });
    test('Should create an action to update armor bonus', () => {
        expect(updateArmorBonus(0, 'test')).toEqual({
            type: actionTypes.UPDATE_ARMOR_BONUS,
            id: 0,
            value: 'test'
        });
    });
    test('Should create an action to update armor max dex', () => {
        expect(updateArmorMaxDex(0, 'test')).toEqual({
            type: actionTypes.UPDATE_ARMOR_MAX_DEX,
            id: 0,
            value: 'test'
        });
    });
    test('Should create an action to update armor penalty', () => {
        expect(updateArmorPenalty(0, 'test')).toEqual({
            type: actionTypes.UPDATE_ARMOR_PENALTY,
            id: 0,
            value: 'test'
        });
    });
    test('Should create an action to update armor max speed', () => {
        expect(updateArmorMaxSpeed(0, 'test')).toEqual({
            type: actionTypes.UPDATE_ARMOR_MAX_SPEED,
            id: 0,
            value: 'test'
        });
    });
    test('Should create an action to update armor weight', () => {
        expect(updateArmorWeight(0, 'test')).toEqual({
            type: actionTypes.UPDATE_ARMOR_WEIGHT,
            id: 0,
            value: 'test'
        });
    });
    test('Should create an action to update armor type', () => {
        expect(updateArmorType(0, 'test')).toEqual({
            type: actionTypes.UPDATE_ARMOR_TYPE,
            id: 0,
            value: 'test'
        });
    });
    test('Should create an action to update armor spell failure', () => {
        expect(updateArmorSpellFailure(0, 'test')).toEqual({
            type: actionTypes.UPDATE_ARMOR_SPELL_FAILURE,
            id: 0,
            value: 'test'
        });
    });
    test('Should create an action to update armor notes', () => {
        expect(updateArmorNotes(0, 'test')).toEqual({
            type: actionTypes.UPDATE_ARMOR_NOTES,
            id: 0,
            value: 'test'
        });
    });
    test('Should create an action to add armor', () => {
        expect(addArmor()).toEqual({
            type: actionTypes.ADD_ARMOR
        });
    });
    test('Should create an action to reset armor', () => {
        expect(resetArmor(0)).toEqual({
            type: actionTypes.RESET_ARMOR,
            id: 0
        });
    });
    test('Should create an action to delete armor', () => {
        expect(deleteArmor(0)).toEqual({
            type: actionTypes.DELETE_ARMOR,
            id: 0
        });
    });
});
