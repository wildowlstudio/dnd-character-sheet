export const actionTypes = {
    UPDATE_FORTITUDE_BASE: 'UPDATE_FORTITUDE_BASE',
    UPDATE_FORTITUDE_ABILITY_MOD: 'UPDATE_FORTITUDE_ABILITY_MOD',
    UPDATE_FORTITUDE_MAGIC_MOD: 'UPDATE_FORTITUDE_MAGIC_MOD',
    UPDATE_FORTITUDE_MISC_MOD: 'UPDATE_FORTITUDE_MISC_MOD',
    UPDATE_FORTITUDE_TEMP_MOD: 'UPDATE_FORTITUDE_TEMP_MOD',
    UPDATE_FORTITUDE_OTHER_MOD: 'UPDATE_FORTITUDE_OTHER_MOD',
    UPDATE_REFLEX_BASE: 'UPDATE_REFLEX_BASE',
    UPDATE_REFLEX_ABILITY_MOD: 'UPDATE_REFLEX_ABILITY_MOD',
    UPDATE_REFLEX_MAGIC_MOD: 'UPDATE_REFLEX_MAGIC_MOD',
    UPDATE_REFLEX_MISC_MOD: 'UPDATE_REFLEX_MISC_MOD',
    UPDATE_REFLEX_TEMP_MOD: 'UPDATE_REFLEX_TEMP_MOD',
    UPDATE_REFLEX_OTHER_MOD: 'UPDATE_REFLEX_OTHER_MOD',
    UPDATE_WILLPOWER_BASE: 'UPDATE_WILLPOWER_BASE',
    UPDATE_WILLPOWER_ABILITY_MOD: 'UPDATE_WILLPOWER_ABILITY_MOD',
    UPDATE_WILLPOWER_MAGIC_MOD: 'UPDATE_WILLPOWER_MAGIC_MOD',
    UPDATE_WILLPOWER_MISC_MOD: 'UPDATE_WILLPOWER_MISC_MOD',
    UPDATE_WILLPOWER_TEMP_MOD: 'UPDATE_WILLPOWER_TEMP_MOD',
    UPDATE_WILLPOWER_OTHER_MOD: 'UPDATE_WILLPOWER_OTHER_MOD',
    RESET_SAVING_THROWS: 'RESET_SAVING_THROWS'
};

export function updateFortitudeBase(value) {
    return {type: actionTypes.UPDATE_FORTITUDE_BASE, value};
}

export function updateFortitudeAbilityMod(value) {
    return {type: actionTypes.UPDATE_FORTITUDE_ABILITY_MOD, value};
}

export function updateFortitudeMagicMod(value) {
    return {type: actionTypes.UPDATE_FORTITUDE_MAGIC_MOD, value};
}

export function updateFortitudeMiscMod(value) {
    return {type: actionTypes.UPDATE_FORTITUDE_MISC_MOD, value};
}

export function updateFortitudeTempMod(value) {
    return {type: actionTypes.UPDATE_FORTITUDE_TEMP_MOD, value};
}

export function updateFortitudeOtherMod(value) {
    return {type: actionTypes.UPDATE_FORTITUDE_OTHER_MOD, value};
}

export function updateReflexBase(value) {
    return {type: actionTypes.UPDATE_REFLEX_BASE, value};
}

export function updateReflexAbilityMod(value) {
    return {type: actionTypes.UPDATE_REFLEX_ABILITY_MOD, value};
}

export function updateReflexMagicMod(value) {
    return {type: actionTypes.UPDATE_REFLEX_MAGIC_MOD, value};
}

export function updateReflexMiscMod(value) {
    return {type: actionTypes.UPDATE_REFLEX_MISC_MOD, value};
}

export function updateReflexTempMod(value) {
    return {type: actionTypes.UPDATE_REFLEX_TEMP_MOD, value};
}

export function updateReflexOtherMod(value) {
    return {type: actionTypes.UPDATE_REFLEX_OTHER_MOD, value};
}

export function updateWillpowerBase(value) {
    return {type: actionTypes.UPDATE_WILLPOWER_BASE, value};
}

export function updateWillpowerAbilityMod(value) {
    return {type: actionTypes.UPDATE_WILLPOWER_ABILITY_MOD, value};
}

export function updateWillpowerMagicMod(value) {
    return {type: actionTypes.UPDATE_WILLPOWER_MAGIC_MOD, value};
}

export function updateWillpowerMiscMod(value) {
    return {type: actionTypes.UPDATE_WILLPOWER_MISC_MOD, value};
}

export function updateWillpowerTempMod(value) {
    return {type: actionTypes.UPDATE_WILLPOWER_TEMP_MOD, value};
}

export function updateWillpowerOtherMod(value) {
    return {type: actionTypes.UPDATE_WILLPOWER_OTHER_MOD, value};
}

export function resetSavingThrows() {
    return {type: actionTypes.RESET_SAVING_THROWS};
}
