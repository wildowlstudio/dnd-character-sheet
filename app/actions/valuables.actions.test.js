import {
    actionTypes,
    resetValuables,
    updateValuablesBronzeOption,
    updateValuablesCopperOption,
    updateValuablesGoldOption,
    updateValuablesIronOption,
    updateValuablesMoney,
    updateValuablesPlatinumOption,
    updateValuablesSilverOption
} from './valuables.actions';

describe('Action tests', () => {
    test('Should create an action to UPDATE_VALUABLES_MONEY', () => {
        expect(updateValuablesMoney('test')).toEqual({
            type: actionTypes.UPDATE_VALUABLES_MONEY,
            value: 'test'
        });
    });

    test('Should create an action to UPDATE_VALUABLES_BRONZE_OPTION', () => {
        expect(updateValuablesBronzeOption('test')).toEqual({
            type: actionTypes.UPDATE_VALUABLES_BRONZE_OPTION,
            value: 'test'
        });
    });

    test('Should create an action to UPDATE_VALUABLES_IRON_OPTION', () => {
        expect(updateValuablesIronOption('test')).toEqual({
            type: actionTypes.UPDATE_VALUABLES_IRON_OPTION,
            value: 'test'
        });
    });

    test('Should create an action to UPDATE_VALUABLES_COPPER_OPTION', () => {
        expect(updateValuablesCopperOption('test')).toEqual({
            type: actionTypes.UPDATE_VALUABLES_COPPER_OPTION,
            value: 'test'
        });
    });

    test('Should create an action to UPDATE_VALUABLES_SILVER_OPTION', () => {
        expect(updateValuablesSilverOption('test')).toEqual({
            type: actionTypes.UPDATE_VALUABLES_SILVER_OPTION,
            value: 'test'
        });
    });

    test('Should create an action to UPDATE_VALUABLES_GOLD_OPTION', () => {
        expect(updateValuablesGoldOption('test')).toEqual({
            type: actionTypes.UPDATE_VALUABLES_GOLD_OPTION,
            value: 'test'
        });
    });

    test('Should create an action to UPDATE_VALUABLES_PLATINUM_OPTION', () => {
        expect(updateValuablesPlatinumOption('test')).toEqual({
            type: actionTypes.UPDATE_VALUABLES_PLATINUM_OPTION,
            value: 'test'
        });
    });

    test('Should create an action to RESET_VALUABLES', () => {
        expect(resetValuables()).toEqual({
            type: actionTypes.RESET_VALUABLES
        });
    });
});
