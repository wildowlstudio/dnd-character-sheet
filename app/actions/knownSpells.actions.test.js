import {
    actionTypes,
    addKnownSpell,
    cleanKnownSpells,
    deleteKnownSpell,
    moveKnownSpell,
    resetKnownSpells,
    updateKnownSpell
} from './knownSpell.actions';

describe('Action tests', () => {
    test('Should create an action to UPDATE_KNOWN_SPELL', () => {
        expect(updateKnownSpell()).toEqual({
            type: actionTypes.UPDATE_KNOWN_SPELL
        });
    });

    test('Should create an action to RESET_KNOWN_SPELLS', () => {
        expect(resetKnownSpells()).toEqual({
            type: actionTypes.RESET_KNOWN_SPELLS
        });
    });

    test('Should create an action to ADD_KNOWN_SPELL', () => {
        expect(addKnownSpell()).toEqual({
            type: actionTypes.ADD_KNOWN_SPELL
        });
    });

    test('Should create an action to MOVE_KNOWN_SPELL', () => {
        expect(moveKnownSpell()).toEqual({
            type: actionTypes.MOVE_KNOWN_SPELL
        });
    });

    test('Should create an action to CLEAN_KNOWN_SPELLS', () => {
        expect(cleanKnownSpells()).toEqual({
            type: actionTypes.CLEAN_KNOWN_SPELLS
        });
    });

    test('Should create an action to DELETE_KNOWN_SPELL', () => {
        expect(deleteKnownSpell()).toEqual({
            type: actionTypes.DELETE_KNOWN_SPELL
        });
    });
});
