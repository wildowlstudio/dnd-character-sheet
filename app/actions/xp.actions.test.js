import {actionTypes, resetXp, updateXp} from './xp.actions';

describe('Action tests', () => {
    test('Should create an action to UPDATE_XP', () => {
        expect(updateXp('test')).toEqual({
            type: actionTypes.UPDATE_XP,
            value: 'test'
        });
    });

    test('Should create an action to RESET_XP', () => {
        expect(resetXp()).toEqual({
            type: actionTypes.RESET_XP
        });
    });
});
