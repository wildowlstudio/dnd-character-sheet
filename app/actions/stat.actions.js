export const actionTypes = {
    UPDATE_STAT_MOD: 'UPDATE_STAT_MOD',
    UPDATE_STAT_TEMP_MOD: 'UPDATE_STAT_TEMP_MOD',
    UPDATE_STAT_TEMP_SCORE: 'UPDATE_STAT_TEMP_SCORE',
    RESET_STATS: 'RESET_STATS'
};

export function updateStatMod(stat, value) {
    return {type: actionTypes.UPDATE_STAT_MOD, stat, value};
}

export function updateStatTempMod(stat, value) {
    return {type: actionTypes.UPDATE_STAT_TEMP_MOD, stat, value};
}

export function updateStatTempScore(stat, value) {
    return {type: actionTypes.UPDATE_STAT_TEMP_SCORE, stat, value};
}

export function resetStats() {
    return {type: actionTypes.RESET_STATS};
}
