import {resetBaseAttack, updateBaseAttack} from '../actions/baseAttack.actions';

import BaseAttack from '../views/baseAttack.view';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        baseAttack: state.sheet.baseAttack
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        resetBaseAttack: () => dispatch(resetBaseAttack()),
        updateBaseAttack: (value) => dispatch(updateBaseAttack(value))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(BaseAttack);
