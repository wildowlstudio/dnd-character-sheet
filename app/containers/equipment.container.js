import {
    addEquipmentItem,
    deleteEquipmentItem,
    deleteEquipmentItems,
    moveEquipmentItem,
    resetEquipment,
    sortEquipment,
    updateEquipmentItemCapacity,
    updateEquipmentItemName,
    updateEquipmentItemValue,
    updateEquipmentItemWeight,
    updateEquipmentOptionCapacity,
    updateEquipmentOptionSum,
    updateEquipmentOptionValue,
    updateEquipmentOptionWeight
} from '../actions/equipment.actions';

import Equipment from '../views/equipment.view';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        equipment: state.sheet.equipment,
        options: state.sheet.options.equipment
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        addEquipmentItem: () => dispatch(addEquipmentItem()),
        deleteEquipmentItem: (id, parentId) =>
            dispatch(deleteEquipmentItem(id, parentId)),
        deleteEquipmentItems: () => dispatch(deleteEquipmentItems()),
        resetEquipment: () => dispatch(resetEquipment()),
        sortEquipment: (value, desc) => dispatch(sortEquipment(value, desc)),
        updateEquipmentItemCapacity: (id, value, parentId) =>
            dispatch(updateEquipmentItemCapacity(id, value, parentId)),
        updateEquipmentItemName: (id, value, parentId) =>
            dispatch(updateEquipmentItemName(id, value, parentId)),
        updateEquipmentItemValue: (id, value, parentId) =>
            dispatch(updateEquipmentItemValue(id, value, parentId)),
        updateEquipmentItemWeight: (id, value, parentId) =>
            dispatch(updateEquipmentItemWeight(id, value, parentId)),
        updateEquipmentOptionCapacity: (value) =>
            dispatch(updateEquipmentOptionCapacity(value)),
        updateEquipmentOptionSum: (value) => dispatch(updateEquipmentOptionSum(value)),
        updateEquipmentOptionValue: (value) =>
            dispatch(updateEquipmentOptionValue(value)),
        updateEquipmentOptionWeight: (value) =>
            dispatch(updateEquipmentOptionWeight(value)),
        moveEquipmentItem: (id, parentId, below, targetId, targetParentId) =>
            dispatch(moveEquipmentItem(id, parentId, below, targetId, targetParentId))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Equipment);
