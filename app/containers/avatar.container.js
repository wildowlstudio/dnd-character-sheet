import {resetAvatar, updateAvatar} from '../actions/avatar.actions';

import Avatar from '../views/avatar.view';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        avatar: state.sheet.avatar
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        resetAvatar: () => dispatch(resetAvatar()),
        updateAvatar: (value) => dispatch(updateAvatar(value))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Avatar);
