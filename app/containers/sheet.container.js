import {
    addArmor,
    deleteArmor,
    resetArmor,
    updateArmorBonus,
    updateArmorMaxDex,
    updateArmorMaxSpeed,
    updateArmorName,
    updateArmorNotes,
    updateArmorPenalty,
    updateArmorSpellFailure,
    updateArmorType,
    updateArmorWeight
} from '../actions/armor.actions';
import {
    addWeapon,
    deleteWeapon,
    resetWeapon,
    updateWeaponAttackBonus,
    updateWeaponCritical,
    updateWeaponDamage,
    updateWeaponKind,
    updateWeaponName,
    updateWeaponNotes,
    updateWeaponRange,
    updateWeaponSize,
    updateWeaponWeight
} from '../actions/weapon.actions';

import Sheet from '../views/sheet.view';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        armors: state.sheet.armors,
        weapons: state.sheet.weapons
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        addArmor: () => dispatch(addArmor()),
        addWeapon: () => dispatch(addWeapon()),
        deleteArmor: (id) => dispatch(deleteArmor(id)),
        deleteWeapon: (id) => dispatch(deleteWeapon(id)),
        resetArmor: (id) => dispatch(resetArmor(id)),
        resetWeapon: (id) => dispatch(resetWeapon(id)),
        updateArmorBonus: (id, value) => dispatch(updateArmorBonus(id, value)),
        updateArmorMaxDex: (id, value) => dispatch(updateArmorMaxDex(id, value)),
        updateArmorMaxSpeed: (id, value) => dispatch(updateArmorMaxSpeed(id, value)),
        updateArmorName: (id, value) => dispatch(updateArmorName(id, value)),
        updateArmorNotes: (id, value) => dispatch(updateArmorNotes(id, value)),
        updateArmorPenalty: (id, value) => dispatch(updateArmorPenalty(id, value)),
        updateArmorSpellFailure: (id, value) =>
            dispatch(updateArmorSpellFailure(id, value)),
        updateArmorType: (id, value) => dispatch(updateArmorType(id, value)),
        updateArmorWeight: (id, value) => dispatch(updateArmorWeight(id, value)),
        updateWeaponAttackBonus: (id, value) =>
            dispatch(updateWeaponAttackBonus(id, value)),
        updateWeaponCritical: (id, value) => dispatch(updateWeaponCritical(id, value)),
        updateWeaponDamage: (id, value) => dispatch(updateWeaponDamage(id, value)),
        updateWeaponKind: (id, value) => dispatch(updateWeaponKind(id, value)),
        updateWeaponName: (id, value) => dispatch(updateWeaponName(id, value)),
        updateWeaponNotes: (id, value) => dispatch(updateWeaponNotes(id, value)),
        updateWeaponRange: (id, value) => dispatch(updateWeaponRange(id, value)),
        updateWeaponSize: (id, value) => dispatch(updateWeaponSize(id, value)),
        updateWeaponWeight: (id, value) => dispatch(updateWeaponWeight(id, value))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Sheet);
