import {
    resetSavingThrows,
    updateFortitudeAbilityMod,
    updateFortitudeBase,
    updateFortitudeMagicMod,
    updateFortitudeMiscMod,
    updateFortitudeOtherMod,
    updateFortitudeTempMod,
    updateReflexAbilityMod,
    updateReflexBase,
    updateReflexMagicMod,
    updateReflexMiscMod,
    updateReflexOtherMod,
    updateReflexTempMod,
    updateWillpowerAbilityMod,
    updateWillpowerBase,
    updateWillpowerMagicMod,
    updateWillpowerMiscMod,
    updateWillpowerOtherMod,
    updateWillpowerTempMod
} from '../actions/savingThrow.actions';

import SavingThrows from '../views/savingThrows.view';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        fortitude: state.sheet.fortitude,
        reflex: state.sheet.reflex,
        willpower: state.sheet.willpower
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        resetSavingThrows: () => dispatch(resetSavingThrows()),
        updateFortitudeAbilityMod: (value) => dispatch(updateFortitudeAbilityMod(value)),
        updateFortitudeBase: (value) => dispatch(updateFortitudeBase(value)),
        updateFortitudeMagicMod: (value) => dispatch(updateFortitudeMagicMod(value)),
        updateFortitudeMiscMod: (value) => dispatch(updateFortitudeMiscMod(value)),
        updateFortitudeOtherMod: (value) => dispatch(updateFortitudeOtherMod(value)),
        updateFortitudeTempMod: (value) => dispatch(updateFortitudeTempMod(value)),
        updateReflexAbilityMod: (value) => dispatch(updateReflexAbilityMod(value)),
        updateReflexBase: (value) => dispatch(updateReflexBase(value)),
        updateReflexMagicMod: (value) => dispatch(updateReflexMagicMod(value)),
        updateReflexMiscMod: (value) => dispatch(updateReflexMiscMod(value)),
        updateReflexOtherMod: (value) => dispatch(updateReflexOtherMod(value)),
        updateReflexTempMod: (value) => dispatch(updateReflexTempMod(value)),
        updateWillpowerAbilityMod: (value) => dispatch(updateWillpowerAbilityMod(value)),
        updateWillpowerBase: (value) => dispatch(updateWillpowerBase(value)),
        updateWillpowerMagicMod: (value) => dispatch(updateWillpowerMagicMod(value)),
        updateWillpowerMiscMod: (value) => dispatch(updateWillpowerMiscMod(value)),
        updateWillpowerOtherMod: (value) => dispatch(updateWillpowerOtherMod(value)),
        updateWillpowerTempMod: (value) => dispatch(updateWillpowerTempMod(value))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SavingThrows);
