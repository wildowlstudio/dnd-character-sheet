import {
    addKnownSpell,
    cleanKnownSpells,
    deleteKnownSpell,
    moveKnownSpell,
    resetKnownSpells,
    updateKnownSpell
} from '../actions/knownSpell.actions';

import KnownSpells from '../views/knownSpells.view';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        spells: state.sheet.knownSpells
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        addKnownSpell: () => dispatch(addKnownSpell()),
        cleanKnownSpells: () => dispatch(cleanKnownSpells()),
        deleteKnownSpell: (id) => dispatch(deleteKnownSpell(id)),
        moveKnownSpell: (id, below, targetId) =>
            dispatch(moveKnownSpell(id, below, targetId)),
        resetKnownSpells: () => dispatch(resetKnownSpells()),
        updateKnownSpell: (id, value) => dispatch(updateKnownSpell(id, value))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(KnownSpells);
