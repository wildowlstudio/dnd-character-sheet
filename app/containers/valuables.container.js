import {
    resetValuables,
    updateValuablesBronzeOption,
    updateValuablesCopperOption,
    updateValuablesGoldOption,
    updateValuablesIronOption,
    updateValuablesMoney,
    updateValuablesPlatinumOption,
    updateValuablesSilverOption
} from '../actions/valuables.actions';

import Valuables from '../views/valuables.view';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        valuables: state.sheet.valuables
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        resetValuables: (value) => dispatch(resetValuables(value)),
        updateValuablesBronzeOption: (value) =>
            dispatch(updateValuablesBronzeOption(value)),
        updateValuablesCopperOption: (value) =>
            dispatch(updateValuablesCopperOption(value)),
        updateValuablesGoldOption: (value) => dispatch(updateValuablesGoldOption(value)),
        updateValuablesIronOption: (value) => dispatch(updateValuablesIronOption(value)),
        updateValuablesMoney: (value) => dispatch(updateValuablesMoney(value)),
        updateValuablesPlatinumOption: (value) =>
            dispatch(updateValuablesPlatinumOption(value)),
        updateValuablesSilverOption: (value) =>
            dispatch(updateValuablesSilverOption(value))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Valuables);
