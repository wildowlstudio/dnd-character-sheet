import {
    resetStats,
    updateStatMod,
    updateStatTempMod,
    updateStatTempScore
} from '../actions/stat.actions';

import Stats from '../views/stats.view';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        stats: state.sheet.stats
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        resetStats: () => dispatch(resetStats()),
        updateStatMod: (stat, value) => dispatch(updateStatMod(stat, value)),
        updateStatTempMod: (stat, value) => dispatch(updateStatTempMod(stat, value)),
        updateStatTempScore: (stat, value) => dispatch(updateStatTempScore(stat, value))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Stats);
