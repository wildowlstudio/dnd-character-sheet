import {resetNotes, updateNotes} from '../actions/notes.actions';

import Notes from '../views/notes.view';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        notes: state.sheet.notes
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        resetNotes: () => dispatch(resetNotes()),
        updateNotes: (value) => dispatch(updateNotes(value))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Notes);
