import {resetSpeed, updateSpeed} from '../actions/speed.actions';

import Speed from '../views/speed.view';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        speed: state.sheet.speed
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        resetSpeed: () => dispatch(resetSpeed()),
        updateSpeed: (value) => dispatch(updateSpeed(value))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Speed);
