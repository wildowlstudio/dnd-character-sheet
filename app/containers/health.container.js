import {
    resetHealth,
    updateAcArmor,
    updateAcDex,
    updateAcMisc,
    updateAcShield,
    updateAcSize,
    updateCurrentHp,
    updateHp,
    updateSubdualDamage
} from '../actions/health.actions';

import Health from '../views/health.view';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        hp: state.sheet.hp,
        ac: state.sheet.ac
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateHp: (value) => dispatch(updateHp(value)),
        updateCurrentHp: (value) => dispatch(updateCurrentHp(value)),
        updateSubdualDamage: (value) => dispatch(updateSubdualDamage(value)),
        updateAcArmor: (value) => dispatch(updateAcArmor(value)),
        updateAcShield: (value) => dispatch(updateAcShield(value)),
        updateAcDex: (value) => dispatch(updateAcDex(value)),
        updateAcSize: (value) => dispatch(updateAcSize(value)),
        updateAcMisc: (value) => dispatch(updateAcMisc(value)),
        resetHealth: (value) => dispatch(resetHealth())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Health);
