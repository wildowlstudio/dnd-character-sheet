import {
    resetSpells,
    updateSpellBonus,
    updateSpellKnown,
    updateSpellPerDay,
    updateSpellSaveDc
} from '../actions/spell.actions';

import Spells from '../views/spells.view';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        spells: state.sheet.spells
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        resetSpells: () => dispatch(resetSpells()),
        updateSpellBonus: (id, value) => dispatch(updateSpellBonus(id, value)),
        updateSpellKnown: (id, value) => dispatch(updateSpellKnown(id, value)),
        updateSpellPerDay: (id, value) => dispatch(updateSpellPerDay(id, value)),
        updateSpellSaveDc: (id, value) => dispatch(updateSpellSaveDc(id, value))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Spells);
