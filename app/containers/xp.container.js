import {resetXp, updateXp} from '../actions/xp.actions';

import Xp from '../views/xp.view';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        xp: state.sheet.xp
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        resetXp: () => dispatch(resetXp()),
        updateXp: (value) => dispatch(updateXp(value))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Xp);
