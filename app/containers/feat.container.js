import {
    addFeat,
    cleanFeats,
    deleteFeat,
    moveFeat,
    resetFeats,
    updateFeat
} from '../actions/feat.actions';

import Feat from '../views/feat.view';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        feats: state.sheet.feats
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        addFeat: () => dispatch(addFeat()),
        cleanFeats: () => dispatch(cleanFeats()),
        deleteFeat: (id) => dispatch(deleteFeat(id)),
        moveFeat: (id, below, targetId) => dispatch(moveFeat(id, below, targetId)),
        resetFeats: () => dispatch(resetFeats()),
        updateFeat: (id, value) => dispatch(updateFeat(id, value))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Feat);
