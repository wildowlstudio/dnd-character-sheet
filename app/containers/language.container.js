import {
    addLanguage,
    cleanLanguages,
    deleteLanguage,
    moveLanguage,
    resetLanguages,
    updateLanguage
} from '../actions/language.actions';

import Language from '../views/language.view';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        languages: state.sheet.languages
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        addLanguage: () => dispatch(addLanguage()),
        cleanLanguages: () => dispatch(cleanLanguages()),
        deleteLanguage: (id) => dispatch(deleteLanguage(id)),
        moveLanguage: (id, below, targetId) =>
            dispatch(moveLanguage(id, below, targetId)),
        resetLanguages: () => dispatch(resetLanguages()),
        updateLanguage: (id, value) => dispatch(updateLanguage(id, value))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Language);
