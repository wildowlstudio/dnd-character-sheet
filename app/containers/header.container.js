import {
    updateAge,
    updateAlignment,
    updateClass,
    updateDeity,
    updateEyes,
    updateGender,
    updateHair,
    updateHeight,
    updateLevel,
    updateName,
    updatePlayer,
    updateRace,
    updateSize,
    updateWeight
} from '../actions/basic.actions';

import Header from '../views/header.view';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        basic: state.sheet.basic
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateAge: (value) => dispatch(updateAge(value)),
        updateAlignment: (value) => dispatch(updateAlignment(value)),
        updateClass: (value) => dispatch(updateClass(value)),
        updateDeity: (value) => dispatch(updateDeity(value)),
        updateEyes: (value) => dispatch(updateEyes(value)),
        updateGender: (value) => dispatch(updateGender(value)),
        updateHair: (value) => dispatch(updateHair(value)),
        updateHeight: (value) => dispatch(updateHeight(value)),
        updateLevel: (value) => dispatch(updateLevel(value)),
        updateName: (value) => dispatch(updateName(value)),
        updatePlayer: (value) => dispatch(updatePlayer(value)),
        updateRace: (value) => dispatch(updateRace(value)),
        updateSize: (value) => dispatch(updateSize(value)),
        updateWeight: (value) => dispatch(updateWeight(value))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
