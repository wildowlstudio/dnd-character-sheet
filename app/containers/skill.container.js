import {
    addSkill,
    deleteSkill,
    deleteSkills,
    resetSkills,
    sortSkills,
    updateSkillAbilityMod,
    updateSkillAttribute,
    updateSkillClass,
    updateSkillMaxClassRank,
    updateSkillMaxCrossClassRank,
    updateSkillMiscMod,
    updateSkillName,
    updateSkillSkillRank
} from '../actions/skill.actions';

import Skill from '../views/skill.view';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        skill: state.sheet.skill,
        skills: state.sheet.skills
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        addSkill: () => dispatch(addSkill()),
        deleteSkill: (id) => dispatch(deleteSkill(id)),
        deleteSkills: () => dispatch(deleteSkills()),
        resetSkills: () => dispatch(resetSkills()),
        sortSkills: (value, desc) => dispatch(sortSkills(value, desc)),
        updateSkillAbilityMod: (id, value) => dispatch(updateSkillAbilityMod(id, value)),
        updateSkillAttribute: (id, value) => dispatch(updateSkillAttribute(id, value)),
        updateSkillClass: (id, value) => dispatch(updateSkillClass(id, value)),
        updateSkillMaxClassRank: (value) => dispatch(updateSkillMaxClassRank(value)),
        updateSkillMaxCrossClassRank: (value) =>
            dispatch(updateSkillMaxCrossClassRank(value)),
        updateSkillMiscMod: (id, value) => dispatch(updateSkillMiscMod(id, value)),
        updateSkillName: (id, value) => dispatch(updateSkillName(id, value)),
        updateSkillSkillRank: (id, value) => dispatch(updateSkillSkillRank(id, value))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Skill);
