import {
    resetInitiative,
    updateInitiativeDexMod,
    updateInitiativeMiscMod
} from '../actions/initiative.actions';

import Initiative from '../views/initiative.view';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        initiative: state.sheet.initiative
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        resetInitiative: () => dispatch(resetInitiative()),
        updateInitiativeDexMod: (value) => dispatch(updateInitiativeDexMod(value)),
        updateInitiativeMiscMod: (value) => dispatch(updateInitiativeMiscMod(value))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Initiative);
