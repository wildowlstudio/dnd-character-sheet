import Main from '../views/main.view';
import {connect} from 'react-redux';
import {resetSheet} from '../actions/global.actions';

const mapStateToProps = (state) => {
    return {};
};

const mapDispatchToProps = (dispatch) => {
    return {
        resetSheet: () => dispatch(resetSheet())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Main);
