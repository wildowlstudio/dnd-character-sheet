# Installation

Download node.js from site [nodejs.org](https://nodejs.org/en/). Use LTS version. Npm will be installed with node. Project uses yarn for better package managing. Yarn can be downloaded from [https://yarnpkg.com/lang/en/docs/install](https://yarnpkg.com/lang/en/docs/install/).

## Installing packages

To install packages run

```bash
yarn install
```

## Starting application

To start the development server run

```bash
yarn start
```

The server will be accessible on [localhost:8081](localhost:8081).

## Development

The development will be done using [Feature Branch Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow) with Pull Requests.

## Materials

Sheet is based on [this polish version](http://dnd.polter.pl/plik/view/id/1605.html) and [this english version](https://www.rpglibrary.org/sheets/blackmoors3e/dnd3_character_sheet_std_105c.pdf)

## Git workflow

Branch off *develop* branch

```bash
git checkout develop
git pull
git checkout -b <feature-branch>
...make changes, commits...
git push -u origin <feature-branch>
...make changes, commits...
git push
...create pull request and wait for it to be accepted...
git checkout develop
git pull
git merge --no-ff <feature-branch>
...resolve potential conflicts...
git push
git branch -d <feature-branch>
git push origin :<feature-branch>
```