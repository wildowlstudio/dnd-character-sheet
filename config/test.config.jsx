import Adapter from 'enzyme-adapter-react-16';
import Enzyme from 'enzyme';
import React from 'react';

Enzyme.configure({adapter: new Adapter()});

/* eslint-disable */
jest.mock('react-i18next', () => ({
    // this mock makes sure any components using the translate HoC receive the t function as a prop
    translate: () => Component => props => <Component t={t => t || ''} {...props} />
}));
/* eslint-enable */
