const webpack = require('webpack');
const webpackMerge = require('webpack-merge');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const commonConfig = require('./webpack.common.js');
const path = require('path');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const CompressionPlugin = require('zopfli-webpack-plugin');

const publicPath = '';

module.exports = function(env) {
    const config = webpackMerge(commonConfig, {
        devtool: 'source-map',

        output: {
            path: path.join(__dirname, '../dist'),
            publicPath,
            filename: '[name].[hash].js',
            chunkFilename: '[id].[hash].chunk.js'
        },

        plugins: [
            new webpack.DefinePlugin({
                'process.env': {
                    NODE_ENV: JSON.stringify('production')
                }
            }),
            new webpack.optimize.UglifyJsPlugin({
                sourceMap: true,
                compress: {
                    warnings: false
                }
            }),
            new webpack.LoaderOptionsPlugin({
                minimize: true
            }),
            new ExtractTextPlugin('[name].[hash].css'),
            new CompressionPlugin({
                asset: '[path].gz[query]',
                algorithm: 'zopfli',
                test: /\.(js|css|html|svg)$/,
                threshold: 10240,
                minRatio: 0.8
            })
        ]
    });
    if (env && env.analyze) {
        config.plugins.push(new BundleAnalyzerPlugin());
    }
    return config;
};
