const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');

const extractSass = new ExtractTextPlugin({
    filename: '[name].[contenthash].css',
    disable: process.env.NODE_ENV === 'development'
});

module.exports = {
    context: path.resolve(__dirname, '../'),
    entry: {
        polyfills: 'babel-polyfill',
        vendor: './vendor.js',
        app: './index.jsx'
    },

    resolve: {
        extensions: ['.js', '.jsx']
    },

    module: {
        rules: [
            {
                test: /\.flow$/,
                use: 'ignore-loader'
            },
            {
                test: /\.snap$/,
                loader: 'ignore-loader'
            },
            {
                test: /\.test\./,
                loader: 'ignore-loader'
            },
            {
                test: /\.jsx$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            plugins: [
                                'transform-runtime',
                                'transform-object-rest-spread',
                                'transform-react-remove-prop-types'
                            ],
                            presets: [['env', {modules: false}], 'react']
                        }
                    }
                ]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            plugins: [
                                'transform-runtime',
                                'transform-object-rest-spread'
                            ],
                            presets: ['env', 'react']
                        }
                    }
                ]
            },
            {
                test: /\.html$/,
                use: ['html-loader']
            },
            {
                test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                use: ['file-loader?name=assets/[name].[ext]']
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: 'css-loader?sourceMap'
                })
            },
            {
                test: /\.scss$/,
                use: extractSass.extract({
                    use: [
                        {
                            loader: 'css-loader'
                        },
                        {
                            loader: 'sass-loader'
                        }
                    ],
                    // use style-loader in development
                    fallback: 'style-loader'
                })
            }
        ]
    },

    plugins: [
        extractSass,
        new webpack.optimize.CommonsChunkPlugin({
            name: ['app', 'vendor', 'polyfills']
        }),
        new HtmlWebpackPlugin({
            template: 'index.html',
            favicon: 'favicon.ico'
        })
    ]
};
