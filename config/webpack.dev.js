const webpackMerge = require('webpack-merge');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const commonConfig = require('./webpack.common.js');
const path = require('path');

module.exports = webpackMerge(commonConfig, {
    devtool: 'source-map',

    output: {
        path: path.join(__dirname, '/'),
        publicPath: '/',
        filename: '[name].js',
        chunkFilename: '[id].chunk.js',
        devtoolModuleFilenameTemplate: '../[resource-path]'
    },

    plugins: [new ExtractTextPlugin('[name].css')],

    devServer: {
        historyApiFallback: true,
        stats: 'minimal'
        // host: '192.168.100.8'
    }
});
