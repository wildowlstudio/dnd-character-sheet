import ReactDOM from 'react-dom';

ReactDOM.createPortal = () => {
    return null;
};

global.requestAnimationFrame = (callback) => {
    setTimeout(callback, 0);
};
