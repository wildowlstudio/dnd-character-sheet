import './app/sass/shared.scss';

import {I18nextProvider} from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import MainContainer from './app/containers/main.container';
import {Provider} from 'react-redux';
import React from 'react';
import {ThemeProvider} from 'styled-components';
import {render} from 'react-dom';
import store from './app/utils/store.util';
import theme from './theme.json';

const i18n = require('i18next');
const i18nextXHRBackend = require('i18next-xhr-backend');

if (process.env.NODE_ENV !== 'production') {
    require.ensure(['why-did-you-update'], function(require) {
        const {whyDidYouUpdate} = require('why-did-you-update');
        whyDidYouUpdate(React, {
            exclude: /^(I18n|[sS]tyled|Translate|Connect|Element)/
        });
    });
}

function loadLocales(url, options, callback, data) {
    try {
        let waitForLocale = require(`bundle-loader!./app${url}`); // eslint-disable-line import/no-dynamic-require
        waitForLocale((locale) => {
            callback(locale, {status: '200'});
        });
    } catch (e) {
        callback(null, {status: '404'});
    }
}

window.i18n = i18n;

i18n
    .use(i18nextXHRBackend)
    .use(LanguageDetector)
    .init({
        lngWhitelist: ['en', 'pl'],
        fallbackLng: 'en',
        useCookie: true,
        backend: {
            parse: (data) => data,
            ajax: loadLocales
        },
        react: {
            wait: true
        },
        ns: ['common'],
        defaultNS: 'common'
    });

render(
    <I18nextProvider i18n={i18n.cloneInstance()}>
        <Provider store={store}>
            <ThemeProvider theme={theme}>
                <MainContainer />
            </ThemeProvider>
        </Provider>
    </I18nextProvider>,
    document.getElementById('web-application')
);
